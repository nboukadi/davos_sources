-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: ethz.systems.fpga:hls:mac_ip_encode:2.0
-- IP Revision: 2004071224

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY mac_ip_encode_ip IS
  PORT (
    s_axis_ip_TVALID : IN STD_LOGIC;
    s_axis_ip_TREADY : OUT STD_LOGIC;
    s_axis_ip_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_ip_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_ip_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_arp_lookup_reply_V_TVALID : IN STD_LOGIC;
    s_axis_arp_lookup_reply_V_TREADY : OUT STD_LOGIC;
    s_axis_arp_lookup_reply_V_TDATA : IN STD_LOGIC_VECTOR(55 DOWNTO 0);
    m_axis_ip_TVALID : OUT STD_LOGIC;
    m_axis_ip_TREADY : IN STD_LOGIC;
    m_axis_ip_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_ip_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_ip_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_arp_lookup_request_V_V_TVALID : OUT STD_LOGIC;
    m_axis_arp_lookup_request_V_V_TREADY : IN STD_LOGIC;
    m_axis_arp_lookup_request_V_V_TDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    myMacAddress_V : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    regSubNetMask_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regDefaultGateway_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    ap_clk : IN STD_LOGIC;
    ap_rst_n : IN STD_LOGIC
  );
END mac_ip_encode_ip;

ARCHITECTURE mac_ip_encode_ip_arch OF mac_ip_encode_ip IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF mac_ip_encode_ip_arch: ARCHITECTURE IS "yes";
  COMPONENT mac_ip_encode_mac_ip_encode_top IS
    PORT (
      s_axis_ip_TVALID : IN STD_LOGIC;
      s_axis_ip_TREADY : OUT STD_LOGIC;
      s_axis_ip_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_ip_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_ip_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      s_axis_arp_lookup_reply_V_TVALID : IN STD_LOGIC;
      s_axis_arp_lookup_reply_V_TREADY : OUT STD_LOGIC;
      s_axis_arp_lookup_reply_V_TDATA : IN STD_LOGIC_VECTOR(55 DOWNTO 0);
      m_axis_ip_TVALID : OUT STD_LOGIC;
      m_axis_ip_TREADY : IN STD_LOGIC;
      m_axis_ip_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_ip_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_ip_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_arp_lookup_request_V_V_TVALID : OUT STD_LOGIC;
      m_axis_arp_lookup_request_V_V_TREADY : IN STD_LOGIC;
      m_axis_arp_lookup_request_V_V_TDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      myMacAddress_V : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
      regSubNetMask_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regDefaultGateway_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      ap_clk : IN STD_LOGIC;
      ap_rst_n : IN STD_LOGIC
    );
  END COMPONENT mac_ip_encode_mac_ip_encode_top;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF mac_ip_encode_ip_arch: ARCHITECTURE IS "mac_ip_encode_mac_ip_encode_top,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF mac_ip_encode_ip_arch : ARCHITECTURE IS "mac_ip_encode_ip,mac_ip_encode_mac_ip_encode_top,{}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF mac_ip_encode_ip_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst_n: SIGNAL IS "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst_n: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axis_ip:s_axis_arp_lookup_reply_V:m_axis_ip:m_axis_arp_lookup_request_V_V, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regDefaultGateway_V: SIGNAL IS "XIL_INTERFACENAME regDefaultGateway_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regDefaultGateway_V: SIGNAL IS "xilinx.com:signal:data:1.0 regDefaultGateway_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regSubNetMask_V: SIGNAL IS "XIL_INTERFACENAME regSubNetMask_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regSubNetMask_V: SIGNAL IS "xilinx.com:signal:data:1.0 regSubNetMask_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF myMacAddress_V: SIGNAL IS "XIL_INTERFACENAME myMacAddress_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF myMacAddress_V: SIGNAL IS "xilinx.com:signal:data:1.0 myMacAddress_V DATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_arp_lookup_request_V_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_arp_lookup_request_V_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_arp_lookup_request_V_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_arp_lookup_request_V_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_arp_lookup_request_V_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_arp_lookup_request_V_V, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_arp_lookup_request_V_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_arp_lookup_request_V_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ip_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ip TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ip_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ip TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ip_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ip TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ip_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ip TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_ip_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_ip, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ip_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ip TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_arp_lookup_reply_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_arp_lookup_reply_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_arp_lookup_reply_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_arp_lookup_reply_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_arp_lookup_reply_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_arp_lookup_reply_V, TDATA_NUM_BYTES 7, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_arp_lookup_reply_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_arp_lookup_reply_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_ip_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_ip TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_ip_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_ip TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_ip_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_ip TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_ip_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_ip TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_ip_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_ip, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_ip_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_ip TVALID";
BEGIN
  U0 : mac_ip_encode_mac_ip_encode_top
    PORT MAP (
      s_axis_ip_TVALID => s_axis_ip_TVALID,
      s_axis_ip_TREADY => s_axis_ip_TREADY,
      s_axis_ip_TDATA => s_axis_ip_TDATA,
      s_axis_ip_TKEEP => s_axis_ip_TKEEP,
      s_axis_ip_TLAST => s_axis_ip_TLAST,
      s_axis_arp_lookup_reply_V_TVALID => s_axis_arp_lookup_reply_V_TVALID,
      s_axis_arp_lookup_reply_V_TREADY => s_axis_arp_lookup_reply_V_TREADY,
      s_axis_arp_lookup_reply_V_TDATA => s_axis_arp_lookup_reply_V_TDATA,
      m_axis_ip_TVALID => m_axis_ip_TVALID,
      m_axis_ip_TREADY => m_axis_ip_TREADY,
      m_axis_ip_TDATA => m_axis_ip_TDATA,
      m_axis_ip_TKEEP => m_axis_ip_TKEEP,
      m_axis_ip_TLAST => m_axis_ip_TLAST,
      m_axis_arp_lookup_request_V_V_TVALID => m_axis_arp_lookup_request_V_V_TVALID,
      m_axis_arp_lookup_request_V_V_TREADY => m_axis_arp_lookup_request_V_V_TREADY,
      m_axis_arp_lookup_request_V_V_TDATA => m_axis_arp_lookup_request_V_V_TDATA,
      myMacAddress_V => myMacAddress_V,
      regSubNetMask_V => regSubNetMask_V,
      regDefaultGateway_V => regDefaultGateway_V,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n
    );
END mac_ip_encode_ip_arch;
