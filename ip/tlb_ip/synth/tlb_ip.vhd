-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: ethz.systems.fpga:hls:tlb:0.09
-- IP Revision: 2004071812

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY tlb_ip IS
  PORT (
    regTlbMissCount_V_ap_vld : OUT STD_LOGIC;
    regPageCrossingCount_V_ap_vld : OUT STD_LOGIC;
    ap_clk : IN STD_LOGIC;
    ap_rst_n : IN STD_LOGIC;
    s_axis_mem_write_cmd_V_TVALID : IN STD_LOGIC;
    s_axis_mem_write_cmd_V_TREADY : OUT STD_LOGIC;
    s_axis_mem_write_cmd_V_TDATA : IN STD_LOGIC_VECTOR(95 DOWNTO 0);
    s_axis_mem_read_cmd_V_TVALID : IN STD_LOGIC;
    s_axis_mem_read_cmd_V_TREADY : OUT STD_LOGIC;
    s_axis_mem_read_cmd_V_TDATA : IN STD_LOGIC_VECTOR(95 DOWNTO 0);
    m_axis_dma_write_cmd_V_TVALID : OUT STD_LOGIC;
    m_axis_dma_write_cmd_V_TREADY : IN STD_LOGIC;
    m_axis_dma_write_cmd_V_TDATA : OUT STD_LOGIC_VECTOR(95 DOWNTO 0);
    m_axis_dma_read_cmd_V_TVALID : OUT STD_LOGIC;
    m_axis_dma_read_cmd_V_TREADY : IN STD_LOGIC;
    m_axis_dma_read_cmd_V_TDATA : OUT STD_LOGIC_VECTOR(95 DOWNTO 0);
    s_axis_tlb_interface_V_TVALID : IN STD_LOGIC;
    s_axis_tlb_interface_V_TREADY : OUT STD_LOGIC;
    s_axis_tlb_interface_V_TDATA : IN STD_LOGIC_VECTOR(135 DOWNTO 0);
    regTlbMissCount_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    regPageCrossingCount_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END tlb_ip;

ARCHITECTURE tlb_ip_arch OF tlb_ip IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF tlb_ip_arch: ARCHITECTURE IS "yes";
  COMPONENT tlb_tlb IS
    PORT (
      regTlbMissCount_V_ap_vld : OUT STD_LOGIC;
      regPageCrossingCount_V_ap_vld : OUT STD_LOGIC;
      ap_clk : IN STD_LOGIC;
      ap_rst_n : IN STD_LOGIC;
      s_axis_mem_write_cmd_V_TVALID : IN STD_LOGIC;
      s_axis_mem_write_cmd_V_TREADY : OUT STD_LOGIC;
      s_axis_mem_write_cmd_V_TDATA : IN STD_LOGIC_VECTOR(95 DOWNTO 0);
      s_axis_mem_read_cmd_V_TVALID : IN STD_LOGIC;
      s_axis_mem_read_cmd_V_TREADY : OUT STD_LOGIC;
      s_axis_mem_read_cmd_V_TDATA : IN STD_LOGIC_VECTOR(95 DOWNTO 0);
      m_axis_dma_write_cmd_V_TVALID : OUT STD_LOGIC;
      m_axis_dma_write_cmd_V_TREADY : IN STD_LOGIC;
      m_axis_dma_write_cmd_V_TDATA : OUT STD_LOGIC_VECTOR(95 DOWNTO 0);
      m_axis_dma_read_cmd_V_TVALID : OUT STD_LOGIC;
      m_axis_dma_read_cmd_V_TREADY : IN STD_LOGIC;
      m_axis_dma_read_cmd_V_TDATA : OUT STD_LOGIC_VECTOR(95 DOWNTO 0);
      s_axis_tlb_interface_V_TVALID : IN STD_LOGIC;
      s_axis_tlb_interface_V_TREADY : OUT STD_LOGIC;
      s_axis_tlb_interface_V_TDATA : IN STD_LOGIC_VECTOR(135 DOWNTO 0);
      regTlbMissCount_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      regPageCrossingCount_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
  END COMPONENT tlb_tlb;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF tlb_ip_arch: ARCHITECTURE IS "tlb_tlb,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF tlb_ip_arch : ARCHITECTURE IS "tlb_ip,tlb_tlb,{}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF tlb_ip_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF regPageCrossingCount_V: SIGNAL IS "XIL_INTERFACENAME regPageCrossingCount_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regPageCrossingCount_V: SIGNAL IS "xilinx.com:signal:data:1.0 regPageCrossingCount_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regTlbMissCount_V: SIGNAL IS "XIL_INTERFACENAME regTlbMissCount_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regTlbMissCount_V: SIGNAL IS "xilinx.com:signal:data:1.0 regTlbMissCount_V DATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tlb_interface_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tlb_interface_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tlb_interface_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tlb_interface_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_tlb_interface_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_tlb_interface_V, TDATA_NUM_BYTES 17, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tlb_interface_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tlb_interface_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_dma_read_cmd_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_dma_read_cmd_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_dma_read_cmd_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_dma_read_cmd_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_dma_read_cmd_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_dma_read_cmd_V, TDATA_NUM_BYTES 12, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_dma_read_cmd_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_dma_read_cmd_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_dma_write_cmd_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_dma_write_cmd_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_dma_write_cmd_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_dma_write_cmd_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_dma_write_cmd_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_dma_write_cmd_V, TDATA_NUM_BYTES 12, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_dma_write_cmd_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_dma_write_cmd_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_mem_read_cmd_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_mem_read_cmd_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_mem_read_cmd_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_mem_read_cmd_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_mem_read_cmd_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_mem_read_cmd_V, TDATA_NUM_BYTES 12, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_mem_read_cmd_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_mem_read_cmd_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_mem_write_cmd_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_mem_write_cmd_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_mem_write_cmd_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_mem_write_cmd_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_mem_write_cmd_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_mem_write_cmd_V, TDATA_NUM_BYTES 12, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_mem_write_cmd_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_mem_write_cmd_V TVALID";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst_n: SIGNAL IS "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst_n: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axis_mem_write_cmd_V:s_axis_mem_read_cmd_V:m_axis_dma_write_cmd_V:m_axis_dma_read_cmd_V:s_axis_tlb_interface_V, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
BEGIN
  U0 : tlb_tlb
    PORT MAP (
      regTlbMissCount_V_ap_vld => regTlbMissCount_V_ap_vld,
      regPageCrossingCount_V_ap_vld => regPageCrossingCount_V_ap_vld,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      s_axis_mem_write_cmd_V_TVALID => s_axis_mem_write_cmd_V_TVALID,
      s_axis_mem_write_cmd_V_TREADY => s_axis_mem_write_cmd_V_TREADY,
      s_axis_mem_write_cmd_V_TDATA => s_axis_mem_write_cmd_V_TDATA,
      s_axis_mem_read_cmd_V_TVALID => s_axis_mem_read_cmd_V_TVALID,
      s_axis_mem_read_cmd_V_TREADY => s_axis_mem_read_cmd_V_TREADY,
      s_axis_mem_read_cmd_V_TDATA => s_axis_mem_read_cmd_V_TDATA,
      m_axis_dma_write_cmd_V_TVALID => m_axis_dma_write_cmd_V_TVALID,
      m_axis_dma_write_cmd_V_TREADY => m_axis_dma_write_cmd_V_TREADY,
      m_axis_dma_write_cmd_V_TDATA => m_axis_dma_write_cmd_V_TDATA,
      m_axis_dma_read_cmd_V_TVALID => m_axis_dma_read_cmd_V_TVALID,
      m_axis_dma_read_cmd_V_TREADY => m_axis_dma_read_cmd_V_TREADY,
      m_axis_dma_read_cmd_V_TDATA => m_axis_dma_read_cmd_V_TDATA,
      s_axis_tlb_interface_V_TVALID => s_axis_tlb_interface_V_TVALID,
      s_axis_tlb_interface_V_TREADY => s_axis_tlb_interface_V_TREADY,
      s_axis_tlb_interface_V_TDATA => s_axis_tlb_interface_V_TDATA,
      regTlbMissCount_V => regTlbMissCount_V,
      regPageCrossingCount_V => regPageCrossingCount_V
    );
END tlb_ip_arch;
