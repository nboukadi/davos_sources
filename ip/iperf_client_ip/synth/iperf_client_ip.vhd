-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: ethz.systems.fpga:hls:iperf_client:1.0
-- IP Revision: 2004071222

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY iperf_client_ip IS
  PORT (
    m_axis_listen_port_V_V_TVALID : OUT STD_LOGIC;
    m_axis_listen_port_V_V_TREADY : IN STD_LOGIC;
    m_axis_listen_port_V_V_TDATA : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_listen_port_status_V_TVALID : IN STD_LOGIC;
    s_axis_listen_port_status_V_TREADY : OUT STD_LOGIC;
    s_axis_listen_port_status_V_TDATA : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_notifications_V_TVALID : IN STD_LOGIC;
    s_axis_notifications_V_TREADY : OUT STD_LOGIC;
    s_axis_notifications_V_TDATA : IN STD_LOGIC_VECTOR(87 DOWNTO 0);
    m_axis_read_package_V_TVALID : OUT STD_LOGIC;
    m_axis_read_package_V_TREADY : IN STD_LOGIC;
    m_axis_read_package_V_TDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_rx_metadata_V_V_TVALID : IN STD_LOGIC;
    s_axis_rx_metadata_V_V_TREADY : OUT STD_LOGIC;
    s_axis_rx_metadata_V_V_TDATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_rx_data_TVALID : IN STD_LOGIC;
    s_axis_rx_data_TREADY : OUT STD_LOGIC;
    s_axis_rx_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_rx_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_rx_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_open_connection_V_TVALID : OUT STD_LOGIC;
    m_axis_open_connection_V_TREADY : IN STD_LOGIC;
    m_axis_open_connection_V_TDATA : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    s_axis_open_status_V_TVALID : IN STD_LOGIC;
    s_axis_open_status_V_TREADY : OUT STD_LOGIC;
    s_axis_open_status_V_TDATA : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    m_axis_close_connection_V_V_TVALID : OUT STD_LOGIC;
    m_axis_close_connection_V_V_TREADY : IN STD_LOGIC;
    m_axis_close_connection_V_V_TDATA : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_tx_metadata_V_TVALID : OUT STD_LOGIC;
    m_axis_tx_metadata_V_TREADY : IN STD_LOGIC;
    m_axis_tx_metadata_V_TDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axis_tx_data_TVALID : OUT STD_LOGIC;
    m_axis_tx_data_TREADY : IN STD_LOGIC;
    m_axis_tx_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_tx_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_tx_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_tx_status_V_TVALID : IN STD_LOGIC;
    s_axis_tx_status_V_TREADY : OUT STD_LOGIC;
    s_axis_tx_status_V_TDATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    runExperiment_V : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    dualModeEn_V : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    useConn_V : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    pkgWordCount_V : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    packetGap_V : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    timeInSeconds_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    timeInCycles_V : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    regIpAddress0_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress1_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress2_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress3_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress4_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress5_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress6_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress7_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress8_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regIpAddress9_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    ap_clk : IN STD_LOGIC;
    ap_rst_n : IN STD_LOGIC
  );
END iperf_client_ip;

ARCHITECTURE iperf_client_ip_arch OF iperf_client_ip IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF iperf_client_ip_arch: ARCHITECTURE IS "yes";
  COMPONENT iperf_client_iperf_client IS
    PORT (
      m_axis_listen_port_V_V_TVALID : OUT STD_LOGIC;
      m_axis_listen_port_V_V_TREADY : IN STD_LOGIC;
      m_axis_listen_port_V_V_TDATA : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      s_axis_listen_port_status_V_TVALID : IN STD_LOGIC;
      s_axis_listen_port_status_V_TREADY : OUT STD_LOGIC;
      s_axis_listen_port_status_V_TDATA : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axis_notifications_V_TVALID : IN STD_LOGIC;
      s_axis_notifications_V_TREADY : OUT STD_LOGIC;
      s_axis_notifications_V_TDATA : IN STD_LOGIC_VECTOR(87 DOWNTO 0);
      m_axis_read_package_V_TVALID : OUT STD_LOGIC;
      m_axis_read_package_V_TREADY : IN STD_LOGIC;
      m_axis_read_package_V_TDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axis_rx_metadata_V_V_TVALID : IN STD_LOGIC;
      s_axis_rx_metadata_V_V_TREADY : OUT STD_LOGIC;
      s_axis_rx_metadata_V_V_TDATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      s_axis_rx_data_TVALID : IN STD_LOGIC;
      s_axis_rx_data_TREADY : OUT STD_LOGIC;
      s_axis_rx_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_rx_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_rx_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_open_connection_V_TVALID : OUT STD_LOGIC;
      m_axis_open_connection_V_TREADY : IN STD_LOGIC;
      m_axis_open_connection_V_TDATA : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      s_axis_open_status_V_TVALID : IN STD_LOGIC;
      s_axis_open_status_V_TREADY : OUT STD_LOGIC;
      s_axis_open_status_V_TDATA : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
      m_axis_close_connection_V_V_TVALID : OUT STD_LOGIC;
      m_axis_close_connection_V_V_TREADY : IN STD_LOGIC;
      m_axis_close_connection_V_V_TDATA : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      m_axis_tx_metadata_V_TVALID : OUT STD_LOGIC;
      m_axis_tx_metadata_V_TREADY : IN STD_LOGIC;
      m_axis_tx_metadata_V_TDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axis_tx_data_TVALID : OUT STD_LOGIC;
      m_axis_tx_data_TREADY : IN STD_LOGIC;
      m_axis_tx_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_tx_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_tx_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      s_axis_tx_status_V_TVALID : IN STD_LOGIC;
      s_axis_tx_status_V_TREADY : OUT STD_LOGIC;
      s_axis_tx_status_V_TDATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      runExperiment_V : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      dualModeEn_V : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      useConn_V : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
      pkgWordCount_V : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      packetGap_V : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      timeInSeconds_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      timeInCycles_V : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      regIpAddress0_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress1_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress2_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress3_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress4_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress5_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress6_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress7_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress8_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regIpAddress9_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      ap_clk : IN STD_LOGIC;
      ap_rst_n : IN STD_LOGIC
    );
  END COMPONENT iperf_client_iperf_client;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF iperf_client_ip_arch: ARCHITECTURE IS "iperf_client_iperf_client,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF iperf_client_ip_arch : ARCHITECTURE IS "iperf_client_ip,iperf_client_iperf_client,{}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF iperf_client_ip_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst_n: SIGNAL IS "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst_n: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF m_axis_listen_port_V_V:s_axis_listen_port_status_V:s_axis_notifications_V:m_axis_read_package_V:s_axis_rx_metadata_V_V:s_axis_rx_data:m_axis_open_connection_V:s_axis_open_status_V:m_axis_close_connection_V_V:m_axis_tx_metadata_V:m_axis_tx_data:s_axis_tx_status_V, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress9_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress9_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress9_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress9_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress8_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress8_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress8_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress8_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress7_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress7_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress7_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress7_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress6_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress6_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress6_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress6_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress5_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress5_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress5_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress5_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress4_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress4_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress4_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress4_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress3_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress3_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress3_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress3_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress2_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress2_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress2_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress2_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress1_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress1_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress1_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress1_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regIpAddress0_V: SIGNAL IS "XIL_INTERFACENAME regIpAddress0_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regIpAddress0_V: SIGNAL IS "xilinx.com:signal:data:1.0 regIpAddress0_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF timeInCycles_V: SIGNAL IS "XIL_INTERFACENAME timeInCycles_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF timeInCycles_V: SIGNAL IS "xilinx.com:signal:data:1.0 timeInCycles_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF timeInSeconds_V: SIGNAL IS "XIL_INTERFACENAME timeInSeconds_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF timeInSeconds_V: SIGNAL IS "xilinx.com:signal:data:1.0 timeInSeconds_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF packetGap_V: SIGNAL IS "XIL_INTERFACENAME packetGap_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF packetGap_V: SIGNAL IS "xilinx.com:signal:data:1.0 packetGap_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF pkgWordCount_V: SIGNAL IS "XIL_INTERFACENAME pkgWordCount_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF pkgWordCount_V: SIGNAL IS "xilinx.com:signal:data:1.0 pkgWordCount_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF useConn_V: SIGNAL IS "XIL_INTERFACENAME useConn_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF useConn_V: SIGNAL IS "xilinx.com:signal:data:1.0 useConn_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF dualModeEn_V: SIGNAL IS "XIL_INTERFACENAME dualModeEn_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF dualModeEn_V: SIGNAL IS "xilinx.com:signal:data:1.0 dualModeEn_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF runExperiment_V: SIGNAL IS "XIL_INTERFACENAME runExperiment_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF runExperiment_V: SIGNAL IS "xilinx.com:signal:data:1.0 runExperiment_V DATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_status_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_status_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_status_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_status_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_tx_status_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_tx_status_V, TDATA_NUM_BYTES 8, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_status_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_status_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_tx_data_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_tx_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_metadata_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_metadata_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_metadata_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_metadata_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_tx_metadata_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_tx_metadata_V, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_metadata_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_metadata_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_close_connection_V_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_close_connection_V_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_close_connection_V_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_close_connection_V_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_close_connection_V_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_close_connection_V_V, TDATA_NUM_BYTES 2, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_close_connection_V_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_close_connection_V_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_open_status_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_open_status_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_open_status_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_open_status_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_open_status_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_open_status_V, TDATA_NUM_BYTES 3, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_open_status_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_open_status_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_open_connection_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_open_connection_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_open_connection_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_open_connection_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_open_connection_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_open_connection_V, TDATA_NUM_BYTES 6, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_open_connection_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_open_connection_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_rx_data_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_rx_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_metadata_V_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_metadata_V_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_metadata_V_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_metadata_V_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_rx_metadata_V_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_rx_metadata_V_V, TDATA_NUM_BYTES 2, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_metadata_V_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_metadata_V_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_read_package_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_read_package_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_read_package_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_read_package_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_read_package_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_read_package_V, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_read_package_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_read_package_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_notifications_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_notifications_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_notifications_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_notifications_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_notifications_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_notifications_V, TDATA_NUM_BYTES 11, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_notifications_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_notifications_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_listen_port_status_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_listen_port_status_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_listen_port_status_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_listen_port_status_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_listen_port_status_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_listen_port_status_V, TDATA_NUM_BYTES 1, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_listen_port_status_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_listen_port_status_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_listen_port_V_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_listen_port_V_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_listen_port_V_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_listen_port_V_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_listen_port_V_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_listen_port_V_V, TDATA_NUM_BYTES 2, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_listen_port_V_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_listen_port_V_V TVALID";
BEGIN
  U0 : iperf_client_iperf_client
    PORT MAP (
      m_axis_listen_port_V_V_TVALID => m_axis_listen_port_V_V_TVALID,
      m_axis_listen_port_V_V_TREADY => m_axis_listen_port_V_V_TREADY,
      m_axis_listen_port_V_V_TDATA => m_axis_listen_port_V_V_TDATA,
      s_axis_listen_port_status_V_TVALID => s_axis_listen_port_status_V_TVALID,
      s_axis_listen_port_status_V_TREADY => s_axis_listen_port_status_V_TREADY,
      s_axis_listen_port_status_V_TDATA => s_axis_listen_port_status_V_TDATA,
      s_axis_notifications_V_TVALID => s_axis_notifications_V_TVALID,
      s_axis_notifications_V_TREADY => s_axis_notifications_V_TREADY,
      s_axis_notifications_V_TDATA => s_axis_notifications_V_TDATA,
      m_axis_read_package_V_TVALID => m_axis_read_package_V_TVALID,
      m_axis_read_package_V_TREADY => m_axis_read_package_V_TREADY,
      m_axis_read_package_V_TDATA => m_axis_read_package_V_TDATA,
      s_axis_rx_metadata_V_V_TVALID => s_axis_rx_metadata_V_V_TVALID,
      s_axis_rx_metadata_V_V_TREADY => s_axis_rx_metadata_V_V_TREADY,
      s_axis_rx_metadata_V_V_TDATA => s_axis_rx_metadata_V_V_TDATA,
      s_axis_rx_data_TVALID => s_axis_rx_data_TVALID,
      s_axis_rx_data_TREADY => s_axis_rx_data_TREADY,
      s_axis_rx_data_TDATA => s_axis_rx_data_TDATA,
      s_axis_rx_data_TKEEP => s_axis_rx_data_TKEEP,
      s_axis_rx_data_TLAST => s_axis_rx_data_TLAST,
      m_axis_open_connection_V_TVALID => m_axis_open_connection_V_TVALID,
      m_axis_open_connection_V_TREADY => m_axis_open_connection_V_TREADY,
      m_axis_open_connection_V_TDATA => m_axis_open_connection_V_TDATA,
      s_axis_open_status_V_TVALID => s_axis_open_status_V_TVALID,
      s_axis_open_status_V_TREADY => s_axis_open_status_V_TREADY,
      s_axis_open_status_V_TDATA => s_axis_open_status_V_TDATA,
      m_axis_close_connection_V_V_TVALID => m_axis_close_connection_V_V_TVALID,
      m_axis_close_connection_V_V_TREADY => m_axis_close_connection_V_V_TREADY,
      m_axis_close_connection_V_V_TDATA => m_axis_close_connection_V_V_TDATA,
      m_axis_tx_metadata_V_TVALID => m_axis_tx_metadata_V_TVALID,
      m_axis_tx_metadata_V_TREADY => m_axis_tx_metadata_V_TREADY,
      m_axis_tx_metadata_V_TDATA => m_axis_tx_metadata_V_TDATA,
      m_axis_tx_data_TVALID => m_axis_tx_data_TVALID,
      m_axis_tx_data_TREADY => m_axis_tx_data_TREADY,
      m_axis_tx_data_TDATA => m_axis_tx_data_TDATA,
      m_axis_tx_data_TKEEP => m_axis_tx_data_TKEEP,
      m_axis_tx_data_TLAST => m_axis_tx_data_TLAST,
      s_axis_tx_status_V_TVALID => s_axis_tx_status_V_TVALID,
      s_axis_tx_status_V_TREADY => s_axis_tx_status_V_TREADY,
      s_axis_tx_status_V_TDATA => s_axis_tx_status_V_TDATA,
      runExperiment_V => runExperiment_V,
      dualModeEn_V => dualModeEn_V,
      useConn_V => useConn_V,
      pkgWordCount_V => pkgWordCount_V,
      packetGap_V => packetGap_V,
      timeInSeconds_V => timeInSeconds_V,
      timeInCycles_V => timeInCycles_V,
      regIpAddress0_V => regIpAddress0_V,
      regIpAddress1_V => regIpAddress1_V,
      regIpAddress2_V => regIpAddress2_V,
      regIpAddress3_V => regIpAddress3_V,
      regIpAddress4_V => regIpAddress4_V,
      regIpAddress5_V => regIpAddress5_V,
      regIpAddress6_V => regIpAddress6_V,
      regIpAddress7_V => regIpAddress7_V,
      regIpAddress8_V => regIpAddress8_V,
      regIpAddress9_V => regIpAddress9_V,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n
    );
END iperf_client_ip_arch;
