-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: ethz.systems:hls:toe:1.6
-- IP Revision: 2004071231

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY toe_ip IS
  PORT (
    regSessionCount_V_ap_vld : OUT STD_LOGIC;
    s_axis_tcp_data_TVALID : IN STD_LOGIC;
    s_axis_tcp_data_TREADY : OUT STD_LOGIC;
    s_axis_tcp_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_tcp_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_tcp_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_txwrite_sts_V_TVALID : IN STD_LOGIC;
    s_axis_txwrite_sts_V_TREADY : OUT STD_LOGIC;
    s_axis_txwrite_sts_V_TDATA : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_rxread_data_TVALID : IN STD_LOGIC;
    s_axis_rxread_data_TREADY : OUT STD_LOGIC;
    s_axis_rxread_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_rxread_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_rxread_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_txread_data_TVALID : IN STD_LOGIC;
    s_axis_txread_data_TREADY : OUT STD_LOGIC;
    s_axis_txread_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_txread_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_txread_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_tcp_data_TVALID : OUT STD_LOGIC;
    m_axis_tcp_data_TREADY : IN STD_LOGIC;
    m_axis_tcp_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_tcp_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_tcp_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_txwrite_cmd_V_TVALID : OUT STD_LOGIC;
    m_axis_txwrite_cmd_V_TREADY : IN STD_LOGIC;
    m_axis_txwrite_cmd_V_TDATA : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
    m_axis_txread_cmd_V_TVALID : OUT STD_LOGIC;
    m_axis_txread_cmd_V_TREADY : IN STD_LOGIC;
    m_axis_txread_cmd_V_TDATA : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
    m_axis_rxwrite_data_TVALID : OUT STD_LOGIC;
    m_axis_rxwrite_data_TREADY : IN STD_LOGIC;
    m_axis_rxwrite_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_rxwrite_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_rxwrite_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_txwrite_data_TVALID : OUT STD_LOGIC;
    m_axis_txwrite_data_TREADY : IN STD_LOGIC;
    m_axis_txwrite_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_txwrite_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_txwrite_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_session_lup_rsp_V_TVALID : IN STD_LOGIC;
    s_axis_session_lup_rsp_V_TREADY : OUT STD_LOGIC;
    s_axis_session_lup_rsp_V_TDATA : IN STD_LOGIC_VECTOR(87 DOWNTO 0);
    s_axis_session_upd_rsp_V_TVALID : IN STD_LOGIC;
    s_axis_session_upd_rsp_V_TREADY : OUT STD_LOGIC;
    s_axis_session_upd_rsp_V_TDATA : IN STD_LOGIC_VECTOR(87 DOWNTO 0);
    m_axis_session_lup_req_V_TVALID : OUT STD_LOGIC;
    m_axis_session_lup_req_V_TREADY : IN STD_LOGIC;
    m_axis_session_lup_req_V_TDATA : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
    m_axis_session_upd_req_V_TVALID : OUT STD_LOGIC;
    m_axis_session_upd_req_V_TREADY : IN STD_LOGIC;
    m_axis_session_upd_req_V_TDATA : OUT STD_LOGIC_VECTOR(87 DOWNTO 0);
    s_axis_listen_port_req_V_V_TVALID : IN STD_LOGIC;
    s_axis_listen_port_req_V_V_TREADY : OUT STD_LOGIC;
    s_axis_listen_port_req_V_V_TDATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_rx_data_req_V_TVALID : IN STD_LOGIC;
    s_axis_rx_data_req_V_TREADY : OUT STD_LOGIC;
    s_axis_rx_data_req_V_TDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_open_conn_req_V_TVALID : IN STD_LOGIC;
    s_axis_open_conn_req_V_TREADY : OUT STD_LOGIC;
    s_axis_open_conn_req_V_TDATA : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    s_axis_close_conn_req_V_V_TVALID : IN STD_LOGIC;
    s_axis_close_conn_req_V_V_TREADY : OUT STD_LOGIC;
    s_axis_close_conn_req_V_V_TDATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_tx_data_req_metadata_V_TVALID : IN STD_LOGIC;
    s_axis_tx_data_req_metadata_V_TREADY : OUT STD_LOGIC;
    s_axis_tx_data_req_metadata_V_TDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_tx_data_req_TVALID : IN STD_LOGIC;
    s_axis_tx_data_req_TREADY : OUT STD_LOGIC;
    s_axis_tx_data_req_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_tx_data_req_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_tx_data_req_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_listen_port_rsp_V_TVALID : OUT STD_LOGIC;
    m_axis_listen_port_rsp_V_TREADY : IN STD_LOGIC;
    m_axis_listen_port_rsp_V_TDATA : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_notification_V_TVALID : OUT STD_LOGIC;
    m_axis_notification_V_TREADY : IN STD_LOGIC;
    m_axis_notification_V_TDATA : OUT STD_LOGIC_VECTOR(87 DOWNTO 0);
    m_axis_rx_data_rsp_metadata_V_V_TVALID : OUT STD_LOGIC;
    m_axis_rx_data_rsp_metadata_V_V_TREADY : IN STD_LOGIC;
    m_axis_rx_data_rsp_metadata_V_V_TDATA : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_rx_data_rsp_TVALID : OUT STD_LOGIC;
    m_axis_rx_data_rsp_TREADY : IN STD_LOGIC;
    m_axis_rx_data_rsp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_rx_data_rsp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_rx_data_rsp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_open_conn_rsp_V_TVALID : OUT STD_LOGIC;
    m_axis_open_conn_rsp_V_TREADY : IN STD_LOGIC;
    m_axis_open_conn_rsp_V_TDATA : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    m_axis_tx_data_rsp_V_TVALID : OUT STD_LOGIC;
    m_axis_tx_data_rsp_V_TREADY : IN STD_LOGIC;
    m_axis_tx_data_rsp_V_TDATA : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    axis_data_count_V : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    axis_max_data_count_V : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    myIpAddress_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    regSessionCount_V : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ap_clk : IN STD_LOGIC;
    ap_rst_n : IN STD_LOGIC
  );
END toe_ip;

ARCHITECTURE toe_ip_arch OF toe_ip IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF toe_ip_arch: ARCHITECTURE IS "yes";
  COMPONENT toe_toe_top IS
    PORT (
      regSessionCount_V_ap_vld : OUT STD_LOGIC;
      s_axis_tcp_data_TVALID : IN STD_LOGIC;
      s_axis_tcp_data_TREADY : OUT STD_LOGIC;
      s_axis_tcp_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_tcp_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_tcp_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      s_axis_txwrite_sts_V_TVALID : IN STD_LOGIC;
      s_axis_txwrite_sts_V_TREADY : OUT STD_LOGIC;
      s_axis_txwrite_sts_V_TDATA : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axis_rxread_data_TVALID : IN STD_LOGIC;
      s_axis_rxread_data_TREADY : OUT STD_LOGIC;
      s_axis_rxread_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_rxread_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_rxread_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      s_axis_txread_data_TVALID : IN STD_LOGIC;
      s_axis_txread_data_TREADY : OUT STD_LOGIC;
      s_axis_txread_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_txread_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_txread_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_tcp_data_TVALID : OUT STD_LOGIC;
      m_axis_tcp_data_TREADY : IN STD_LOGIC;
      m_axis_tcp_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_tcp_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_tcp_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_txwrite_cmd_V_TVALID : OUT STD_LOGIC;
      m_axis_txwrite_cmd_V_TREADY : IN STD_LOGIC;
      m_axis_txwrite_cmd_V_TDATA : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
      m_axis_txread_cmd_V_TVALID : OUT STD_LOGIC;
      m_axis_txread_cmd_V_TREADY : IN STD_LOGIC;
      m_axis_txread_cmd_V_TDATA : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
      m_axis_rxwrite_data_TVALID : OUT STD_LOGIC;
      m_axis_rxwrite_data_TREADY : IN STD_LOGIC;
      m_axis_rxwrite_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_rxwrite_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_rxwrite_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_txwrite_data_TVALID : OUT STD_LOGIC;
      m_axis_txwrite_data_TREADY : IN STD_LOGIC;
      m_axis_txwrite_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_txwrite_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_txwrite_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      s_axis_session_lup_rsp_V_TVALID : IN STD_LOGIC;
      s_axis_session_lup_rsp_V_TREADY : OUT STD_LOGIC;
      s_axis_session_lup_rsp_V_TDATA : IN STD_LOGIC_VECTOR(87 DOWNTO 0);
      s_axis_session_upd_rsp_V_TVALID : IN STD_LOGIC;
      s_axis_session_upd_rsp_V_TREADY : OUT STD_LOGIC;
      s_axis_session_upd_rsp_V_TDATA : IN STD_LOGIC_VECTOR(87 DOWNTO 0);
      m_axis_session_lup_req_V_TVALID : OUT STD_LOGIC;
      m_axis_session_lup_req_V_TREADY : IN STD_LOGIC;
      m_axis_session_lup_req_V_TDATA : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
      m_axis_session_upd_req_V_TVALID : OUT STD_LOGIC;
      m_axis_session_upd_req_V_TREADY : IN STD_LOGIC;
      m_axis_session_upd_req_V_TDATA : OUT STD_LOGIC_VECTOR(87 DOWNTO 0);
      s_axis_listen_port_req_V_V_TVALID : IN STD_LOGIC;
      s_axis_listen_port_req_V_V_TREADY : OUT STD_LOGIC;
      s_axis_listen_port_req_V_V_TDATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      s_axis_rx_data_req_V_TVALID : IN STD_LOGIC;
      s_axis_rx_data_req_V_TREADY : OUT STD_LOGIC;
      s_axis_rx_data_req_V_TDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axis_open_conn_req_V_TVALID : IN STD_LOGIC;
      s_axis_open_conn_req_V_TREADY : OUT STD_LOGIC;
      s_axis_open_conn_req_V_TDATA : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
      s_axis_close_conn_req_V_V_TVALID : IN STD_LOGIC;
      s_axis_close_conn_req_V_V_TREADY : OUT STD_LOGIC;
      s_axis_close_conn_req_V_V_TDATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      s_axis_tx_data_req_metadata_V_TVALID : IN STD_LOGIC;
      s_axis_tx_data_req_metadata_V_TREADY : OUT STD_LOGIC;
      s_axis_tx_data_req_metadata_V_TDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axis_tx_data_req_TVALID : IN STD_LOGIC;
      s_axis_tx_data_req_TREADY : OUT STD_LOGIC;
      s_axis_tx_data_req_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_tx_data_req_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_tx_data_req_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_listen_port_rsp_V_TVALID : OUT STD_LOGIC;
      m_axis_listen_port_rsp_V_TREADY : IN STD_LOGIC;
      m_axis_listen_port_rsp_V_TDATA : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axis_notification_V_TVALID : OUT STD_LOGIC;
      m_axis_notification_V_TREADY : IN STD_LOGIC;
      m_axis_notification_V_TDATA : OUT STD_LOGIC_VECTOR(87 DOWNTO 0);
      m_axis_rx_data_rsp_metadata_V_V_TVALID : OUT STD_LOGIC;
      m_axis_rx_data_rsp_metadata_V_V_TREADY : IN STD_LOGIC;
      m_axis_rx_data_rsp_metadata_V_V_TDATA : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      m_axis_rx_data_rsp_TVALID : OUT STD_LOGIC;
      m_axis_rx_data_rsp_TREADY : IN STD_LOGIC;
      m_axis_rx_data_rsp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_rx_data_rsp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_rx_data_rsp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_open_conn_rsp_V_TVALID : OUT STD_LOGIC;
      m_axis_open_conn_rsp_V_TREADY : IN STD_LOGIC;
      m_axis_open_conn_rsp_V_TDATA : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
      m_axis_tx_data_rsp_V_TVALID : OUT STD_LOGIC;
      m_axis_tx_data_rsp_V_TREADY : IN STD_LOGIC;
      m_axis_tx_data_rsp_V_TDATA : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      axis_data_count_V : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      axis_max_data_count_V : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      myIpAddress_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      regSessionCount_V : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      ap_clk : IN STD_LOGIC;
      ap_rst_n : IN STD_LOGIC
    );
  END COMPONENT toe_toe_top;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF toe_ip_arch: ARCHITECTURE IS "toe_toe_top,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF toe_ip_arch : ARCHITECTURE IS "toe_ip,toe_toe_top,{}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF toe_ip_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst_n: SIGNAL IS "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst_n: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axis_tcp_data:s_axis_txwrite_sts_V:s_axis_rxread_data:s_axis_txread_data:m_axis_tcp_data:m_axis_txwrite_cmd_V:m_axis_txread_cmd_V:m_axis_rxwrite_data:m_axis_txwrite_data:s_axis_session_lup_rsp_V:s_axis_session_upd_rsp_V:m_axis_session_lup_req_V:m_axis_session_upd_req_V:s_axis_listen_port_req_V_V:s_axis_rx_data_req_V:s_axis_open_conn_req_V:s_axis_close_conn_req_V_V:s_axis_tx_data_req_metadata_V:s_axis_tx_data_req:m_axis_listen_port_rsp_V:m_axis_notific" & 
"ation_V:m_axis_rx_data_rsp_metadata_V_V:m_axis_rx_data_rsp:m_axis_open_conn_rsp_V:m_axis_tx_data_rsp_V, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF regSessionCount_V: SIGNAL IS "XIL_INTERFACENAME regSessionCount_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF regSessionCount_V: SIGNAL IS "xilinx.com:signal:data:1.0 regSessionCount_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF myIpAddress_V: SIGNAL IS "XIL_INTERFACENAME myIpAddress_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF myIpAddress_V: SIGNAL IS "xilinx.com:signal:data:1.0 myIpAddress_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF axis_max_data_count_V: SIGNAL IS "XIL_INTERFACENAME axis_max_data_count_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF axis_max_data_count_V: SIGNAL IS "xilinx.com:signal:data:1.0 axis_max_data_count_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF axis_data_count_V: SIGNAL IS "XIL_INTERFACENAME axis_data_count_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF axis_data_count_V: SIGNAL IS "xilinx.com:signal:data:1.0 axis_data_count_V DATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_rsp_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data_rsp_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_rsp_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data_rsp_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_tx_data_rsp_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_tx_data_rsp_V, TDATA_NUM_BYTES 8, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_rsp_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data_rsp_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_open_conn_rsp_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_open_conn_rsp_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_open_conn_rsp_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_open_conn_rsp_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_open_conn_rsp_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_open_conn_rsp_V, TDATA_NUM_BYTES 3, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_open_conn_rsp_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_open_conn_rsp_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_rsp_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data_rsp TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_rsp_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data_rsp TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_rsp_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data_rsp TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_rsp_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data_rsp TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_rx_data_rsp_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_rx_data_rsp, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_rsp_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data_rsp TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_rsp_metadata_V_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data_rsp_metadata_V_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_rsp_metadata_V_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data_rsp_metadata_V_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_rx_data_rsp_metadata_V_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_rx_data_rsp_metadata_V_V, TDATA_NUM_BYTES 2, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_rsp_metadata_V_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data_rsp_metadata_V_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_notification_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_notification_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_notification_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_notification_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_notification_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_notification_V, TDATA_NUM_BYTES 11, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_notification_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_notification_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_listen_port_rsp_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_listen_port_rsp_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_listen_port_rsp_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_listen_port_rsp_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_listen_port_rsp_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_listen_port_rsp_V, TDATA_NUM_BYTES 1, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_listen_port_rsp_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_listen_port_rsp_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_req_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data_req TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_req_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data_req TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_req_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data_req TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_req_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data_req TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_tx_data_req_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_tx_data_req, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_req_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data_req TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_req_metadata_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data_req_metadata_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_req_metadata_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data_req_metadata_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_tx_data_req_metadata_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_tx_data_req_metadata_V, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_req_metadata_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data_req_metadata_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_close_conn_req_V_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_close_conn_req_V_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_close_conn_req_V_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_close_conn_req_V_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_close_conn_req_V_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_close_conn_req_V_V, TDATA_NUM_BYTES 2, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_close_conn_req_V_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_close_conn_req_V_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_open_conn_req_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_open_conn_req_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_open_conn_req_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_open_conn_req_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_open_conn_req_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_open_conn_req_V, TDATA_NUM_BYTES 6, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_open_conn_req_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_open_conn_req_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_req_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data_req_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_req_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data_req_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_rx_data_req_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_rx_data_req_V, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_req_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data_req_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_listen_port_req_V_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_listen_port_req_V_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_listen_port_req_V_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_listen_port_req_V_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_listen_port_req_V_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_listen_port_req_V_V, TDATA_NUM_BYTES 2, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_listen_port_req_V_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_listen_port_req_V_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_session_upd_req_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_session_upd_req_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_session_upd_req_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_session_upd_req_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_session_upd_req_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_session_upd_req_V, TDATA_NUM_BYTES 11, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_session_upd_req_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_session_upd_req_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_session_lup_req_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_session_lup_req_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_session_lup_req_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_session_lup_req_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_session_lup_req_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_session_lup_req_V, TDATA_NUM_BYTES 9, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_session_lup_req_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_session_lup_req_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_session_upd_rsp_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_session_upd_rsp_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_session_upd_rsp_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_session_upd_rsp_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_session_upd_rsp_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_session_upd_rsp_V, TDATA_NUM_BYTES 11, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_session_upd_rsp_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_session_upd_rsp_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_session_lup_rsp_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_session_lup_rsp_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_session_lup_rsp_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_session_lup_rsp_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_session_lup_rsp_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_session_lup_rsp_V, TDATA_NUM_BYTES 11, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_session_lup_rsp_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_session_lup_rsp_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txwrite_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txwrite_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txwrite_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txwrite_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txwrite_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txwrite_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txwrite_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txwrite_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_txwrite_data_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_txwrite_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txwrite_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txwrite_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rxwrite_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rxwrite_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rxwrite_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rxwrite_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rxwrite_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rxwrite_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rxwrite_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rxwrite_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_rxwrite_data_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_rxwrite_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rxwrite_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rxwrite_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txread_cmd_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txread_cmd_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txread_cmd_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txread_cmd_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_txread_cmd_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_txread_cmd_V, TDATA_NUM_BYTES 9, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txread_cmd_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txread_cmd_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txwrite_cmd_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txwrite_cmd_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txwrite_cmd_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txwrite_cmd_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_txwrite_cmd_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_txwrite_cmd_V, TDATA_NUM_BYTES 9, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_txwrite_cmd_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_txwrite_cmd_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_tcp_data_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_tcp_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_txread_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_txread_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_txread_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_txread_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_txread_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_txread_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_txread_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_txread_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_txread_data_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_txread_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_txread_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_txread_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rxread_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rxread_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rxread_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rxread_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rxread_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rxread_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rxread_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rxread_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_rxread_data_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_rxread_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rxread_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rxread_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_txwrite_sts_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_txwrite_sts_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_txwrite_sts_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_txwrite_sts_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_txwrite_sts_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_txwrite_sts_V, TDATA_NUM_BYTES 1, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_txwrite_sts_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_txwrite_sts_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tcp_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tcp_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tcp_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tcp_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tcp_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tcp_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tcp_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tcp_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_tcp_data_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_tcp_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tcp_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tcp_data TVALID";
BEGIN
  U0 : toe_toe_top
    PORT MAP (
      regSessionCount_V_ap_vld => regSessionCount_V_ap_vld,
      s_axis_tcp_data_TVALID => s_axis_tcp_data_TVALID,
      s_axis_tcp_data_TREADY => s_axis_tcp_data_TREADY,
      s_axis_tcp_data_TDATA => s_axis_tcp_data_TDATA,
      s_axis_tcp_data_TKEEP => s_axis_tcp_data_TKEEP,
      s_axis_tcp_data_TLAST => s_axis_tcp_data_TLAST,
      s_axis_txwrite_sts_V_TVALID => s_axis_txwrite_sts_V_TVALID,
      s_axis_txwrite_sts_V_TREADY => s_axis_txwrite_sts_V_TREADY,
      s_axis_txwrite_sts_V_TDATA => s_axis_txwrite_sts_V_TDATA,
      s_axis_rxread_data_TVALID => s_axis_rxread_data_TVALID,
      s_axis_rxread_data_TREADY => s_axis_rxread_data_TREADY,
      s_axis_rxread_data_TDATA => s_axis_rxread_data_TDATA,
      s_axis_rxread_data_TKEEP => s_axis_rxread_data_TKEEP,
      s_axis_rxread_data_TLAST => s_axis_rxread_data_TLAST,
      s_axis_txread_data_TVALID => s_axis_txread_data_TVALID,
      s_axis_txread_data_TREADY => s_axis_txread_data_TREADY,
      s_axis_txread_data_TDATA => s_axis_txread_data_TDATA,
      s_axis_txread_data_TKEEP => s_axis_txread_data_TKEEP,
      s_axis_txread_data_TLAST => s_axis_txread_data_TLAST,
      m_axis_tcp_data_TVALID => m_axis_tcp_data_TVALID,
      m_axis_tcp_data_TREADY => m_axis_tcp_data_TREADY,
      m_axis_tcp_data_TDATA => m_axis_tcp_data_TDATA,
      m_axis_tcp_data_TKEEP => m_axis_tcp_data_TKEEP,
      m_axis_tcp_data_TLAST => m_axis_tcp_data_TLAST,
      m_axis_txwrite_cmd_V_TVALID => m_axis_txwrite_cmd_V_TVALID,
      m_axis_txwrite_cmd_V_TREADY => m_axis_txwrite_cmd_V_TREADY,
      m_axis_txwrite_cmd_V_TDATA => m_axis_txwrite_cmd_V_TDATA,
      m_axis_txread_cmd_V_TVALID => m_axis_txread_cmd_V_TVALID,
      m_axis_txread_cmd_V_TREADY => m_axis_txread_cmd_V_TREADY,
      m_axis_txread_cmd_V_TDATA => m_axis_txread_cmd_V_TDATA,
      m_axis_rxwrite_data_TVALID => m_axis_rxwrite_data_TVALID,
      m_axis_rxwrite_data_TREADY => m_axis_rxwrite_data_TREADY,
      m_axis_rxwrite_data_TDATA => m_axis_rxwrite_data_TDATA,
      m_axis_rxwrite_data_TKEEP => m_axis_rxwrite_data_TKEEP,
      m_axis_rxwrite_data_TLAST => m_axis_rxwrite_data_TLAST,
      m_axis_txwrite_data_TVALID => m_axis_txwrite_data_TVALID,
      m_axis_txwrite_data_TREADY => m_axis_txwrite_data_TREADY,
      m_axis_txwrite_data_TDATA => m_axis_txwrite_data_TDATA,
      m_axis_txwrite_data_TKEEP => m_axis_txwrite_data_TKEEP,
      m_axis_txwrite_data_TLAST => m_axis_txwrite_data_TLAST,
      s_axis_session_lup_rsp_V_TVALID => s_axis_session_lup_rsp_V_TVALID,
      s_axis_session_lup_rsp_V_TREADY => s_axis_session_lup_rsp_V_TREADY,
      s_axis_session_lup_rsp_V_TDATA => s_axis_session_lup_rsp_V_TDATA,
      s_axis_session_upd_rsp_V_TVALID => s_axis_session_upd_rsp_V_TVALID,
      s_axis_session_upd_rsp_V_TREADY => s_axis_session_upd_rsp_V_TREADY,
      s_axis_session_upd_rsp_V_TDATA => s_axis_session_upd_rsp_V_TDATA,
      m_axis_session_lup_req_V_TVALID => m_axis_session_lup_req_V_TVALID,
      m_axis_session_lup_req_V_TREADY => m_axis_session_lup_req_V_TREADY,
      m_axis_session_lup_req_V_TDATA => m_axis_session_lup_req_V_TDATA,
      m_axis_session_upd_req_V_TVALID => m_axis_session_upd_req_V_TVALID,
      m_axis_session_upd_req_V_TREADY => m_axis_session_upd_req_V_TREADY,
      m_axis_session_upd_req_V_TDATA => m_axis_session_upd_req_V_TDATA,
      s_axis_listen_port_req_V_V_TVALID => s_axis_listen_port_req_V_V_TVALID,
      s_axis_listen_port_req_V_V_TREADY => s_axis_listen_port_req_V_V_TREADY,
      s_axis_listen_port_req_V_V_TDATA => s_axis_listen_port_req_V_V_TDATA,
      s_axis_rx_data_req_V_TVALID => s_axis_rx_data_req_V_TVALID,
      s_axis_rx_data_req_V_TREADY => s_axis_rx_data_req_V_TREADY,
      s_axis_rx_data_req_V_TDATA => s_axis_rx_data_req_V_TDATA,
      s_axis_open_conn_req_V_TVALID => s_axis_open_conn_req_V_TVALID,
      s_axis_open_conn_req_V_TREADY => s_axis_open_conn_req_V_TREADY,
      s_axis_open_conn_req_V_TDATA => s_axis_open_conn_req_V_TDATA,
      s_axis_close_conn_req_V_V_TVALID => s_axis_close_conn_req_V_V_TVALID,
      s_axis_close_conn_req_V_V_TREADY => s_axis_close_conn_req_V_V_TREADY,
      s_axis_close_conn_req_V_V_TDATA => s_axis_close_conn_req_V_V_TDATA,
      s_axis_tx_data_req_metadata_V_TVALID => s_axis_tx_data_req_metadata_V_TVALID,
      s_axis_tx_data_req_metadata_V_TREADY => s_axis_tx_data_req_metadata_V_TREADY,
      s_axis_tx_data_req_metadata_V_TDATA => s_axis_tx_data_req_metadata_V_TDATA,
      s_axis_tx_data_req_TVALID => s_axis_tx_data_req_TVALID,
      s_axis_tx_data_req_TREADY => s_axis_tx_data_req_TREADY,
      s_axis_tx_data_req_TDATA => s_axis_tx_data_req_TDATA,
      s_axis_tx_data_req_TKEEP => s_axis_tx_data_req_TKEEP,
      s_axis_tx_data_req_TLAST => s_axis_tx_data_req_TLAST,
      m_axis_listen_port_rsp_V_TVALID => m_axis_listen_port_rsp_V_TVALID,
      m_axis_listen_port_rsp_V_TREADY => m_axis_listen_port_rsp_V_TREADY,
      m_axis_listen_port_rsp_V_TDATA => m_axis_listen_port_rsp_V_TDATA,
      m_axis_notification_V_TVALID => m_axis_notification_V_TVALID,
      m_axis_notification_V_TREADY => m_axis_notification_V_TREADY,
      m_axis_notification_V_TDATA => m_axis_notification_V_TDATA,
      m_axis_rx_data_rsp_metadata_V_V_TVALID => m_axis_rx_data_rsp_metadata_V_V_TVALID,
      m_axis_rx_data_rsp_metadata_V_V_TREADY => m_axis_rx_data_rsp_metadata_V_V_TREADY,
      m_axis_rx_data_rsp_metadata_V_V_TDATA => m_axis_rx_data_rsp_metadata_V_V_TDATA,
      m_axis_rx_data_rsp_TVALID => m_axis_rx_data_rsp_TVALID,
      m_axis_rx_data_rsp_TREADY => m_axis_rx_data_rsp_TREADY,
      m_axis_rx_data_rsp_TDATA => m_axis_rx_data_rsp_TDATA,
      m_axis_rx_data_rsp_TKEEP => m_axis_rx_data_rsp_TKEEP,
      m_axis_rx_data_rsp_TLAST => m_axis_rx_data_rsp_TLAST,
      m_axis_open_conn_rsp_V_TVALID => m_axis_open_conn_rsp_V_TVALID,
      m_axis_open_conn_rsp_V_TREADY => m_axis_open_conn_rsp_V_TREADY,
      m_axis_open_conn_rsp_V_TDATA => m_axis_open_conn_rsp_V_TDATA,
      m_axis_tx_data_rsp_V_TVALID => m_axis_tx_data_rsp_V_TVALID,
      m_axis_tx_data_rsp_V_TREADY => m_axis_tx_data_rsp_V_TREADY,
      m_axis_tx_data_rsp_V_TDATA => m_axis_tx_data_rsp_V_TDATA,
      axis_data_count_V => axis_data_count_V,
      axis_max_data_count_V => axis_max_data_count_V,
      myIpAddress_V => myIpAddress_V,
      regSessionCount_V => regSessionCount_V,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n
    );
END toe_ip_arch;
