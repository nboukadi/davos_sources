#include "xparameters.h"
#include "xil_printf.h"
#include "platform.h"
#include "xil_io.h"

#define CMAC_BASE_ADDRESS  XPAR_CMAC_USPLUS_0_BASEADDR
#define DRAM_BASE_ADDRESS  XPAR_DDR4_0_BASEADDR

void cmac_init(void);

int main(void)
{
	int Status;

    cmac_init();
    sleep(1);


	xil_printf("GT_reset = 0x%x \r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0000));
    xil_printf("TestReg = 0x%x \r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0004));
    xil_printf("RSFEC = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x107C));
    xil_printf("TX_status = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0200));
    xil_printf("RX_status_r0 = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0204));
    xil_printf("RX_status_r1 = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0208));
    xil_printf("RX_lock = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x020C));
    xil_printf("RX_sync = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0210));
    xil_printf("TX_stat = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x023C));
    xil_printf("Revision = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0024));
    xil_printf("Bad TX FCS = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x05B8));
    xil_printf("Bad TX packet FCS = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0740));
    xil_printf("Bad TX stomped FCS = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x0748));
    xil_printf("Bad RX FCS = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x06C0));
    xil_printf("Bad RX packet FCS = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x06C8));
    xil_printf("Bad RX stomped FCS = 0x%x\r\n", Xil_In32(CMAC_BASE_ADDRESS + 0x06D0));
    int i;
    for(i=0;i<4096;i++)
    {
      Xil_Out32(DRAM_BASE_ADDRESS + 0x0004*i, 0x00010203 + 0x04040404*i);
    }

    return 0;

}

void cmac_init() {
	// CMAC configuration
	Xil_Out32(CMAC_BASE_ADDRESS + 0x000C, 0x00000000); //tx off
    Xil_Out32(CMAC_BASE_ADDRESS + 0x0014, 0x00000000); // rx off
    Xil_Out32(CMAC_BASE_ADDRESS + 0x107C, 0x00000003); // rsfec en

    u8 reset = 1;
    if (reset) {
	  //Xil_Out32(CMAC_BASE_ADDRESS + 0x0000, 0x00000001); //reset gt
	  Xil_Out32(CMAC_BASE_ADDRESS + 0x0004, 0xC00001FF); //reset all
	  sleep(1);
	  Xil_Out32(CMAC_BASE_ADDRESS + 0x0000, 0x0);
	  Xil_Out32(CMAC_BASE_ADDRESS + 0x0004, 0x0);
	  sleep(1);
    }
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0090, 0x00000000); //internal loopback
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0014, 0x00000001); // rx en
	Xil_Out32(CMAC_BASE_ADDRESS + 0x000C, 0x00000010); // tx send rfi
	while (!(Xil_In32(CMAC_BASE_ADDRESS + 0x0204) & 0x2)){ //wait till tx aligned
	}

	Xil_Out32(CMAC_BASE_ADDRESS + 0x000C, 0x00000001); //tx on
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0084, 0x00003DFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0088, 0x0001C631);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0048, 0xFFFFFFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x004C, 0xFFFFFFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0050, 0xFFFFFFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0054, 0xFFFFFFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0058, 0x0000FFFF);

	Xil_Out32(CMAC_BASE_ADDRESS + 0x0034, 0xFFFFFFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0038, 0xFFFFFFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x003C, 0xFFFFFFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0040, 0xFFFFFFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0044, 0x0000FFFF);
	Xil_Out32(CMAC_BASE_ADDRESS + 0x0030, 0x000001FF);
}
