// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module ipv4_process_ipv4_512_s (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        s_axis_rx_data_TVALID,
        rx_process2dropFifo_1_1_din,
        rx_process2dropFifo_1_1_full_n,
        rx_process2dropFifo_1_1_write,
        rx_process2dropFifo_2_0_din,
        rx_process2dropFifo_2_0_full_n,
        rx_process2dropFifo_2_0_write,
        rx_process2dropFifo_s_2_din,
        rx_process2dropFifo_s_2_full_n,
        rx_process2dropFifo_s_2_write,
        rx_process2dropLengt_1_din,
        rx_process2dropLengt_1_full_n,
        rx_process2dropLengt_1_write,
        MetaOut_V_TREADY,
        s_axis_rx_data_TDATA,
        s_axis_rx_data_TREADY,
        s_axis_rx_data_TKEEP,
        s_axis_rx_data_TLAST,
        MetaOut_V_TDATA,
        MetaOut_V_TVALID
);

parameter    ap_ST_fsm_pp0_stage0 = 1'd1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
input   s_axis_rx_data_TVALID;
output  [511:0] rx_process2dropFifo_1_1_din;
input   rx_process2dropFifo_1_1_full_n;
output   rx_process2dropFifo_1_1_write;
output  [63:0] rx_process2dropFifo_2_0_din;
input   rx_process2dropFifo_2_0_full_n;
output   rx_process2dropFifo_2_0_write;
output  [0:0] rx_process2dropFifo_s_2_din;
input   rx_process2dropFifo_s_2_full_n;
output   rx_process2dropFifo_s_2_write;
output  [3:0] rx_process2dropLengt_1_din;
input   rx_process2dropLengt_1_full_n;
output   rx_process2dropLengt_1_write;
input   MetaOut_V_TREADY;
input  [511:0] s_axis_rx_data_TDATA;
output   s_axis_rx_data_TREADY;
input  [63:0] s_axis_rx_data_TKEEP;
input  [0:0] s_axis_rx_data_TLAST;
output  [47:0] MetaOut_V_TDATA;
output   MetaOut_V_TVALID;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg rx_process2dropFifo_1_1_write;
reg rx_process2dropFifo_2_0_write;
reg rx_process2dropFifo_s_2_write;
reg rx_process2dropLengt_1_write;
reg s_axis_rx_data_TREADY;

reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [0:0] ap_CS_fsm;
wire    ap_CS_fsm_pp0_stage0;
wire    ap_enable_reg_pp0_iter0;
reg    ap_enable_reg_pp0_iter1;
reg    ap_enable_reg_pp0_iter2;
reg    ap_idle_pp0;
wire   [0:0] tmp_nbreadreq_fu_98_p5;
reg    ap_block_state1_pp0_stage0_iter0;
wire    io_acc_block_signal_op48;
reg   [0:0] tmp_reg_425;
reg   [0:0] or_ln73_reg_447;
reg    ap_predicate_op48_write_state2;
reg   [0:0] metaWritten_load_reg_451;
reg    ap_predicate_op51_write_state2;
reg    ap_block_state2_pp0_stage0_iter1;
wire    MetaOut_V_1_ack_in;
reg    ap_predicate_op56_write_state2;
reg    ap_block_state2_io;
wire    MetaOut_V_1_ack_out;
reg   [1:0] MetaOut_V_1_state;
reg    ap_block_state3_pp0_stage0_iter2;
reg   [0:0] tmp_reg_425_pp0_iter1_reg;
reg   [0:0] or_ln73_reg_447_pp0_iter1_reg;
reg   [0:0] metaWritten_load_reg_451_pp0_iter1_reg;
reg    ap_predicate_op64_write_state3;
reg    ap_block_state3_io;
reg    ap_block_pp0_stage0_11001;
reg   [47:0] MetaOut_V_1_data_out;
reg    MetaOut_V_1_vld_in;
wire    MetaOut_V_1_vld_out;
reg   [47:0] MetaOut_V_1_payload_A;
reg   [47:0] MetaOut_V_1_payload_B;
reg    MetaOut_V_1_sel_rd;
reg    MetaOut_V_1_sel_wr;
wire    MetaOut_V_1_sel;
wire    MetaOut_V_1_load_A;
wire    MetaOut_V_1_load_B;
wire    MetaOut_V_1_state_cmp_full;
reg   [0:0] header_ready;
reg   [15:0] header_idx;
reg   [159:0] header_header_V;
reg   [0:0] metaWritten;
reg    s_axis_rx_data_TDATA_blk_n;
wire    ap_block_pp0_stage0;
reg    MetaOut_V_TDATA_blk_n;
reg    rx_process2dropFifo_1_1_blk_n;
reg    rx_process2dropFifo_2_0_blk_n;
reg    rx_process2dropFifo_s_2_blk_n;
reg    rx_process2dropLengt_1_blk_n;
reg   [511:0] tmp_data_V_reg_429;
reg   [63:0] tmp_keep_V_reg_434;
reg   [0:0] tmp_last_V_reg_439;
wire   [0:0] or_ln73_fu_321_p2;
wire   [0:0] metaWritten_load_load_fu_327_p1;
wire   [0:0] or_ln63_fu_351_p2;
wire   [47:0] tmp_2_fu_414_p4;
reg    ap_block_pp0_stage0_subdone;
reg   [15:0] ap_phi_mux_phi_ln73_phi_fu_150_p4;
wire   [15:0] ap_phi_reg_pp0_iter0_phi_ln73_reg_147;
wire   [0:0] header_ready_load_load_fu_193_p1;
wire   [15:0] add_ln67_fu_314_p2;
reg   [0:0] ap_phi_mux_write_flag_1_i_i_phi_fu_159_p4;
wire   [0:0] ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_156;
reg   [0:0] ap_phi_mux_metaWritten_flag_1_i_phi_fu_170_p6;
wire   [0:0] ap_phi_reg_pp0_iter0_metaWritten_flag_1_i_reg_167;
wire   [0:0] and_ln63_fu_337_p2;
wire   [15:0] select_ln63_fu_343_p3;
wire   [159:0] p_Result_s_fu_302_p2;
wire   [0:0] xor_ln63_fu_331_p2;
reg    ap_block_pp0_stage0_01001;
wire   [24:0] Lo_assign_fu_202_p3;
wire   [0:0] trunc_ln414_fu_224_p1;
wire   [0:0] icmp_ln414_fu_218_p2;
wire   [159:0] st3_fu_228_p3;
wire   [159:0] tmp_V_1_fu_210_p1;
wire   [159:0] select_ln414_fu_236_p3;
reg   [159:0] tmp_1_fu_244_p4;
wire   [159:0] select_ln414_2_fu_262_p3;
wire   [159:0] select_ln414_3_fu_270_p3;
wire   [159:0] and_ln414_fu_278_p2;
wire   [159:0] xor_ln414_fu_284_p2;
wire   [159:0] select_ln414_1_fu_254_p3;
wire   [159:0] and_ln414_1_fu_290_p2;
wire   [159:0] and_ln414_2_fu_296_p2;
wire   [7:0] p_Result_2_1_i_i_fu_404_p4;
wire   [7:0] p_Result_2_i_i19_i_fu_394_p4;
wire   [31:0] tmp_their_address_V_fu_384_p4;
reg   [0:0] ap_NS_fsm;
reg    ap_idle_pp0_0to1;
reg    ap_reset_idle_pp0;
wire    ap_enable_pp0;
reg    ap_condition_200;

// power-on initialization
initial begin
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 1'd1;
#0 ap_enable_reg_pp0_iter1 = 1'b0;
#0 ap_enable_reg_pp0_iter2 = 1'b0;
#0 MetaOut_V_1_state = 2'd0;
#0 MetaOut_V_1_sel_rd = 1'b0;
#0 MetaOut_V_1_sel_wr = 1'b0;
#0 header_ready = 1'd0;
#0 header_idx = 16'd0;
#0 header_header_V = 160'd1180591620717411303493;
#0 metaWritten = 1'd0;
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        MetaOut_V_1_sel_rd <= 1'b0;
    end else begin
        if (((1'b1 == MetaOut_V_1_ack_out) & (1'b1 == MetaOut_V_1_vld_out))) begin
            MetaOut_V_1_sel_rd <= ~MetaOut_V_1_sel_rd;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        MetaOut_V_1_sel_wr <= 1'b0;
    end else begin
        if (((1'b1 == MetaOut_V_1_ack_in) & (1'b1 == MetaOut_V_1_vld_in))) begin
            MetaOut_V_1_sel_wr <= ~MetaOut_V_1_sel_wr;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        MetaOut_V_1_state <= 2'd0;
    end else begin
        if ((((2'd3 == MetaOut_V_1_state) & (1'b0 == MetaOut_V_1_vld_in) & (1'b1 == MetaOut_V_1_ack_out)) | ((2'd2 == MetaOut_V_1_state) & (1'b0 == MetaOut_V_1_vld_in)))) begin
            MetaOut_V_1_state <= 2'd2;
        end else if ((((2'd1 == MetaOut_V_1_state) & (1'b0 == MetaOut_V_TREADY)) | ((2'd3 == MetaOut_V_1_state) & (1'b0 == MetaOut_V_TREADY) & (1'b1 == MetaOut_V_1_vld_in)))) begin
            MetaOut_V_1_state <= 2'd1;
        end else if ((((2'd1 == MetaOut_V_1_state) & (1'b1 == MetaOut_V_1_ack_out)) | (~((1'b0 == MetaOut_V_1_vld_in) & (1'b1 == MetaOut_V_1_ack_out)) & ~((1'b0 == MetaOut_V_TREADY) & (1'b1 == MetaOut_V_1_vld_in)) & (2'd3 == MetaOut_V_1_state)) | ((2'd2 == MetaOut_V_1_state) & (1'b1 == MetaOut_V_1_vld_in)))) begin
            MetaOut_V_1_state <= 2'd3;
        end else begin
            MetaOut_V_1_state <= 2'd2;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_pp0_stage0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((ap_continue == 1'b1)) begin
            ap_done_reg <= 1'b0;
        end else if (((ap_enable_reg_pp0_iter2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter1 <= 1'b0;
    end else begin
        if (((1'b0 == ap_block_pp0_stage0_subdone) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
            ap_enable_reg_pp0_iter1 <= ap_start;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter2 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter2 <= ap_enable_reg_pp0_iter1;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == MetaOut_V_1_load_A)) begin
        MetaOut_V_1_payload_A <= tmp_2_fu_414_p4;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == MetaOut_V_1_load_B)) begin
        MetaOut_V_1_payload_B <= tmp_2_fu_414_p4;
    end
end

always @ (posedge ap_clk) begin
    if (((header_ready_load_load_fu_193_p1 == 1'd0) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        header_header_V <= p_Result_s_fu_302_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        header_idx <= select_ln63_fu_343_p3;
        header_ready <= and_ln63_fu_337_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((or_ln63_fu_351_p2 == 1'd1) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        metaWritten <= xor_ln63_fu_331_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((tmp_nbreadreq_fu_98_p5 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        metaWritten_load_reg_451 <= metaWritten;
        or_ln73_reg_447 <= or_ln73_fu_321_p2;
        tmp_data_V_reg_429 <= s_axis_rx_data_TDATA;
        tmp_keep_V_reg_434 <= s_axis_rx_data_TKEEP;
        tmp_last_V_reg_439 <= s_axis_rx_data_TLAST;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        metaWritten_load_reg_451_pp0_iter1_reg <= metaWritten_load_reg_451;
        or_ln73_reg_447_pp0_iter1_reg <= or_ln73_reg_447;
        tmp_reg_425 <= tmp_nbreadreq_fu_98_p5;
        tmp_reg_425_pp0_iter1_reg <= tmp_reg_425;
    end
end

always @ (*) begin
    if ((1'b1 == MetaOut_V_1_sel)) begin
        MetaOut_V_1_data_out = MetaOut_V_1_payload_B;
    end else begin
        MetaOut_V_1_data_out = MetaOut_V_1_payload_A;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op56_write_state2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        MetaOut_V_1_vld_in = 1'b1;
    end else begin
        MetaOut_V_1_vld_in = 1'b0;
    end
end

always @ (*) begin
    if ((((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0) & (ap_predicate_op56_write_state2 == 1'b1)) | ((ap_enable_reg_pp0_iter2 == 1'b1) & (1'b0 == ap_block_pp0_stage0) & (ap_predicate_op64_write_state3 == 1'b1)))) begin
        MetaOut_V_TDATA_blk_n = MetaOut_V_1_state[1'd1];
    end else begin
        MetaOut_V_TDATA_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = ap_done_reg;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0 = 1'b1;
    end else begin
        ap_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0_0to1 = 1'b1;
    end else begin
        ap_idle_pp0_0to1 = 1'b0;
    end
end

always @ (*) begin
    if (((metaWritten_load_load_fu_327_p1 == 1'd0) & (or_ln73_fu_321_p2 == 1'd1) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        ap_phi_mux_metaWritten_flag_1_i_phi_fu_170_p6 = 1'd1;
    end else if ((((metaWritten_load_load_fu_327_p1 == 1'd1) & (or_ln73_fu_321_p2 == 1'd1) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0)) | ((or_ln73_fu_321_p2 == 1'd0) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0)))) begin
        ap_phi_mux_metaWritten_flag_1_i_phi_fu_170_p6 = 1'd0;
    end else begin
        ap_phi_mux_metaWritten_flag_1_i_phi_fu_170_p6 = ap_phi_reg_pp0_iter0_metaWritten_flag_1_i_reg_167;
    end
end

always @ (*) begin
    if ((1'b1 == ap_condition_200)) begin
        if ((header_ready_load_load_fu_193_p1 == 1'd0)) begin
            ap_phi_mux_phi_ln73_phi_fu_150_p4 = add_ln67_fu_314_p2;
        end else if ((header_ready_load_load_fu_193_p1 == 1'd1)) begin
            ap_phi_mux_phi_ln73_phi_fu_150_p4 = header_idx;
        end else begin
            ap_phi_mux_phi_ln73_phi_fu_150_p4 = ap_phi_reg_pp0_iter0_phi_ln73_reg_147;
        end
    end else begin
        ap_phi_mux_phi_ln73_phi_fu_150_p4 = ap_phi_reg_pp0_iter0_phi_ln73_reg_147;
    end
end

always @ (*) begin
    if ((1'b1 == ap_condition_200)) begin
        if ((header_ready_load_load_fu_193_p1 == 1'd0)) begin
            ap_phi_mux_write_flag_1_i_i_phi_fu_159_p4 = 1'd1;
        end else if ((header_ready_load_load_fu_193_p1 == 1'd1)) begin
            ap_phi_mux_write_flag_1_i_i_phi_fu_159_p4 = 1'd0;
        end else begin
            ap_phi_mux_write_flag_1_i_i_phi_fu_159_p4 = ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_156;
        end
    end else begin
        ap_phi_mux_write_flag_1_i_i_phi_fu_159_p4 = ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_156;
    end
end

always @ (*) begin
    if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0_0to1 == 1'b1))) begin
        ap_reset_idle_pp0 = 1'b1;
    end else begin
        ap_reset_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0) & (ap_predicate_op48_write_state2 == 1'b1))) begin
        rx_process2dropFifo_1_1_blk_n = rx_process2dropFifo_1_1_full_n;
    end else begin
        rx_process2dropFifo_1_1_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op48_write_state2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_process2dropFifo_1_1_write = 1'b1;
    end else begin
        rx_process2dropFifo_1_1_write = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0) & (ap_predicate_op48_write_state2 == 1'b1))) begin
        rx_process2dropFifo_2_0_blk_n = rx_process2dropFifo_2_0_full_n;
    end else begin
        rx_process2dropFifo_2_0_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op48_write_state2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_process2dropFifo_2_0_write = 1'b1;
    end else begin
        rx_process2dropFifo_2_0_write = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0) & (ap_predicate_op48_write_state2 == 1'b1))) begin
        rx_process2dropFifo_s_2_blk_n = rx_process2dropFifo_s_2_full_n;
    end else begin
        rx_process2dropFifo_s_2_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op48_write_state2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_process2dropFifo_s_2_write = 1'b1;
    end else begin
        rx_process2dropFifo_s_2_write = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0) & (ap_predicate_op51_write_state2 == 1'b1))) begin
        rx_process2dropLengt_1_blk_n = rx_process2dropLengt_1_full_n;
    end else begin
        rx_process2dropLengt_1_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op51_write_state2 == 1'b1) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        rx_process2dropLengt_1_write = 1'b1;
    end else begin
        rx_process2dropLengt_1_write = 1'b0;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        s_axis_rx_data_TDATA_blk_n = s_axis_rx_data_TVALID;
    end else begin
        s_axis_rx_data_TDATA_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        s_axis_rx_data_TREADY = 1'b1;
    end else begin
        s_axis_rx_data_TREADY = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_pp0_stage0 : begin
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign Lo_assign_fu_202_p3 = {{header_idx}, {9'd0}};

assign MetaOut_V_1_ack_in = MetaOut_V_1_state[1'd1];

assign MetaOut_V_1_ack_out = MetaOut_V_TREADY;

assign MetaOut_V_1_load_A = (~MetaOut_V_1_sel_wr & MetaOut_V_1_state_cmp_full);

assign MetaOut_V_1_load_B = (MetaOut_V_1_state_cmp_full & MetaOut_V_1_sel_wr);

assign MetaOut_V_1_sel = MetaOut_V_1_sel_rd;

assign MetaOut_V_1_state_cmp_full = ((MetaOut_V_1_state != 2'd1) ? 1'b1 : 1'b0);

assign MetaOut_V_1_vld_out = MetaOut_V_1_state[1'd0];

assign MetaOut_V_TDATA = MetaOut_V_1_data_out;

assign MetaOut_V_TVALID = MetaOut_V_1_state[1'd0];

assign add_ln67_fu_314_p2 = (16'd1 + header_idx);

assign and_ln414_1_fu_290_p2 = (xor_ln414_fu_284_p2 & header_header_V);

assign and_ln414_2_fu_296_p2 = (select_ln414_1_fu_254_p3 & and_ln414_fu_278_p2);

assign and_ln414_fu_278_p2 = (select_ln414_3_fu_270_p3 & select_ln414_2_fu_262_p3);

assign and_ln63_fu_337_p2 = (xor_ln63_fu_331_p2 & or_ln73_fu_321_p2);

assign ap_CS_fsm_pp0_stage0 = ap_CS_fsm[32'd0];

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_pp0_stage0_01001 = ((ap_done_reg == 1'b1) | ((ap_enable_reg_pp0_iter1 == 1'b1) & (((rx_process2dropLengt_1_full_n == 1'b0) & (ap_predicate_op51_write_state2 == 1'b1)) | ((io_acc_block_signal_op48 == 1'b0) & (ap_predicate_op48_write_state2 == 1'b1)))) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (s_axis_rx_data_TVALID == 1'b0)))) | ((ap_enable_reg_pp0_iter2 == 1'b1) & ((2'd1 == MetaOut_V_1_state) | ((2'd3 == MetaOut_V_1_state) & (1'b0 == MetaOut_V_TREADY)))));
end

always @ (*) begin
    ap_block_pp0_stage0_11001 = ((ap_done_reg == 1'b1) | ((ap_enable_reg_pp0_iter1 == 1'b1) & ((1'b1 == ap_block_state2_io) | ((rx_process2dropLengt_1_full_n == 1'b0) & (ap_predicate_op51_write_state2 == 1'b1)) | ((io_acc_block_signal_op48 == 1'b0) & (ap_predicate_op48_write_state2 == 1'b1)))) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (s_axis_rx_data_TVALID == 1'b0)))) | ((ap_enable_reg_pp0_iter2 == 1'b1) & ((2'd1 == MetaOut_V_1_state) | (1'b1 == ap_block_state3_io) | ((2'd3 == MetaOut_V_1_state) & (1'b0 == MetaOut_V_TREADY)))));
end

always @ (*) begin
    ap_block_pp0_stage0_subdone = ((ap_done_reg == 1'b1) | ((ap_enable_reg_pp0_iter1 == 1'b1) & ((1'b1 == ap_block_state2_io) | ((rx_process2dropLengt_1_full_n == 1'b0) & (ap_predicate_op51_write_state2 == 1'b1)) | ((io_acc_block_signal_op48 == 1'b0) & (ap_predicate_op48_write_state2 == 1'b1)))) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (s_axis_rx_data_TVALID == 1'b0)))) | ((ap_enable_reg_pp0_iter2 == 1'b1) & ((2'd1 == MetaOut_V_1_state) | (1'b1 == ap_block_state3_io) | ((2'd3 == MetaOut_V_1_state) & (1'b0 == MetaOut_V_TREADY)))));
end

always @ (*) begin
    ap_block_state1_pp0_stage0_iter0 = ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (s_axis_rx_data_TVALID == 1'b0)));
end

always @ (*) begin
    ap_block_state2_io = ((1'b0 == MetaOut_V_1_ack_in) & (ap_predicate_op56_write_state2 == 1'b1));
end

always @ (*) begin
    ap_block_state2_pp0_stage0_iter1 = (((rx_process2dropLengt_1_full_n == 1'b0) & (ap_predicate_op51_write_state2 == 1'b1)) | ((io_acc_block_signal_op48 == 1'b0) & (ap_predicate_op48_write_state2 == 1'b1)));
end

always @ (*) begin
    ap_block_state3_io = ((1'b0 == MetaOut_V_1_ack_in) & (ap_predicate_op64_write_state3 == 1'b1));
end

always @ (*) begin
    ap_block_state3_pp0_stage0_iter2 = ((2'd1 == MetaOut_V_1_state) | ((2'd3 == MetaOut_V_1_state) & (1'b0 == MetaOut_V_TREADY)));
end

always @ (*) begin
    ap_condition_200 = ((tmp_nbreadreq_fu_98_p5 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0));
end

assign ap_enable_pp0 = (ap_idle_pp0 ^ 1'b1);

assign ap_enable_reg_pp0_iter0 = ap_start;

assign ap_phi_reg_pp0_iter0_metaWritten_flag_1_i_reg_167 = 'bx;

assign ap_phi_reg_pp0_iter0_phi_ln73_reg_147 = 'bx;

assign ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_156 = 'bx;

always @ (*) begin
    ap_predicate_op48_write_state2 = ((or_ln73_reg_447 == 1'd1) & (tmp_reg_425 == 1'd1));
end

always @ (*) begin
    ap_predicate_op51_write_state2 = ((metaWritten_load_reg_451 == 1'd0) & (or_ln73_reg_447 == 1'd1) & (tmp_reg_425 == 1'd1));
end

always @ (*) begin
    ap_predicate_op56_write_state2 = ((metaWritten_load_reg_451 == 1'd0) & (or_ln73_reg_447 == 1'd1) & (tmp_reg_425 == 1'd1));
end

always @ (*) begin
    ap_predicate_op64_write_state3 = ((metaWritten_load_reg_451_pp0_iter1_reg == 1'd0) & (or_ln73_reg_447_pp0_iter1_reg == 1'd1) & (tmp_reg_425_pp0_iter1_reg == 1'd1));
end

assign header_ready_load_load_fu_193_p1 = header_ready;

assign icmp_ln414_fu_218_p2 = ((Lo_assign_fu_202_p3 > 25'd159) ? 1'b1 : 1'b0);

assign io_acc_block_signal_op48 = (rx_process2dropFifo_s_2_full_n & rx_process2dropFifo_2_0_full_n & rx_process2dropFifo_1_1_full_n);

assign metaWritten_load_load_fu_327_p1 = metaWritten;

assign or_ln63_fu_351_p2 = (s_axis_rx_data_TLAST | ap_phi_mux_metaWritten_flag_1_i_phi_fu_170_p6);

assign or_ln73_fu_321_p2 = (header_ready | ap_phi_mux_write_flag_1_i_i_phi_fu_159_p4);

assign p_Result_2_1_i_i_fu_404_p4 = {{header_header_V[23:16]}};

assign p_Result_2_i_i19_i_fu_394_p4 = {{header_header_V[31:24]}};

assign p_Result_s_fu_302_p2 = (and_ln414_2_fu_296_p2 | and_ln414_1_fu_290_p2);

assign rx_process2dropFifo_1_1_din = tmp_data_V_reg_429;

assign rx_process2dropFifo_2_0_din = tmp_keep_V_reg_434;

assign rx_process2dropFifo_s_2_din = tmp_last_V_reg_439;

assign rx_process2dropLengt_1_din = header_header_V[3:0];

assign select_ln414_1_fu_254_p3 = ((icmp_ln414_fu_218_p2[0:0] === 1'b1) ? tmp_1_fu_244_p4 : tmp_V_1_fu_210_p1);

assign select_ln414_2_fu_262_p3 = ((icmp_ln414_fu_218_p2[0:0] === 1'b1) ? 160'd730750818665451459101842416358141509827966271488 : 160'd1461501637330902918203684832716283019655932542975);

assign select_ln414_3_fu_270_p3 = ((icmp_ln414_fu_218_p2[0:0] === 1'b1) ? 160'd1 : 160'd1461501637330902918203684832716283019655932542975);

assign select_ln414_fu_236_p3 = ((icmp_ln414_fu_218_p2[0:0] === 1'b1) ? st3_fu_228_p3 : tmp_V_1_fu_210_p1);

assign select_ln63_fu_343_p3 = ((s_axis_rx_data_TLAST[0:0] === 1'b1) ? 16'd0 : ap_phi_mux_phi_ln73_phi_fu_150_p4);

assign st3_fu_228_p3 = {{trunc_ln414_fu_224_p1}, {159'd0}};

integer ap_tvar_int_0;

always @ (select_ln414_fu_236_p3) begin
    for (ap_tvar_int_0 = 160 - 1; ap_tvar_int_0 >= 0; ap_tvar_int_0 = ap_tvar_int_0 - 1) begin
        if (ap_tvar_int_0 > 159 - 0) begin
            tmp_1_fu_244_p4[ap_tvar_int_0] = 1'b0;
        end else begin
            tmp_1_fu_244_p4[ap_tvar_int_0] = select_ln414_fu_236_p3[159 - ap_tvar_int_0];
        end
    end
end

assign tmp_2_fu_414_p4 = {{{p_Result_2_1_i_i_fu_404_p4}, {p_Result_2_i_i19_i_fu_394_p4}}, {tmp_their_address_V_fu_384_p4}};

assign tmp_V_1_fu_210_p1 = s_axis_rx_data_TDATA[159:0];

assign tmp_nbreadreq_fu_98_p5 = s_axis_rx_data_TVALID;

assign tmp_their_address_V_fu_384_p4 = {{header_header_V[127:96]}};

assign trunc_ln414_fu_224_p1 = s_axis_rx_data_TDATA[0:0];

assign xor_ln414_fu_284_p2 = (160'd1461501637330902918203684832716283019655932542975 ^ and_ln414_fu_278_p2);

assign xor_ln63_fu_331_p2 = (s_axis_rx_data_TLAST ^ 1'd1);

endmodule //ipv4_process_ipv4_512_s
