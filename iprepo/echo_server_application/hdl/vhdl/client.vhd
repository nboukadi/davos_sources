-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2019.1
-- Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity client is
port (
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_continue : IN STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    txMetaData_V_TDATA : OUT STD_LOGIC_VECTOR (31 downto 0);
    txMetaData_V_TVALID : OUT STD_LOGIC;
    txMetaData_V_TREADY : IN STD_LOGIC;
    m_axis_tx_data_TDATA : OUT STD_LOGIC_VECTOR (63 downto 0);
    m_axis_tx_data_TVALID : OUT STD_LOGIC;
    m_axis_tx_data_TREADY : IN STD_LOGIC;
    m_axis_tx_data_TKEEP : OUT STD_LOGIC_VECTOR (7 downto 0);
    m_axis_tx_data_TLAST : OUT STD_LOGIC_VECTOR (0 downto 0);
    esa_sessionidFifo_V_s_dout : IN STD_LOGIC_VECTOR (15 downto 0);
    esa_sessionidFifo_V_s_empty_n : IN STD_LOGIC;
    esa_sessionidFifo_V_s_read : OUT STD_LOGIC;
    esa_lengthFifo_V_V_dout : IN STD_LOGIC_VECTOR (15 downto 0);
    esa_lengthFifo_V_V_empty_n : IN STD_LOGIC;
    esa_lengthFifo_V_V_read : OUT STD_LOGIC;
    esa_dataFifo_V_data_s_dout : IN STD_LOGIC_VECTOR (63 downto 0);
    esa_dataFifo_V_data_s_empty_n : IN STD_LOGIC;
    esa_dataFifo_V_data_s_read : OUT STD_LOGIC;
    esa_dataFifo_V_keep_s_dout : IN STD_LOGIC_VECTOR (7 downto 0);
    esa_dataFifo_V_keep_s_empty_n : IN STD_LOGIC;
    esa_dataFifo_V_keep_s_read : OUT STD_LOGIC;
    esa_dataFifo_V_last_s_dout : IN STD_LOGIC_VECTOR (0 downto 0);
    esa_dataFifo_V_last_s_empty_n : IN STD_LOGIC;
    esa_dataFifo_V_last_s_read : OUT STD_LOGIC );
end;


architecture behav of client is 
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_ST_fsm_state1 : STD_LOGIC_VECTOR (0 downto 0) := "1";
    constant ap_const_lv32_0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000000";
    constant ap_const_boolean_1 : BOOLEAN := true;
    constant ap_const_lv1_0 : STD_LOGIC_VECTOR (0 downto 0) := "0";
    constant ap_const_lv1_1 : STD_LOGIC_VECTOR (0 downto 0) := "1";

    signal ap_done_reg : STD_LOGIC := '0';
    signal ap_CS_fsm : STD_LOGIC_VECTOR (0 downto 0) := "1";
    attribute fsm_encoding : string;
    attribute fsm_encoding of ap_CS_fsm : signal is "none";
    signal ap_CS_fsm_state1 : STD_LOGIC;
    attribute fsm_encoding of ap_CS_fsm_state1 : signal is "none";
    signal esac_fsmState_V : STD_LOGIC_VECTOR (0 downto 0) := "0";
    signal txMetaData_V_TDATA_blk_n : STD_LOGIC;
    signal tmp_4_nbreadreq_fu_66_p3 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_6_nbreadreq_fu_74_p3 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_7_nbwritereq_fu_82_p3 : STD_LOGIC_VECTOR (0 downto 0);
    signal m_axis_tx_data_TDATA_blk_n : STD_LOGIC;
    signal tmp_nbreadreq_fu_109_p5 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_5_nbwritereq_fu_121_p5 : STD_LOGIC_VECTOR (0 downto 0);
    signal esa_sessionidFifo_V_s_blk_n : STD_LOGIC;
    signal esa_lengthFifo_V_V_blk_n : STD_LOGIC;
    signal esa_dataFifo_V_data_s_blk_n : STD_LOGIC;
    signal esa_dataFifo_V_keep_s_blk_n : STD_LOGIC;
    signal esa_dataFifo_V_last_s_blk_n : STD_LOGIC;
    signal ap_predicate_op18_read_state1 : BOOLEAN;
    signal ap_predicate_op19_read_state1 : BOOLEAN;
    signal io_acc_block_signal_op29 : STD_LOGIC;
    signal ap_predicate_op29_read_state1 : BOOLEAN;
    signal ap_block_state1 : BOOLEAN;
    signal ap_predicate_op21_write_state1 : BOOLEAN;
    signal ap_predicate_op33_write_state1 : BOOLEAN;
    signal ap_block_state1_io : BOOLEAN;
    signal tmp_last_V_fu_185_p1 : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_NS_fsm : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_condition_68 : BOOLEAN;
    signal ap_condition_121 : BOOLEAN;
    signal ap_condition_117 : BOOLEAN;


begin




    ap_CS_fsm_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_CS_fsm <= ap_ST_fsm_state1;
            else
                ap_CS_fsm <= ap_NS_fsm;
            end if;
        end if;
    end process;


    ap_done_reg_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_done_reg <= ap_const_logic_0;
            else
                if ((ap_continue = ap_const_logic_1)) then 
                    ap_done_reg <= ap_const_logic_0;
                elsif ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
                    ap_done_reg <= ap_const_logic_1;
                end if; 
            end if;
        end if;
    end process;


    esac_fsmState_V_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_const_boolean_1 = ap_condition_117)) then
                if ((ap_const_boolean_1 = ap_condition_121)) then 
                    esac_fsmState_V <= ap_const_lv1_0;
                elsif ((ap_const_boolean_1 = ap_condition_68)) then 
                    esac_fsmState_V <= ap_const_lv1_1;
                end if;
            end if; 
        end if;
    end process;

    ap_NS_fsm_assign_proc : process (ap_start, ap_done_reg, ap_CS_fsm, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
        case ap_CS_fsm is
            when ap_ST_fsm_state1 => 
                ap_NS_fsm <= ap_ST_fsm_state1;
            when others =>  
                ap_NS_fsm <= "X";
        end case;
    end process;
    ap_CS_fsm_state1 <= ap_CS_fsm(0);

    ap_block_state1_assign_proc : process(ap_start, ap_done_reg, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1)
    begin
                ap_block_state1 <= ((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)));
    end process;


    ap_block_state1_io_assign_proc : process(txMetaData_V_TREADY, m_axis_tx_data_TREADY, ap_predicate_op21_write_state1, ap_predicate_op33_write_state1)
    begin
                ap_block_state1_io <= (((m_axis_tx_data_TREADY = ap_const_logic_0) and (ap_predicate_op33_write_state1 = ap_const_boolean_1)) or ((txMetaData_V_TREADY = ap_const_logic_0) and (ap_predicate_op21_write_state1 = ap_const_boolean_1)));
    end process;


    ap_condition_117_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
                ap_condition_117 <= (not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1));
    end process;


    ap_condition_121_assign_proc : process(esac_fsmState_V, tmp_nbreadreq_fu_109_p5, tmp_5_nbwritereq_fu_121_p5, tmp_last_V_fu_185_p1)
    begin
                ap_condition_121 <= ((tmp_5_nbwritereq_fu_121_p5 = ap_const_lv1_1) and (tmp_nbreadreq_fu_109_p5 = ap_const_lv1_1) and (tmp_last_V_fu_185_p1 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_1));
    end process;


    ap_condition_68_assign_proc : process(esac_fsmState_V, tmp_4_nbreadreq_fu_66_p3, tmp_6_nbreadreq_fu_74_p3, tmp_7_nbwritereq_fu_82_p3)
    begin
                ap_condition_68 <= ((tmp_7_nbwritereq_fu_82_p3 = ap_const_lv1_1) and (tmp_6_nbreadreq_fu_74_p3 = ap_const_lv1_1) and (tmp_4_nbreadreq_fu_66_p3 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_0));
    end process;


    ap_done_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            ap_done <= ap_const_logic_1;
        else 
            ap_done <= ap_done_reg;
        end if; 
    end process;


    ap_idle_assign_proc : process(ap_start, ap_CS_fsm_state1)
    begin
        if (((ap_start = ap_const_logic_0) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            ap_idle <= ap_const_logic_1;
        else 
            ap_idle <= ap_const_logic_0;
        end if; 
    end process;


    ap_predicate_op18_read_state1_assign_proc : process(esac_fsmState_V, tmp_4_nbreadreq_fu_66_p3, tmp_6_nbreadreq_fu_74_p3, tmp_7_nbwritereq_fu_82_p3)
    begin
                ap_predicate_op18_read_state1 <= ((tmp_7_nbwritereq_fu_82_p3 = ap_const_lv1_1) and (tmp_6_nbreadreq_fu_74_p3 = ap_const_lv1_1) and (tmp_4_nbreadreq_fu_66_p3 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_0));
    end process;


    ap_predicate_op19_read_state1_assign_proc : process(esac_fsmState_V, tmp_4_nbreadreq_fu_66_p3, tmp_6_nbreadreq_fu_74_p3, tmp_7_nbwritereq_fu_82_p3)
    begin
                ap_predicate_op19_read_state1 <= ((tmp_7_nbwritereq_fu_82_p3 = ap_const_lv1_1) and (tmp_6_nbreadreq_fu_74_p3 = ap_const_lv1_1) and (tmp_4_nbreadreq_fu_66_p3 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_0));
    end process;


    ap_predicate_op21_write_state1_assign_proc : process(esac_fsmState_V, tmp_4_nbreadreq_fu_66_p3, tmp_6_nbreadreq_fu_74_p3, tmp_7_nbwritereq_fu_82_p3)
    begin
                ap_predicate_op21_write_state1 <= ((tmp_7_nbwritereq_fu_82_p3 = ap_const_lv1_1) and (tmp_6_nbreadreq_fu_74_p3 = ap_const_lv1_1) and (tmp_4_nbreadreq_fu_66_p3 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_0));
    end process;


    ap_predicate_op29_read_state1_assign_proc : process(esac_fsmState_V, tmp_nbreadreq_fu_109_p5, tmp_5_nbwritereq_fu_121_p5)
    begin
                ap_predicate_op29_read_state1 <= ((tmp_5_nbwritereq_fu_121_p5 = ap_const_lv1_1) and (tmp_nbreadreq_fu_109_p5 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_1));
    end process;


    ap_predicate_op33_write_state1_assign_proc : process(esac_fsmState_V, tmp_nbreadreq_fu_109_p5, tmp_5_nbwritereq_fu_121_p5)
    begin
                ap_predicate_op33_write_state1 <= ((tmp_5_nbwritereq_fu_121_p5 = ap_const_lv1_1) and (tmp_nbreadreq_fu_109_p5 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_1));
    end process;


    ap_ready_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            ap_ready <= ap_const_logic_1;
        else 
            ap_ready <= ap_const_logic_0;
        end if; 
    end process;


    esa_dataFifo_V_data_s_blk_n_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esac_fsmState_V, esa_dataFifo_V_data_s_empty_n, tmp_nbreadreq_fu_109_p5, tmp_5_nbwritereq_fu_121_p5)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1))) and (tmp_5_nbwritereq_fu_121_p5 = ap_const_lv1_1) and (tmp_nbreadreq_fu_109_p5 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            esa_dataFifo_V_data_s_blk_n <= esa_dataFifo_V_data_s_empty_n;
        else 
            esa_dataFifo_V_data_s_blk_n <= ap_const_logic_1;
        end if; 
    end process;


    esa_dataFifo_V_data_s_read_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1) and (ap_predicate_op29_read_state1 = ap_const_boolean_1))) then 
            esa_dataFifo_V_data_s_read <= ap_const_logic_1;
        else 
            esa_dataFifo_V_data_s_read <= ap_const_logic_0;
        end if; 
    end process;


    esa_dataFifo_V_keep_s_blk_n_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esac_fsmState_V, esa_dataFifo_V_keep_s_empty_n, tmp_nbreadreq_fu_109_p5, tmp_5_nbwritereq_fu_121_p5)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1))) and (tmp_5_nbwritereq_fu_121_p5 = ap_const_lv1_1) and (tmp_nbreadreq_fu_109_p5 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            esa_dataFifo_V_keep_s_blk_n <= esa_dataFifo_V_keep_s_empty_n;
        else 
            esa_dataFifo_V_keep_s_blk_n <= ap_const_logic_1;
        end if; 
    end process;


    esa_dataFifo_V_keep_s_read_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1) and (ap_predicate_op29_read_state1 = ap_const_boolean_1))) then 
            esa_dataFifo_V_keep_s_read <= ap_const_logic_1;
        else 
            esa_dataFifo_V_keep_s_read <= ap_const_logic_0;
        end if; 
    end process;


    esa_dataFifo_V_last_s_blk_n_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esac_fsmState_V, esa_dataFifo_V_last_s_empty_n, tmp_nbreadreq_fu_109_p5, tmp_5_nbwritereq_fu_121_p5)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1))) and (tmp_5_nbwritereq_fu_121_p5 = ap_const_lv1_1) and (tmp_nbreadreq_fu_109_p5 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            esa_dataFifo_V_last_s_blk_n <= esa_dataFifo_V_last_s_empty_n;
        else 
            esa_dataFifo_V_last_s_blk_n <= ap_const_logic_1;
        end if; 
    end process;


    esa_dataFifo_V_last_s_read_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1) and (ap_predicate_op29_read_state1 = ap_const_boolean_1))) then 
            esa_dataFifo_V_last_s_read <= ap_const_logic_1;
        else 
            esa_dataFifo_V_last_s_read <= ap_const_logic_0;
        end if; 
    end process;


    esa_lengthFifo_V_V_blk_n_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esac_fsmState_V, esa_lengthFifo_V_V_empty_n, tmp_4_nbreadreq_fu_66_p3, tmp_6_nbreadreq_fu_74_p3, tmp_7_nbwritereq_fu_82_p3)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1))) and (tmp_7_nbwritereq_fu_82_p3 = ap_const_lv1_1) and (tmp_6_nbreadreq_fu_74_p3 = ap_const_lv1_1) and (tmp_4_nbreadreq_fu_66_p3 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_0) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            esa_lengthFifo_V_V_blk_n <= esa_lengthFifo_V_V_empty_n;
        else 
            esa_lengthFifo_V_V_blk_n <= ap_const_logic_1;
        end if; 
    end process;


    esa_lengthFifo_V_V_read_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1) and (ap_predicate_op19_read_state1 = ap_const_boolean_1))) then 
            esa_lengthFifo_V_V_read <= ap_const_logic_1;
        else 
            esa_lengthFifo_V_V_read <= ap_const_logic_0;
        end if; 
    end process;


    esa_sessionidFifo_V_s_blk_n_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esac_fsmState_V, esa_sessionidFifo_V_s_empty_n, tmp_4_nbreadreq_fu_66_p3, tmp_6_nbreadreq_fu_74_p3, tmp_7_nbwritereq_fu_82_p3)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1))) and (tmp_7_nbwritereq_fu_82_p3 = ap_const_lv1_1) and (tmp_6_nbreadreq_fu_74_p3 = ap_const_lv1_1) and (tmp_4_nbreadreq_fu_66_p3 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_0) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            esa_sessionidFifo_V_s_blk_n <= esa_sessionidFifo_V_s_empty_n;
        else 
            esa_sessionidFifo_V_s_blk_n <= ap_const_logic_1;
        end if; 
    end process;


    esa_sessionidFifo_V_s_read_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1) and (ap_predicate_op18_read_state1 = ap_const_boolean_1))) then 
            esa_sessionidFifo_V_s_read <= ap_const_logic_1;
        else 
            esa_sessionidFifo_V_s_read <= ap_const_logic_0;
        end if; 
    end process;

    io_acc_block_signal_op29 <= (esa_dataFifo_V_last_s_empty_n and esa_dataFifo_V_keep_s_empty_n and esa_dataFifo_V_data_s_empty_n);
    m_axis_tx_data_TDATA <= esa_dataFifo_V_data_s_dout;

    m_axis_tx_data_TDATA_blk_n_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, m_axis_tx_data_TREADY, esac_fsmState_V, tmp_nbreadreq_fu_109_p5, tmp_5_nbwritereq_fu_121_p5)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1))) and (tmp_5_nbwritereq_fu_121_p5 = ap_const_lv1_1) and (tmp_nbreadreq_fu_109_p5 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            m_axis_tx_data_TDATA_blk_n <= m_axis_tx_data_TREADY;
        else 
            m_axis_tx_data_TDATA_blk_n <= ap_const_logic_1;
        end if; 
    end process;

    m_axis_tx_data_TKEEP <= esa_dataFifo_V_keep_s_dout;
    m_axis_tx_data_TLAST <= esa_dataFifo_V_last_s_dout;

    m_axis_tx_data_TVALID_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_predicate_op33_write_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1) and (ap_predicate_op33_write_state1 = ap_const_boolean_1))) then 
            m_axis_tx_data_TVALID <= ap_const_logic_1;
        else 
            m_axis_tx_data_TVALID <= ap_const_logic_0;
        end if; 
    end process;

    tmp_4_nbreadreq_fu_66_p3 <= (0=>(esa_sessionidFifo_V_s_empty_n), others=>'-');
    tmp_5_nbwritereq_fu_121_p5 <= (0=>m_axis_tx_data_TREADY, others=>'-');
    tmp_6_nbreadreq_fu_74_p3 <= (0=>(esa_lengthFifo_V_V_empty_n), others=>'-');
    tmp_7_nbwritereq_fu_82_p3 <= (0=>txMetaData_V_TREADY, others=>'-');
    tmp_last_V_fu_185_p1 <= esa_dataFifo_V_last_s_dout;
    tmp_nbreadreq_fu_109_p5 <= (0=>(esa_dataFifo_V_last_s_empty_n and esa_dataFifo_V_keep_s_empty_n and esa_dataFifo_V_data_s_empty_n), others=>'-');
    txMetaData_V_TDATA <= (esa_lengthFifo_V_V_dout & esa_sessionidFifo_V_s_dout);

    txMetaData_V_TDATA_blk_n_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, txMetaData_V_TREADY, esac_fsmState_V, tmp_4_nbreadreq_fu_66_p3, tmp_6_nbreadreq_fu_74_p3, tmp_7_nbwritereq_fu_82_p3)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1))) and (tmp_7_nbwritereq_fu_82_p3 = ap_const_lv1_1) and (tmp_6_nbreadreq_fu_74_p3 = ap_const_lv1_1) and (tmp_4_nbreadreq_fu_66_p3 = ap_const_lv1_1) and (esac_fsmState_V = ap_const_lv1_0) and (ap_const_logic_1 = ap_CS_fsm_state1))) then 
            txMetaData_V_TDATA_blk_n <= txMetaData_V_TREADY;
        else 
            txMetaData_V_TDATA_blk_n <= ap_const_logic_1;
        end if; 
    end process;


    txMetaData_V_TVALID_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_state1, esa_sessionidFifo_V_s_empty_n, esa_lengthFifo_V_V_empty_n, ap_predicate_op18_read_state1, ap_predicate_op19_read_state1, io_acc_block_signal_op29, ap_predicate_op29_read_state1, ap_predicate_op21_write_state1, ap_block_state1_io)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_const_boolean_1 = ap_block_state1_io) or (ap_done_reg = ap_const_logic_1) or ((io_acc_block_signal_op29 = ap_const_logic_0) and (ap_predicate_op29_read_state1 = ap_const_boolean_1)) or ((esa_lengthFifo_V_V_empty_n = ap_const_logic_0) and (ap_predicate_op19_read_state1 = ap_const_boolean_1)) or ((esa_sessionidFifo_V_s_empty_n = ap_const_logic_0) and (ap_predicate_op18_read_state1 = ap_const_boolean_1)))) and (ap_const_logic_1 = ap_CS_fsm_state1) and (ap_predicate_op21_write_state1 = ap_const_boolean_1))) then 
            txMetaData_V_TVALID <= ap_const_logic_1;
        else 
            txMetaData_V_TVALID <= ap_const_logic_0;
        end if; 
    end process;

end behav;
