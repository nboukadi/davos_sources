// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module rocev2_read_req_table (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        tx_readReqTable_upd_s_0_dout,
        tx_readReqTable_upd_s_0_empty_n,
        tx_readReqTable_upd_s_0_read,
        rx_readReqTable_upd_3_dout,
        rx_readReqTable_upd_3_empty_n,
        rx_readReqTable_upd_3_read,
        rx_readReqTable_upd_1_din,
        rx_readReqTable_upd_1_full_n,
        rx_readReqTable_upd_1_write
);

parameter    ap_ST_fsm_pp0_stage0 = 1'd1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
input  [39:0] tx_readReqTable_upd_s_0_dout;
input   tx_readReqTable_upd_s_0_empty_n;
output   tx_readReqTable_upd_s_0_read;
input  [40:0] rx_readReqTable_upd_3_dout;
input   rx_readReqTable_upd_3_empty_n;
output   rx_readReqTable_upd_3_read;
output  [24:0] rx_readReqTable_upd_1_din;
input   rx_readReqTable_upd_1_full_n;
output   rx_readReqTable_upd_1_write;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg tx_readReqTable_upd_s_0_read;
reg rx_readReqTable_upd_3_read;
reg rx_readReqTable_upd_1_write;

reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [0:0] ap_CS_fsm;
wire    ap_CS_fsm_pp0_stage0;
wire    ap_enable_reg_pp0_iter0;
reg    ap_enable_reg_pp0_iter1;
reg    ap_enable_reg_pp0_iter2;
reg    ap_enable_reg_pp0_iter3;
reg    ap_idle_pp0;
wire   [0:0] tmp_nbreadreq_fu_58_p3;
reg    ap_block_state1_pp0_stage0_iter0;
reg   [0:0] tmp_reg_196;
wire   [0:0] tmp_42_nbreadreq_fu_72_p3;
reg    ap_predicate_op12_read_state2;
reg    ap_block_state2_pp0_stage0_iter1;
wire    ap_block_state3_pp0_stage0_iter2;
reg   [0:0] tmp_reg_196_pp0_iter2_reg;
reg   [0:0] tmp_42_reg_210;
reg   [0:0] tmp_42_reg_210_pp0_iter2_reg;
reg   [0:0] tmp_43_reg_214;
reg   [0:0] tmp_43_reg_214_pp0_iter2_reg;
reg    ap_predicate_op35_write_state4;
reg    ap_block_state4_pp0_stage0_iter3;
reg    ap_block_pp0_stage0_11001;
wire   [8:0] req_table_max_fwd_re_address0;
reg    req_table_max_fwd_re_ce0;
wire   [23:0] req_table_max_fwd_re_q0;
wire   [8:0] req_table_max_fwd_re_address1;
reg    req_table_max_fwd_re_ce1;
reg    req_table_max_fwd_re_we1;
wire   [8:0] req_table_oldest_out_address0;
reg    req_table_oldest_out_ce0;
wire   [23:0] req_table_oldest_out_q0;
wire   [8:0] req_table_oldest_out_address1;
reg    req_table_oldest_out_ce1;
reg    req_table_oldest_out_we1;
wire   [23:0] req_table_oldest_out_d1;
reg    tx_readReqTable_upd_s_0_blk_n;
wire    ap_block_pp0_stage0;
reg    rx_readReqTable_upd_3_blk_n;
reg    rx_readReqTable_upd_1_blk_n;
reg   [0:0] tmp_reg_196_pp0_iter1_reg;
wire   [15:0] trunc_ln321_fu_136_p1;
reg   [15:0] trunc_ln321_reg_200;
reg   [23:0] tmp_max_fwd_readreq_s_reg_205;
wire   [0:0] tmp_43_fu_165_p3;
reg   [23:0] tmp_oldest_outstandi_1_reg_228;
wire   [0:0] valid_fu_183_p2;
reg   [0:0] valid_reg_233;
reg    ap_block_pp0_stage0_subdone;
wire   [63:0] zext_ln544_6_fu_173_p1;
wire   [63:0] zext_ln544_fu_179_p1;
reg    ap_block_pp0_stage0_01001;
wire   [15:0] trunc_ln321_4_fu_150_p1;
reg   [0:0] ap_NS_fsm;
reg    ap_idle_pp0_0to2;
reg    ap_reset_idle_pp0;
wire    ap_enable_pp0;

// power-on initialization
initial begin
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 1'd1;
#0 ap_enable_reg_pp0_iter1 = 1'b0;
#0 ap_enable_reg_pp0_iter2 = 1'b0;
#0 ap_enable_reg_pp0_iter3 = 1'b0;
end

rocev2_state_table_state_table_req_old_1 #(
    .DataWidth( 24 ),
    .AddressRange( 500 ),
    .AddressWidth( 9 ))
req_table_max_fwd_re_U(
    .clk(ap_clk),
    .reset(ap_rst),
    .address0(req_table_max_fwd_re_address0),
    .ce0(req_table_max_fwd_re_ce0),
    .q0(req_table_max_fwd_re_q0),
    .address1(req_table_max_fwd_re_address1),
    .ce1(req_table_max_fwd_re_ce1),
    .we1(req_table_max_fwd_re_we1),
    .d1(tmp_max_fwd_readreq_s_reg_205)
);

rocev2_state_table_state_table_req_old_1 #(
    .DataWidth( 24 ),
    .AddressRange( 500 ),
    .AddressWidth( 9 ))
req_table_oldest_out_U(
    .clk(ap_clk),
    .reset(ap_rst),
    .address0(req_table_oldest_out_address0),
    .ce0(req_table_oldest_out_ce0),
    .q0(req_table_oldest_out_q0),
    .address1(req_table_oldest_out_address1),
    .ce1(req_table_oldest_out_ce1),
    .we1(req_table_oldest_out_we1),
    .d1(req_table_oldest_out_d1)
);

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_pp0_stage0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((ap_continue == 1'b1)) begin
            ap_done_reg <= 1'b0;
        end else if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter3 == 1'b1))) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter1 <= 1'b0;
    end else begin
        if (((1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0_subdone))) begin
            ap_enable_reg_pp0_iter1 <= ap_start;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter2 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter2 <= ap_enable_reg_pp0_iter1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter3 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter3 <= ap_enable_reg_pp0_iter2;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_196 == 1'd0) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        tmp_42_reg_210 <= tmp_42_nbreadreq_fu_72_p3;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b0 == ap_block_pp0_stage0_11001)) begin
        tmp_42_reg_210_pp0_iter2_reg <= tmp_42_reg_210;
        tmp_43_reg_214_pp0_iter2_reg <= tmp_43_reg_214;
        tmp_reg_196_pp0_iter2_reg <= tmp_reg_196_pp0_iter1_reg;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_196 == 1'd0) & (tmp_42_nbreadreq_fu_72_p3 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        tmp_43_reg_214 <= rx_readReqTable_upd_3_dout[32'd40];
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_nbreadreq_fu_58_p3 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        tmp_max_fwd_readreq_s_reg_205 <= {{tx_readReqTable_upd_s_0_dout[39:16]}};
        trunc_ln321_reg_200 <= trunc_ln321_fu_136_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_43_reg_214 == 1'd0) & (tmp_reg_196_pp0_iter1_reg == 1'd0) & (tmp_42_reg_210 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        tmp_oldest_outstandi_1_reg_228 <= req_table_oldest_out_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        tmp_reg_196 <= tmp_nbreadreq_fu_58_p3;
        tmp_reg_196_pp0_iter1_reg <= tmp_reg_196;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_43_reg_214 == 1'd0) & (tmp_reg_196_pp0_iter1_reg == 1'd0) & (tmp_42_reg_210 == 1'd1))) begin
        valid_reg_233 <= valid_fu_183_p2;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter3 == 1'b1))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = ap_done_reg;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter3 == 1'b0) & (ap_enable_reg_pp0_iter2 == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0 = 1'b1;
    end else begin
        ap_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter2 == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0_0to2 = 1'b1;
    end else begin
        ap_idle_pp0_0to2 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0_0to2 == 1'b1))) begin
        ap_reset_idle_pp0 = 1'b1;
    end else begin
        ap_reset_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        req_table_max_fwd_re_ce0 = 1'b1;
    end else begin
        req_table_max_fwd_re_ce0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        req_table_max_fwd_re_ce1 = 1'b1;
    end else begin
        req_table_max_fwd_re_ce1 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_196 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        req_table_max_fwd_re_we1 = 1'b1;
    end else begin
        req_table_max_fwd_re_we1 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        req_table_oldest_out_ce0 = 1'b1;
    end else begin
        req_table_oldest_out_ce0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        req_table_oldest_out_ce1 = 1'b1;
    end else begin
        req_table_oldest_out_ce1 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_reg_196 == 1'd0) & (tmp_42_nbreadreq_fu_72_p3 == 1'd1) & (tmp_43_fu_165_p3 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        req_table_oldest_out_we1 = 1'b1;
    end else begin
        req_table_oldest_out_we1 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter3 == 1'b1) & (ap_predicate_op35_write_state4 == 1'b1) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_readReqTable_upd_1_blk_n = rx_readReqTable_upd_1_full_n;
    end else begin
        rx_readReqTable_upd_1_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter3 == 1'b1) & (ap_predicate_op35_write_state4 == 1'b1))) begin
        rx_readReqTable_upd_1_write = 1'b1;
    end else begin
        rx_readReqTable_upd_1_write = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op12_read_state2 == 1'b1) & (1'b0 == ap_block_pp0_stage0))) begin
        rx_readReqTable_upd_3_blk_n = rx_readReqTable_upd_3_empty_n;
    end else begin
        rx_readReqTable_upd_3_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_predicate_op12_read_state2 == 1'b1))) begin
        rx_readReqTable_upd_3_read = 1'b1;
    end else begin
        rx_readReqTable_upd_3_read = 1'b0;
    end
end

always @ (*) begin
    if ((~((ap_start == 1'b0) | (ap_done_reg == 1'b1)) & (tmp_nbreadreq_fu_58_p3 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (1'b0 == ap_block_pp0_stage0))) begin
        tx_readReqTable_upd_s_0_blk_n = tx_readReqTable_upd_s_0_empty_n;
    end else begin
        tx_readReqTable_upd_s_0_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_nbreadreq_fu_58_p3 == 1'd1) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        tx_readReqTable_upd_s_0_read = 1'b1;
    end else begin
        tx_readReqTable_upd_s_0_read = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_pp0_stage0 : begin
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign ap_CS_fsm_pp0_stage0 = ap_CS_fsm[32'd0];

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_pp0_stage0_01001 = ((ap_done_reg == 1'b1) | ((rx_readReqTable_upd_1_full_n == 1'b0) & (ap_enable_reg_pp0_iter3 == 1'b1) & (ap_predicate_op35_write_state4 == 1'b1)) | ((rx_readReqTable_upd_3_empty_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1) & (ap_predicate_op12_read_state2 == 1'b1)) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_58_p3 == 1'd1) & (tx_readReqTable_upd_s_0_empty_n == 1'b0)))));
end

always @ (*) begin
    ap_block_pp0_stage0_11001 = ((ap_done_reg == 1'b1) | ((rx_readReqTable_upd_1_full_n == 1'b0) & (ap_enable_reg_pp0_iter3 == 1'b1) & (ap_predicate_op35_write_state4 == 1'b1)) | ((rx_readReqTable_upd_3_empty_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1) & (ap_predicate_op12_read_state2 == 1'b1)) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_58_p3 == 1'd1) & (tx_readReqTable_upd_s_0_empty_n == 1'b0)))));
end

always @ (*) begin
    ap_block_pp0_stage0_subdone = ((ap_done_reg == 1'b1) | ((rx_readReqTable_upd_1_full_n == 1'b0) & (ap_enable_reg_pp0_iter3 == 1'b1) & (ap_predicate_op35_write_state4 == 1'b1)) | ((rx_readReqTable_upd_3_empty_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1) & (ap_predicate_op12_read_state2 == 1'b1)) | ((ap_start == 1'b1) & ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_58_p3 == 1'd1) & (tx_readReqTable_upd_s_0_empty_n == 1'b0)))));
end

always @ (*) begin
    ap_block_state1_pp0_stage0_iter0 = ((ap_start == 1'b0) | (ap_done_reg == 1'b1) | ((tmp_nbreadreq_fu_58_p3 == 1'd1) & (tx_readReqTable_upd_s_0_empty_n == 1'b0)));
end

always @ (*) begin
    ap_block_state2_pp0_stage0_iter1 = ((rx_readReqTable_upd_3_empty_n == 1'b0) & (ap_predicate_op12_read_state2 == 1'b1));
end

assign ap_block_state3_pp0_stage0_iter2 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_state4_pp0_stage0_iter3 = ((rx_readReqTable_upd_1_full_n == 1'b0) & (ap_predicate_op35_write_state4 == 1'b1));
end

assign ap_enable_pp0 = (ap_idle_pp0 ^ 1'b1);

assign ap_enable_reg_pp0_iter0 = ap_start;

always @ (*) begin
    ap_predicate_op12_read_state2 = ((tmp_reg_196 == 1'd0) & (tmp_42_nbreadreq_fu_72_p3 == 1'd1));
end

always @ (*) begin
    ap_predicate_op35_write_state4 = ((tmp_43_reg_214_pp0_iter2_reg == 1'd0) & (tmp_reg_196_pp0_iter2_reg == 1'd0) & (tmp_42_reg_210_pp0_iter2_reg == 1'd1));
end

assign req_table_max_fwd_re_address0 = zext_ln544_6_fu_173_p1;

assign req_table_max_fwd_re_address1 = zext_ln544_fu_179_p1;

assign req_table_oldest_out_address0 = zext_ln544_6_fu_173_p1;

assign req_table_oldest_out_address1 = zext_ln544_6_fu_173_p1;

assign req_table_oldest_out_d1 = {{rx_readReqTable_upd_3_dout[39:16]}};

assign rx_readReqTable_upd_1_din = {{valid_reg_233}, {tmp_oldest_outstandi_1_reg_228}};

assign tmp_42_nbreadreq_fu_72_p3 = rx_readReqTable_upd_3_empty_n;

assign tmp_43_fu_165_p3 = rx_readReqTable_upd_3_dout[32'd40];

assign tmp_nbreadreq_fu_58_p3 = tx_readReqTable_upd_s_0_empty_n;

assign trunc_ln321_4_fu_150_p1 = rx_readReqTable_upd_3_dout[15:0];

assign trunc_ln321_fu_136_p1 = tx_readReqTable_upd_s_0_dout[15:0];

assign valid_fu_183_p2 = ((req_table_oldest_out_q0 < req_table_max_fwd_re_q0) ? 1'b1 : 1'b0);

assign zext_ln544_6_fu_173_p1 = trunc_ln321_4_fu_150_p1;

assign zext_ln544_fu_179_p1 = trunc_ln321_reg_200;

endmodule //rocev2_read_req_table
