# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "AXI_ID_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ENABLE_DDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NUM_DDR_CHANNELS" -parent ${Page_0}


}

proc update_PARAM_VALUE.AXI_ID_WIDTH { PARAM_VALUE.AXI_ID_WIDTH } {
	# Procedure called to update AXI_ID_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AXI_ID_WIDTH { PARAM_VALUE.AXI_ID_WIDTH } {
	# Procedure called to validate AXI_ID_WIDTH
	return true
}

proc update_PARAM_VALUE.ENABLE_DDR { PARAM_VALUE.ENABLE_DDR } {
	# Procedure called to update ENABLE_DDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ENABLE_DDR { PARAM_VALUE.ENABLE_DDR } {
	# Procedure called to validate ENABLE_DDR
	return true
}

proc update_PARAM_VALUE.NUM_DDR_CHANNELS { PARAM_VALUE.NUM_DDR_CHANNELS } {
	# Procedure called to update NUM_DDR_CHANNELS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_DDR_CHANNELS { PARAM_VALUE.NUM_DDR_CHANNELS } {
	# Procedure called to validate NUM_DDR_CHANNELS
	return true
}


proc update_MODELPARAM_VALUE.AXI_ID_WIDTH { MODELPARAM_VALUE.AXI_ID_WIDTH PARAM_VALUE.AXI_ID_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AXI_ID_WIDTH}] ${MODELPARAM_VALUE.AXI_ID_WIDTH}
}

proc update_MODELPARAM_VALUE.NUM_DDR_CHANNELS { MODELPARAM_VALUE.NUM_DDR_CHANNELS PARAM_VALUE.NUM_DDR_CHANNELS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_DDR_CHANNELS}] ${MODELPARAM_VALUE.NUM_DDR_CHANNELS}
}

proc update_MODELPARAM_VALUE.ENABLE_DDR { MODELPARAM_VALUE.ENABLE_DDR PARAM_VALUE.ENABLE_DDR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ENABLE_DDR}] ${MODELPARAM_VALUE.ENABLE_DDR}
}

