
module os_0 (
  input wire pcie_clk,
  input wire pcie_aresetn,
  
  input wire [0 : 0] mem_clk,
  input wire [0 : 0] mem_aresetn,
  
  input wire net_clk,
  input wire net_aresetn,
  
  output wire user_clk,
  output wire user_aresetn,
  
  input wire [31 : 0] s_axil_control_awaddr,
  input wire s_axil_control_awvalid,
  output wire s_axil_control_awready,
  input wire [31 : 0] s_axil_control_wdata,
  input wire [3 : 0] s_axil_control_wstrb,
  input wire s_axil_control_wvalid,
  output wire s_axil_control_wready,
  output wire [1 : 0] s_axil_control_bresp,
  output wire s_axil_control_bvalid,
  input wire s_axil_control_bready,
  input wire [31 : 0] s_axil_control_araddr,
  input wire s_axil_control_arvalid,
  output wire s_axil_control_arready,
  output wire [31 : 0] s_axil_control_rdata,
  output wire [1 : 0] s_axil_control_rresp,
  output wire s_axil_control_rvalid,
  input wire s_axil_control_rready,
  
  input wire [3 : 0] s_axi_control_awid,
  input wire [7 : 0] s_axi_control_awlen,
  input wire [2 : 0] s_axi_control_awsize,
  input wire [1 : 0] s_axi_control_awburst,
  input wire [3 : 0] s_axi_control_awcache,
  input wire [2 : 0] s_axi_control_awprot,
  input wire [63 : 0] s_axi_control_awaddr,
  input wire s_axi_control_awlock,
  input wire s_axi_control_awvalid,
  output wire s_axi_control_awready,
  input wire [511 : 0] s_axi_control_wdata,
  input wire [63 : 0] s_axi_control_wstrb,
  input wire s_axi_control_wlast,
  input wire s_axi_control_wvalid,
  output wire s_axi_control_wready,
  output wire [3 : 0] s_axi_control_bid,
  output wire [1 : 0] s_axi_control_bresp,
  output wire s_axi_control_bvalid,
  input wire s_axi_control_bready,
  input wire [3 : 0] s_axi_control_arid,
  input wire [63 : 0] s_axi_control_araddr,
  input wire [7 : 0] s_axi_control_arlen,
  input wire [2 : 0] s_axi_control_arsize,
  input wire [1 : 0] s_axi_control_arburst,
  input wire [3 : 0] s_axi_control_arcache,
  input wire [2 : 0] s_axi_control_arprot,
  input wire s_axi_control_arlock,
  input wire s_axi_control_arvalid,
  output wire s_axi_control_arready,
  output wire [3 : 0] s_axi_control_rid,
  output wire [1 : 0] s_axi_control_rresp,
  output wire [511 : 0] s_axi_control_rdata,
  output wire s_axi_control_rvalid,
  output wire s_axi_control_rlast,
  input wire s_axi_control_rready,
  
  input wire ddr_calib_complete,
  
  
  
  output wire [0 : 0] m_axi_awid,
  output wire [31 : 0] m_axi_awaddr,
  
  output wire [7 : 0] m_axi_awlen,
  output wire [2 : 0] m_axi_awsize,
  output wire [1 : 0] m_axi_awburst,
  output wire [0 : 0] m_axi_awlock,
  output wire [3 : 0] m_axi_awcache,
  output wire [2 : 0] m_axi_awprot,
  output wire [0 : 0] m_axi_awvalid,
  input wire [0 : 0] m_axi_awready,
  output wire [511 : 0] m_axi_wdata,
  output wire [63 : 0] m_axi_wstrb,
  output wire [0 : 0] m_axi_wlast,
  output wire [0 : 0] m_axi_wvalid,
  input wire [0 : 0] m_axi_wready,
  output wire [0 : 0] m_axi_bready,
  input wire [0 : 0] m_axi_bid,
  input wire [1 : 0] m_axi_bresp,
  input wire [0 : 0] m_axi_bvalid,
  output wire [0 : 0] m_axi_arid,
  output wire [31 : 0] m_axi_araddr,
  output wire [7 : 0] m_axi_arlen,
  output wire [2 : 0] m_axi_arsize,
  output wire [1 : 0] m_axi_arburst,
  output wire [0 : 0] m_axi_arlock,
  output wire [3 : 0] m_axi_arcache,
  output wire [2 : 0] m_axi_arprot,
  output wire [0 : 0] m_axi_arvalid,
  input wire [0 : 0] m_axi_arready,
  output wire [0 : 0] m_axi_rready,
  input wire [0 : 0] m_axi_rid,
  input wire [511 : 0] m_axi_rdata,
  input wire [1 : 0] m_axi_rresp,
  input wire [0 : 0] m_axi_rlast,
  input wire [0 : 0] m_axi_rvalid,
  output wire m_axis_dma_c2h_valid,
  input wire m_axis_dma_c2h_ready,
  output wire [511 : 0] m_axis_dma_c2h_data,
  output wire [63 : 0] m_axis_dma_c2h_keep,
  output wire m_axis_dma_c2h_last,
  output wire m_axis_dma_c2h_dest,
  input wire s_axis_dma_h2c_valid,
  output wire s_axis_dma_h2c_ready,
  input wire [511 : 0] s_axis_dma_h2c_data,
  input wire [63 : 0] s_axis_dma_h2c_keep,
  input wire s_axis_dma_h2c_last,
  input wire c2h_dsc_byp_ready_0,
  output wire [63 : 0] c2h_dsc_byp_addr_0,
  output wire [31 : 0] c2h_dsc_byp_len_0,
  output wire c2h_dsc_byp_load_0,
  input wire h2c_dsc_byp_ready_0,
  output wire [63 : 0] h2c_dsc_byp_addr_0,
  output wire [31 : 0] h2c_dsc_byp_len_0,
  output wire h2c_dsc_byp_load_0,
  input wire [7 : 0] c2h_sts_0,
  input wire [7 : 0] h2c_sts_0,
  
  input wire s_axis_net_rx_valid,
  output wire s_axis_net_rx_ready,
  input wire [511 : 0] s_axis_net_rx_data,
  input wire [63 : 0] s_axis_net_rx_keep,
  input wire s_axis_net_rx_last,
  
  output wire m_axis_net_tx_valid,
  input wire m_axis_net_tx_ready,
  output wire [511 : 0] m_axis_net_tx_data,
  output wire [63 : 0] m_axis_net_tx_keep,
  output wire m_axis_net_tx_last,
  output wire m_axis_net_tx_dest,
  
  input wire s_axis_qp_interface_valid,
  output wire s_axis_qp_interface_ready,
  input wire [143 : 0] s_axis_qp_interface_data,
  
  input wire s_axis_qp_conn_interface_valid,
  output wire s_axis_qp_conn_interface_ready,
  input wire [183 : 0] s_axis_qp_conn_interface_data,
  
  input wire s_axis_roce_role_tx_meta_valid,
  output wire s_axis_roce_role_tx_meta_ready,
  input wire [159 : 0] s_axis_roce_role_tx_meta_data,
  
  input wire s_axis_roce_role_tx_data_valid,
  output wire s_axis_roce_role_tx_data_ready,
  input wire [511 : 0] s_axis_roce_role_tx_data_data,
  input wire [63 : 0] s_axis_roce_role_tx_data_keep,
  input wire s_axis_roce_role_tx_data_last
);




wire  [0:0] m_axi_awid_unpacked [0:0];
assign {>>{m_axi_awid}} = m_axi_awid_unpacked;
wire  [31:0] m_axi_awaddr_unpacked [0:0];
assign {>>{m_axi_awaddr}} = m_axi_awaddr_unpacked;
wire  [7:0] m_axi_awlen_unpacked [0:0];
assign {>>{m_axi_awlen}} = m_axi_awlen_unpacked;
wire  [2:0] m_axi_awsize_unpacked [0:0];
assign {>>{m_axi_awsize}} = m_axi_awsize_unpacked;
wire  [1:0] m_axi_awburst_unpacked [0:0];
assign {>>{m_axi_awburst}} = m_axi_awburst_unpacked;
wire  [0:0] m_axi_awlock_unpacked [0:0];
assign {>>{m_axi_awlock}} = m_axi_awlock_unpacked;
wire  [3:0] m_axi_awcache_unpacked [0:0];
assign {>>{m_axi_awcache}} = m_axi_awcache_unpacked;
wire  [2:0] m_axi_awprot_unpacked [0:0];
assign {>>{m_axi_awprot}} = m_axi_awprot_unpacked;
wire  [511:0] m_axi_wdata_unpacked [0:0];
assign {>>{m_axi_wdata}} = m_axi_wdata_unpacked;
wire  [63:0] m_axi_wstrb_unpacked [0:0];
assign {>>{m_axi_wstrb}} = m_axi_wstrb_unpacked;
wire  [0:0] m_axi_bid_unpacked [0:0];
assign {>>{m_axi_bid_unpacked}} = m_axi_bid;
wire  [1:0] m_axi_bresp_unpacked [0:0];
assign {>>{m_axi_bresp_unpacked}} = m_axi_bresp;
wire  [0:0] m_axi_arid_unpacked [0:0];
assign {>>{m_axi_arid}} = m_axi_arid_unpacked;
wire  [31:0] m_axi_araddr_unpacked [0:0];
assign {>>{m_axi_araddr}} = m_axi_araddr_unpacked;
wire  [7:0] m_axi_arlen_unpacked [0:0];
assign {>>{m_axi_arlen}} = m_axi_arlen_unpacked;
wire  [2:0] m_axi_arsize_unpacked [0:0];
assign {>>{m_axi_arsize}} = m_axi_arsize_unpacked;
wire  [1:0] m_axi_arburst_unpacked [0:0];
assign {>>{m_axi_arburst}} = m_axi_arburst_unpacked;
wire  [0:0] m_axi_arlock_unpacked [0:0];
assign {>>{m_axi_arlock}} = m_axi_arlock_unpacked;
wire  [3:0] m_axi_arcache_unpacked [0:0];
assign {>>{m_axi_arcache}} = m_axi_arcache_unpacked;
wire  [2:0] m_axi_arprot_unpacked [0:0];
assign {>>{m_axi_arprot}} = m_axi_arprot_unpacked;
wire  [0:0] m_axi_rid_unpacked [0:0];
assign {>>{m_axi_rid_unpacked}} = m_axi_rid;
wire  [511:0] m_axi_rdata_unpacked [0:0];
assign {>>{m_axi_rdata_unpacked}} = m_axi_rdata;
wire  [1:0] m_axi_rresp_unpacked [0:0];
assign {>>{m_axi_rresp_unpacked}} = m_axi_rresp;



axi_mm s_axi_control();
assign s_axi_control.awid = s_axi_control_awid;
assign s_axi_control.awlen = s_axi_control_awlen;
assign s_axi_control.awsize = s_axi_control_awsize;
assign s_axi_control.awburst = s_axi_control_awburst;
assign s_axi_control.awcache = s_axi_control_awcache;
assign s_axi_control.awprot = s_axi_control_awprot;
assign s_axi_control.awaddr = s_axi_control_awaddr;
assign s_axi_control.awlock = s_axi_control_awlock;
assign s_axi_control.awvalid = s_axi_control_awvalid;
assign s_axi_control_awready = s_axi_control.awready;
assign s_axi_control.wdata = s_axi_control_wdata;
assign s_axi_control.wstrb = s_axi_control_wstrb;
assign s_axi_control.wlast = s_axi_control_wlast;
assign s_axi_control.wvalid = s_axi_control_wvalid;
assign s_axi_control_wready = s_axi_control.wready;
assign s_axi_control_bid = s_axi_control.bid;
assign s_axi_control_bresp = s_axi_control.bresp;
assign s_axi_control_bvalid = s_axi_control.bvalid;
assign s_axi_control.bready = s_axi_control_bready;
assign s_axi_control.arid = s_axi_control_arid;
assign s_axi_control.araddr = s_axi_control_araddr;
assign s_axi_control.arlen = s_axi_control_arlen;
assign s_axi_control.arsize = s_axi_control_arsize;
assign s_axi_control.arburst = s_axi_control_arburst;
assign s_axi_control.arcache = s_axi_control_arcache;
assign s_axi_control.arprot = s_axi_control_arprot;
assign s_axi_control.arlock = s_axi_control_arlock;
assign s_axi_control.arvalid = s_axi_control_arvalid;
assign s_axi_control_arready = s_axi_control.arready;
assign s_axi_control_rid = s_axi_control.rid;
assign s_axi_control_rresp = s_axi_control.rresp;
assign s_axi_control_rdata = s_axi_control.rdata;
assign s_axi_control_rvalid = s_axi_control.rvalid;
assign s_axi_control_rlast = s_axi_control.rlast;
assign s_axi_control.rready = s_axi_control_rready;

axi_lite s_axil_control();
assign s_axil_control.awaddr = s_axil_control_awaddr;
assign s_axil_control.awvalid = s_axil_control_awvalid;
assign s_axil_control_awready = s_axil_control.awready;
assign s_axil_control.wdata = s_axil_control_wdata;
assign s_axil_control.wstrb = s_axil_control_wstrb;
assign s_axil_control.wvalid = s_axil_control_wvalid;
assign s_axil_control_wready = s_axil_control.wready;
assign s_axil_control_bresp = s_axil_control.bresp;
assign s_axil_control_bvalid = s_axil_control.bvalid;
assign s_axil_control.bready = s_axil_control_bready;
assign s_axil_control.araddr = s_axil_control_araddr;
assign s_axil_control.arvalid = s_axil_control_arvalid;
assign s_axil_control_arready = s_axil_control.arready;
assign s_axil_control_rdata = s_axil_control.rdata;
assign s_axil_control_rresp = s_axil_control.rresp;
assign s_axil_control_rvalid = s_axil_control.rvalid;
assign s_axil_control.rready = s_axil_control_rready;

axi_stream m_axis_dma_c2h();
assign m_axis_dma_c2h_valid = m_axis_dma_c2h.valid;
assign m_axis_dma_c2h_data = m_axis_dma_c2h.data;
assign m_axis_dma_c2h_last = m_axis_dma_c2h.last;
assign m_axis_dma_c2h.ready = m_axis_dma_c2h_ready;
assign m_axis_dma_c2h_keep = m_axis_dma_c2h.keep;
assign m_axis_dma_c2h_dest = m_axis_dma_c2h.dest;

axi_stream #(512) s_axis_dma_h2c();
assign s_axis_dma_h2c.valid = s_axis_dma_h2c_valid;
assign s_axis_dma_h2c_ready = s_axis_dma_h2c.ready;
assign s_axis_dma_h2c.data = s_axis_dma_h2c_data;
assign s_axis_dma_h2c.keep = s_axis_dma_h2c_keep;
assign s_axis_dma_h2c.last = s_axis_dma_h2c_last;

axi_stream #(512) s_axis_net_rx();
assign s_axis_net_rx.valid = s_axis_net_rx_valid;
assign s_axis_net_rx_ready = s_axis_net_rx.ready;
assign s_axis_net_rx.data = s_axis_net_rx_data;
assign s_axis_net_rx.keep = s_axis_net_rx_keep;
assign s_axis_net_rx.last = s_axis_net_rx_last;

axi_stream #(512) m_axis_net_tx();
assign m_axis_net_tx_valid = m_axis_net_tx.valid;
assign m_axis_net_tx.ready = m_axis_net_tx_ready;
assign m_axis_net_tx_data = m_axis_net_tx.data;
assign m_axis_net_tx_keep = m_axis_net_tx.keep;
assign m_axis_net_tx_last = m_axis_net_tx.last;
assign m_axis_net_tx_dest = m_axis_net_tx.dest;

axis_meta #(144) s_axis_qp_interface();
assign s_axis_qp_interface.valid = s_axis_qp_interface_valid;
assign s_axis_qp_interface_ready = s_axis_qp_interface.ready;
assign s_axis_qp_interface.data = s_axis_qp_interface_data;

axis_meta #(184) s_axis_qp_conn_interface();
assign s_axis_qp_conn_interface.valid = s_axis_qp_conn_interface_valid;
assign s_axis_qp_conn_interface_ready = s_axis_qp_conn_interface.ready;
assign s_axis_qp_conn_interface.data = s_axis_qp_conn_interface_data;

axis_meta #(.WIDTH(160))    s_axis_roce_role_tx_meta();
assign s_axis_roce_role_tx_meta.valid = s_axis_roce_role_tx_meta_valid;
assign s_axis_roce_role_tx_meta_ready = s_axis_roce_role_tx_meta.ready;
assign s_axis_roce_role_tx_meta.data = s_axis_roce_role_tx_meta_data;

axi_stream #(512) s_axis_roce_role_tx_data();
assign s_axis_roce_role_tx_data_valid = s_axis_roce_role_tx_data.valid;
assign s_axis_roce_role_tx_data.ready = s_axis_roce_role_tx_data_ready;
assign s_axis_roce_role_tx_data_data = s_axis_roce_role_tx_data.data;
assign s_axis_roce_role_tx_data_keep = s_axis_roce_role_tx_data.keep;
assign s_axis_roce_role_tx_data_last = s_axis_roce_role_tx_data.last;

///*
// * DMA Driver
// */
//dma_driver dma_driver_inst (
//  .sys_clk(pcie_ref_clk),                                              // input wire sys_clk
//  .sys_clk_gt(pcie_ref_clk_gt),
//  .sys_rst_n(perst_n),                                          // input wire sys_rst_n
//  .user_lnk_up(pcie_lnk_up),                                      // output wire user_lnk_up
//  .pcie_tx_p(pcie_tx_p),                                      // output wire [7 : 0] pci_exp_txp
//  .pcie_tx_n(pcie_tx_n),                                      // output wire [7 : 0] pci_exp_txn
//  .pcie_rx_p(pcie_rx_p),                                      // input wire [7 : 0] pci_exp_rxp
//  .pcie_rx_n(pcie_rx_n),                                      // input wire [7 : 0] pci_exp_rxn
//  .pcie_clk(pcie_clk),                                            // output wire axi_aclk
//  .pcie_aresetn(pcie_aresetn),                                      // output wire axi_aresetn
//  //.usr_irq_req(1'b0),                                      // input wire [0 : 0] usr_irq_req
//  //.usr_irq_ack(),                                      // output wire [0 : 0] usr_irq_ack
//  //.msi_enable(),                                        // output wire msi_enable
//  //.msi_vector_width(),                            // output wire [2 : 0] msi_vector_width
  
// // Axi Lite Control Master interface   
//  .m_axil(s_axil_control),
//  // AXI MM Control Interface 
//  .m_axim(s_axi_control),
////  .pcie_tx_p(pcie_tx_p),                                      // output wire [7 : 0] pci_exp_txp

//  // AXI Stream Interface
//  .s_axis_c2h_data(m_axis_dma_c2h),
//  .m_axis_h2c_data(s_axis_dma_h2c),

//  // Descriptor Bypass
//  .c2h_dsc_byp_ready_0    (c2h_dsc_byp_ready_0),
//  //.c2h_dsc_byp_src_addr_0 (64'h0),
//  .c2h_dsc_byp_addr_0     (c2h_dsc_byp_addr_0),
//  .c2h_dsc_byp_len_0      (c2h_dsc_byp_len_0),
//  //.c2h_dsc_byp_ctl_0      (16'h13), //was 16'h3
//  .c2h_dsc_byp_load_0     (c2h_dsc_byp_load_0),
  
//  .h2c_dsc_byp_ready_0    (h2c_dsc_byp_ready_0),
//  .h2c_dsc_byp_addr_0     (h2c_dsc_byp_addr_0),
//  //.h2c_dsc_byp_dst_addr_0 (64'h0),
//  .h2c_dsc_byp_len_0      (h2c_dsc_byp_len_0),
//  //.h2c_dsc_byp_ctl_0      (16'h13), //was 16'h3
//  .h2c_dsc_byp_load_0     (h2c_dsc_byp_load_0),
  
//  .c2h_sts_0(c2h_sts_0),                                          // output wire [7 : 0] c2h_sts_0
//  .h2c_sts_0(h2c_sts_0)                                          // output wire [7 : 0] h2c_sts_0
//);

///*
// * Operating System (not board-specific)
// */
  os #(
    .AXI_ID_WIDTH(1),
    .NUM_DDR_CHANNELS(1),
    .ENABLE_DDR(1) //   .C_M_AXIS_DMA_C2H_WIDTH(512)
  ) inst (
    .pcie_clk(pcie_clk),
    .pcie_aresetn(pcie_aresetn),
    .mem_clk(mem_clk),
    .mem_aresetn(mem_aresetn),
    .net_clk(net_clk),
    .net_aresetn(net_aresetn),
    .user_clk(user_clk),
    .user_aresetn(user_aresetn),
    .s_axil_control(s_axil_control),
    .s_axi_control(s_axi_control),
    .s_axis_qp_interface(s_axis_qp_interface),
    .s_axis_qp_conn_interface(s_axis_qp_conn_interface),
    .s_axis_roce_role_tx_meta(s_axis_roce_role_tx_meta),
    .s_axis_roce_role_tx_data(s_axis_roce_role_tx_data),
    .ddr_calib_complete(ddr_calib_complete),
    .m_axi_awid(m_axi_awid_unpacked),
    .m_axi_awaddr(m_axi_awaddr_unpacked),
    .m_axi_awlen(m_axi_awlen_unpacked),
    .m_axi_awsize(m_axi_awsize_unpacked),
    .m_axi_awburst(m_axi_awburst_unpacked),
    .m_axi_awlock(m_axi_awlock_unpacked),
    .m_axi_awcache(m_axi_awcache_unpacked),
    .m_axi_awprot(m_axi_awprot_unpacked),
    .m_axi_awvalid(m_axi_awvalid),
    .m_axi_awready(m_axi_awready),
    .m_axi_wdata(m_axi_wdata_unpacked),
    .m_axi_wstrb(m_axi_wstrb_unpacked),
    .m_axi_wlast(m_axi_wlast),
    .m_axi_wvalid(m_axi_wvalid),
    .m_axi_wready(m_axi_wready),
    .m_axi_bready(m_axi_bready),
    .m_axi_bid(m_axi_bid_unpacked),
    .m_axi_bresp(m_axi_bresp_unpacked),
    .m_axi_bvalid(m_axi_bvalid),
    .m_axi_arid(m_axi_arid_unpacked),
    .m_axi_araddr(m_axi_araddr_unpacked),
    .m_axi_arlen(m_axi_arlen_unpacked),
    .m_axi_arsize(m_axi_arsize_unpacked),
    .m_axi_arburst(m_axi_arburst_unpacked),
    .m_axi_arlock(m_axi_arlock_unpacked),
    .m_axi_arcache(m_axi_arcache_unpacked),
    .m_axi_arprot(m_axi_arprot_unpacked),
    .m_axi_arvalid(m_axi_arvalid),
    .m_axi_arready(m_axi_arready),
    .m_axi_rready(m_axi_rready),
    .m_axi_rid(m_axi_rid_unpacked),
    .m_axi_rdata(m_axi_rdata_unpacked),
    .m_axi_rresp(m_axi_rresp_unpacked),
    .m_axi_rlast(m_axi_rlast),
    .m_axi_rvalid(m_axi_rvalid),
    .m_axis_dma_c2h(m_axis_dma_c2h),
    .s_axis_dma_h2c(s_axis_dma_h2c),
    .c2h_dsc_byp_ready_0(c2h_dsc_byp_ready_0),
    .c2h_dsc_byp_addr_0(c2h_dsc_byp_addr_0),
    .c2h_dsc_byp_len_0(c2h_dsc_byp_len_0),
    .c2h_dsc_byp_load_0(c2h_dsc_byp_load_0),
    .h2c_dsc_byp_ready_0(h2c_dsc_byp_ready_0),
    .h2c_dsc_byp_addr_0(h2c_dsc_byp_addr_0),
    .h2c_dsc_byp_len_0(h2c_dsc_byp_len_0),
    .h2c_dsc_byp_load_0(h2c_dsc_byp_load_0),
    .c2h_sts_0(c2h_sts_0),
    .h2c_sts_0(h2c_sts_0),
    .s_axis_net_rx(s_axis_net_rx),
    .m_axis_net_tx(m_axis_net_tx)
  );
endmodule
