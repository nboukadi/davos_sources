-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: ethz.systems.fpga:hls:udp:0.4
-- IP Revision: 2004071231

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY udp_ip IS
  PORT (
    s_axis_rx_meta_V_TVALID : IN STD_LOGIC;
    s_axis_rx_meta_V_TREADY : OUT STD_LOGIC;
    s_axis_rx_meta_V_TDATA : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    s_axis_rx_data_TVALID : IN STD_LOGIC;
    s_axis_rx_data_TREADY : OUT STD_LOGIC;
    s_axis_rx_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_rx_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_rx_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_rx_meta_V_TVALID : OUT STD_LOGIC;
    m_axis_rx_meta_V_TREADY : IN STD_LOGIC;
    m_axis_rx_meta_V_TDATA : OUT STD_LOGIC_VECTOR(175 DOWNTO 0);
    m_axis_rx_data_TVALID : OUT STD_LOGIC;
    m_axis_rx_data_TREADY : IN STD_LOGIC;
    m_axis_rx_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_rx_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_rx_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axis_tx_meta_V_TVALID : IN STD_LOGIC;
    s_axis_tx_meta_V_TREADY : OUT STD_LOGIC;
    s_axis_tx_meta_V_TDATA : IN STD_LOGIC_VECTOR(175 DOWNTO 0);
    s_axis_tx_data_TVALID : IN STD_LOGIC;
    s_axis_tx_data_TREADY : OUT STD_LOGIC;
    s_axis_tx_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_tx_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_tx_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_tx_meta_V_TVALID : OUT STD_LOGIC;
    m_axis_tx_meta_V_TREADY : IN STD_LOGIC;
    m_axis_tx_meta_V_TDATA : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    m_axis_tx_data_TVALID : OUT STD_LOGIC;
    m_axis_tx_data_TREADY : IN STD_LOGIC;
    m_axis_tx_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_tx_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_tx_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    reg_ip_address_V : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    reg_listen_port_V : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ap_clk : IN STD_LOGIC;
    ap_rst_n : IN STD_LOGIC
  );
END udp_ip;

ARCHITECTURE udp_ip_arch OF udp_ip IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF udp_ip_arch: ARCHITECTURE IS "yes";
  COMPONENT udp_udp_top IS
    PORT (
      s_axis_rx_meta_V_TVALID : IN STD_LOGIC;
      s_axis_rx_meta_V_TREADY : OUT STD_LOGIC;
      s_axis_rx_meta_V_TDATA : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
      s_axis_rx_data_TVALID : IN STD_LOGIC;
      s_axis_rx_data_TREADY : OUT STD_LOGIC;
      s_axis_rx_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_rx_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_rx_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_rx_meta_V_TVALID : OUT STD_LOGIC;
      m_axis_rx_meta_V_TREADY : IN STD_LOGIC;
      m_axis_rx_meta_V_TDATA : OUT STD_LOGIC_VECTOR(175 DOWNTO 0);
      m_axis_rx_data_TVALID : OUT STD_LOGIC;
      m_axis_rx_data_TREADY : IN STD_LOGIC;
      m_axis_rx_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_rx_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_rx_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      s_axis_tx_meta_V_TVALID : IN STD_LOGIC;
      s_axis_tx_meta_V_TREADY : OUT STD_LOGIC;
      s_axis_tx_meta_V_TDATA : IN STD_LOGIC_VECTOR(175 DOWNTO 0);
      s_axis_tx_data_TVALID : IN STD_LOGIC;
      s_axis_tx_data_TREADY : OUT STD_LOGIC;
      s_axis_tx_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_tx_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_tx_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_tx_meta_V_TVALID : OUT STD_LOGIC;
      m_axis_tx_meta_V_TREADY : IN STD_LOGIC;
      m_axis_tx_meta_V_TDATA : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      m_axis_tx_data_TVALID : OUT STD_LOGIC;
      m_axis_tx_data_TREADY : IN STD_LOGIC;
      m_axis_tx_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_tx_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_tx_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      reg_ip_address_V : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
      reg_listen_port_V : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      ap_clk : IN STD_LOGIC;
      ap_rst_n : IN STD_LOGIC
    );
  END COMPONENT udp_udp_top;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF udp_ip_arch: ARCHITECTURE IS "udp_udp_top,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF udp_ip_arch : ARCHITECTURE IS "udp_ip,udp_udp_top,{}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF udp_ip_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst_n: SIGNAL IS "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst_n: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axis_rx_meta_V:s_axis_rx_data:m_axis_rx_meta_V:m_axis_rx_data:s_axis_tx_meta_V:s_axis_tx_data:m_axis_tx_meta_V:m_axis_tx_data, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reg_listen_port_V: SIGNAL IS "XIL_INTERFACENAME reg_listen_port_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF reg_listen_port_V: SIGNAL IS "xilinx.com:signal:data:1.0 reg_listen_port_V DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reg_ip_address_V: SIGNAL IS "XIL_INTERFACENAME reg_ip_address_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF reg_ip_address_V: SIGNAL IS "xilinx.com:signal:data:1.0 reg_ip_address_V DATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_tx_data_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_tx_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_meta_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_meta_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_meta_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_meta_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_tx_meta_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_tx_meta_V, TDATA_NUM_BYTES 6, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tx_meta_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tx_meta_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_tx_data_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_tx_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_meta_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_meta_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_meta_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_meta_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_tx_meta_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_tx_meta_V, TDATA_NUM_BYTES 22, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_tx_meta_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_tx_meta_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_rx_data_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_rx_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_meta_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_meta_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_meta_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_meta_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_rx_meta_V_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_rx_meta_V, TDATA_NUM_BYTES 22, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_rx_meta_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_rx_meta_V TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_rx_data_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_rx_data, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_data_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_data TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_meta_V_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_meta_V TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_meta_V_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_meta_V TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_rx_meta_V_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_rx_meta_V, TDATA_NUM_BYTES 6, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_rx_meta_V_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_rx_meta_V TVALID";
BEGIN
  U0 : udp_udp_top
    PORT MAP (
      s_axis_rx_meta_V_TVALID => s_axis_rx_meta_V_TVALID,
      s_axis_rx_meta_V_TREADY => s_axis_rx_meta_V_TREADY,
      s_axis_rx_meta_V_TDATA => s_axis_rx_meta_V_TDATA,
      s_axis_rx_data_TVALID => s_axis_rx_data_TVALID,
      s_axis_rx_data_TREADY => s_axis_rx_data_TREADY,
      s_axis_rx_data_TDATA => s_axis_rx_data_TDATA,
      s_axis_rx_data_TKEEP => s_axis_rx_data_TKEEP,
      s_axis_rx_data_TLAST => s_axis_rx_data_TLAST,
      m_axis_rx_meta_V_TVALID => m_axis_rx_meta_V_TVALID,
      m_axis_rx_meta_V_TREADY => m_axis_rx_meta_V_TREADY,
      m_axis_rx_meta_V_TDATA => m_axis_rx_meta_V_TDATA,
      m_axis_rx_data_TVALID => m_axis_rx_data_TVALID,
      m_axis_rx_data_TREADY => m_axis_rx_data_TREADY,
      m_axis_rx_data_TDATA => m_axis_rx_data_TDATA,
      m_axis_rx_data_TKEEP => m_axis_rx_data_TKEEP,
      m_axis_rx_data_TLAST => m_axis_rx_data_TLAST,
      s_axis_tx_meta_V_TVALID => s_axis_tx_meta_V_TVALID,
      s_axis_tx_meta_V_TREADY => s_axis_tx_meta_V_TREADY,
      s_axis_tx_meta_V_TDATA => s_axis_tx_meta_V_TDATA,
      s_axis_tx_data_TVALID => s_axis_tx_data_TVALID,
      s_axis_tx_data_TREADY => s_axis_tx_data_TREADY,
      s_axis_tx_data_TDATA => s_axis_tx_data_TDATA,
      s_axis_tx_data_TKEEP => s_axis_tx_data_TKEEP,
      s_axis_tx_data_TLAST => s_axis_tx_data_TLAST,
      m_axis_tx_meta_V_TVALID => m_axis_tx_meta_V_TVALID,
      m_axis_tx_meta_V_TREADY => m_axis_tx_meta_V_TREADY,
      m_axis_tx_meta_V_TDATA => m_axis_tx_meta_V_TDATA,
      m_axis_tx_data_TVALID => m_axis_tx_data_TVALID,
      m_axis_tx_data_TREADY => m_axis_tx_data_TREADY,
      m_axis_tx_data_TDATA => m_axis_tx_data_TDATA,
      m_axis_tx_data_TKEEP => m_axis_tx_data_TKEEP,
      m_axis_tx_data_TLAST => m_axis_tx_data_TLAST,
      reg_ip_address_V => reg_ip_address_V,
      reg_listen_port_V => reg_listen_port_V,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n
    );
END udp_ip_arch;
