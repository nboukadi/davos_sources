-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: ethz.systems.fpga:hls:ip_handler:2.0
-- IP Revision: 2004071223

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ip_handler_ip IS
  PORT (
    s_axis_raw_TVALID : IN STD_LOGIC;
    s_axis_raw_TREADY : OUT STD_LOGIC;
    s_axis_raw_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_raw_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_raw_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_arp_TVALID : OUT STD_LOGIC;
    m_axis_arp_TREADY : IN STD_LOGIC;
    m_axis_arp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_arp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_arp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_icmpv6_TVALID : OUT STD_LOGIC;
    m_axis_icmpv6_TREADY : IN STD_LOGIC;
    m_axis_icmpv6_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_icmpv6_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_icmpv6_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_ipv6udp_TVALID : OUT STD_LOGIC;
    m_axis_ipv6udp_TREADY : IN STD_LOGIC;
    m_axis_ipv6udp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_ipv6udp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_ipv6udp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_icmp_TVALID : OUT STD_LOGIC;
    m_axis_icmp_TREADY : IN STD_LOGIC;
    m_axis_icmp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_icmp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_icmp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_udp_TVALID : OUT STD_LOGIC;
    m_axis_udp_TREADY : IN STD_LOGIC;
    m_axis_udp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_udp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_udp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_tcp_TVALID : OUT STD_LOGIC;
    m_axis_tcp_TREADY : IN STD_LOGIC;
    m_axis_tcp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_tcp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_tcp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_roce_TVALID : OUT STD_LOGIC;
    m_axis_roce_TREADY : IN STD_LOGIC;
    m_axis_roce_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_roce_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_roce_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    myIpAddress_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    ap_clk : IN STD_LOGIC;
    ap_rst_n : IN STD_LOGIC
  );
END ip_handler_ip;

ARCHITECTURE ip_handler_ip_arch OF ip_handler_ip IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF ip_handler_ip_arch: ARCHITECTURE IS "yes";
  COMPONENT ip_handler_ip_handler_top IS
    PORT (
      s_axis_raw_TVALID : IN STD_LOGIC;
      s_axis_raw_TREADY : OUT STD_LOGIC;
      s_axis_raw_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
      s_axis_raw_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_raw_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_arp_TVALID : OUT STD_LOGIC;
      m_axis_arp_TREADY : IN STD_LOGIC;
      m_axis_arp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_arp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_arp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_icmpv6_TVALID : OUT STD_LOGIC;
      m_axis_icmpv6_TREADY : IN STD_LOGIC;
      m_axis_icmpv6_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_icmpv6_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_icmpv6_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_ipv6udp_TVALID : OUT STD_LOGIC;
      m_axis_ipv6udp_TREADY : IN STD_LOGIC;
      m_axis_ipv6udp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_ipv6udp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_ipv6udp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_icmp_TVALID : OUT STD_LOGIC;
      m_axis_icmp_TREADY : IN STD_LOGIC;
      m_axis_icmp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_icmp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_icmp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_udp_TVALID : OUT STD_LOGIC;
      m_axis_udp_TREADY : IN STD_LOGIC;
      m_axis_udp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_udp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_udp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_tcp_TVALID : OUT STD_LOGIC;
      m_axis_tcp_TREADY : IN STD_LOGIC;
      m_axis_tcp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_tcp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_tcp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_roce_TVALID : OUT STD_LOGIC;
      m_axis_roce_TREADY : IN STD_LOGIC;
      m_axis_roce_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
      m_axis_roce_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_roce_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      myIpAddress_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      ap_clk : IN STD_LOGIC;
      ap_rst_n : IN STD_LOGIC
    );
  END COMPONENT ip_handler_ip_handler_top;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF ip_handler_ip_arch: ARCHITECTURE IS "ip_handler_ip_handler_top,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF ip_handler_ip_arch : ARCHITECTURE IS "ip_handler_ip,ip_handler_ip_handler_top,{}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF ip_handler_ip_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst_n: SIGNAL IS "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst_n: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axis_raw:m_axis_arp:m_axis_icmpv6:m_axis_ipv6udp:m_axis_icmp:m_axis_udp:m_axis_tcp:m_axis_roce, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF myIpAddress_V: SIGNAL IS "XIL_INTERFACENAME myIpAddress_V, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF myIpAddress_V: SIGNAL IS "xilinx.com:signal:data:1.0 myIpAddress_V DATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_roce_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_roce TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_roce_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_roce TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_roce_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_roce TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_roce_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_roce TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_roce_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_roce, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_roce_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_roce TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_tcp_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_tcp, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_tcp_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_tcp TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_udp_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_udp TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_udp_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_udp TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_udp_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_udp TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_udp_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_udp TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_udp_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_udp, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_udp_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_udp TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmp_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmp TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmp_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmp TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmp_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmp TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmp_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmp TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_icmp_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_icmp, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmp_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmp TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ipv6udp_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ipv6udp TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ipv6udp_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ipv6udp TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ipv6udp_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ipv6udp TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ipv6udp_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ipv6udp TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_ipv6udp_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_ipv6udp, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_ipv6udp_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_ipv6udp TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmpv6_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmpv6 TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmpv6_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmpv6 TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmpv6_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmpv6 TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmpv6_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmpv6 TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_icmpv6_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_icmpv6, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_icmpv6_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_icmpv6 TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_arp_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_arp TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_arp_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_arp TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_arp_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_arp TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_arp_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_arp TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_arp_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis_arp, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_arp_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis_arp TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_raw_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_raw TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_raw_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_raw TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_raw_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_raw TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_raw_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_raw TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_raw_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis_raw, TDATA_NUM_BYTES 64, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_raw_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis_raw TVALID";
BEGIN
  U0 : ip_handler_ip_handler_top
    PORT MAP (
      s_axis_raw_TVALID => s_axis_raw_TVALID,
      s_axis_raw_TREADY => s_axis_raw_TREADY,
      s_axis_raw_TDATA => s_axis_raw_TDATA,
      s_axis_raw_TKEEP => s_axis_raw_TKEEP,
      s_axis_raw_TLAST => s_axis_raw_TLAST,
      m_axis_arp_TVALID => m_axis_arp_TVALID,
      m_axis_arp_TREADY => m_axis_arp_TREADY,
      m_axis_arp_TDATA => m_axis_arp_TDATA,
      m_axis_arp_TKEEP => m_axis_arp_TKEEP,
      m_axis_arp_TLAST => m_axis_arp_TLAST,
      m_axis_icmpv6_TVALID => m_axis_icmpv6_TVALID,
      m_axis_icmpv6_TREADY => m_axis_icmpv6_TREADY,
      m_axis_icmpv6_TDATA => m_axis_icmpv6_TDATA,
      m_axis_icmpv6_TKEEP => m_axis_icmpv6_TKEEP,
      m_axis_icmpv6_TLAST => m_axis_icmpv6_TLAST,
      m_axis_ipv6udp_TVALID => m_axis_ipv6udp_TVALID,
      m_axis_ipv6udp_TREADY => m_axis_ipv6udp_TREADY,
      m_axis_ipv6udp_TDATA => m_axis_ipv6udp_TDATA,
      m_axis_ipv6udp_TKEEP => m_axis_ipv6udp_TKEEP,
      m_axis_ipv6udp_TLAST => m_axis_ipv6udp_TLAST,
      m_axis_icmp_TVALID => m_axis_icmp_TVALID,
      m_axis_icmp_TREADY => m_axis_icmp_TREADY,
      m_axis_icmp_TDATA => m_axis_icmp_TDATA,
      m_axis_icmp_TKEEP => m_axis_icmp_TKEEP,
      m_axis_icmp_TLAST => m_axis_icmp_TLAST,
      m_axis_udp_TVALID => m_axis_udp_TVALID,
      m_axis_udp_TREADY => m_axis_udp_TREADY,
      m_axis_udp_TDATA => m_axis_udp_TDATA,
      m_axis_udp_TKEEP => m_axis_udp_TKEEP,
      m_axis_udp_TLAST => m_axis_udp_TLAST,
      m_axis_tcp_TVALID => m_axis_tcp_TVALID,
      m_axis_tcp_TREADY => m_axis_tcp_TREADY,
      m_axis_tcp_TDATA => m_axis_tcp_TDATA,
      m_axis_tcp_TKEEP => m_axis_tcp_TKEEP,
      m_axis_tcp_TLAST => m_axis_tcp_TLAST,
      m_axis_roce_TVALID => m_axis_roce_TVALID,
      m_axis_roce_TREADY => m_axis_roce_TREADY,
      m_axis_roce_TDATA => m_axis_roce_TDATA,
      m_axis_roce_TKEEP => m_axis_roce_TKEEP,
      m_axis_roce_TLAST => m_axis_roce_TLAST,
      myIpAddress_V => myIpAddress_V,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n
    );
END ip_handler_ip_arch;
