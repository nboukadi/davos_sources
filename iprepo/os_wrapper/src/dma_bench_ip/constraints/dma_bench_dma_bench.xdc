set_false_path -through [get_ports regBaseAddr_V[*]]
set_false_path -through [get_ports memorySize_V[*]]
set_false_path -through [get_ports numberOfAccesses_V[*]]
set_false_path -through [get_ports chunkLength_V[*]]
set_false_path -through [get_ports strideLength_V[*]]
set_false_path -through [get_ports isWrite_V]
set_false_path -through [get_ports start_V]
