-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.labs:hls:icmp_server:1.67
-- IP Revision: 2004071222

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY icmp_server_ip IS
  PORT (
    s_axis_TVALID : IN STD_LOGIC;
    s_axis_TREADY : OUT STD_LOGIC;
    s_axis_TDATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_TKEEP : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    udpIn_TVALID : IN STD_LOGIC;
    udpIn_TREADY : OUT STD_LOGIC;
    udpIn_TDATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    udpIn_TKEEP : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    udpIn_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    ttlIn_TVALID : IN STD_LOGIC;
    ttlIn_TREADY : OUT STD_LOGIC;
    ttlIn_TDATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    ttlIn_TKEEP : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ttlIn_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_TVALID : OUT STD_LOGIC;
    m_axis_TREADY : IN STD_LOGIC;
    m_axis_TDATA : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_TKEEP : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    ap_clk : IN STD_LOGIC;
    ap_rst_n : IN STD_LOGIC
  );
END icmp_server_ip;

ARCHITECTURE icmp_server_ip_arch OF icmp_server_ip IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF icmp_server_ip_arch: ARCHITECTURE IS "yes";
  COMPONENT icmp_server IS
    PORT (
      s_axis_TVALID : IN STD_LOGIC;
      s_axis_TREADY : OUT STD_LOGIC;
      s_axis_TDATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      s_axis_TKEEP : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axis_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      udpIn_TVALID : IN STD_LOGIC;
      udpIn_TREADY : OUT STD_LOGIC;
      udpIn_TDATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      udpIn_TKEEP : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      udpIn_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      ttlIn_TVALID : IN STD_LOGIC;
      ttlIn_TREADY : OUT STD_LOGIC;
      ttlIn_TDATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      ttlIn_TKEEP : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      ttlIn_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axis_TVALID : OUT STD_LOGIC;
      m_axis_TREADY : IN STD_LOGIC;
      m_axis_TDATA : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_TKEEP : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axis_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      ap_clk : IN STD_LOGIC;
      ap_rst_n : IN STD_LOGIC
    );
  END COMPONENT icmp_server;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF icmp_server_ip_arch: ARCHITECTURE IS "icmp_server,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF icmp_server_ip_arch : ARCHITECTURE IS "icmp_server_ip,icmp_server,{}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF icmp_server_ip_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst_n: SIGNAL IS "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst_n: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF s_axis:udpIn:ttlIn:m_axis, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axis_TVALID: SIGNAL IS "XIL_INTERFACENAME m_axis, TDATA_NUM_BYTES 8, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axis_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 m_axis TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF ttlIn_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 ttlIn TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF ttlIn_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 ttlIn TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF ttlIn_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 ttlIn TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF ttlIn_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 ttlIn TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ttlIn_TVALID: SIGNAL IS "XIL_INTERFACENAME ttlIn, TDATA_NUM_BYTES 8, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ttlIn_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 ttlIn TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF udpIn_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 udpIn TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF udpIn_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 udpIn TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF udpIn_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 udpIn TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF udpIn_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 udpIn TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF udpIn_TVALID: SIGNAL IS "XIL_INTERFACENAME udpIn, TDATA_NUM_BYTES 8, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF udpIn_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 udpIn TVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_TLAST: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis TLAST";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_TKEEP: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis TKEEP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_TDATA: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis TDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_TREADY: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis TREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axis_TVALID: SIGNAL IS "XIL_INTERFACENAME s_axis, TDATA_NUM_BYTES 8, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axis_TVALID: SIGNAL IS "xilinx.com:interface:axis:1.0 s_axis TVALID";
BEGIN
  U0 : icmp_server
    PORT MAP (
      s_axis_TVALID => s_axis_TVALID,
      s_axis_TREADY => s_axis_TREADY,
      s_axis_TDATA => s_axis_TDATA,
      s_axis_TKEEP => s_axis_TKEEP,
      s_axis_TLAST => s_axis_TLAST,
      udpIn_TVALID => udpIn_TVALID,
      udpIn_TREADY => udpIn_TREADY,
      udpIn_TDATA => udpIn_TDATA,
      udpIn_TKEEP => udpIn_TKEEP,
      udpIn_TLAST => udpIn_TLAST,
      ttlIn_TVALID => ttlIn_TVALID,
      ttlIn_TREADY => ttlIn_TREADY,
      ttlIn_TDATA => ttlIn_TDATA,
      ttlIn_TKEEP => ttlIn_TKEEP,
      ttlIn_TLAST => ttlIn_TLAST,
      m_axis_TVALID => m_axis_TVALID,
      m_axis_TREADY => m_axis_TREADY,
      m_axis_TDATA => m_axis_TDATA,
      m_axis_TKEEP => m_axis_TKEEP,
      m_axis_TLAST => m_axis_TLAST,
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n
    );
END icmp_server_ip_arch;
