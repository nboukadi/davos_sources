-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2019.1
-- Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity arp_subnet_process_arp_pkg_512_s is
port (
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_continue : IN STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    s_axis_TVALID : IN STD_LOGIC;
    arpTableInsertFifo_V_din : OUT STD_LOGIC_VECTOR (80 downto 0);
    arpTableInsertFifo_V_full_n : IN STD_LOGIC;
    arpTableInsertFifo_V_write : OUT STD_LOGIC;
    arpReplyMetaFifo_V_din : OUT STD_LOGIC_VECTOR (127 downto 0);
    arpReplyMetaFifo_V_full_n : IN STD_LOGIC;
    arpReplyMetaFifo_V_write : OUT STD_LOGIC;
    s_axis_TDATA : IN STD_LOGIC_VECTOR (511 downto 0);
    s_axis_TREADY : OUT STD_LOGIC;
    s_axis_TKEEP : IN STD_LOGIC_VECTOR (63 downto 0);
    s_axis_TLAST : IN STD_LOGIC_VECTOR (0 downto 0);
    myIpAddress_V : IN STD_LOGIC_VECTOR (31 downto 0);
    regRequestCount_V : OUT STD_LOGIC_VECTOR (15 downto 0);
    regRequestCount_V_ap_vld : OUT STD_LOGIC;
    regReplyCount_V : OUT STD_LOGIC_VECTOR (15 downto 0);
    regReplyCount_V_ap_vld : OUT STD_LOGIC );
end;


architecture behav of arp_subnet_process_arp_pkg_512_s is 
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_ST_fsm_pp0_stage0 : STD_LOGIC_VECTOR (0 downto 0) := "1";
    constant ap_const_lv32_0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000000";
    constant ap_const_boolean_1 : BOOLEAN := true;
    constant ap_const_lv1_1 : STD_LOGIC_VECTOR (0 downto 0) := "1";
    constant ap_const_lv1_0 : STD_LOGIC_VECTOR (0 downto 0) := "0";
    constant ap_const_boolean_0 : BOOLEAN := false;
    constant ap_const_lv16_0 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
    constant ap_const_lv9_0 : STD_LOGIC_VECTOR (8 downto 0) := "000000000";
    constant ap_const_lv25_14F : STD_LOGIC_VECTOR (24 downto 0) := "0000000000000000101001111";
    constant ap_const_lv335_lc_2 : STD_LOGIC_VECTOR (334 downto 0) := "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    constant ap_const_lv32_14F : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000101001111";
    constant ap_const_lv336_lc_3 : STD_LOGIC_VECTOR (335 downto 0) := "100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    constant ap_const_lv336_lc_4 : STD_LOGIC_VECTOR (335 downto 0) := "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
    constant ap_const_lv336_lc_5 : STD_LOGIC_VECTOR (335 downto 0) := "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001";
    constant ap_const_lv16_1 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000001";
    constant ap_const_lv32_130 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000100110000";
    constant ap_const_lv32_A0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000010100000";
    constant ap_const_lv32_AF : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000010101111";
    constant ap_const_lv16_100 : STD_LOGIC_VECTOR (15 downto 0) := "0000000100000000";
    constant ap_const_lv16_200 : STD_LOGIC_VECTOR (15 downto 0) := "0000001000000000";
    constant ap_const_lv32_E0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000011100000";
    constant ap_const_lv32_FF : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000011111111";
    constant ap_const_lv32_B0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000010110000";
    constant ap_const_lv32_DF : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000011011111";
    constant ap_const_lv32_30 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000110000";
    constant ap_const_lv32_5F : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000001011111";

    signal ap_done_reg : STD_LOGIC := '0';
    signal ap_CS_fsm : STD_LOGIC_VECTOR (0 downto 0) := "1";
    attribute fsm_encoding : string;
    attribute fsm_encoding of ap_CS_fsm : signal is "none";
    signal ap_CS_fsm_pp0_stage0 : STD_LOGIC;
    attribute fsm_encoding of ap_CS_fsm_pp0_stage0 : signal is "none";
    signal ap_enable_reg_pp0_iter0 : STD_LOGIC;
    signal ap_enable_reg_pp0_iter1 : STD_LOGIC := '0';
    signal ap_enable_reg_pp0_iter2 : STD_LOGIC := '0';
    signal ap_idle_pp0 : STD_LOGIC;
    signal tmp_nbreadreq_fu_128_p5 : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_block_state1_pp0_stage0_iter0 : BOOLEAN;
    signal ap_block_state2_pp0_stage0_iter1 : BOOLEAN;
    signal tmp_reg_510 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_reg_510_pp0_iter1_reg : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_last_V_reg_514 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_last_V_reg_514_pp0_iter1_reg : STD_LOGIC_VECTOR (0 downto 0);
    signal icmp_ln879_reg_521 : STD_LOGIC_VECTOR (0 downto 0);
    signal icmp_ln879_1_reg_525 : STD_LOGIC_VECTOR (0 downto 0);
    signal icmp_ln879_2_reg_529 : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_predicate_op59_write_state3 : BOOLEAN;
    signal ap_predicate_op67_write_state3 : BOOLEAN;
    signal ap_block_state3_pp0_stage0_iter2 : BOOLEAN;
    signal ap_block_pp0_stage0_11001 : BOOLEAN;
    signal header_ready : STD_LOGIC_VECTOR (0 downto 0) := "0";
    signal header_idx : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
    signal header_header_V : STD_LOGIC_VECTOR (335 downto 0) := "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000110000000000000100000000001000000000000011000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    signal pag_requestCounter_V : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
    signal pag_replyCounter_V : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
    signal s_axis_TDATA_blk_n : STD_LOGIC;
    signal ap_block_pp0_stage0 : BOOLEAN;
    signal arpReplyMetaFifo_V_blk_n : STD_LOGIC;
    signal arpTableInsertFifo_V_blk_n : STD_LOGIC;
    signal tmp_last_V_fu_223_p1 : STD_LOGIC_VECTOR (0 downto 0);
    signal icmp_ln879_fu_388_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal icmp_ln879_1_fu_403_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal icmp_ln879_2_fu_409_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_ipAddress_V_reg_533 : STD_LOGIC_VECTOR (31 downto 0);
    signal tmp_macAddress_V_reg_538 : STD_LOGIC_VECTOR (47 downto 0);
    signal tmp_srcMacAddr_V_reg_543 : STD_LOGIC_VECTOR (47 downto 0);
    signal tmp_3_reg_548 : STD_LOGIC_VECTOR (79 downto 0);
    signal ap_block_pp0_stage0_subdone : BOOLEAN;
    signal ap_phi_mux_phi_ln73_phi_fu_181_p4 : STD_LOGIC_VECTOR (15 downto 0);
    signal ap_phi_reg_pp0_iter0_phi_ln73_reg_178 : STD_LOGIC_VECTOR (15 downto 0);
    signal header_ready_load_load_fu_227_p1 : STD_LOGIC_VECTOR (0 downto 0);
    signal add_ln67_fu_348_p2 : STD_LOGIC_VECTOR (15 downto 0);
    signal ap_phi_mux_write_flag_1_i_i_phi_fu_190_p4 : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_187 : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_phi_mux_header_ready_1_new_0_phi_fu_201_p4 : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_phi_reg_pp0_iter0_header_ready_1_new_0_reg_198 : STD_LOGIC_VECTOR (0 downto 0);
    signal or_ln73_fu_355_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_phi_mux_header_idx_new_0_i_phi_fu_211_p4 : STD_LOGIC_VECTOR (15 downto 0);
    signal ap_phi_reg_pp0_iter0_header_idx_new_0_i_reg_208 : STD_LOGIC_VECTOR (15 downto 0);
    signal p_Result_s_fu_336_p2 : STD_LOGIC_VECTOR (335 downto 0);
    signal add_ln700_fu_492_p2 : STD_LOGIC_VECTOR (15 downto 0);
    signal add_ln700_1_fu_468_p2 : STD_LOGIC_VECTOR (15 downto 0);
    signal ap_block_pp0_stage0_01001 : BOOLEAN;
    signal Lo_assign_fu_236_p3 : STD_LOGIC_VECTOR (24 downto 0);
    signal trunc_ln414_fu_258_p1 : STD_LOGIC_VECTOR (0 downto 0);
    signal icmp_ln414_fu_252_p2 : STD_LOGIC_VECTOR (0 downto 0);
    signal st4_fu_262_p3 : STD_LOGIC_VECTOR (335 downto 0);
    signal tmp_V_fu_244_p1 : STD_LOGIC_VECTOR (335 downto 0);
    signal select_ln414_fu_270_p3 : STD_LOGIC_VECTOR (335 downto 0);
    signal tmp_1_fu_278_p4 : STD_LOGIC_VECTOR (335 downto 0);
    signal select_ln414_2_fu_296_p3 : STD_LOGIC_VECTOR (335 downto 0);
    signal select_ln414_3_fu_304_p3 : STD_LOGIC_VECTOR (335 downto 0);
    signal and_ln414_fu_312_p2 : STD_LOGIC_VECTOR (335 downto 0);
    signal xor_ln414_fu_318_p2 : STD_LOGIC_VECTOR (335 downto 0);
    signal select_ln414_1_fu_288_p3 : STD_LOGIC_VECTOR (335 downto 0);
    signal and_ln414_1_fu_324_p2 : STD_LOGIC_VECTOR (335 downto 0);
    signal and_ln414_2_fu_330_p2 : STD_LOGIC_VECTOR (335 downto 0);
    signal p_Result_i41_i_fu_378_p4 : STD_LOGIC_VECTOR (31 downto 0);
    signal p_Result_i43_i_fu_393_p4 : STD_LOGIC_VECTOR (15 downto 0);
    signal ap_NS_fsm : STD_LOGIC_VECTOR (0 downto 0);
    signal ap_idle_pp0_0to1 : STD_LOGIC;
    signal ap_reset_idle_pp0 : STD_LOGIC;
    signal ap_enable_pp0 : STD_LOGIC;
    signal ap_condition_116 : BOOLEAN;


begin




    ap_CS_fsm_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_CS_fsm <= ap_ST_fsm_pp0_stage0;
            else
                ap_CS_fsm <= ap_NS_fsm;
            end if;
        end if;
    end process;


    ap_done_reg_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_done_reg <= ap_const_logic_0;
            else
                if ((ap_continue = ap_const_logic_1)) then 
                    ap_done_reg <= ap_const_logic_0;
                elsif (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (ap_enable_reg_pp0_iter2 = ap_const_logic_1))) then 
                    ap_done_reg <= ap_const_logic_1;
                end if; 
            end if;
        end if;
    end process;


    ap_enable_reg_pp0_iter1_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_enable_reg_pp0_iter1 <= ap_const_logic_0;
            else
                if (((ap_const_logic_1 = ap_CS_fsm_pp0_stage0) and (ap_const_boolean_0 = ap_block_pp0_stage0_subdone))) then 
                    ap_enable_reg_pp0_iter1 <= ap_start;
                end if; 
            end if;
        end if;
    end process;


    ap_enable_reg_pp0_iter2_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_enable_reg_pp0_iter2 <= ap_const_logic_0;
            else
                if ((ap_const_boolean_0 = ap_block_pp0_stage0_subdone)) then 
                    ap_enable_reg_pp0_iter2 <= ap_enable_reg_pp0_iter1;
                end if; 
            end if;
        end if;
    end process;

    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (header_ready_load_load_fu_227_p1 = ap_const_lv1_0) and (tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                header_header_V <= p_Result_s_fu_336_p2;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                header_idx <= ap_phi_mux_header_idx_new_0_i_phi_fu_211_p4;
                header_ready <= ap_phi_mux_header_ready_1_new_0_phi_fu_201_p4;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (tmp_last_V_reg_514 = ap_const_lv1_1) and (tmp_reg_510 = ap_const_lv1_1) and (icmp_ln879_fu_388_p2 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                icmp_ln879_1_reg_525 <= icmp_ln879_1_fu_403_p2;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (icmp_ln879_1_fu_403_p2 = ap_const_lv1_0) and (tmp_last_V_reg_514 = ap_const_lv1_1) and (tmp_reg_510 = ap_const_lv1_1) and (icmp_ln879_fu_388_p2 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                icmp_ln879_2_reg_529 <= icmp_ln879_2_fu_409_p2;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (tmp_last_V_reg_514 = ap_const_lv1_1) and (tmp_reg_510 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                icmp_ln879_reg_521 <= icmp_ln879_fu_388_p2;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (icmp_ln879_1_reg_525 = ap_const_lv1_0) and (icmp_ln879_2_reg_529 = ap_const_lv1_1) and (icmp_ln879_reg_521 = ap_const_lv1_1) and (tmp_last_V_reg_514_pp0_iter1_reg = ap_const_lv1_1) and (tmp_reg_510_pp0_iter1_reg = ap_const_lv1_1) and (ap_enable_reg_pp0_iter2 = ap_const_logic_1))) then
                pag_replyCounter_V <= add_ln700_1_fu_468_p2;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (icmp_ln879_1_reg_525 = ap_const_lv1_1) and (icmp_ln879_reg_521 = ap_const_lv1_1) and (tmp_last_V_reg_514_pp0_iter1_reg = ap_const_lv1_1) and (tmp_reg_510_pp0_iter1_reg = ap_const_lv1_1) and (ap_enable_reg_pp0_iter2 = ap_const_logic_1))) then
                pag_requestCounter_V <= add_ln700_fu_492_p2;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (tmp_last_V_reg_514 = ap_const_lv1_1) and (tmp_reg_510 = ap_const_lv1_1) and (icmp_ln879_1_fu_403_p2 = ap_const_lv1_1) and (icmp_ln879_fu_388_p2 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                tmp_3_reg_548 <= header_header_V(255 downto 176);
                tmp_srcMacAddr_V_reg_543 <= header_header_V(95 downto 48);
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (icmp_ln879_1_fu_403_p2 = ap_const_lv1_0) and (tmp_last_V_reg_514 = ap_const_lv1_1) and (tmp_reg_510 = ap_const_lv1_1) and (icmp_ln879_2_fu_409_p2 = ap_const_lv1_1) and (icmp_ln879_fu_388_p2 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                tmp_ipAddress_V_reg_533 <= header_header_V(255 downto 224);
                tmp_macAddress_V_reg_538 <= header_header_V(223 downto 176);
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                tmp_last_V_reg_514 <= s_axis_TLAST;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then
                tmp_last_V_reg_514_pp0_iter1_reg <= tmp_last_V_reg_514;
                tmp_reg_510 <= tmp_nbreadreq_fu_128_p5;
                tmp_reg_510_pp0_iter1_reg <= tmp_reg_510;
            end if;
        end if;
    end process;

    ap_NS_fsm_assign_proc : process (ap_CS_fsm, ap_block_pp0_stage0_subdone, ap_reset_idle_pp0)
    begin
        case ap_CS_fsm is
            when ap_ST_fsm_pp0_stage0 => 
                ap_NS_fsm <= ap_ST_fsm_pp0_stage0;
            when others =>  
                ap_NS_fsm <= "X";
        end case;
    end process;
    Lo_assign_fu_236_p3 <= (header_idx & ap_const_lv9_0);
    add_ln67_fu_348_p2 <= std_logic_vector(unsigned(ap_const_lv16_1) + unsigned(header_idx));
    add_ln700_1_fu_468_p2 <= std_logic_vector(unsigned(pag_replyCounter_V) + unsigned(ap_const_lv16_1));
    add_ln700_fu_492_p2 <= std_logic_vector(unsigned(pag_requestCounter_V) + unsigned(ap_const_lv16_1));
    and_ln414_1_fu_324_p2 <= (xor_ln414_fu_318_p2 and header_header_V);
    and_ln414_2_fu_330_p2 <= (select_ln414_1_fu_288_p3 and and_ln414_fu_312_p2);
    and_ln414_fu_312_p2 <= (select_ln414_3_fu_304_p3 and select_ln414_2_fu_296_p3);
    ap_CS_fsm_pp0_stage0 <= ap_CS_fsm(0);
        ap_block_pp0_stage0 <= not((ap_const_boolean_1 = ap_const_boolean_1));

    ap_block_pp0_stage0_01001_assign_proc : process(ap_start, ap_done_reg, ap_enable_reg_pp0_iter2, s_axis_TVALID, tmp_nbreadreq_fu_128_p5, arpTableInsertFifo_V_full_n, ap_predicate_op59_write_state3, arpReplyMetaFifo_V_full_n, ap_predicate_op67_write_state3)
    begin
                ap_block_pp0_stage0_01001 <= ((ap_done_reg = ap_const_logic_1) or ((ap_enable_reg_pp0_iter2 = ap_const_logic_1) and (((arpReplyMetaFifo_V_full_n = ap_const_logic_0) and (ap_predicate_op67_write_state3 = ap_const_boolean_1)) or ((arpTableInsertFifo_V_full_n = ap_const_logic_0) and (ap_predicate_op59_write_state3 = ap_const_boolean_1)))) or ((ap_start = ap_const_logic_1) and ((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1) or ((tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (s_axis_TVALID = ap_const_logic_0)))));
    end process;


    ap_block_pp0_stage0_11001_assign_proc : process(ap_start, ap_done_reg, ap_enable_reg_pp0_iter2, s_axis_TVALID, tmp_nbreadreq_fu_128_p5, arpTableInsertFifo_V_full_n, ap_predicate_op59_write_state3, arpReplyMetaFifo_V_full_n, ap_predicate_op67_write_state3)
    begin
                ap_block_pp0_stage0_11001 <= ((ap_done_reg = ap_const_logic_1) or ((ap_enable_reg_pp0_iter2 = ap_const_logic_1) and (((arpReplyMetaFifo_V_full_n = ap_const_logic_0) and (ap_predicate_op67_write_state3 = ap_const_boolean_1)) or ((arpTableInsertFifo_V_full_n = ap_const_logic_0) and (ap_predicate_op59_write_state3 = ap_const_boolean_1)))) or ((ap_start = ap_const_logic_1) and ((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1) or ((tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (s_axis_TVALID = ap_const_logic_0)))));
    end process;


    ap_block_pp0_stage0_subdone_assign_proc : process(ap_start, ap_done_reg, ap_enable_reg_pp0_iter2, s_axis_TVALID, tmp_nbreadreq_fu_128_p5, arpTableInsertFifo_V_full_n, ap_predicate_op59_write_state3, arpReplyMetaFifo_V_full_n, ap_predicate_op67_write_state3)
    begin
                ap_block_pp0_stage0_subdone <= ((ap_done_reg = ap_const_logic_1) or ((ap_enable_reg_pp0_iter2 = ap_const_logic_1) and (((arpReplyMetaFifo_V_full_n = ap_const_logic_0) and (ap_predicate_op67_write_state3 = ap_const_boolean_1)) or ((arpTableInsertFifo_V_full_n = ap_const_logic_0) and (ap_predicate_op59_write_state3 = ap_const_boolean_1)))) or ((ap_start = ap_const_logic_1) and ((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1) or ((tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (s_axis_TVALID = ap_const_logic_0)))));
    end process;


    ap_block_state1_pp0_stage0_iter0_assign_proc : process(ap_start, ap_done_reg, s_axis_TVALID, tmp_nbreadreq_fu_128_p5)
    begin
                ap_block_state1_pp0_stage0_iter0 <= ((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1) or ((tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (s_axis_TVALID = ap_const_logic_0)));
    end process;

        ap_block_state2_pp0_stage0_iter1 <= not((ap_const_boolean_1 = ap_const_boolean_1));

    ap_block_state3_pp0_stage0_iter2_assign_proc : process(arpTableInsertFifo_V_full_n, ap_predicate_op59_write_state3, arpReplyMetaFifo_V_full_n, ap_predicate_op67_write_state3)
    begin
                ap_block_state3_pp0_stage0_iter2 <= (((arpReplyMetaFifo_V_full_n = ap_const_logic_0) and (ap_predicate_op67_write_state3 = ap_const_boolean_1)) or ((arpTableInsertFifo_V_full_n = ap_const_logic_0) and (ap_predicate_op59_write_state3 = ap_const_boolean_1)));
    end process;


    ap_condition_116_assign_proc : process(ap_start, ap_CS_fsm_pp0_stage0, tmp_nbreadreq_fu_128_p5, ap_block_pp0_stage0)
    begin
                ap_condition_116 <= ((tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0) and (ap_const_boolean_0 = ap_block_pp0_stage0));
    end process;


    ap_done_assign_proc : process(ap_done_reg, ap_enable_reg_pp0_iter2, ap_block_pp0_stage0_11001)
    begin
        if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (ap_enable_reg_pp0_iter2 = ap_const_logic_1))) then 
            ap_done <= ap_const_logic_1;
        else 
            ap_done <= ap_done_reg;
        end if; 
    end process;

    ap_enable_pp0 <= (ap_idle_pp0 xor ap_const_logic_1);
    ap_enable_reg_pp0_iter0 <= ap_start;

    ap_idle_assign_proc : process(ap_start, ap_CS_fsm_pp0_stage0, ap_idle_pp0)
    begin
        if (((ap_start = ap_const_logic_0) and (ap_idle_pp0 = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then 
            ap_idle <= ap_const_logic_1;
        else 
            ap_idle <= ap_const_logic_0;
        end if; 
    end process;


    ap_idle_pp0_assign_proc : process(ap_enable_reg_pp0_iter0, ap_enable_reg_pp0_iter1, ap_enable_reg_pp0_iter2)
    begin
        if (((ap_enable_reg_pp0_iter2 = ap_const_logic_0) and (ap_enable_reg_pp0_iter1 = ap_const_logic_0) and (ap_enable_reg_pp0_iter0 = ap_const_logic_0))) then 
            ap_idle_pp0 <= ap_const_logic_1;
        else 
            ap_idle_pp0 <= ap_const_logic_0;
        end if; 
    end process;


    ap_idle_pp0_0to1_assign_proc : process(ap_enable_reg_pp0_iter0, ap_enable_reg_pp0_iter1)
    begin
        if (((ap_enable_reg_pp0_iter1 = ap_const_logic_0) and (ap_enable_reg_pp0_iter0 = ap_const_logic_0))) then 
            ap_idle_pp0_0to1 <= ap_const_logic_1;
        else 
            ap_idle_pp0_0to1 <= ap_const_logic_0;
        end if; 
    end process;


    ap_phi_mux_header_idx_new_0_i_phi_fu_211_p4_assign_proc : process(tmp_last_V_fu_223_p1, ap_phi_mux_phi_ln73_phi_fu_181_p4, ap_phi_reg_pp0_iter0_header_idx_new_0_i_reg_208, ap_condition_116)
    begin
        if ((ap_const_boolean_1 = ap_condition_116)) then
            if ((tmp_last_V_fu_223_p1 = ap_const_lv1_0)) then 
                ap_phi_mux_header_idx_new_0_i_phi_fu_211_p4 <= ap_phi_mux_phi_ln73_phi_fu_181_p4;
            elsif ((tmp_last_V_fu_223_p1 = ap_const_lv1_1)) then 
                ap_phi_mux_header_idx_new_0_i_phi_fu_211_p4 <= ap_const_lv16_0;
            else 
                ap_phi_mux_header_idx_new_0_i_phi_fu_211_p4 <= ap_phi_reg_pp0_iter0_header_idx_new_0_i_reg_208;
            end if;
        else 
            ap_phi_mux_header_idx_new_0_i_phi_fu_211_p4 <= ap_phi_reg_pp0_iter0_header_idx_new_0_i_reg_208;
        end if; 
    end process;


    ap_phi_mux_header_ready_1_new_0_phi_fu_201_p4_assign_proc : process(tmp_last_V_fu_223_p1, ap_phi_reg_pp0_iter0_header_ready_1_new_0_reg_198, or_ln73_fu_355_p2, ap_condition_116)
    begin
        if ((ap_const_boolean_1 = ap_condition_116)) then
            if ((tmp_last_V_fu_223_p1 = ap_const_lv1_0)) then 
                ap_phi_mux_header_ready_1_new_0_phi_fu_201_p4 <= or_ln73_fu_355_p2;
            elsif ((tmp_last_V_fu_223_p1 = ap_const_lv1_1)) then 
                ap_phi_mux_header_ready_1_new_0_phi_fu_201_p4 <= ap_const_lv1_0;
            else 
                ap_phi_mux_header_ready_1_new_0_phi_fu_201_p4 <= ap_phi_reg_pp0_iter0_header_ready_1_new_0_reg_198;
            end if;
        else 
            ap_phi_mux_header_ready_1_new_0_phi_fu_201_p4 <= ap_phi_reg_pp0_iter0_header_ready_1_new_0_reg_198;
        end if; 
    end process;


    ap_phi_mux_phi_ln73_phi_fu_181_p4_assign_proc : process(header_idx, ap_phi_reg_pp0_iter0_phi_ln73_reg_178, header_ready_load_load_fu_227_p1, add_ln67_fu_348_p2, ap_condition_116)
    begin
        if ((ap_const_boolean_1 = ap_condition_116)) then
            if ((header_ready_load_load_fu_227_p1 = ap_const_lv1_0)) then 
                ap_phi_mux_phi_ln73_phi_fu_181_p4 <= add_ln67_fu_348_p2;
            elsif ((header_ready_load_load_fu_227_p1 = ap_const_lv1_1)) then 
                ap_phi_mux_phi_ln73_phi_fu_181_p4 <= header_idx;
            else 
                ap_phi_mux_phi_ln73_phi_fu_181_p4 <= ap_phi_reg_pp0_iter0_phi_ln73_reg_178;
            end if;
        else 
            ap_phi_mux_phi_ln73_phi_fu_181_p4 <= ap_phi_reg_pp0_iter0_phi_ln73_reg_178;
        end if; 
    end process;


    ap_phi_mux_write_flag_1_i_i_phi_fu_190_p4_assign_proc : process(header_ready_load_load_fu_227_p1, ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_187, ap_condition_116)
    begin
        if ((ap_const_boolean_1 = ap_condition_116)) then
            if ((header_ready_load_load_fu_227_p1 = ap_const_lv1_0)) then 
                ap_phi_mux_write_flag_1_i_i_phi_fu_190_p4 <= ap_const_lv1_1;
            elsif ((header_ready_load_load_fu_227_p1 = ap_const_lv1_1)) then 
                ap_phi_mux_write_flag_1_i_i_phi_fu_190_p4 <= ap_const_lv1_0;
            else 
                ap_phi_mux_write_flag_1_i_i_phi_fu_190_p4 <= ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_187;
            end if;
        else 
            ap_phi_mux_write_flag_1_i_i_phi_fu_190_p4 <= ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_187;
        end if; 
    end process;

    ap_phi_reg_pp0_iter0_header_idx_new_0_i_reg_208 <= "XXXXXXXXXXXXXXXX";
    ap_phi_reg_pp0_iter0_header_ready_1_new_0_reg_198 <= "X";
    ap_phi_reg_pp0_iter0_phi_ln73_reg_178 <= "XXXXXXXXXXXXXXXX";
    ap_phi_reg_pp0_iter0_write_flag_1_i_i_reg_187 <= "X";

    ap_predicate_op59_write_state3_assign_proc : process(tmp_reg_510_pp0_iter1_reg, tmp_last_V_reg_514_pp0_iter1_reg, icmp_ln879_reg_521, icmp_ln879_1_reg_525, icmp_ln879_2_reg_529)
    begin
                ap_predicate_op59_write_state3 <= ((icmp_ln879_1_reg_525 = ap_const_lv1_0) and (icmp_ln879_2_reg_529 = ap_const_lv1_1) and (icmp_ln879_reg_521 = ap_const_lv1_1) and (tmp_last_V_reg_514_pp0_iter1_reg = ap_const_lv1_1) and (tmp_reg_510_pp0_iter1_reg = ap_const_lv1_1));
    end process;


    ap_predicate_op67_write_state3_assign_proc : process(tmp_reg_510_pp0_iter1_reg, tmp_last_V_reg_514_pp0_iter1_reg, icmp_ln879_reg_521, icmp_ln879_1_reg_525)
    begin
                ap_predicate_op67_write_state3 <= ((icmp_ln879_1_reg_525 = ap_const_lv1_1) and (icmp_ln879_reg_521 = ap_const_lv1_1) and (tmp_last_V_reg_514_pp0_iter1_reg = ap_const_lv1_1) and (tmp_reg_510_pp0_iter1_reg = ap_const_lv1_1));
    end process;


    ap_ready_assign_proc : process(ap_start, ap_CS_fsm_pp0_stage0, ap_block_pp0_stage0_11001)
    begin
        if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then 
            ap_ready <= ap_const_logic_1;
        else 
            ap_ready <= ap_const_logic_0;
        end if; 
    end process;


    ap_reset_idle_pp0_assign_proc : process(ap_start, ap_idle_pp0_0to1)
    begin
        if (((ap_start = ap_const_logic_0) and (ap_idle_pp0_0to1 = ap_const_logic_1))) then 
            ap_reset_idle_pp0 <= ap_const_logic_1;
        else 
            ap_reset_idle_pp0 <= ap_const_logic_0;
        end if; 
    end process;


    arpReplyMetaFifo_V_blk_n_assign_proc : process(ap_enable_reg_pp0_iter2, arpReplyMetaFifo_V_full_n, ap_predicate_op67_write_state3, ap_block_pp0_stage0)
    begin
        if (((ap_enable_reg_pp0_iter2 = ap_const_logic_1) and (ap_predicate_op67_write_state3 = ap_const_boolean_1) and (ap_const_boolean_0 = ap_block_pp0_stage0))) then 
            arpReplyMetaFifo_V_blk_n <= arpReplyMetaFifo_V_full_n;
        else 
            arpReplyMetaFifo_V_blk_n <= ap_const_logic_1;
        end if; 
    end process;

    arpReplyMetaFifo_V_din <= (tmp_3_reg_548 & tmp_srcMacAddr_V_reg_543);

    arpReplyMetaFifo_V_write_assign_proc : process(ap_enable_reg_pp0_iter2, ap_predicate_op67_write_state3, ap_block_pp0_stage0_11001)
    begin
        if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (ap_enable_reg_pp0_iter2 = ap_const_logic_1) and (ap_predicate_op67_write_state3 = ap_const_boolean_1))) then 
            arpReplyMetaFifo_V_write <= ap_const_logic_1;
        else 
            arpReplyMetaFifo_V_write <= ap_const_logic_0;
        end if; 
    end process;


    arpTableInsertFifo_V_blk_n_assign_proc : process(ap_enable_reg_pp0_iter2, arpTableInsertFifo_V_full_n, ap_predicate_op59_write_state3, ap_block_pp0_stage0)
    begin
        if (((ap_enable_reg_pp0_iter2 = ap_const_logic_1) and (ap_predicate_op59_write_state3 = ap_const_boolean_1) and (ap_const_boolean_0 = ap_block_pp0_stage0))) then 
            arpTableInsertFifo_V_blk_n <= arpTableInsertFifo_V_full_n;
        else 
            arpTableInsertFifo_V_blk_n <= ap_const_logic_1;
        end if; 
    end process;

    arpTableInsertFifo_V_din <= ((ap_const_lv1_1 & tmp_macAddress_V_reg_538) & tmp_ipAddress_V_reg_533);

    arpTableInsertFifo_V_write_assign_proc : process(ap_enable_reg_pp0_iter2, ap_predicate_op59_write_state3, ap_block_pp0_stage0_11001)
    begin
        if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (ap_enable_reg_pp0_iter2 = ap_const_logic_1) and (ap_predicate_op59_write_state3 = ap_const_boolean_1))) then 
            arpTableInsertFifo_V_write <= ap_const_logic_1;
        else 
            arpTableInsertFifo_V_write <= ap_const_logic_0;
        end if; 
    end process;

    header_ready_load_load_fu_227_p1 <= header_ready;
    icmp_ln414_fu_252_p2 <= "1" when (unsigned(Lo_assign_fu_236_p3) > unsigned(ap_const_lv25_14F)) else "0";
    icmp_ln879_1_fu_403_p2 <= "1" when (p_Result_i43_i_fu_393_p4 = ap_const_lv16_100) else "0";
    icmp_ln879_2_fu_409_p2 <= "1" when (p_Result_i43_i_fu_393_p4 = ap_const_lv16_200) else "0";
    icmp_ln879_fu_388_p2 <= "1" when (p_Result_i41_i_fu_378_p4 = myIpAddress_V) else "0";
    or_ln73_fu_355_p2 <= (header_ready or ap_phi_mux_write_flag_1_i_i_phi_fu_190_p4);
    p_Result_i41_i_fu_378_p4 <= header_header_V(335 downto 304);
    p_Result_i43_i_fu_393_p4 <= header_header_V(175 downto 160);
    p_Result_s_fu_336_p2 <= (and_ln414_2_fu_330_p2 or and_ln414_1_fu_324_p2);
    regReplyCount_V <= std_logic_vector(unsigned(pag_replyCounter_V) + unsigned(ap_const_lv16_1));

    regReplyCount_V_ap_vld_assign_proc : process(ap_enable_reg_pp0_iter2, tmp_reg_510_pp0_iter1_reg, tmp_last_V_reg_514_pp0_iter1_reg, icmp_ln879_reg_521, icmp_ln879_1_reg_525, icmp_ln879_2_reg_529, ap_block_pp0_stage0_11001)
    begin
        if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (icmp_ln879_1_reg_525 = ap_const_lv1_0) and (icmp_ln879_2_reg_529 = ap_const_lv1_1) and (icmp_ln879_reg_521 = ap_const_lv1_1) and (tmp_last_V_reg_514_pp0_iter1_reg = ap_const_lv1_1) and (tmp_reg_510_pp0_iter1_reg = ap_const_lv1_1) and (ap_enable_reg_pp0_iter2 = ap_const_logic_1))) then 
            regReplyCount_V_ap_vld <= ap_const_logic_1;
        else 
            regReplyCount_V_ap_vld <= ap_const_logic_0;
        end if; 
    end process;

    regRequestCount_V <= std_logic_vector(unsigned(pag_requestCounter_V) + unsigned(ap_const_lv16_1));

    regRequestCount_V_ap_vld_assign_proc : process(ap_enable_reg_pp0_iter2, tmp_reg_510_pp0_iter1_reg, tmp_last_V_reg_514_pp0_iter1_reg, icmp_ln879_reg_521, icmp_ln879_1_reg_525, ap_block_pp0_stage0_11001)
    begin
        if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (icmp_ln879_1_reg_525 = ap_const_lv1_1) and (icmp_ln879_reg_521 = ap_const_lv1_1) and (tmp_last_V_reg_514_pp0_iter1_reg = ap_const_lv1_1) and (tmp_reg_510_pp0_iter1_reg = ap_const_lv1_1) and (ap_enable_reg_pp0_iter2 = ap_const_logic_1))) then 
            regRequestCount_V_ap_vld <= ap_const_logic_1;
        else 
            regRequestCount_V_ap_vld <= ap_const_logic_0;
        end if; 
    end process;


    s_axis_TDATA_blk_n_assign_proc : process(ap_start, ap_done_reg, ap_CS_fsm_pp0_stage0, s_axis_TVALID, tmp_nbreadreq_fu_128_p5, ap_block_pp0_stage0)
    begin
        if ((not(((ap_start = ap_const_logic_0) or (ap_done_reg = ap_const_logic_1))) and (tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0) and (ap_const_boolean_0 = ap_block_pp0_stage0))) then 
            s_axis_TDATA_blk_n <= s_axis_TVALID;
        else 
            s_axis_TDATA_blk_n <= ap_const_logic_1;
        end if; 
    end process;


    s_axis_TREADY_assign_proc : process(ap_start, ap_CS_fsm_pp0_stage0, tmp_nbreadreq_fu_128_p5, ap_block_pp0_stage0_11001)
    begin
        if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (tmp_nbreadreq_fu_128_p5 = ap_const_lv1_1) and (ap_start = ap_const_logic_1) and (ap_const_logic_1 = ap_CS_fsm_pp0_stage0))) then 
            s_axis_TREADY <= ap_const_logic_1;
        else 
            s_axis_TREADY <= ap_const_logic_0;
        end if; 
    end process;

    select_ln414_1_fu_288_p3 <= 
        tmp_1_fu_278_p4 when (icmp_ln414_fu_252_p2(0) = '1') else 
        tmp_V_fu_244_p1;
    select_ln414_2_fu_296_p3 <= 
        ap_const_lv336_lc_3 when (icmp_ln414_fu_252_p2(0) = '1') else 
        ap_const_lv336_lc_4;
    select_ln414_3_fu_304_p3 <= 
        ap_const_lv336_lc_5 when (icmp_ln414_fu_252_p2(0) = '1') else 
        ap_const_lv336_lc_4;
    select_ln414_fu_270_p3 <= 
        st4_fu_262_p3 when (icmp_ln414_fu_252_p2(0) = '1') else 
        tmp_V_fu_244_p1;
    st4_fu_262_p3 <= (trunc_ln414_fu_258_p1 & ap_const_lv335_lc_2);
    
    tmp_1_fu_278_p4_proc : process(select_ln414_fu_270_p3)
    variable vlo_cpy : STD_LOGIC_VECTOR(336+32 - 1 downto 0);
    variable vhi_cpy : STD_LOGIC_VECTOR(336+32 - 1 downto 0);
    variable v0_cpy : STD_LOGIC_VECTOR(336 - 1 downto 0);
    variable tmp_1_fu_278_p4_i : integer;
    variable section : STD_LOGIC_VECTOR(336 - 1 downto 0);
    variable tmp_mask : STD_LOGIC_VECTOR(336 - 1 downto 0);
    variable resvalue, res_value, res_mask : STD_LOGIC_VECTOR(336 - 1 downto 0);
    begin
        vlo_cpy := (others => '0');
        vlo_cpy(9 - 1 downto 0) := ap_const_lv32_14F(9 - 1 downto 0);
        vhi_cpy := (others => '0');
        vhi_cpy(9 - 1 downto 0) := ap_const_lv32_0(9 - 1 downto 0);
        v0_cpy := select_ln414_fu_270_p3;
        if (vlo_cpy(9 - 1 downto 0) > vhi_cpy(9 - 1 downto 0)) then
            vhi_cpy(9-1 downto 0) := std_logic_vector(336-1-unsigned(ap_const_lv32_0(9-1 downto 0)));
            vlo_cpy(9-1 downto 0) := std_logic_vector(336-1-unsigned(ap_const_lv32_14F(9-1 downto 0)));
            for tmp_1_fu_278_p4_i in 0 to 336-1 loop
                v0_cpy(tmp_1_fu_278_p4_i) := select_ln414_fu_270_p3(336-1-tmp_1_fu_278_p4_i);
            end loop;
        end if;
        res_value := std_logic_vector(shift_right(unsigned(v0_cpy), to_integer(unsigned('0' & vlo_cpy(9-1 downto 0)))));

        section := (others=>'0');
        section(9-1 downto 0) := std_logic_vector(unsigned(vhi_cpy(9-1 downto 0)) - unsigned(vlo_cpy(9-1 downto 0)));
        tmp_mask := (others => '1');
        res_mask := std_logic_vector(shift_left(unsigned(tmp_mask),to_integer(unsigned('0' & section(31-1 downto 0)))));
        res_mask := res_mask(336-2 downto 0) & '0';
        resvalue := res_value and not res_mask;
        tmp_1_fu_278_p4 <= resvalue(336-1 downto 0);
    end process;

    tmp_V_fu_244_p1 <= s_axis_TDATA(336 - 1 downto 0);
    tmp_last_V_fu_223_p1 <= s_axis_TLAST;
    tmp_nbreadreq_fu_128_p5 <= (0=>(s_axis_TVALID), others=>'-');
    trunc_ln414_fu_258_p1 <= s_axis_TDATA(1 - 1 downto 0);
    xor_ln414_fu_318_p2 <= (ap_const_lv336_lc_4 xor and_ln414_fu_312_p2);
end behav;
