# ==============================================================
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
# ==============================================================

Family       : virtexuplushbm
Device       : xcvu37p
Package      : -fsvh2892
Speed Grade  : -2-e-es1
Clock Period : 6.400 ns
