library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--include "davos_config.svh"
--include "davos_types.svh"

entity os_wrapper_vhdl is
  generic(
    AXI_ID_WIDTH : integer  := 1;
    NUM_DDR_CHANNELS : integer  := 1;
    ENABLE_DDR : integer  := 1
  );
  PORT(
    pcie_clk : in std_logic;
    pcie_aresetn: in std_logic;
    mem_clk: in std_logic;
    mem_aresetn: in std_logic;
    net_clk: in std_logic;
    net_aresetn: in std_logic;
    user_clk: out std_logic;
    user_aresetn: out std_logic;
    
    s_axil_control_awaddr : in STD_LOGIC_VECTOR(31 DOWNTO 0); 
    s_axil_control_awprot : in STD_LOGIC_VECTOR(2 DOWNTO 0);   
    s_axil_control_awvalid : in STD_LOGIC;                     
    s_axil_control_awready : out STD_LOGIC;                      
    s_axil_control_wdata : in STD_LOGIC_VECTOR(31 DOWNTO 0);   
    s_axil_control_wstrb : in STD_LOGIC_VECTOR(3 DOWNTO 0);    
    s_axil_control_wvalid : in STD_LOGIC;                      
    s_axil_control_wready : out STD_LOGIC;                       
    s_axil_control_bvalid : out STD_LOGIC;                       
    s_axil_control_bresp : out STD_LOGIC_VECTOR(1 DOWNTO 0);     
    s_axil_control_bready : in STD_LOGIC;                      
    s_axil_control_araddr : in STD_LOGIC_VECTOR(31 DOWNTO 0);  
    s_axil_control_arprot : in STD_LOGIC_VECTOR(2 DOWNTO 0);   
    s_axil_control_arvalid : in STD_LOGIC;                     
    s_axil_control_arready : out STD_LOGIC;                      
    s_axil_control_rdata : out STD_LOGIC_VECTOR(31 DOWNTO 0);    
    s_axil_control_rresp : out STD_LOGIC_VECTOR(1 DOWNTO 0);     
    s_axil_control_rvalid : out STD_LOGIC;                       
    s_axil_control_rready : in STD_LOGIC;   
    
                       
--    s_axil_control : axi_lite.slave;
--    s_axim_control :  axi_mm.slave;
    s_axim_control_awid : IN STD_LOGIC_VECTOR(3 DOWNTO 0);             
    s_axim_control_awaddr : IN STD_LOGIC_VECTOR(63 DOWNTO 0);          
    s_axim_control_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);            
    s_axim_control_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);           
    s_axim_control_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);          
    s_axim_control_awlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);           
    s_axim_control_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);          
    s_axim_control_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);           
    s_axim_control_awqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);            
    s_axim_control_awvalid : IN STD_LOGIC;                             
    s_axim_control_awready : OUT STD_LOGIC;                            
    s_axim_control_wdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);          
    s_axim_control_wstrb : IN STD_LOGIC_VECTOR(63 DOWNTO 0);           
    s_axim_control_wlast : IN STD_LOGIC;                               
    s_axim_control_wvalid : IN STD_LOGIC;                              
    s_axim_control_wready : OUT STD_LOGIC;                             
    s_axim_control_bready : IN STD_LOGIC;                              
    s_axim_control_bid : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);             
    s_axim_control_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);           
    s_axim_control_bvalid : OUT STD_LOGIC;                             
    s_axim_control_arid : IN STD_LOGIC_VECTOR(3 DOWNTO 0);             
    s_axim_control_araddr : IN STD_LOGIC_VECTOR(63 DOWNTO 0);          
    s_axim_control_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);            
    s_axim_control_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);           
    s_axim_control_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);          
    s_axim_control_arlock : IN STD_LOGIC_VECTOR(0 DOWNTO 0);           
    s_axim_control_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);          
    s_axim_control_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);           
    s_axim_control_arqos : IN STD_LOGIC_VECTOR(3 DOWNTO 0);            
    s_axim_control_arvalid : IN STD_LOGIC;                             
    s_axim_control_arready : OUT STD_LOGIC;                            
    s_axim_control_rready : IN STD_LOGIC;                              
    s_axim_control_rlast : OUT STD_LOGIC;                              
    s_axim_control_rvalid : OUT STD_LOGIC;                             
    s_axim_control_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);           
    s_axim_control_rid : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);             
    s_axim_control_rdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0); 
    
    m_axi_awid : out STD_LOGIC_VECTOR(0 DOWNTO 0);             
    m_axi_awaddr : out STD_LOGIC_VECTOR(31 DOWNTO 0);          
    m_axi_awlen : out STD_LOGIC_VECTOR(7 DOWNTO 0);            
    m_axi_awsize : out STD_LOGIC_VECTOR(2 DOWNTO 0);           
    m_axi_awburst : out STD_LOGIC_VECTOR(1 DOWNTO 0);          
    m_axi_awlock : out STD_LOGIC_VECTOR(0 DOWNTO 0);           
    m_axi_awcache : out STD_LOGIC_VECTOR(3 DOWNTO 0);          
    m_axi_awprot : out STD_LOGIC_VECTOR(2 DOWNTO 0);           
    m_axi_awqos : out STD_LOGIC_VECTOR(3 DOWNTO 0);            
    m_axi_awvalid : out STD_LOGIC;                             
    m_axi_awready : in STD_LOGIC;                            
    m_axi_wdata : out STD_LOGIC_VECTOR(511 DOWNTO 0);          
    m_axi_wstrb : out STD_LOGIC_VECTOR(63 DOWNTO 0);           
    m_axi_wlast : out STD_LOGIC;                               
    m_axi_wvalid : out STD_LOGIC;                              
    m_axi_wready : in STD_LOGIC;                             
    m_axi_bready : out STD_LOGIC;                              
    m_axi_bid : in STD_LOGIC_VECTOR(0 DOWNTO 0);             
    m_axi_bresp : in STD_LOGIC_VECTOR(1 DOWNTO 0);           
    m_axi_bvalid : in STD_LOGIC;                             
    m_axi_arid : out STD_LOGIC_VECTOR(0 DOWNTO 0);             
    m_axi_araddr : out STD_LOGIC_VECTOR(31 DOWNTO 0);          
    m_axi_arlen : out STD_LOGIC_VECTOR(7 DOWNTO 0);            
    m_axi_arsize : out STD_LOGIC_VECTOR(2 DOWNTO 0);           
    m_axi_arburst : out STD_LOGIC_VECTOR(1 DOWNTO 0);          
    m_axi_arlock : out STD_LOGIC_VECTOR(0 DOWNTO 0);           
    m_axi_arcache : out STD_LOGIC_VECTOR(3 DOWNTO 0);          
    m_axi_arprot : out STD_LOGIC_VECTOR(2 DOWNTO 0);           
    m_axi_arqos : out STD_LOGIC_VECTOR(3 DOWNTO 0);            
    m_axi_arvalid : out STD_LOGIC;                             
    m_axi_arready : in STD_LOGIC;                            
    m_axi_rready : out STD_LOGIC;                              
    m_axi_rlast : in STD_LOGIC;                              
    m_axi_rvalid : in STD_LOGIC;                             
    m_axi_rresp : in STD_LOGIC_VECTOR(1 DOWNTO 0);           
    m_axi_rid : in STD_LOGIC_VECTOR(0 DOWNTO 0);             
    m_axi_rdata : in STD_LOGIC_VECTOR(511 DOWNTO 0);       
      
    s_axis_qp_interface_valid    : in std_logic;          
    s_axis_qp_interface_ready    : out std_logic;                              
    s_axis_qp_interface_data     : in std_logic_vector(143 downto 0);
                             
    s_axis_qp_conn_interface_valid : in std_logic;                            
    s_axis_qp_conn_interface_ready : out std_logic;                           
    s_axis_qp_conn_interface_data  : in std_logic_vector(183 downto 0);    
    
    s_axis_roce_role_tx_meta_tvalid : in std_logic;                         
    s_axis_roce_role_tx_meta_tready : out std_logic;                        
    s_axis_roce_role_tx_meta_tdata  : in std_logic_vector(191 downto 0); 
       
    s_axis_roce_role_tx_data_tvalid : in std_logic;                         
    s_axis_roce_role_tx_data_tready : out std_logic;                        
    s_axis_roce_role_tx_data_tdata  : in std_logic_vector(511 downto 0);    
    s_axis_roce_role_tx_data_tkeep  : in std_logic_vector(63 downto 0);     
    s_axis_roce_role_tx_data_tlast  : in std_logic;    

--    m_axis_dma_c2h :axi_stream.master;
--    s_axis_dma_h2c : axi_stream.slave ;
    m_axis_dma_c2h_tvalid : out std_logic;                            
    m_axis_dma_c2h_tready : in std_logic;                             
    m_axis_dma_c2h_tdata  : out std_logic_vector(511 DOWNTO 0);       
    m_axis_dma_c2h_tkeep  : out std_logic_vector(63 downto 0);        
    m_axis_dma_c2h_tlast  : out std_logic;                            
    m_axis_dma_c2h_tdest  : out std_logic;                            
    s_axis_dma_h2c_tvalid : in std_logic;                             
    s_axis_dma_h2c_tready : out std_logic;                            
    s_axis_dma_h2c_tdata  : in std_logic_vector(511 downto 0);        
    s_axis_dma_h2c_tkeep  : in std_logic_vector(63 downto 0);         
    s_axis_dma_h2c_tlast  : in std_logic;                   
              
    ddr_calib_complete : in std_logic;
    
    c2h_dsc_byp_ready_0: in std_logic; 
    c2h_dsc_byp_addr_0: out std_logic_vector(63 downto 0); 
    c2h_dsc_byp_len_0: out std_logic_vector(31 downto 0); 
    c2h_dsc_byp_load_0 : out std_logic;     
                                    
    h2c_dsc_byp_ready_0 : in std_logic;     
    h2c_dsc_byp_addr_0: out std_logic_vector(63 downto 0); 
    h2c_dsc_byp_len_0: out std_logic_vector(31 downto 0);
    h2c_dsc_byp_load_0 : out std_logic;     
                                    
    c2h_sts_0 : in std_logic_vector(7 downto 0); 
    h2c_sts_0 : in std_logic_vector(7 downto 0);  
    
    s_axis_rx_tdata : in std_logic_vector(511 downto 0);
    s_axis_rx_tkeep : in std_logic_vector(63 downto 0);
    s_axis_rx_tlast : in std_logic;
    s_axis_rx_tuser : in std_logic_vector(0 downto 0);
    s_axis_rx_tvalid: in std_logic;
    s_axis_rx_tready: out std_logic;
    
    m_axis_tx_tdata : out std_logic_vector(511 downto 0);
    m_axis_tx_tkeep : out std_logic_vector(63 downto 0);
    m_axis_tx_tlast : out std_logic;
    m_axis_tx_tuser : out std_logic_vector(0 downto 0);
    m_axis_tx_tvalid: out std_logic;
    m_axis_tx_tdest : out std_logic;
    m_axis_tx_tready: in std_logic
    
  );
end os_wrapper_vhdl;

architecture Behavioral of os_wrapper_vhdl is
  -- Add parameters for block design so f = 200000232 when refreshing module
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER of m_axi_wdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 333250000";
  ATTRIBUTE X_INTERFACE_PARAMETER of s_axis_rx_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
  ATTRIBUTE X_INTERFACE_PARAMETER of m_axis_tx_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";


begin

os_wrapper_i : entity work.os_0
--generic map (
--  AXI_ID_WIDTH     => AXI_ID_WIDTH,
--  NUM_DDR_CHANNELS => NUM_DDR_CHANNELS,
--  ENABLE_DDR       => ENABLE_DDR
--)
port map (
  pcie_clk               => pcie_clk,
  pcie_aresetn           => pcie_aresetn,
  mem_clk                => mem_clk,
  mem_aresetn            => mem_aresetn,
  net_clk                => net_clk,
  net_aresetn            => net_aresetn,
  user_clk               => user_clk,
  user_aresetn           => user_aresetn,
  
  s_axil_control_awaddr  => s_axil_control_awaddr,
--  s_axil_control_awprot  => s_axil_control_awprot,
  s_axil_control_awvalid => s_axil_control_awvalid,
  s_axil_control_awready => s_axil_control_awready,
  s_axil_control_wdata   => s_axil_control_wdata,
  s_axil_control_wstrb   => s_axil_control_wstrb,
  s_axil_control_wvalid  => s_axil_control_wvalid,
  s_axil_control_wready  => s_axil_control_wready,
  s_axil_control_bvalid  => s_axil_control_bvalid,
  s_axil_control_bresp   => s_axil_control_bresp,
  s_axil_control_bready  => s_axil_control_bready,
  s_axil_control_araddr  => s_axil_control_araddr,
--  s_axil_control_arprot  => s_axil_control_arprot,
  s_axil_control_arvalid => s_axil_control_arvalid,
  s_axil_control_arready => s_axil_control_arready,
  s_axil_control_rdata   => s_axil_control_rdata,
  s_axil_control_rresp   => s_axil_control_rresp,
  s_axil_control_rvalid  => s_axil_control_rvalid,
  s_axil_control_rready  => s_axil_control_rready,
  s_axi_control_awid    => s_axim_control_awid,
  s_axi_control_awaddr  => s_axim_control_awaddr,
  s_axi_control_awlen   => s_axim_control_awlen,
  s_axi_control_awsize  => s_axim_control_awsize,
  s_axi_control_awburst => s_axim_control_awburst,
  s_axi_control_awlock  => s_axim_control_awlock,
  s_axi_control_awcache => s_axim_control_awcache,
  s_axi_control_awprot  => s_axim_control_awprot,
--  s_axi_control_awqos   => s_axim_control_awqos,
  s_axi_control_awvalid => s_axim_control_awvalid,
  s_axi_control_awready => s_axim_control_awready,
  s_axi_control_wdata   => s_axim_control_wdata,
  s_axi_control_wstrb   => s_axim_control_wstrb,
  s_axi_control_wlast   => s_axim_control_wlast,
  s_axi_control_wvalid  => s_axim_control_wvalid,
  s_axi_control_wready  => s_axim_control_wready,
  s_axi_control_bready  => s_axim_control_bready,
  s_axi_control_bid     => s_axim_control_bid,
  s_axi_control_bresp   => s_axim_control_bresp,
  s_axi_control_bvalid  => s_axim_control_bvalid,
  s_axi_control_arid    => s_axim_control_arid,
  s_axi_control_araddr  => s_axim_control_araddr,
  s_axi_control_arlen   => s_axim_control_arlen,
  s_axi_control_arsize  => s_axim_control_arsize,
  s_axi_control_arburst => s_axim_control_arburst,
  s_axi_control_arlock  => s_axim_control_arlock,
  s_axi_control_arcache => s_axim_control_arcache,
  s_axi_control_arprot  => s_axim_control_arprot,
--  s_axi_control_arqos   => s_axim_control_arqos,
  s_axi_control_arvalid => s_axim_control_arvalid,
  s_axi_control_arready => s_axim_control_arready,
  s_axi_control_rready  => s_axim_control_rready,
  s_axi_control_rlast   => s_axim_control_rlast,
  s_axi_control_rvalid  => s_axim_control_rvalid,
  s_axi_control_rresp   => s_axim_control_rresp,
  s_axi_control_rid     => s_axim_control_rid,
  s_axi_control_rdata   => s_axim_control_rdata,
  
  s_axis_qp_interface_valid      =>   s_axis_qp_interface_valid,
  s_axis_qp_interface_ready      =>   s_axis_qp_interface_ready,
  s_axis_qp_interface_data       =>   s_axis_qp_interface_data,
  s_axis_qp_conn_interface_valid =>   s_axis_qp_conn_interface_valid,
  s_axis_qp_conn_interface_ready =>   s_axis_qp_conn_interface_ready,
  s_axis_qp_conn_interface_data  =>   s_axis_qp_conn_interface_data,
  
  s_axis_roce_role_tx_meta_valid => s_axis_roce_role_tx_meta_tvalid,
  s_axis_roce_role_tx_meta_ready => s_axis_roce_role_tx_meta_tready,
  s_axis_roce_role_tx_meta_data  => s_axis_roce_role_tx_meta_tdata ,
  s_axis_roce_role_tx_data_valid => s_axis_roce_role_tx_data_tvalid,
  s_axis_roce_role_tx_data_ready => s_axis_roce_role_tx_data_tready,
  s_axis_roce_role_tx_data_data  => s_axis_roce_role_tx_data_tdata ,
  s_axis_roce_role_tx_data_keep  => s_axis_roce_role_tx_data_tkeep ,
  s_axis_roce_role_tx_data_last  => s_axis_roce_role_tx_data_tlast ,
  
  ddr_calib_complete => ddr_calib_complete,
  
  m_axi_awid             => m_axi_awid,
  m_axi_awaddr           => m_axi_awaddr,
  m_axi_awlen            => m_axi_awlen,
  m_axi_awsize           => m_axi_awsize,
  m_axi_awburst          => m_axi_awburst,
  m_axi_awlock           => m_axi_awlock,
  m_axi_awcache          => m_axi_awcache,
  m_axi_awprot           => m_axi_awprot,
--  m_axi_awqos            => m_axi_awqos,
  m_axi_awvalid          => m_axi_awvalid,
  m_axi_awready          => m_axi_awready,
  m_axi_wdata            => m_axi_wdata,
  m_axi_wstrb            => m_axi_wstrb,
  m_axi_wlast            => m_axi_wlast,
  m_axi_wvalid           => m_axi_wvalid,
  m_axi_wready           => m_axi_wready,
  m_axi_bready           => m_axi_bready,
  m_axi_bid              => m_axi_bid,
  m_axi_bresp            => m_axi_bresp,
  m_axi_bvalid           => m_axi_bvalid,
  m_axi_arid             => m_axi_arid,
  m_axi_araddr           => m_axi_araddr,
  m_axi_arlen            => m_axi_arlen,
  m_axi_arsize           => m_axi_arsize,
  m_axi_arburst          => m_axi_arburst,
  m_axi_arlock           => m_axi_arlock,
  m_axi_arcache          => m_axi_arcache,
  m_axi_arprot           => m_axi_arprot,
--  m_axi_arqos            => m_axi_arqos,
  m_axi_arvalid          => m_axi_arvalid,
  m_axi_arready          => m_axi_arready,
  m_axi_rready           => m_axi_rready,
  m_axi_rlast            => m_axi_rlast,
  m_axi_rvalid           => m_axi_rvalid,
  m_axi_rresp            => m_axi_rresp,
  m_axi_rid              => m_axi_rid,
  m_axi_rdata            => m_axi_rdata,
  
  m_axis_dma_c2h_valid =>   m_axis_dma_c2h_tvalid,
  m_axis_dma_c2h_ready =>   m_axis_dma_c2h_tready,
  m_axis_dma_c2h_data  =>   m_axis_dma_c2h_tdata,
  m_axis_dma_c2h_keep  =>   m_axis_dma_c2h_tkeep,
  m_axis_dma_c2h_last  =>   m_axis_dma_c2h_tlast,
  m_axis_dma_c2h_dest  =>   m_axis_dma_c2h_tdest,
  s_axis_dma_h2c_valid =>   s_axis_dma_h2c_tvalid,
  s_axis_dma_h2c_ready =>   s_axis_dma_h2c_tready,
  s_axis_dma_h2c_data  =>   s_axis_dma_h2c_tdata,
  s_axis_dma_h2c_keep  =>   s_axis_dma_h2c_tkeep,
  s_axis_dma_h2c_last  =>   s_axis_dma_h2c_tlast,
  
  c2h_dsc_byp_ready_0  =>   c2h_dsc_byp_ready_0,
  c2h_dsc_byp_addr_0   =>   c2h_dsc_byp_addr_0,
  c2h_dsc_byp_len_0    =>   c2h_dsc_byp_len_0,
  c2h_dsc_byp_load_0   =>   c2h_dsc_byp_load_0,
  
  h2c_dsc_byp_ready_0  =>   h2c_dsc_byp_ready_0,
  h2c_dsc_byp_addr_0   =>   h2c_dsc_byp_addr_0,
  h2c_dsc_byp_len_0    =>   h2c_dsc_byp_len_0,
  h2c_dsc_byp_load_0   =>   h2c_dsc_byp_load_0,
  
  c2h_sts_0            =>   c2h_sts_0,
  h2c_sts_0            =>   h2c_sts_0,

  s_axis_net_rx_valid   =>   s_axis_rx_tvalid,
  s_axis_net_rx_ready   => s_axis_rx_tready,
  s_axis_net_rx_data     => s_axis_rx_tdata,
  s_axis_net_rx_keep     => s_axis_rx_tkeep,
  s_axis_net_rx_last     => s_axis_rx_tlast,
  
  m_axis_net_tx_valid    => m_axis_tx_tvalid,
  m_axis_net_tx_ready    => m_axis_tx_tready,
  m_axis_net_tx_data     => m_axis_tx_tdata, 
  m_axis_net_tx_keep     => m_axis_tx_tkeep, 
  m_axis_net_tx_last     => m_axis_tx_tlast, 
  m_axis_net_tx_dest     => m_axis_tx_tdest
);




end Behavioral;
