library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL;
use work.crc32.all;
use work.utility.all;

------------------------------------------------------------------- 
-- Structure of the testbench entity
-- CMAC --> IB_Handler --> IP_Handler --> ARP_Server  --------------------> ARP_MIE_Merge --> Ethernet_Padding --> CMAC
--                                    --> RoCEv2 Core --> MAC_IP_Encode -->
-------------------------------------------------------------------   
    
entity tb_rdma_core is
end tb_rdma_core;

architecture tb_rdma_core of tb_rdma_core is
    constant clk_period : time := 10 ns;
    signal core_clk : std_logic;
    signal core_rst_n : std_logic;
    
    signal regInvalidPsnDropCount_V_ap_vld   : STD_LOGIC;
    signal regCrcDropPkgCount_V_ap_vld       : STD_LOGIC;
    
    signal s_axis_rx_data_TVALID             : STD_LOGIC;
    signal s_axis_rx_data_TREADY             : STD_LOGIC;
    signal s_axis_rx_data_TDATA,s_axis_rx_data_TDATA_rev              : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal s_axis_rx_data_TKEEP              : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal s_axis_rx_data_TLAST              : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal s_axis_cmac_TVALID                : STD_LOGIC;
    signal s_axis_cmac_TREADY                : STD_LOGIC;
    signal s_axis_cmac_TDATA                 : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal s_axis_cmac_TKEEP                 : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal s_axis_cmac_TLAST                 : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal s_axis_arp_TVALID                 : STD_LOGIC;
    signal s_axis_arp_TREADY                 : STD_LOGIC;
    signal s_axis_arp_TDATA                  : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal s_axis_arp_TKEEP                  : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal s_axis_arp_TLAST                  : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal m_axis_os_TVALID                 : STD_LOGIC;
    signal m_axis_os_TREADY                 : STD_LOGIC;
    signal m_axis_os_TDATA                  : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal m_axis_os_TKEEP                  : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_os_TLAST                  : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal m_axis_ibh_TVALID                 : STD_LOGIC;
    signal m_axis_ibh_TREADY                 : STD_LOGIC;
    signal m_axis_ibh_TDATA                  : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal m_axis_ibh_TKEEP                  : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_ibh_TLAST                  : STD_LOGIC_VECTOR(0 DOWNTO 0);   
    
    signal m_axis_ibh_tx_TVALID              : STD_LOGIC;
    signal m_axis_ibh_tx_TREADY              : STD_LOGIC;
    signal m_axis_ibh_tx_TDATA, m_axis_ibh_tx_TDATA_rev  : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal m_axis_ibh_tx_TKEEP               : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_ibh_tx_TLAST               : STD_LOGIC_VECTOR(0 DOWNTO 0);  
    signal m_axis_ibh_tx_TUSER               : STD_LOGIC_VECTOR(0 DOWNTO 0);  
    
    signal s_axis_tx_meta_V_TVALID           : STD_LOGIC;
    signal s_axis_tx_meta_V_TREADY           : STD_LOGIC;
    signal s_axis_tx_meta_V_TDATA            : STD_LOGIC_VECTOR(191 DOWNTO 0);
    signal s_axis_tx_data_TVALID             : STD_LOGIC;
    signal s_axis_tx_data_TREADY             : STD_LOGIC;
    signal s_axis_tx_data_TDATA              : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal s_axis_tx_data_TKEEP              : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal s_axis_tx_data_TLAST              : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal m_axis_tx_data_TVALID             : STD_LOGIC;
    signal m_axis_tx_data_TREADY             : STD_LOGIC;
    signal m_axis_tx_data_TDATA              : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal m_axis_tx_data_TKEEP              : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_tx_data_TLAST              : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal m_axis_mem_write_cmd_TVALID       : STD_LOGIC;
    signal m_axis_mem_write_cmd_TREADY       : STD_LOGIC;
    signal m_axis_mem_write_cmd_TDATA        : STD_LOGIC_VECTOR(95 DOWNTO 0);
    signal m_axis_mem_write_cmd_TDEST        : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal m_axis_mem_read_cmd_TVALID        : STD_LOGIC;
    signal m_axis_mem_read_cmd_TREADY        : STD_LOGIC;
    signal m_axis_mem_read_cmd_TDATA         : STD_LOGIC_VECTOR(95 DOWNTO 0);
    signal m_axis_mem_read_cmd_TDEST         : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal m_axis_mem_write_data_TVALID      : STD_LOGIC;
    signal m_axis_mem_write_data_TREADY      : STD_LOGIC;
    signal m_axis_mem_write_data_TDATA       : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal m_axis_mem_write_data_TKEEP       : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_mem_write_data_TLAST       : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal m_axis_mem_write_data_TDEST       : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal s_axis_mem_read_data_TVALID       : STD_LOGIC;
    signal s_axis_mem_read_data_TREADY       : STD_LOGIC;
    signal s_axis_mem_read_data_TDATA        : STD_LOGIC_VECTOR(511 DOWNTO 0);
    signal s_axis_mem_read_data_TKEEP        : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal s_axis_mem_read_data_TLAST        : STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal s_axis_qp_interface_V_TVALID      : STD_LOGIC;
    signal s_axis_qp_interface_V_TREADY      : STD_LOGIC;
    signal s_axis_qp_interface_V_TDATA       : STD_LOGIC_VECTOR(143 DOWNTO 0);
    signal s_axis_qp_conn_interface_V_TVALID : STD_LOGIC;
    signal s_axis_qp_conn_interface_V_TREADY : STD_LOGIC;
    signal s_axis_qp_conn_interface_V_TDATA  : STD_LOGIC_VECTOR(183 DOWNTO 0);
    signal local_ip_address_V                : STD_LOGIC_VECTOR(127 DOWNTO 0);
    signal regCrcDropPkgCount_V              : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal regInvalidPsnDropCount_V          : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal ap_clk                            : STD_LOGIC;
    signal ap_rst_n                          : STD_LOGIC;
    
    signal m_axis_tx_data_TDATA_rev          : STD_LOGIC_VECTOR(511 DOWNTO 0);
    
    signal m_axis_pad_tx_data_TVALID             : STD_LOGIC;
    signal m_axis_pad_tx_data_TREADY             : STD_LOGIC;
    signal m_axis_pad_tx_data_TDATA              : STD_LOGIC_VECTOR(511 downto 0);
    signal m_axis_pad_tx_data_TKEEP              : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_pad_tx_data_TLAST              : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal qpState   : std_logic_vector(2 downto 0);   
    signal qp_num    : std_logic_vector(23 downto 0);         
    signal remote_psn: std_logic_vector(23 downto 0);    
    signal local_psn : std_logic_vector(23 downto 0);      
    signal r_key     : std_logic_vector(15 downto 0);         
    signal vaddr     : std_logic_vector(47 downto 0);  
    
    signal local_qp  : std_logic_vector(15 downto 0); 
    signal remote_qp : std_logic_vector(23 downto 0);
    signal remote_ip : std_logic_vector(127 downto 0);
    signal remote_ud : std_logic_vector(15 downto 0); 
    signal  transfer_count : integer;
  
    COMPONENT rocev2_0
    PORT (
        regInvalidPsnDropCount_V_ap_vld : OUT STD_LOGIC;
        regCrcDropPkgCount_V_ap_vld : OUT STD_LOGIC;
        
        s_axis_rx_data_TVALID : IN STD_LOGIC;
        s_axis_rx_data_TREADY : OUT STD_LOGIC;
        s_axis_rx_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        s_axis_rx_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_rx_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        
        s_axis_tx_meta_V_TVALID : IN STD_LOGIC;
        s_axis_tx_meta_V_TREADY : OUT STD_LOGIC;
        s_axis_tx_meta_V_TDATA : IN STD_LOGIC_VECTOR(191 DOWNTO 0);
        
        s_axis_tx_data_TVALID : IN STD_LOGIC;
        s_axis_tx_data_TREADY : OUT STD_LOGIC;
        s_axis_tx_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        s_axis_tx_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_tx_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        
        m_axis_tx_data_TVALID : OUT STD_LOGIC;
        m_axis_tx_data_TREADY : IN STD_LOGIC;
        m_axis_tx_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_tx_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_tx_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        
        m_axis_mem_write_cmd_TVALID : OUT STD_LOGIC;
        m_axis_mem_write_cmd_TREADY : IN STD_LOGIC;
        m_axis_mem_write_cmd_TDATA : OUT STD_LOGIC_VECTOR(95 DOWNTO 0);
        m_axis_mem_write_cmd_TDEST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        
        m_axis_mem_read_cmd_TVALID : OUT STD_LOGIC;
        m_axis_mem_read_cmd_TREADY : IN STD_LOGIC;
        m_axis_mem_read_cmd_TDATA : OUT STD_LOGIC_VECTOR(95 DOWNTO 0);
        m_axis_mem_read_cmd_TDEST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        
        m_axis_mem_write_data_TVALID : OUT STD_LOGIC;
        m_axis_mem_write_data_TREADY : IN STD_LOGIC;
        m_axis_mem_write_data_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_mem_write_data_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_mem_write_data_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_mem_write_data_TDEST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        
        s_axis_mem_read_data_TVALID : IN STD_LOGIC;
        s_axis_mem_read_data_TREADY : OUT STD_LOGIC;
        s_axis_mem_read_data_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        s_axis_mem_read_data_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_mem_read_data_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        
        s_axis_qp_interface_V_TVALID : IN STD_LOGIC;
        s_axis_qp_interface_V_TREADY : OUT STD_LOGIC;
        s_axis_qp_interface_V_TDATA : IN STD_LOGIC_VECTOR(143 DOWNTO 0);
        
        s_axis_qp_conn_interface_V_TVALID : IN STD_LOGIC;
        s_axis_qp_conn_interface_V_TREADY : OUT STD_LOGIC;
        s_axis_qp_conn_interface_V_TDATA : IN STD_LOGIC_VECTOR(183 DOWNTO 0);
        
        local_ip_address_V : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
        regCrcDropPkgCount_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        regInvalidPsnDropCount_V : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        
        ap_clk : IN STD_LOGIC;
        ap_rst_n : IN STD_LOGIC
    );
    END COMPONENT;

    COMPONENT mac_ip_encode_0
    PORT (
        s_axis_ip_TVALID : IN STD_LOGIC;
        s_axis_ip_TREADY : OUT STD_LOGIC;
        s_axis_ip_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        s_axis_ip_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_ip_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axis_arp_lookup_reply_V_TVALID : IN STD_LOGIC;
        s_axis_arp_lookup_reply_V_TREADY : OUT STD_LOGIC;
        s_axis_arp_lookup_reply_V_TDATA : IN STD_LOGIC_VECTOR(55 DOWNTO 0);
        m_axis_ip_TVALID : OUT STD_LOGIC;
        m_axis_ip_TREADY : IN STD_LOGIC;
        m_axis_ip_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_ip_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_ip_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_arp_lookup_request_V_V_TVALID : OUT STD_LOGIC;
        m_axis_arp_lookup_request_V_V_TREADY : IN STD_LOGIC;
        m_axis_arp_lookup_request_V_V_TDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        myMacAddress_V : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
        regSubNetMask_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        regDefaultGateway_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        ap_clk : IN STD_LOGIC;
        ap_rst_n : IN STD_LOGIC
    );
    END COMPONENT;

    COMPONENT arp_server_subnet_0
    PORT (
        regRequestCount_V_ap_vld : OUT STD_LOGIC;
        regReplyCount_V_ap_vld : OUT STD_LOGIC;
        s_axis_TVALID : IN STD_LOGIC;
        s_axis_TREADY : OUT STD_LOGIC;
        s_axis_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        s_axis_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axis_arp_lookup_request_V_V_TVALID : IN STD_LOGIC;
        s_axis_arp_lookup_request_V_V_TREADY : OUT STD_LOGIC;
        s_axis_arp_lookup_request_V_V_TDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axis_host_arp_lookup_request_V_V_TVALID : IN STD_LOGIC;
        s_axis_host_arp_lookup_request_V_V_TREADY : OUT STD_LOGIC;
        s_axis_host_arp_lookup_request_V_V_TDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axis_TVALID : OUT STD_LOGIC;
        m_axis_TREADY : IN STD_LOGIC;
        m_axis_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_arp_lookup_reply_V_TVALID : OUT STD_LOGIC;
        m_axis_arp_lookup_reply_V_TREADY : IN STD_LOGIC;
        m_axis_arp_lookup_reply_V_TDATA : OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
        m_axis_host_arp_lookup_reply_V_TVALID : OUT STD_LOGIC;
        m_axis_host_arp_lookup_reply_V_TREADY : IN STD_LOGIC;
        m_axis_host_arp_lookup_reply_V_TDATA : OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
        myMacAddress_V : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
        myIpAddress_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        regRequestCount_V : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        regReplyCount_V : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        ap_clk : IN STD_LOGIC;
        ap_rst_n : IN STD_LOGIC
    );
    END COMPONENT;

    COMPONENT axi_datamover_0
    PORT (
        m_axi_mm2s_aclk : IN STD_LOGIC;
        m_axi_mm2s_aresetn : IN STD_LOGIC;
        mm2s_err : OUT STD_LOGIC;
        m_axis_mm2s_cmdsts_aclk : IN STD_LOGIC;
        m_axis_mm2s_cmdsts_aresetn : IN STD_LOGIC;
        s_axis_mm2s_cmd_tvalid : IN STD_LOGIC;
        s_axis_mm2s_cmd_tready : OUT STD_LOGIC;
        s_axis_mm2s_cmd_tdata : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
        m_axis_mm2s_sts_tvalid : OUT STD_LOGIC;
        m_axis_mm2s_sts_tready : IN STD_LOGIC;
        m_axis_mm2s_sts_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axis_mm2s_sts_tkeep : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_mm2s_sts_tlast : OUT STD_LOGIC;
        m_axi_mm2s_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axi_mm2s_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        m_axi_mm2s_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axi_mm2s_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_mm2s_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_mm2s_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
        m_axi_mm2s_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_mm2s_aruser : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        m_axi_mm2s_arvalid : OUT STD_LOGIC;
        m_axi_mm2s_arready : IN STD_LOGIC;
        m_axi_mm2s_rdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axi_mm2s_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        m_axi_mm2s_rlast : IN STD_LOGIC;
        m_axi_mm2s_rvalid : IN STD_LOGIC;
        m_axi_mm2s_rready : OUT STD_LOGIC;
        m_axis_mm2s_tdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_mm2s_tkeep : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_mm2s_tlast : OUT STD_LOGIC;
        m_axis_mm2s_tvalid : OUT STD_LOGIC;
        m_axis_mm2s_tready : IN STD_LOGIC
    );
    END COMPONENT;
    
    COMPONENT ethernet_frame_padding_512_0
      PORT (
        ap_clk : IN STD_LOGIC;
        ap_rst_n : IN STD_LOGIC;
        s_axis_TVALID : IN STD_LOGIC;
        s_axis_TREADY : OUT STD_LOGIC;
        s_axis_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        s_axis_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        s_axis_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_TVALID : OUT STD_LOGIC;
        m_axis_TREADY : IN STD_LOGIC;
        m_axis_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
      );
    END COMPONENT;
    
    COMPONENT axis_interconnect_0
    PORT (
        ACLK : IN STD_LOGIC;
        ARESETN : IN STD_LOGIC;
        S00_AXIS_ACLK : IN STD_LOGIC;
        S01_AXIS_ACLK : IN STD_LOGIC;
        S00_AXIS_ARESETN : IN STD_LOGIC;
        S01_AXIS_ARESETN : IN STD_LOGIC;
        S00_AXIS_TVALID : IN STD_LOGIC;
        S01_AXIS_TVALID : IN STD_LOGIC;
        S00_AXIS_TREADY : OUT STD_LOGIC;
        S01_AXIS_TREADY : OUT STD_LOGIC;
        S00_AXIS_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        S01_AXIS_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        S00_AXIS_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        S01_AXIS_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        S00_AXIS_TLAST : IN STD_LOGIC;
        S01_AXIS_TLAST : IN STD_LOGIC;
        M00_AXIS_ACLK : IN STD_LOGIC;
        M00_AXIS_ARESETN : IN STD_LOGIC;
        M00_AXIS_TVALID : OUT STD_LOGIC;
        M00_AXIS_TREADY : IN STD_LOGIC;
        M00_AXIS_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        M00_AXIS_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        M00_AXIS_TLAST : OUT STD_LOGIC;
        S00_ARB_REQ_SUPPRESS : IN STD_LOGIC;
        S01_ARB_REQ_SUPPRESS : IN STD_LOGIC
    );
    END COMPONENT;
    
    COMPONENT ip_handler_0
    PORT (
        s_axis_raw_TVALID : IN STD_LOGIC;
        s_axis_raw_TREADY : OUT STD_LOGIC;
        s_axis_raw_TDATA : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
        s_axis_raw_TKEEP : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_raw_TLAST : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_arp_TVALID : OUT STD_LOGIC;
        m_axis_arp_TREADY : IN STD_LOGIC;
        m_axis_arp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_arp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_arp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_icmpv6_TVALID : OUT STD_LOGIC;
        m_axis_icmpv6_TREADY : IN STD_LOGIC;
        m_axis_icmpv6_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_icmpv6_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_icmpv6_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_ipv6udp_TVALID : OUT STD_LOGIC;
        m_axis_ipv6udp_TREADY : IN STD_LOGIC;
        m_axis_ipv6udp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_ipv6udp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_ipv6udp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_icmp_TVALID : OUT STD_LOGIC;
        m_axis_icmp_TREADY : IN STD_LOGIC;
        m_axis_icmp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_icmp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_icmp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_udp_TVALID : OUT STD_LOGIC;
        m_axis_udp_TREADY : IN STD_LOGIC;
        m_axis_udp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_udp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_udp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_tcp_TVALID : OUT STD_LOGIC;
        m_axis_tcp_TREADY : IN STD_LOGIC;
        m_axis_tcp_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_tcp_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_tcp_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        m_axis_roce_TVALID : OUT STD_LOGIC;
        m_axis_roce_TREADY : IN STD_LOGIC;
        m_axis_roce_TDATA : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
        m_axis_roce_TKEEP : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_roce_TLAST : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
        myIpAddress_V : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        ap_clk : IN STD_LOGIC;
        ap_rst_n : IN STD_LOGIC
    );
    END COMPONENT;

    signal regRequestCount_V_ap_vld, regReplyCount_V_ap_vld : std_logic;
    signal s_axis_TVALID             : STD_LOGIC;
    signal s_axis_TREADY             : STD_LOGIC;
    signal s_axis_TDATA, s_axis_TDATA_rev : STD_LOGIC_VECTOR(511 downto 0);
    signal s_axis_TKEEP              : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal s_axis_TLAST              : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal m_axis_TVALID             : STD_LOGIC;
    signal m_axis_TREADY             : STD_LOGIC;
    signal m_axis_TDATA, m_axis_TDATA_rev  : STD_LOGIC_VECTOR(511 downto 0);
    signal m_axis_TKEEP              : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_TLAST              : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal m_axis_arp_TVALID             : STD_LOGIC;
    signal m_axis_arp_TREADY             : STD_LOGIC;
    signal m_axis_arp_TDATA, m_axis_arp_TDATA_rev  : STD_LOGIC_VECTOR(511 downto 0);
    signal m_axis_arp_TKEEP              : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_arp_TLAST              : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal m_axis_ip_TVALID             : STD_LOGIC;
    signal m_axis_ip_TREADY             : STD_LOGIC;
    signal m_axis_ip_TDATA, m_axis_ip_TDATA_rev : STD_LOGIC_VECTOR(511 downto 0);
    signal m_axis_ip_TKEEP             : STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m_axis_ip_TLAST          : STD_LOGIC_VECTOR(0 DOWNTO 0);
    
    signal s_axis_arp_lookup_request_V_V_TVALID : STD_LOGIC;                          
    signal s_axis_arp_lookup_request_V_V_TREADY :STD_LOGIC;                         
    signal s_axis_arp_lookup_request_V_V_TDATA : STD_LOGIC_VECTOR(31 DOWNTO 0);       
    signal s_axis_host_arp_lookup_request_V_V_TVALID : STD_LOGIC;                     
    signal s_axis_host_arp_lookup_request_V_V_TREADY : STD_LOGIC;                    
    signal s_axis_host_arp_lookup_request_V_V_TDATA : STD_LOGIC_VECTOR(31 DOWNTO 0);  
    
    signal m_axis_arp_lookup_reply_V_TVALID : STD_LOGIC;                          
    signal m_axis_arp_lookup_reply_V_TREADY : STD_LOGIC;                           
    signal m_axis_arp_lookup_reply_V_TDATA : STD_LOGIC_VECTOR(55 DOWNTO 0);       
    signal m_axis_host_arp_lookup_reply_V_TVALID : STD_LOGIC;                     
    signal m_axis_host_arp_lookup_reply_V_TREADY : STD_LOGIC;                      
    signal m_axis_host_arp_lookup_reply_V_TDATA :  STD_LOGIC_VECTOR(55 DOWNTO 0);   
    
    signal myMacAddress_V : STD_LOGIC_VECTOR(47 DOWNTO 0) := reverse_bytes(x"248a07b8385a");                  
    signal myIpAddress_V : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"20a0a8c0";                   
    signal regRequestCount_V :  STD_LOGIC_VECTOR(15 DOWNTO 0);              
    signal regReplyCount_V :  STD_LOGIC_VECTOR(15 DOWNTO 0);  
    signal regSubNetMask_V : std_logic_vector(31 downto 0) := reverse_bytes(x"ffffff00");
    signal regDefaultGateway_V : std_logic_vector(31 downto 0) :=x"0aa0a8c0";
    
    signal cmd_data_s : std_logic_vector(71 downto 0) := (others => '0');
    signal trigger    : std_logic;
    signal data_ibh, data_ibh_rev : std_logic_vector(511 downto 0);

begin
    
    ------------------------------------------------------------------- 
    -- Ensures the Ethernet frames contain a miminum amount of 64 valid bytes
    -- Last four bytes won't be padded because the CRC32 will be placed there by the CMAC
    -------------------------------------------------------------------    
    eth_pad : ethernet_frame_padding_512_0
    port map (
      ap_clk        => core_clk ,       
      ap_rst_n      => core_rst_n,     
      s_axis_TDATA  => m_axis_TDATA,      
      s_axis_TVALID => m_axis_TVALID,     
      s_axis_TREADY => m_axis_TREADY,     
      s_axis_TKEEP  => m_axis_TKEEP,      
      s_axis_TLAST  => m_axis_TLAST,      
        
      m_axis_TDATA  => m_axis_pad_tx_data_TDATA,  
      m_axis_TVALID => m_axis_pad_tx_data_TVALID,
      m_axis_TREADY => m_axis_pad_tx_data_TREADY,
      m_axis_TKEEP  => m_axis_pad_tx_data_TKEEP,  
      m_axis_TLAST  => m_axis_pad_tx_data_TLAST
    );
    
    m_axis_TDATA_rev <= reverse_bytes(m_axis_TDATA);
    
    ------------------------------------------------------------------- 
    -- Stores/provides the MAC addresses of remote IP targets
    ------------------------------------------------------------------- 
    arp_sever : arp_server_subnet_0
    PORT MAP (
        regRequestCount_V_ap_vld => regRequestCount_V_ap_vld,
        regReplyCount_V_ap_vld => regReplyCount_V_ap_vld,
        s_axis_TVALID => s_axis_arp_TVALID, 
        s_axis_TREADY => s_axis_arp_TREADY, 
        s_axis_TDATA  => s_axis_arp_TDATA,  
        s_axis_TKEEP  => s_axis_arp_TKEEP,  
        s_axis_TLAST  => s_axis_arp_TLAST,  
        s_axis_arp_lookup_request_V_V_TVALID => s_axis_arp_lookup_request_V_V_TVALID,
        s_axis_arp_lookup_request_V_V_TREADY => s_axis_arp_lookup_request_V_V_TREADY,
        s_axis_arp_lookup_request_V_V_TDATA => s_axis_arp_lookup_request_V_V_TDATA,
        s_axis_host_arp_lookup_request_V_V_TVALID => s_axis_host_arp_lookup_request_V_V_TVALID,
        s_axis_host_arp_lookup_request_V_V_TREADY => s_axis_host_arp_lookup_request_V_V_TREADY,
        s_axis_host_arp_lookup_request_V_V_TDATA => s_axis_host_arp_lookup_request_V_V_TDATA,
        m_axis_TVALID => m_axis_arp_TVALID,
        m_axis_TREADY => m_axis_arp_TREADY,
        m_axis_TDATA =>  m_axis_arp_TDATA,
        m_axis_TKEEP =>  m_axis_arp_TKEEP,
        m_axis_TLAST =>  m_axis_arp_TLAST,
        m_axis_arp_lookup_reply_V_TVALID => m_axis_arp_lookup_reply_V_TVALID,
        m_axis_arp_lookup_reply_V_TREADY => m_axis_arp_lookup_reply_V_TREADY,
        m_axis_arp_lookup_reply_V_TDATA => m_axis_arp_lookup_reply_V_TDATA,
        m_axis_host_arp_lookup_reply_V_TVALID => m_axis_host_arp_lookup_reply_V_TVALID,
        m_axis_host_arp_lookup_reply_V_TREADY => m_axis_host_arp_lookup_reply_V_TREADY,
        m_axis_host_arp_lookup_reply_V_TDATA => m_axis_host_arp_lookup_reply_V_TDATA,
        myMacAddress_V => myMacAddress_V,
        myIpAddress_V => myIpAddress_V,
        regRequestCount_V => regRequestCount_V,
        regReplyCount_V => regReplyCount_V,
        ap_clk => core_clk,
        ap_rst_n => core_rst_n 
    );
    
    ------------------------------------------------------------------- 
    -- Accepts IP frames and adds the stored MAC adress, so a complete Ethernet frame is transmitted
    -------------------------------------------------------------------   
    mac_ip : mac_ip_encode_0
    PORT MAP (
        s_axis_ip_TVALID => m_axis_tx_data_TVALID, 
        s_axis_ip_TREADY => m_axis_tx_data_TREADY, 
        s_axis_ip_TDATA => m_axis_tx_data_TDATA,   
        s_axis_ip_TKEEP => m_axis_tx_data_TKEEP,   
        s_axis_ip_TLAST => m_axis_tx_data_TLAST,   
        s_axis_arp_lookup_reply_V_TVALID => m_axis_arp_lookup_reply_V_TVALID,     
        s_axis_arp_lookup_reply_V_TREADY => m_axis_arp_lookup_reply_V_TREADY,     
        s_axis_arp_lookup_reply_V_TDATA => m_axis_arp_lookup_reply_V_TDATA,       
        m_axis_ip_TVALID => m_axis_ip_TVALID,
        m_axis_ip_TREADY => m_axis_ip_TREADY,
        m_axis_ip_TDATA  => m_axis_ip_TDATA,
        m_axis_ip_TKEEP  => m_axis_ip_TKEEP,
        m_axis_ip_TLAST  => m_axis_ip_TLAST,
        m_axis_arp_lookup_request_V_V_TVALID => s_axis_arp_lookup_request_V_V_TVALID,      
        m_axis_arp_lookup_request_V_V_TREADY => s_axis_arp_lookup_request_V_V_TREADY,      
        m_axis_arp_lookup_request_V_V_TDATA => s_axis_arp_lookup_request_V_V_TDATA,        
        myMacAddress_V => myMacAddress_V,
        regSubNetMask_V => regSubNetMask_V,
        regDefaultGateway_V => regDefaultGateway_V,
        ap_clk => core_clk,
        ap_rst_n => core_rst_n
    );
    
    ------------------------------------------------------------------- 
    -- Test to obtain data from DDR memory (Convert AXI-Stream to AXI-MemoryMapped)
    -- Not necessary to use yet since data will be provided through the RoCEv2 s_axis_tx_data_x interface
    -------------------------------------------------------------------     
    mem_data : axi_datamover_0
    PORT MAP (
        m_axi_mm2s_aclk => core_clk,
        m_axi_mm2s_aresetn => core_rst_n,
        mm2s_err => open,
        m_axis_mm2s_cmdsts_aclk => core_clk,
        m_axis_mm2s_cmdsts_aresetn => core_rst_n,
        
        s_axis_mm2s_cmd_tvalid => m_axis_mem_read_cmd_TVALID,
        s_axis_mm2s_cmd_tready => m_axis_mem_read_cmd_TREADY,
        s_axis_mm2s_cmd_tdata => cmd_data_s,
        
        m_axis_mm2s_sts_tvalid => open,
        m_axis_mm2s_sts_tready => '1',
        m_axis_mm2s_sts_tdata => open,
        m_axis_mm2s_sts_tkeep => open,
        m_axis_mm2s_sts_tlast => open,
        
        m_axi_mm2s_arid    => open,
        m_axi_mm2s_araddr  => open,
        m_axi_mm2s_arlen   => open,
        m_axi_mm2s_arsize  => open,
        m_axi_mm2s_arburst => open,
        m_axi_mm2s_arprot  => open,
        m_axi_mm2s_arcache => open,
        m_axi_mm2s_aruser  => open,
        m_axi_mm2s_arvalid => open,
        m_axi_mm2s_arready => '1',
        m_axi_mm2s_rdata   => (others =>  '1'),
        m_axi_mm2s_rresp   => "00",
        m_axi_mm2s_rlast   => '1',
        m_axi_mm2s_rvalid  => '1',
        m_axi_mm2s_rready  => open,
        m_axis_mm2s_tdata  => open,
        m_axis_mm2s_tkeep  => open,
        m_axis_mm2s_tlast  => open,
        m_axis_mm2s_tvalid => open,
        m_axis_mm2s_tready => '1'
    );
    cmd_data_s(63 downto 32) <= m_axis_mem_read_cmd_TDATA(31 downto 0);
    cmd_data_s(7 downto 0) <= x"39";
    
    ------------------------------------------------------------------- 
    -- Merge the AXI streams from the MAC_IP encoder and ARP server
    -------------------------------------------------------------------         
    merge_arp_mie : axis_interconnect_0
    PORT MAP (
        ACLK => core_clk,
        ARESETN => core_rst_n,
        S00_AXIS_ACLK => core_clk,
        S01_AXIS_ACLK => core_clk,
        S00_AXIS_ARESETN => core_rst_n,
        S01_AXIS_ARESETN => core_rst_n,
        M00_AXIS_ACLK    => core_clk,
        M00_AXIS_ARESETN => core_rst_n,
        
        --MAC IP encoder output
        S00_AXIS_TVALID => m_axis_ip_TVALID,
        S00_AXIS_TREADY => m_axis_ip_TREADY,
        S00_AXIS_TDATA  => m_axis_ip_TDATA, 
        S00_AXIS_TKEEP  => m_axis_ip_TKEEP, 
        S00_AXIS_TLAST  => m_axis_ip_TLAST(0), 
        
        --ARP output
        S01_AXIS_TVALID => m_axis_arp_TVALID,   
        S01_AXIS_TREADY => m_axis_arp_TREADY,   
        S01_AXIS_TDATA  => m_axis_arp_TDATA,    
        S01_AXIS_TKEEP  => m_axis_arp_TKEEP,    
        S01_AXIS_TLAST  => m_axis_arp_TLAST(0),    
        
        M00_AXIS_TVALID => m_axis_TVALID,
        M00_AXIS_TREADY => m_axis_TREADY,
        M00_AXIS_TDATA  => m_axis_TDATA,
        M00_AXIS_TKEEP  => m_axis_TKEEP, 
        M00_AXIS_TLAST  => m_axis_TLAST(0),
        S00_ARB_REQ_SUPPRESS => '0',
        S01_ARB_REQ_SUPPRESS => '0'
    ); 
    
    ------------------------------------------------------------------- 
    -- Direct IP packets to the right component and strip the MAC layer
    -------------------------------------------------------------------   
    ip_splitter : ip_handler_0
    PORT MAP (
        -- Incoming frames
        s_axis_raw_TVALID => m_axis_os_TVALID,    
        s_axis_raw_TREADY => open,
        s_axis_raw_TDATA  => m_axis_os_TDATA,     
        s_axis_raw_TKEEP  => m_axis_os_TKEEP,     
        s_axis_raw_TLAST  => m_axis_os_TLAST,                       
        
        m_axis_arp_TVALID => s_axis_arp_TVALID,
        m_axis_arp_TREADY => s_axis_arp_TREADY,
        m_axis_arp_TDATA  => s_axis_arp_TDATA,
        m_axis_arp_TKEEP  => s_axis_arp_TKEEP,
        m_axis_arp_TLAST  => s_axis_arp_TLAST,
        
        m_axis_icmpv6_TVALID => open,
        m_axis_icmpv6_TREADY => '0',
        m_axis_icmpv6_TDATA  => open,
        m_axis_icmpv6_TKEEP  => open,
        m_axis_icmpv6_TLAST  => open,
        m_axis_ipv6udp_TVALID => open,
        m_axis_ipv6udp_TREADY => '0',
        m_axis_ipv6udp_TDATA  => open,
        m_axis_ipv6udp_TKEEP  => open,
        m_axis_ipv6udp_TLAST  => open,
        m_axis_icmp_TVALID => open,
        m_axis_icmp_TREADY => '0',
        m_axis_icmp_TDATA  => open,
        m_axis_icmp_TKEEP  => open,
        m_axis_icmp_TLAST  => open,
        m_axis_udp_TVALID => open,
        m_axis_udp_TREADY => '0',
        m_axis_udp_TDATA  => open,
        m_axis_udp_TKEEP  => open,
        m_axis_udp_TLAST  => open,
        m_axis_tcp_TVALID => open,
        m_axis_tcp_TREADY => '0',
        m_axis_tcp_TDATA  => open,
        m_axis_tcp_TKEEP  => open,
        m_axis_tcp_TLAST  => open,
        
        m_axis_roce_TVALID => m_axis_ibh_TVALID,   
        m_axis_roce_TREADY => m_axis_ibh_TREADY,                
        m_axis_roce_TDATA  => m_axis_ibh_TDATA,     
        m_axis_roce_TKEEP  => m_axis_ibh_TKEEP,     
        m_axis_roce_TLAST  => m_axis_ibh_TLAST,     
        
        myIpAddress_V => local_ip_address_V(31 downto 0),
        ap_clk => core_clk,
        ap_rst_n => core_rst_n
    );
     
    ------------------------------------------------------------------- 
    -- The RoCEv2 HLS core
    -------------------------------------------------------------------    
    roce_core : rocev2_0
    PORT MAP (
        regInvalidPsnDropCount_V_ap_vld => regInvalidPsnDropCount_V_ap_vld,
        regCrcDropPkgCount_V_ap_vld => regCrcDropPkgCount_V_ap_vld,
        
        -- Receive RDMA IP frames
        s_axis_rx_data_TVALID => m_axis_ibh_TVALID,
        s_axis_rx_data_TREADY => m_axis_ibh_TREADY,
        s_axis_rx_data_TDATA  => m_axis_ibh_TDATA, 
        s_axis_rx_data_TKEEP  => m_axis_ibh_TKEEP, 
        s_axis_rx_data_TLAST  => m_axis_ibh_TLAST, 
        
        -- RDMA read/write instruction bus
        s_axis_tx_meta_V_TVALID => s_axis_tx_meta_V_TVALID,
        s_axis_tx_meta_V_TREADY => s_axis_tx_meta_V_TREADY,
        s_axis_tx_meta_V_TDATA  => s_axis_tx_meta_V_TDATA,
        
        -- Send data with RDMA (e.g. data for RDMA Write)
        s_axis_tx_data_TVALID => s_axis_tx_data_TVALID,
        s_axis_tx_data_TREADY => s_axis_tx_data_TREADY,
        s_axis_tx_data_TDATA  => s_axis_tx_data_TDATA,
        s_axis_tx_data_TKEEP  => s_axis_tx_data_TKEEP,
        s_axis_tx_data_TLAST  => s_axis_tx_data_TLAST,
        
        -- Transmit RDMA IP frames
        m_axis_tx_data_TVALID => m_axis_tx_data_TVALID,
        m_axis_tx_data_TREADY => m_axis_tx_data_TREADY,
        m_axis_tx_data_TDATA  => m_axis_tx_data_TDATA,
        m_axis_tx_data_TKEEP  => m_axis_tx_data_TKEEP,
        m_axis_tx_data_TLAST  => m_axis_tx_data_TLAST,
        
        -- Commands to write or read data to/from DDR memory
        m_axis_mem_write_cmd_TVALID => m_axis_mem_write_cmd_TVALID,
        m_axis_mem_write_cmd_TREADY => m_axis_mem_write_cmd_TREADY,
        m_axis_mem_write_cmd_TDATA  => m_axis_mem_write_cmd_TDATA,
        m_axis_mem_write_cmd_TDEST  => m_axis_mem_write_cmd_TDEST,
        
        m_axis_mem_read_cmd_TVALID => m_axis_mem_read_cmd_TVALID,
        m_axis_mem_read_cmd_TREADY => m_axis_mem_read_cmd_TREADY,
        m_axis_mem_read_cmd_TDATA  => m_axis_mem_read_cmd_TDATA,
        m_axis_mem_read_cmd_TDEST  => m_axis_mem_read_cmd_TDEST,
        
        -- Data that is written to or read from memory (accompanied by command for address info etc.)
        m_axis_mem_write_data_TVALID => m_axis_mem_write_data_TVALID,
        m_axis_mem_write_data_TREADY => m_axis_mem_write_data_TREADY,
        m_axis_mem_write_data_TDATA  => m_axis_mem_write_data_TDATA,
        m_axis_mem_write_data_TKEEP  => m_axis_mem_write_data_TKEEP,
        m_axis_mem_write_data_TLAST  => m_axis_mem_write_data_TLAST,
        m_axis_mem_write_data_TDEST  => m_axis_mem_write_data_TDEST,
        
        s_axis_mem_read_data_TVALID => s_axis_mem_read_data_TVALID,
        s_axis_mem_read_data_TREADY => s_axis_mem_read_data_TREADY,
        s_axis_mem_read_data_TDATA  => s_axis_mem_read_data_TDATA,
        s_axis_mem_read_data_TKEEP  => s_axis_mem_read_data_TKEEP,
        s_axis_mem_read_data_TLAST  => s_axis_mem_read_data_TLAST,
        
        -- Configure the QP interface (Initializing QP's)
        s_axis_qp_interface_V_TVALID => s_axis_qp_interface_V_TVALID,
        s_axis_qp_interface_V_TREADY => s_axis_qp_interface_V_TREADY,
        s_axis_qp_interface_V_TDATA  => s_axis_qp_interface_V_TDATA,
        
        -- Manage QP configuration (Changing the configs of specifig QP's)
        s_axis_qp_conn_interface_V_TVALID => s_axis_qp_conn_interface_V_TVALID,
        s_axis_qp_conn_interface_V_TREADY => s_axis_qp_conn_interface_V_TREADY,
        s_axis_qp_conn_interface_V_TDATA  => s_axis_qp_conn_interface_V_TDATA,
        
        local_ip_address_V => local_ip_address_V,
        regCrcDropPkgCount_V => regCrcDropPkgCount_V,
        regInvalidPsnDropCount_V => regInvalidPsnDropCount_V,
        ap_clk => core_clk,
        ap_rst_n => core_rst_n
    );
    
    local_ip_address_V(31 downto 0) <= myIpAddress_V;
    local_ip_address_V(127 downto 32) <= (others => '0'); 
    
    ------------------------------------------------------------------- 
    -- Infiniband vhdl core; handles only connect requests and rdma send messages
    ------------------------------------------------------------------- 
    ibh : entity work.ib_handler
    port map (
      core_clk                        => core_clk,
      core_rst_n                      => core_rst_n,
      con_start                       => '0',
      qp_connected                    => open,
      rem_psn                         => open,
      rem_qpn                         => open,
      rem_ip                          => open,
      rem_mac                         => open,

      -- Set up connection when RDMA ConnectRequest is received
      s_axis_qp_interface_tvalid      => s_axis_qp_interface_V_TVALID,
      s_axis_qp_interface_tready      => s_axis_qp_interface_V_TREADY,
      s_axis_qp_interface_tdata       => s_axis_qp_interface_V_TDATA, 
      
      -- Configure/Update the QP when RDMA Send received
      s_axis_qp_conn_interface_tvalid => s_axis_qp_conn_interface_V_TVALID, 
      s_axis_qp_conn_interface_tready => s_axis_qp_conn_interface_V_TREADY, 
      s_axis_qp_conn_interface_tdata  => s_axis_qp_conn_interface_V_TDATA,  
      
      s_axis_roce_role_tx_meta_tvalid => s_axis_tx_meta_V_TVALID,  
      s_axis_roce_role_tx_meta_tready => s_axis_tx_meta_V_TREADY,  
      s_axis_roce_role_tx_meta_tdata  => s_axis_tx_meta_V_TDATA,   
      s_axis_roce_role_tx_data_tvalid => s_axis_tx_data_TVALID,  
      s_axis_roce_role_tx_data_tready => s_axis_tx_data_TREADY,  
      s_axis_roce_role_tx_data_tdata  => s_axis_tx_data_TDATA,   
      s_axis_roce_role_tx_data_tkeep  => s_axis_tx_data_TKEEP,   
      s_axis_roce_role_tx_data_tlast  => s_axis_tx_data_TLAST(0),   
      
      -- Passthrough to RoCEv2 core
      m_axis_os_data_tvalid           => m_axis_os_TVALID, 
      m_axis_os_data_tdata            => m_axis_os_TDATA,  
      m_axis_os_data_tkeep            => m_axis_os_TKEEP,  
      m_axis_os_data_tlast            => m_axis_os_TLAST(0),  
                                         
      cmac_rx_axis_tdata              => s_axis_rx_data_tdata,
      cmac_rx_axis_tkeep              => s_axis_rx_data_tkeep,
      cmac_rx_axis_tlast              => s_axis_rx_data_tlast(0),
      cmac_rx_axis_tuser              => "0",
      cmac_rx_axis_tvalid             => s_axis_rx_data_tvalid,
      
      cmac_tx_axis_tdata              => m_axis_ibh_tx_tdata,
      cmac_tx_axis_tkeep              => m_axis_ibh_tx_tkeep,
      cmac_tx_axis_tlast              => m_axis_ibh_tx_tlast(0),
      cmac_tx_axis_tuser              => m_axis_ibh_tx_tuser,
      cmac_tx_axis_tvalid             => m_axis_ibh_tx_tvalid
    );
    --Reverse data word bytes for easy copy/paste in tools
    m_axis_ibh_tx_tdata_rev <= reverse_bytes(m_axis_ibh_tx_tdata);
    
    ------------------------------------------------------------------- 
    -- Enable all ready signals 
    -------------------------------------------------------------------    
    m_axis_mem_write_cmd_TREADY <= '1';
    m_axis_mem_read_cmd_TREADY <= '1';
    m_axis_mem_write_data_TREADY <= '1';
    
    ------------------------------------------------------------------- 
    -- Old static values
    -------------------------------------------------------------------    
--    -- QP interface configuration
--    s_axis_qp_interface_V_TDATA(2 downto 0)   <= qpState;
--    s_axis_qp_interface_V_TDATA(26 downto 3)  <= qp_num;
--    s_axis_qp_interface_V_TDATA(50 downto 27) <= remote_psn;
--    s_axis_qp_interface_V_TDATA(74 downto 51) <= local_psn;
--    s_axis_qp_interface_V_TDATA(90 downto 75) <= r_key;
--    s_axis_qp_interface_V_TDATA(138 downto 91) <= vaddr;
--    s_axis_qp_interface_V_TDATA(143 downto 139) <= (others => '0');
    
--    -- QP connection configuration
--    s_axis_qp_conn_interface_V_TDATA(15 downto 0)    <= local_qp;
--    s_axis_qp_conn_interface_V_TDATA(39 downto 16)   <= remote_qp;
--    s_axis_qp_conn_interface_V_TDATA(167 downto 40)  <= remote_ip;
--    s_axis_qp_conn_interface_V_TDATA(183 downto 168) <= remote_ud;
    
     -- Static qp interface config
    --qpState    <= "010";
    --qp_num     <= x"000003";
    --remote_psn <= x"80ce4e";
    --local_psn  <= x"80ce4e";
    --r_key      <= x"0411";
    --vaddr(47 downto 5)      <= (others => '0');
    --vaddr(4 downto 0)      <= (others => '1');
    
    -- Static qp conn config
--    local_qp <= x"0003"; 
--    remote_qp <= x"000011";
--    remote_ip (95 downto 0) <= (others => '0');
--    remote_ip (127 downto 96) <= x"0aa0a8c0";
--    remote_ud <= x"0000";
    
        
    -- TX data config
    --s_axis_tx_meta_V_TDATA(2 downto 0) <= "001"; --APP_WRITE
    --s_axis_tx_meta_V_TDATA(26 downto 3) <= x"000003";--rem_qpn; --QPN
    --s_axis_tx_meta_V_TDATA(154 downto 123) <= x"12ad4521"; --rkey;
    --s_axis_tx_meta_V_TDATA(185 downto 154) <= x"00000580"; --length;
    --s_axis_tx_meta_V_TDATA(191 downto 186) <= (others => '0');
    
    m_axis_tx_data_TDATA_rev <= reverse_bytes(m_axis_tx_data_TDATA);
    s_axis_rx_data_TDATA_rev <= reverse_bytes(s_axis_rx_data_TDATA);
    m_axis_ip_TDATA_rev      <= reverse_bytes(m_axis_ip_TDATA);
    

    ------------------------------------------------------------------- 
    -- Read from DDR memory, not used yet
    -------------------------------------------------------------------     
    readdata : process (core_clk)
      variable count : integer := 0;
    begin
      if rising_edge (core_clk) then
        if core_rst_n = '0' then
          s_axis_mem_read_data_TVALID <= '0';
          s_axis_mem_read_data_TDATA  <= (others => '0');
          s_axis_mem_read_data_TKEEP  <=(others => '0') ;
          s_axis_mem_read_data_TLAST  <= "0";
          count := 0;
        else
          s_axis_mem_read_data_TKEEP  <=(others => '1') ;
          s_axis_mem_read_data_TLAST  <= "0";
          
          if count = 0 then
            s_axis_mem_read_data_TVALID <= '1';
            if s_axis_mem_read_data_TREADY = '1' then
              count := 1;
              s_axis_mem_read_data_TLAST  <= "0";
            end if;
            
          elsif count = 20 and s_axis_mem_read_data_TREADY = '1' then
              s_axis_mem_read_data_TLAST  <= "1";
              count := count + 1;
              
          elsif count = 21 and s_axis_mem_read_data_TREADY = '1'then
              s_axis_mem_read_data_TVALID <= '0';
              s_axis_mem_read_data_TLAST  <= "0";
              count := count + 1;
              
          elsif count = 22 then --and s_axis_tx_data_TREADY = '1' then
            count := 0;
            
          elsif s_axis_mem_read_data_TREADY = '1' then
            count := count + 1;
          end if;  
          
          s_axis_mem_read_data_TDATA  <= (others => '1');
          s_axis_mem_read_data_TDATA(511 downto 504)  <= std_logic_vector(to_unsigned(count,8));    
        end if;
      end if;
    end process;
    
    ------------------------------------------------------------------- 
    -- Reset the cores
    ------------------------------------------------------------------- 
    process
    begin
        core_rst_n <=  '0';
        wait for CLK_PERIOD * 30;
        core_rst_n <=  '1';
        wait;
    end process;
    
    ------------------------------------------------------------------- 
    -- Generate clocks
    ------------------------------------------------------------------- 
    Clk_process :process
    begin
        core_clk <= '1';
        wait for CLK_PERIOD/2;  --for half of clock period clk stays at '0'.
        core_clk <= '0';
        wait for CLK_PERIOD/2;  --for next half of clock period clk stays at '1'.
    end process;
    ------------------------------------------------------------------- 
    -- Simulate the server side wich responds to the FPGA
    ------------------------------------------------------------------- 
    server : process (core_clk) 
        variable start_count, count : integer := 0;
        variable start, arp : std_logic := '0';
    begin 
        if core_rst_n = '0' then
            s_axis_rx_data_tdata <= (others => '0');
            s_axis_rx_data_tkeep <= (others => '0');
            s_axis_rx_data_tvalid <= '0';
            s_axis_rx_data_tlast  <= "0";
        elsif rising_edge(core_clk) then
            s_axis_rx_data_tvalid <= '0';
            -- First set up a connection by transmitting a ConnectRequest
            if arp = '0' then
                s_axis_rx_data_tdata <= reverse_bytes(x"ffffffffffff_2076175c5e9a_08060001080006040001_2076175c5e9a_c0a8a00a_000000000000_c0a8a020_00000000000000000000000000000000000000000000");
                s_axis_rx_data_tvalid <= '1';
                s_axis_rx_data_tkeep(40 downto 0)   <= (others => '1');
                s_axis_rx_data_tkeep(63 downto 41)  <= (others => '0');
                s_axis_rx_data_tlast  <= "1";
                arp := '1';
            elsif arp = '1' and start = '0' then 
                s_axis_rx_data_tkeep<= (others => '1');
                s_axis_rx_data_tvalid <= '1';
                s_axis_rx_data_tlast  <= "0";
                
                if start_count = 0 then
                    --s_axis_rx_data_tdata <= reverse_bytes(x"248a07b8385a_2076175c5e9a_08004502003c2f2d400040114a06c0a8a00ac0a8a020d68912b7002800000440ffff00000003801925da00000000025200200009");
                    s_axis_rx_data_tdata <= reverse_bytes(x"2076175c5e9a248a07b8385a08004502013473564000401104e5c0a8a00ac0a8a020d54012b7012000006440ffff000000010000008c80010000000000010107");
                    start_count := start_count + 1;
                elsif start_count = 1 then    
                    s_axis_rx_data_tdata <= reverse_bytes(x"020300000000000000020dbaef06001000000000000006efba0d000000000000000001061c06248a070300b8385a000000000000000000012801000000010000");
                    start_count := start_count + 1;
                elsif start_count = 2 then
                    s_axis_rx_data_tdata <= reverse_bytes(x"00b021ebfeb7ffff37f0ffffffff00000000000000000000ffffc0a8a00a00000000000000000000ffffc0a8a020000000070040009800000000000000000000");
                    start_count := start_count + 1;
                elsif start_count = 3 then
                    s_axis_rx_data_tdata <= reverse_bytes(x"0000000000000000000000000000000000000000000000000000000000000000000000408946000000000000000000000000c0a8a00a00000000000000000000");
                    start_count := start_count + 1;
                elsif start_count = 4 then
                    s_axis_rx_data_tdata <= reverse_bytes(x"0000c0a8a0200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f0ba");
                    start_count := start_count + 1;
                elsif start_count = 5 then
                    s_axis_rx_data_tdata <= reverse_bytes(x"24a30000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
                    s_axis_rx_data_tkeep(1 downto 0)  <= (others => '1');
                    s_axis_rx_data_tkeep(63 downto 2) <= (others => '0');
                    s_axis_rx_data_tvalid <= '1';
                    s_axis_rx_data_tlast  <= "1";
                    start := '1';
                end if;
            else
            -- Every 20 cycles a RDMA Send is sent to indicate a RDMA Write message can be transmitted
                count := count + 1;
                if count = 40 then
                    --s_axis_rx_data_tdata <= reverse_bytes(x"248a07b8385a_2076175c5e9a_08004502003C735F4000401105D4_C0A8A00A_C0A8A020_DD7512B7002800000440FFFF00000003801925DA0000000000A53C900007");
                    s_axis_rx_data_tdata <= reverse_bytes(x"248a07b8385a_2076175c5e9a_08004502003c2f2d400040114a06c0a8a00ac0a8a020d68912b7002800000440ffff00000003801925da00000000025200200009");
                    s_axis_rx_data_tvalid <= '1'; 
                    s_axis_rx_data_tkeep  <= (others => '1');         
                    s_axis_rx_data_tlast  <= "0";
                elsif count = 41 then
                    s_axis_rx_data_tdata <= reverse_bytes(x"030D0000004099908948000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
                    s_axis_rx_data_tvalid <= '1';           
                    s_axis_rx_data_tkeep(9 downto 0)   <= (others => '1');
                    s_axis_rx_data_tkeep(63 downto 10) <= (others => '0');
                    s_axis_rx_data_tlast  <= "1";           
                    count := 0;                             
                end if;                                     
            end if;                                         
            -- Respond to ARP requests from the FPGA ffffffffffff2076175c5e9a08060001080006040001248a07b8385a_c0a8a00a_000000000000_c0a8a020_00000000000000000000000000000000000000000000
            if m_axis_tvalid = '1' and m_axis_tdata_rev = x"ffffffffffff248a07b8385a08060001080006040001248a07b8385ac0a8a020000000000000c0a8a00a00000000000000000000000000000000000000000000" then
                s_axis_rx_data_tdata <= reverse_bytes(x"248a07b8385a_2076175c5e9a_080600010800060400022076175c5e9ac0a8a00a248a07b8385ac0a8a02000000000000000000000000000000000000000000000");
                s_axis_rx_data_tvalid <= '1';
                s_axis_rx_data_tlast  <= "1";
            end if;
        end if;
    end process;

end architecture;
