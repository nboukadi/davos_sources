----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/21/2019 02:49:13 PM
-- Design Name: 
-- Module Name: rdma_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity rdma_top is
  port (
    --sys_clk_p : in std_logic;
    --sys_clk_n : in std_logic;
    
    -- 100 MHz clock
    ddr4_clk_p : in std_logic;
    ddr4_clk_n : in std_logic;
    
    -- 100 MHz clock
    qdr4_clk_p : in std_logic;
    qdr4_clk_n : in std_logic;
    
    -- 156,25 MHz clock 
    qsfp4_clock_p : in std_logic;
    qsfp4_clock_n : in std_logic;
    sys_rst : in std_logic;
    
    sys_clk_n : in STD_LOGIC;
    sys_clk_p : in STD_LOGIC;
--    pcie_rxn  : in STD_LOGIC_VECTOR ( 7 downto 0 );
--    pcie_rxp  : in STD_LOGIC_VECTOR ( 7 downto 0 );
--    pcie_txn  : out STD_LOGIC_VECTOR ( 7 downto 0 );
--    pcie_txp  : out STD_LOGIC_VECTOR ( 7 downto 0 );
--    perst_n    : in STD_LOGIC;
    
    -- QSFP4 data signals
    gt_rx_0_n : in std_logic;
    gt_rx_0_p : in std_logic;
    gt_rx_1_n : in std_logic;
    gt_rx_1_p : in std_logic;
    gt_rx_2_n : in std_logic;
    gt_rx_2_p : in std_logic;
    gt_rx_3_n : in std_logic;
    gt_rx_3_p : in std_logic;
    
    gt_tx_0_n : out std_logic;
    gt_tx_0_p : out std_logic;
    gt_tx_1_n : out std_logic;
    gt_tx_1_p : out std_logic;
    gt_tx_2_n : out std_logic;
    gt_tx_2_p : out std_logic;
    gt_tx_3_n : out std_logic;
    gt_tx_3_p : out std_logic;
    
    i2c0_scl_ls : inout std_logic;
    i2c0_sda_ls : inout std_logic;
    
    uart_0_rxd : in std_logic;
    uart_0_txd : out std_logic;
    
    qsfp4_intl_ls    : in std_logic; 
    qsfp4_modskll_ls : out std_logic; -- Module select (wrong doc. should be modsell)
    qsfp4_resetl_ls  : out std_logic; -- Reset
    qsfp4_modprsl_ls : in std_logic; -- Module present (wrong doc. is input not output)
    qsfp4_lpmode_ls  : out std_logic; -- Low power mode 
    
    ddr4_act_n : out std_logic;
    ddr4_adr   : out std_logic_vector ( 13 downto 0 );
    ddr4_ba    : out std_logic_vector ( 1 downto 0 );
    ddr4_bg    : out std_logic_vector ( 0 downto 0 );
    ddr4_ck_c  : out std_logic_vector ( 0 downto 0 );
    ddr4_ck_t  : out std_logic_vector ( 0 downto 0 );
    ddr4_cke   : out std_logic_vector ( 0 downto 0 );
    ddr4_cs_n  : out std_logic_vector ( 1 downto 0 );
    ddr4_dm_n  : inout std_logic_vector ( 8 downto 0 );
    ddr4_dq    : inout std_logic_vector ( 71 downto 0 );
    ddr4_dqs_c : inout std_logic_vector ( 8 downto 0 );
    ddr4_dqs_t : inout std_logic_vector ( 8 downto 0 );
    ddr4_odt   : out std_logic_vector ( 0 downto 0 );
    ddr4_reset_n:out std_logic;
    ddr4_we_b  : out std_logic; --A14
    ddr4_cas_b : out std_logic; --A15
    ddr4_ras_b : out std_logic --A16
  );
end rdma_top;



architecture STRUCTURE of rdma_top is
   signal drp_clk, sys_rst_n: std_logic;
   signal clk_200 : std_logic;
   signal ddr4_clk : std_logic;
   signal ddr4_adr_1 : std_logic_vector(16 downto 0);
 
  component design_1_wrapper
  port (
    clk_100MHz : in STD_LOGIC;
    reset : in STD_LOGIC;
    clk_out_200 : out STD_LOGIC;
    clk_out_200_rst : out STD_LOGIC_vector(0 to 0);
    ddr_clk : in STD_LOGIC;
    gt_rx_0_gt_port_0_n : in STD_LOGIC;
    gt_rx_0_gt_port_0_p : in STD_LOGIC;
    gt_rx_0_gt_port_1_n : in STD_LOGIC;
    gt_rx_0_gt_port_1_p : in STD_LOGIC;
    gt_rx_0_gt_port_2_n : in STD_LOGIC;
    gt_rx_0_gt_port_2_p : in STD_LOGIC;
    gt_rx_0_gt_port_3_n : in STD_LOGIC;
    gt_rx_0_gt_port_3_p : in STD_LOGIC;
    ddr4_out_act_n : out STD_LOGIC;
    ddr4_out_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
    ddr4_out_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_out_bg : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_ck_c : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_ck_t : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_cs_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_out_dm_n : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
    ddr4_out_dqs_c : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_dqs_t : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_reset_n : out STD_LOGIC;
    gt_tx_0_gt_port_0_n : out STD_LOGIC;
    gt_tx_0_gt_port_0_p : out STD_LOGIC;
    gt_tx_0_gt_port_1_n : out STD_LOGIC;
    gt_tx_0_gt_port_1_p : out STD_LOGIC;
    gt_tx_0_gt_port_2_n : out STD_LOGIC;
    gt_tx_0_gt_port_2_p : out STD_LOGIC;
    gt_tx_0_gt_port_3_n : out STD_LOGIC;
    gt_tx_0_gt_port_3_p : out STD_LOGIC;
    qsfp4_156mhz_clk_n : in STD_LOGIC;
    qsfp4_156mhz_clk_p : in STD_LOGIC;
    cmac_rx_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_rx_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_rx_tlast : out STD_LOGIC;
    cmac_rx_tready : in STD_LOGIC;
    cmac_rx_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_rx_tvalid : out STD_LOGIC;
    cmac_tx_0_tvalid : in STD_LOGIC;
    cmac_tx_0_tready : out STD_LOGIC;
    cmac_tx_0_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_tx_0_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_tx_0_tlast : in STD_LOGIC;
    cmac_tx_0_tdest : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_0_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_1_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_tx_1_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_tx_1_tlast : in STD_LOGIC;
    cmac_tx_1_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_1_tvalid : in STD_LOGIC;
        mem_m_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_m_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_arid : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    mem_m_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_arready : out STD_LOGIC;
    mem_m_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_arvalid : in STD_LOGIC;
    mem_m_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_m_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_awid : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    mem_m_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_awready : out STD_LOGIC;
    mem_m_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_awvalid : in STD_LOGIC;
    mem_m_axi_bid : out STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_bready : in STD_LOGIC;
    mem_m_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_bvalid : out STD_LOGIC;
    mem_m_axi_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    mem_m_axi_rid : out STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_rlast : out STD_LOGIC;
    mem_m_axi_rready : in STD_LOGIC;
    mem_m_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_rvalid : out STD_LOGIC;
    mem_m_axi_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    mem_m_axi_wlast : in STD_LOGIC;
    mem_m_axi_wready : out STD_LOGIC;
    mem_m_axi_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
    mem_m_axi_wvalid : in STD_LOGIC;
    mem_clk : out STD_LOGIC;
    mem_calib : out STD_LOGIC;
    mem_reset : out STD_LOGIC_VECTOR ( 0 to 0 )
--    pcie_rx_n_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
--    pcie_tx_p_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
--    pcie_rx_p_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
--    pcie_tx_n_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
--    pcie_ref_clk_0 : in STD_LOGIC;
--    pcie_ref_clk_gt_0 : in STD_LOGIC;
--    perst_n_0 : in STD_LOGIC
  );
  end component;
 
   COMPONENT ila_0
  PORT (
	clk : IN STD_LOGIC;
	probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
   );
  END COMPONENT  ;
  signal sys_clk_gt, sys_clk : std_logic;
  signal cmac_rx_tdata : std_logic_vector(511 downto 0);
  signal clk_200_rst : std_logic;
  signal cmac_rx_tkeep : std_logic_vector(63 downto 0);
  signal cmac_rx_tlast : std_logic;
  signal cmac_rx_tuser : std_logic_vector(0 downto 0);
  signal cmac_rx_tvalid : std_logic;
  signal ibh_crc_axis_TDATA : std_logic_vector(511 downto 0);
  signal ibh_crc_axis_TKEEP : std_logic_vector(63 downto 0);
  signal ibh_crc_axis_TLAST : std_logic;
  signal ibh_crc_axis_TUSER : std_logic_vector(0 downto 0);
  signal ibh_crc_axis_TVALID : std_logic;
  signal m_axis_os_data_TDATA : std_logic_vector(511 downto 0);
  signal m_axis_os_data_TKEEP : std_logic_vector(63 downto 0);
  signal m_axis_os_data_TLAST : std_logic;
  signal m_axis_os_data_TVALID : std_logic;
  signal s_axis_qp_conn_interface_TDATA : std_logic_vector(183 downto 0);
  signal s_axis_qp_conn_interface_TREADY : std_logic;
  signal s_axis_qp_conn_interface_TVALID : std_logic;
  signal s_axis_qp_interface_TDATA : std_logic_vector(143 downto 0);
  signal s_axis_qp_interface_TREADY : std_logic;
  signal s_axis_qp_interface_TVALID : std_logic;
  signal s_axis_roce_role_tx_data_TDATA : std_logic_vector(511 downto 0);
  signal s_axis_roce_role_tx_data_TKEEP : std_logic_vector(63 downto 0);
  signal s_axis_roce_role_tx_data_TLAST : std_logic;
  signal s_axis_roce_role_tx_data_TREADY : std_logic;
  signal s_axis_roce_role_tx_data_TVALID : std_logic;
  signal s_axis_roce_role_tx_meta_TDATA : std_logic_vector(191 downto 0);
  signal s_axis_roce_role_tx_meta_TREADY : std_logic;
  signal s_axis_roce_role_tx_meta_TVALID : std_logic;
  signal icrc32_out_axis_TDATA : std_logic_vector(511 downto 0);
  signal icrc32_out_axis_TKEEP : std_logic_vector(63 downto 0);
  signal icrc32_out_axis_TLAST : std_logic;
  signal icrc32_out_axis_TUSER : std_logic_vector(0 downto 0);
  signal icrc32_out_axis_TVALID : std_logic;
  signal clk_out_200_rst : std_logic;
  signal cmac_rx_tready : STD_LOGIC;
  signal cmac_tx_0_tdata : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal cmac_tx_0_tdest : STD_LOGIC_VECTOR ( 0 to 0 );
  signal cmac_tx_0_tkeep : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal cmac_tx_0_tlast : STD_LOGIC;
  signal cmac_tx_0_tready : STD_LOGIC;
  signal cmac_tx_0_tuser : STD_LOGIC_VECTOR ( 0 to 0 );
  signal cmac_tx_0_tvalid : STD_LOGIC;
  signal cmac_tx_1_tdata : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal cmac_tx_1_tkeep : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal cmac_tx_1_tlast : STD_LOGIC;
  signal cmac_tx_1_tuser : STD_LOGIC_VECTOR ( 0 to 0 );
  signal cmac_tx_1_tvalid : STD_LOGIC;
  signal mem_calib : STD_LOGIC;
  signal mem_clk : STD_LOGIC;
  signal mem_reset : std_logic;
  signal mem_m_axi_ARADDR : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal mem_m_axi_ARBURST : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal mem_m_axi_ARCACHE : STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal mem_m_axi_ARID : STD_LOGIC_VECTOR(0 DOWNTO 0);
  signal mem_m_axi_ARLEN : STD_LOGIC_VECTOR(7 DOWNTO 0);
  signal mem_m_axi_ARLOCK : STD_LOGIC_VECTOR(0 DOWNTO 0);
  signal mem_m_axi_ARPROT : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal mem_m_axi_ARQOS : STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal mem_m_axi_ARREADY : STD_LOGIC;
  signal mem_m_axi_ARSIZE : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal mem_m_axi_ARVALID : STD_LOGIC;
  signal mem_m_axi_AWADDR : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal mem_m_axi_AWBURST : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal mem_m_axi_AWCACHE : STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal mem_m_axi_AWID : STD_LOGIC_VECTOR(0 DOWNTO 0);
  signal mem_m_axi_AWLEN : STD_LOGIC_VECTOR(7 DOWNTO 0);
  signal mem_m_axi_AWLOCK : STD_LOGIC_VECTOR(0 DOWNTO 0);
  signal mem_m_axi_AWPROT : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal mem_m_axi_AWQOS : STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal mem_m_axi_AWREADY : STD_LOGIC;
  signal mem_m_axi_AWSIZE : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal mem_m_axi_AWVALID : STD_LOGIC;
  signal mem_m_axi_BID : STD_LOGIC_VECTOR(0 DOWNTO 0);
  signal mem_m_axi_BREADY : STD_LOGIC;
  signal mem_m_axi_BRESP : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal mem_m_axi_BVALID : STD_LOGIC;
  signal mem_m_axi_RDATA : STD_LOGIC_VECTOR(511 DOWNTO 0);
  signal mem_m_axi_RID : STD_LOGIC_VECTOR(0 DOWNTO 0);
  signal mem_m_axi_RLAST : STD_LOGIC;
  signal mem_m_axi_RREADY : STD_LOGIC;
  signal mem_m_axi_RRESP : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal mem_m_axi_RVALID : STD_LOGIC;
  signal mem_m_axi_WDATA : STD_LOGIC_VECTOR(511 DOWNTO 0);
  signal mem_m_axi_WLAST : STD_LOGIC;
  signal mem_m_axi_WREADY : STD_LOGIC;
  signal mem_m_axi_WSTRB : STD_LOGIC_VECTOR(63 DOWNTO 0);
  signal mem_m_axi_WVALID : STD_LOGIC;
  signal os_wrapper_m_axis_tx_TDATA : std_logic_vector(511 downto 0);
  signal os_wrapper_m_axis_tx_TDEST : std_logic;
  signal os_wrapper_m_axis_tx_TKEEP : std_logic_vector(63 downto 0);
  signal os_wrapper_m_axis_tx_TLAST : std_logic;
  signal os_wrapper_m_axis_tx_TREADY : std_logic;
  signal os_wrapper_m_axis_tx_TUSER : std_logic_vector(0 downto 0);
  signal os_wrapper_m_axis_tx_TVALID : std_logic;
  
--    component icrc32 is
--  port (
--    core_clk : in STD_LOGIC;
--    core_rst_n : in STD_LOGIC;
--    crc_in_axis_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
--    crc_in_axis_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
--    crc_in_axis_tlast : in STD_LOGIC;
--    crc_in_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
--    crc_in_axis_tvalid : in STD_LOGIC;
--    rev_out_axis_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    rev_out_axis_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    rev_out_axis_tlast : out STD_LOGIC;
--    rev_out_axis_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
--    rev_out_axis_tvalid : out STD_LOGIC;
--    crc_out_axis_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    crc_out_axis_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    crc_out_axis_tlast : out STD_LOGIC;
--    crc_out_axis_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
--    crc_out_axis_tvalid : out STD_LOGIC
--  );
--  end component icrc32;
  
--  component IB_handler is
--  port (
--    core_clk : in STD_LOGIC;
--    core_rst_n : in STD_LOGIC;
--    con_start : in STD_LOGIC;
--    qp_connected : out STD_LOGIC;
--    rem_psn : out STD_LOGIC_VECTOR ( 23 downto 0 );
--    rem_qpn : out STD_LOGIC_VECTOR ( 23 downto 0 );
--    rem_ip : out STD_LOGIC_VECTOR ( 31 downto 0 );
--    rem_mac : out STD_LOGIC_VECTOR ( 23 downto 0 );
--    s_axis_qp_interface_tvalid : out STD_LOGIC;
--    s_axis_qp_interface_tready : in STD_LOGIC;
--    s_axis_qp_interface_tdata : out STD_LOGIC_VECTOR ( 143 downto 0 );
--    s_axis_qp_conn_interface_tvalid : out STD_LOGIC;
--    s_axis_qp_conn_interface_tready : in STD_LOGIC;
--    s_axis_qp_conn_interface_tdata : out STD_LOGIC_VECTOR ( 183 downto 0 );
--    s_axis_roce_role_tx_meta_tvalid : out STD_LOGIC;
--    s_axis_roce_role_tx_meta_tready : in STD_LOGIC;
--    s_axis_roce_role_tx_meta_tdata : out STD_LOGIC_VECTOR ( 191 downto 0 );
--    s_axis_roce_role_tx_data_tvalid : out STD_LOGIC;
--    s_axis_roce_role_tx_data_tready : in STD_LOGIC;
--    s_axis_roce_role_tx_data_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    s_axis_roce_role_tx_data_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    s_axis_roce_role_tx_data_tlast : out STD_LOGIC;
--    m_axis_os_data_tvalid : out STD_LOGIC;
--    m_axis_os_data_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    m_axis_os_data_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    m_axis_os_data_tlast : out STD_LOGIC;
--    cmac_rx_axis_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
--    cmac_rx_axis_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
--    cmac_rx_axis_tlast : in STD_LOGIC;
--    cmac_rx_axis_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
--    cmac_rx_axis_tvalid : in STD_LOGIC;
--    cmac_tx_axis_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    cmac_tx_axis_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    cmac_tx_axis_tlast : out STD_LOGIC;
--    cmac_tx_axis_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
--    cmac_tx_axis_tvalid : out STD_LOGIC
--  );
--  end component IB_handler;
  
--  component os_wrapper_vhdl is
--  port (
--    pcie_clk : in STD_LOGIC;
--    pcie_aresetn : in STD_LOGIC;
--    mem_clk : in STD_LOGIC;
--    mem_aresetn : in STD_LOGIC;
--    net_clk : in STD_LOGIC;
--    net_aresetn : in STD_LOGIC;
--    user_clk : out STD_LOGIC;
--    user_aresetn : out STD_LOGIC;
--    s_axil_control_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
--    s_axil_control_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
--    s_axil_control_awvalid : in STD_LOGIC;
--    s_axil_control_awready : out STD_LOGIC;
--    s_axil_control_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
--    s_axil_control_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axil_control_wvalid : in STD_LOGIC;
--    s_axil_control_wready : out STD_LOGIC;
--    s_axil_control_bvalid : out STD_LOGIC;
--    s_axil_control_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
--    s_axil_control_bready : in STD_LOGIC;
--    s_axil_control_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
--    s_axil_control_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
--    s_axil_control_arvalid : in STD_LOGIC;
--    s_axil_control_arready : out STD_LOGIC;
--    s_axil_control_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
--    s_axil_control_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
--    s_axil_control_rvalid : out STD_LOGIC;
--    s_axil_control_rready : in STD_LOGIC;
--    s_axim_control_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axim_control_awaddr : in STD_LOGIC_VECTOR ( 63 downto 0 );
--    s_axim_control_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
--    s_axim_control_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
--    s_axim_control_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
--    s_axim_control_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
--    s_axim_control_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axim_control_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
--    s_axim_control_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axim_control_awvalid : in STD_LOGIC;
--    s_axim_control_awready : out STD_LOGIC;
--    s_axim_control_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
--    s_axim_control_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
--    s_axim_control_wlast : in STD_LOGIC;
--    s_axim_control_wvalid : in STD_LOGIC;
--    s_axim_control_wready : out STD_LOGIC;
--    s_axim_control_bready : in STD_LOGIC;
--    s_axim_control_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axim_control_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
--    s_axim_control_bvalid : out STD_LOGIC;
--    s_axim_control_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axim_control_araddr : in STD_LOGIC_VECTOR ( 63 downto 0 );
--    s_axim_control_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
--    s_axim_control_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
--    s_axim_control_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
--    s_axim_control_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
--    s_axim_control_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axim_control_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
--    s_axim_control_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axim_control_arvalid : in STD_LOGIC;
--    s_axim_control_arready : out STD_LOGIC;
--    s_axim_control_rready : in STD_LOGIC;
--    s_axim_control_rlast : out STD_LOGIC;
--    s_axim_control_rvalid : out STD_LOGIC;
--    s_axim_control_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
--    s_axim_control_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
--    s_axim_control_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    m_axi_awid : out STD_LOGIC_VECTOR ( 0 to 0 );
--    m_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
--    m_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
--    m_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
--    m_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
--    m_axi_awlock : out STD_LOGIC_VECTOR ( 0 to 0 );
--    m_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
--    m_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
--    m_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
--    m_axi_awvalid : out STD_LOGIC;
--    m_axi_awready : in STD_LOGIC;
--    m_axi_wdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    m_axi_wstrb : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    m_axi_wlast : out STD_LOGIC;
--    m_axi_wvalid : out STD_LOGIC;
--    m_axi_wready : in STD_LOGIC;
--    m_axi_bready : out STD_LOGIC;
--    m_axi_bid : in STD_LOGIC_VECTOR ( 0 to 0 );
--    m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
--    m_axi_bvalid : in STD_LOGIC;
--    m_axi_arid : out STD_LOGIC_VECTOR ( 0 to 0 );
--    m_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
--    m_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
--    m_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
--    m_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
--    m_axi_arlock : out STD_LOGIC_VECTOR ( 0 to 0 );
--    m_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
--    m_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
--    m_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
--    m_axi_arvalid : out STD_LOGIC;
--    m_axi_arready : in STD_LOGIC;
--    m_axi_rready : out STD_LOGIC;
--    m_axi_rlast : in STD_LOGIC;
--    m_axi_rvalid : in STD_LOGIC;
--    m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
--    m_axi_rid : in STD_LOGIC_VECTOR ( 0 to 0 );
--    m_axi_rdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
--    s_axis_qp_interface_valid : in STD_LOGIC;
--    s_axis_qp_interface_ready : out STD_LOGIC;
--    s_axis_qp_interface_data : in STD_LOGIC_VECTOR ( 143 downto 0 );
--    s_axis_qp_conn_interface_valid : in STD_LOGIC;
--    s_axis_qp_conn_interface_ready : out STD_LOGIC;
--    s_axis_qp_conn_interface_data : in STD_LOGIC_VECTOR ( 183 downto 0 );
--    s_axis_roce_role_tx_meta_tvalid : in STD_LOGIC;
--    s_axis_roce_role_tx_meta_tready : out STD_LOGIC;
--    s_axis_roce_role_tx_meta_tdata : in STD_LOGIC_VECTOR ( 159 downto 0 );
--    s_axis_roce_role_tx_data_tvalid : in STD_LOGIC;
--    s_axis_roce_role_tx_data_tready : out STD_LOGIC;
--    s_axis_roce_role_tx_data_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
--    s_axis_roce_role_tx_data_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
--    s_axis_roce_role_tx_data_tlast : in STD_LOGIC;
--    m_axis_dma_c2h_tvalid : out STD_LOGIC;
--    m_axis_dma_c2h_tready : in STD_LOGIC;
--    m_axis_dma_c2h_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    m_axis_dma_c2h_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    m_axis_dma_c2h_tlast : out STD_LOGIC;
--    m_axis_dma_c2h_tdest : out STD_LOGIC;
--    s_axis_dma_h2c_tvalid : in STD_LOGIC;
--    s_axis_dma_h2c_tready : out STD_LOGIC;
--    s_axis_dma_h2c_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
--    s_axis_dma_h2c_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
--    s_axis_dma_h2c_tlast : in STD_LOGIC;
--    ddr_calib_complete : in STD_LOGIC;
--    c2h_dsc_byp_ready_0 : in STD_LOGIC;
--    c2h_dsc_byp_addr_0 : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    c2h_dsc_byp_len_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
--    c2h_dsc_byp_load_0 : out STD_LOGIC;
--    h2c_dsc_byp_ready_0 : in STD_LOGIC;
--    h2c_dsc_byp_addr_0 : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    h2c_dsc_byp_len_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
--    h2c_dsc_byp_load_0 : out STD_LOGIC;
--    c2h_sts_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
--    h2c_sts_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
--    s_axis_rx_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
--    s_axis_rx_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
--    s_axis_rx_tlast : in STD_LOGIC;
--    s_axis_rx_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
--    s_axis_rx_tvalid : in STD_LOGIC;
--    s_axis_rx_tready : out STD_LOGIC;
--    m_axis_tx_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
--    m_axis_tx_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
--    m_axis_tx_tlast : out STD_LOGIC;
--    m_axis_tx_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
--    m_axis_tx_tvalid : out STD_LOGIC;
--    m_axis_tx_tdest : out STD_LOGIC;
--    m_axis_tx_tready : in STD_LOGIC
--  );
--  end component os_wrapper_vhdl;
  
  
  
  
  
  
  
begin

  os_wrapper_vhdl_0 : entity work.os_wrapper_vhdl
     port map (
      c2h_dsc_byp_addr_0 => open,
      c2h_dsc_byp_len_0 =>  open,
      c2h_dsc_byp_load_0 =>  open,
      c2h_dsc_byp_ready_0 => '0',
      c2h_sts_0(7 downto 0) => B"00000000",
      ddr_calib_complete => mem_calib,
      h2c_dsc_byp_addr_0 =>  open,
      h2c_dsc_byp_len_0 =>  open,
      h2c_dsc_byp_load_0 =>  open,
      h2c_dsc_byp_ready_0 => '0',
      h2c_sts_0(7 downto 0) => B"00000000",
      
      m_axi_araddr => mem_m_axi_ARADDR,
      m_axi_arburst => mem_m_axi_ARBURST,
      m_axi_arcache => mem_m_axi_ARCACHE,
      m_axi_arid => mem_m_axi_ARID,
      m_axi_arlen=> mem_m_axi_ARLEN,
      m_axi_arlock => mem_m_axi_ARLOCK,
      m_axi_arprot => mem_m_axi_ARPROT,
      m_axi_arqos => mem_m_axi_ARQOS,
      m_axi_arready => mem_m_axi_ARREADY,
      m_axi_arsize => mem_m_axi_ARSIZE,
      m_axi_arvalid => mem_m_axi_ARVALID,
      m_axi_awaddr => mem_m_axi_AWADDR,
      m_axi_awburst => mem_m_axi_AWBURST,
      m_axi_awcache => mem_m_axi_AWCACHE,
      m_axi_awid => mem_m_axi_AWID,
      m_axi_awlen => mem_m_axi_AWLEN,
      m_axi_awlock => mem_m_axi_AWLOCK,
      m_axi_awprot=> mem_m_axi_AWPROT,
      m_axi_awqos => mem_m_axi_AWQOS,
      m_axi_awready => mem_m_axi_AWREADY,
      m_axi_awsize => mem_m_axi_AWSIZE,
      m_axi_awvalid => mem_m_axi_AWVALID,
      m_axi_bid => mem_m_axi_BID,
      m_axi_bready => mem_m_axi_BREADY,
      m_axi_bresp => mem_m_axi_BRESP,
      m_axi_bvalid => mem_m_axi_BVALID,
      m_axi_rdata => mem_m_axi_RDATA,
      m_axi_rid => mem_m_axi_RID,
      m_axi_rlast => mem_m_axi_RLAST,
      m_axi_rready => mem_m_axi_RREADY,
      m_axi_rresp => mem_m_axi_RRESP,
      m_axi_rvalid => mem_m_axi_RVALID,
      m_axi_wdata => mem_m_axi_WDATA,
      m_axi_wlast => mem_m_axi_WLAST,
      m_axi_wready => mem_m_axi_WREADY,
      m_axi_wstrb => mem_m_axi_WSTRB,
      m_axi_wvalid => mem_m_axi_WVALID,
      
      m_axis_dma_c2h_tdata => open,
      m_axis_dma_c2h_tdest => open,
      m_axis_dma_c2h_tkeep => open,
      m_axis_dma_c2h_tlast => open,
      m_axis_dma_c2h_tready => '1',
      m_axis_dma_c2h_tvalid => open,
      m_axis_tx_tdata => os_wrapper_m_axis_tx_TDATA,
      m_axis_tx_tdest => os_wrapper_m_axis_tx_TDEST,
      m_axis_tx_tkeep => os_wrapper_m_axis_tx_TKEEP,
      m_axis_tx_tlast => os_wrapper_m_axis_tx_TLAST,
      m_axis_tx_tready => os_wrapper_m_axis_tx_TREADY,
      m_axis_tx_tuser => os_wrapper_m_axis_tx_TUSER,
      m_axis_tx_tvalid => os_wrapper_m_axis_tx_TVALID,
      mem_aresetn => mem_reset,
      mem_clk => mem_clk,
      net_aresetn => clk_200_rst,
      net_clk => clk_200,
      pcie_aresetn => clk_200_rst,
      pcie_clk => clk_200,
      s_axil_control_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axil_control_arprot(2 downto 0) => B"000",
      s_axil_control_arready => open,
      s_axil_control_arvalid => '0',
      s_axil_control_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axil_control_awprot(2 downto 0) => B"000",
      s_axil_control_awready =>open,
      s_axil_control_awvalid => '0',
      s_axil_control_bready => '0',
      s_axil_control_bresp => open,
      s_axil_control_bvalid => open,
      s_axil_control_rdata => open,
      s_axil_control_rready => '0',
      s_axil_control_rresp => open,
      s_axil_control_rvalid => open,
      s_axil_control_wdata(31 downto 0) => B"00000000000000000000000000000000",
      s_axil_control_wready => open,
      s_axil_control_wstrb(3 downto 0) => B"1111",
      s_axil_control_wvalid => '0',
      s_axim_control_araddr(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      s_axim_control_arburst(1 downto 0) => B"01",
      s_axim_control_arcache(3 downto 0) => B"0011",
      s_axim_control_arid(3 downto 0) => B"0000",
      s_axim_control_arlen(7 downto 0) => B"00000000",
      s_axim_control_arlock(0) => '0',
      s_axim_control_arprot(2 downto 0) => B"000",
      s_axim_control_arqos(3 downto 0) => B"0000",
      s_axim_control_arready => open,
      s_axim_control_arsize(2 downto 0) => B"110",
      s_axim_control_arvalid => '0',
      s_axim_control_awaddr(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      s_axim_control_awburst(1 downto 0) => B"01",
      s_axim_control_awcache(3 downto 0) => B"0011",
      s_axim_control_awid(3 downto 0) => B"0000",
      s_axim_control_awlen(7 downto 0) => B"00000000",
      s_axim_control_awlock(0) => '0',
      s_axim_control_awprot(2 downto 0) => B"000",
      s_axim_control_awqos(3 downto 0) => B"0000",
      s_axim_control_awready => open,
      s_axim_control_awsize(2 downto 0) => B"110",
      s_axim_control_awvalid => '0',
      s_axim_control_bid => open,
      s_axim_control_bready => '0',
      s_axim_control_bresp => open,
      s_axim_control_bvalid => open,
      s_axim_control_rdata => open,
      s_axim_control_rid => open,
      s_axim_control_rlast => open,
      s_axim_control_rready => '0',
      s_axim_control_rresp => open,
      s_axim_control_rvalid => open,
      s_axim_control_wdata(511 downto 0) => B"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
      s_axim_control_wlast => '0',
      s_axim_control_wready => open,
      s_axim_control_wstrb(63 downto 0) => B"1111111111111111111111111111111111111111111111111111111111111111",
      s_axim_control_wvalid => '0',
      s_axis_dma_h2c_tdata(511 downto 0) => B"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
      s_axis_dma_h2c_tkeep(63 downto 0) => B"1111111111111111111111111111111111111111111111111111111111111111",
      s_axis_dma_h2c_tlast => '0',
      s_axis_dma_h2c_tready => open,
      s_axis_dma_h2c_tvalid => '0',
      
      s_axis_qp_conn_interface_data => s_axis_qp_conn_interface_TDATA,
      s_axis_qp_conn_interface_ready => s_axis_qp_conn_interface_TREADY,
      s_axis_qp_conn_interface_valid => s_axis_qp_conn_interface_TVALID,
      s_axis_qp_interface_data => s_axis_qp_interface_TDATA,
      s_axis_qp_interface_ready => s_axis_qp_interface_TREADY,
      s_axis_qp_interface_valid => s_axis_qp_interface_TVALID,
      s_axis_roce_role_tx_data_tdata => s_axis_roce_role_tx_data_TDATA,
      s_axis_roce_role_tx_data_tkeep => s_axis_roce_role_tx_data_TKEEP,
      s_axis_roce_role_tx_data_tlast  => s_axis_roce_role_tx_data_TLAST,
      s_axis_roce_role_tx_data_tready => s_axis_roce_role_tx_data_TREADY,
      s_axis_roce_role_tx_data_tvalid => s_axis_roce_role_tx_data_TVALID,
      s_axis_roce_role_tx_meta_tdata => s_axis_roce_role_tx_meta_TDATA,
      s_axis_roce_role_tx_meta_tready => s_axis_roce_role_tx_meta_TREADY,
      s_axis_roce_role_tx_meta_tvalid => s_axis_roce_role_tx_meta_TVALID,
      
      s_axis_rx_tdata => m_axis_os_data_TDATA,
      s_axis_rx_tkeep => m_axis_os_data_TKEEP,
      s_axis_rx_tlast => m_axis_os_data_TLAST,
      s_axis_rx_tready => open,
      s_axis_rx_tuser(0) => '0',
      s_axis_rx_tvalid => m_axis_os_data_TVALID,
      user_aresetn => open,
      user_clk => open
    );
    
  icrc32_0: entity work.icrc32
     port map (
      core_clk => clk_200,
      core_rst_n => clk_200_rst,
      
      crc_in_axis_tdata => ibh_crc_axis_TDATA,
      crc_in_axis_tkeep => ibh_crc_axis_TKEEP,
      crc_in_axis_tlast => ibh_crc_axis_TLAST,
      crc_in_axis_tuser => ibh_crc_axis_TUSER,
      crc_in_axis_tvalid => ibh_crc_axis_TVALID,
      
      crc_out_axis_tdata => icrc32_out_axis_TDATA,
      crc_out_axis_tkeep => icrc32_out_axis_TKEEP,
      crc_out_axis_tlast => icrc32_out_axis_TLAST,
      crc_out_axis_tuser => icrc32_out_axis_TUSER,
      crc_out_axis_tvalid => icrc32_out_axis_TVALID,
      
      rev_out_axis_tdata => open,
      rev_out_axis_tkeep => open,
      rev_out_axis_tlast => open,
      rev_out_axis_tuser => open,
      rev_out_axis_tvalid => open
    );

  IB_handler_0: entity work.IB_handler
     port map (
      cmac_rx_axis_tdata => cmac_rx_tdata, 
      cmac_rx_axis_tkeep => cmac_rx_tkeep, 
      cmac_rx_axis_tlast => cmac_rx_tlast, 
      cmac_rx_axis_tuser => cmac_rx_tuser, 
      cmac_rx_axis_tvalid=> cmac_rx_tvalid,
                            
      cmac_tx_axis_tdata => ibh_crc_axis_TDATA,
      cmac_tx_axis_tkeep => ibh_crc_axis_TKEEP,
      cmac_tx_axis_tlast => ibh_crc_axis_TLAST,
      cmac_tx_axis_tuser => ibh_crc_axis_TUSER,
      cmac_tx_axis_tvalid=> ibh_crc_axis_TVALID,
      con_start => '0',
      core_clk   => clk_200,
      core_rst_n => clk_200_rst,
      m_axis_os_data_tdata => m_axis_os_data_TDATA,
      m_axis_os_data_tkeep => m_axis_os_data_TKEEP,
      m_axis_os_data_tlast => m_axis_os_data_TLAST,
      m_axis_os_data_tvalid => m_axis_os_data_TVALID,
      qp_connected => open,
      rem_ip => open,
      rem_mac => open,
      rem_psn => open,
      rem_qpn => open,
      s_axis_qp_conn_interface_tdata => s_axis_qp_conn_interface_TDATA,
      s_axis_qp_conn_interface_tready => s_axis_qp_conn_interface_TREADY,
      s_axis_qp_conn_interface_tvalid => s_axis_qp_conn_interface_TVALID,
      s_axis_qp_interface_tdata => s_axis_qp_interface_TDATA,
      s_axis_qp_interface_tready => s_axis_qp_interface_TREADY,
      s_axis_qp_interface_tvalid => s_axis_qp_interface_TVALID,
      s_axis_roce_role_tx_data_tdata => s_axis_roce_role_tx_data_TDATA,
      s_axis_roce_role_tx_data_tkeep => s_axis_roce_role_tx_data_TKEEP,
      s_axis_roce_role_tx_data_tlast => s_axis_roce_role_tx_data_TLAST,
      s_axis_roce_role_tx_data_tready => s_axis_roce_role_tx_data_TREADY,
      s_axis_roce_role_tx_data_tvalid => s_axis_roce_role_tx_data_TVALID,
      s_axis_roce_role_tx_meta_tdata => s_axis_roce_role_tx_meta_TDATA,
      s_axis_roce_role_tx_meta_tready => s_axis_roce_role_tx_meta_TREADY,
      s_axis_roce_role_tx_meta_tvalid => s_axis_roce_role_tx_meta_TVALID
    );





  design_1_i : component design_1_wrapper
  port map (
    clk_100MHz => drp_clk,
    gt_rx_0_gt_port_0_n => gt_rx_0_n,
    gt_rx_0_gt_port_0_p => gt_rx_0_p,
    gt_rx_0_gt_port_1_n => gt_rx_1_n,
    gt_rx_0_gt_port_1_p => gt_rx_1_p,
    gt_rx_0_gt_port_2_n => gt_rx_2_n,
    gt_rx_0_gt_port_2_p => gt_rx_2_p,
    gt_rx_0_gt_port_3_n => gt_rx_3_n,
    gt_rx_0_gt_port_3_p => gt_rx_3_p,
    gt_tx_0_gt_port_0_n => gt_tx_0_n,
    gt_tx_0_gt_port_0_p => gt_tx_0_p,
    gt_tx_0_gt_port_1_n => gt_tx_1_n,
    gt_tx_0_gt_port_1_p => gt_tx_1_p,
    gt_tx_0_gt_port_2_n => gt_tx_2_n,
    gt_tx_0_gt_port_2_p => gt_tx_2_p,
    gt_tx_0_gt_port_3_n => gt_tx_3_n,
    gt_tx_0_gt_port_3_p => gt_tx_3_p,
    qsfp4_156mhz_clk_n => qsfp4_clock_n,
    qsfp4_156mhz_clk_p => qsfp4_clock_p,
    reset => sys_rst,
    clk_out_200 => clk_200,
    clk_out_200_rst(0) => clk_200_rst,
    cmac_rx_tdata => cmac_rx_tdata,
    cmac_rx_tkeep => cmac_rx_tkeep,
    cmac_rx_tlast => cmac_rx_tlast,
    cmac_rx_tready=> cmac_rx_tready,
    cmac_rx_tuser => cmac_rx_tuser,
    cmac_rx_tvalid=> cmac_rx_tvalid,
    cmac_tx_0_tdata => os_wrapper_m_axis_tx_TDATA,  
    cmac_tx_0_tdest(0) => os_wrapper_m_axis_tx_TDEST,  
    cmac_tx_0_tkeep => os_wrapper_m_axis_tx_TKEEP,  
    cmac_tx_0_tlast => os_wrapper_m_axis_tx_TLAST,  
    cmac_tx_0_tready=>  os_wrapper_m_axis_tx_TREADY,
    cmac_tx_0_tuser => os_wrapper_m_axis_tx_TUSER,  
    cmac_tx_0_tvalid=>  os_wrapper_m_axis_tx_TVALID,
    cmac_tx_1_tdata => icrc32_out_axis_TDATA,  
    cmac_tx_1_tkeep => icrc32_out_axis_TKEEP,  
    cmac_tx_1_tlast => icrc32_out_axis_TLAST,  
    cmac_tx_1_tuser => icrc32_out_axis_TUSER,  
    cmac_tx_1_tvalid=>  icrc32_out_axis_TVALID,
    
--    pcie_ref_clk_0 => sys_clk,
--    pcie_ref_clk_gt_0 => sys_clk_gt,
--    pcie_rx_n_0(7 downto 0) => pcie_rxn(7 downto 0),
--    pcie_rx_p_0(7 downto 0) => pcie_rxp(7 downto 0),
--    pcie_tx_n_0(7 downto 0) => pcie_txn(7 downto 0),
--    pcie_tx_p_0(7 downto 0) => pcie_txp(7 downto 0),
--    perst_n_0 => perst_n,
    -- DDR4 interface from/to memory chips
    ddr4_out_act_n => ddr4_act_n,
    ddr4_out_adr   => ddr4_adr_1,
    ddr4_out_ba    => ddr4_ba,
    ddr4_out_bg    => ddr4_bg,
    ddr4_out_ck_c  => ddr4_ck_c,
    ddr4_out_ck_t  => ddr4_ck_t,
    ddr4_out_cke   => ddr4_cke,
    ddr4_out_cs_n  => ddr4_cs_n,
    ddr4_out_dm_n  => ddr4_dm_n,
    ddr4_out_dq    => ddr4_dq,
    ddr4_out_dqs_c => ddr4_dqs_c,
    ddr4_out_dqs_t => ddr4_dqs_t,
    ddr4_out_odt   => ddr4_odt,
    ddr4_out_reset_n => ddr4_reset_n,
    ddr_clk => ddr4_clk,
    
    mem_m_axi_araddr => mem_m_axi_ARADDR,
    mem_m_axi_arburst => mem_m_axi_ARBURST,
    mem_m_axi_arcache => mem_m_axi_ARCACHE,
    mem_m_axi_arid => mem_m_axi_ARID,
    mem_m_axi_arlen=> mem_m_axi_ARLEN,
    mem_m_axi_arlock => mem_m_axi_ARLOCK,
    mem_m_axi_arprot => mem_m_axi_ARPROT,
    mem_m_axi_arqos => mem_m_axi_ARQOS,
    mem_m_axi_arready => mem_m_axi_ARREADY,
    mem_m_axi_arsize => mem_m_axi_ARSIZE,
    mem_m_axi_arvalid => mem_m_axi_ARVALID,
    mem_m_axi_awaddr => mem_m_axi_AWADDR,
    mem_m_axi_awburst => mem_m_axi_AWBURST,
    mem_m_axi_awcache => mem_m_axi_AWCACHE,
    mem_m_axi_awid => mem_m_axi_AWID,
    mem_m_axi_awlen => mem_m_axi_AWLEN,
    mem_m_axi_awlock => mem_m_axi_AWLOCK,
    mem_m_axi_awprot=> mem_m_axi_AWPROT,
    mem_m_axi_awqos => mem_m_axi_AWQOS,
    mem_m_axi_awready => mem_m_axi_AWREADY,
    mem_m_axi_awsize => mem_m_axi_AWSIZE,
    mem_m_axi_awvalid => mem_m_axi_AWVALID,
    mem_m_axi_bid => mem_m_axi_BID,
    mem_m_axi_bready => mem_m_axi_BREADY,
    mem_m_axi_bresp => mem_m_axi_BRESP,
    mem_m_axi_bvalid => mem_m_axi_BVALID,
    mem_m_axi_rdata => mem_m_axi_RDATA,
    mem_m_axi_rid => mem_m_axi_RID,
    mem_m_axi_rlast => mem_m_axi_RLAST,
    mem_m_axi_rready => mem_m_axi_RREADY,
    mem_m_axi_rresp => mem_m_axi_RRESP,
    mem_m_axi_rvalid => mem_m_axi_RVALID,
    mem_m_axi_wdata => mem_m_axi_WDATA,
    mem_m_axi_wlast => mem_m_axi_WLAST,
    mem_m_axi_wready => mem_m_axi_WREADY,
    mem_m_axi_wstrb => mem_m_axi_WSTRB,
    mem_m_axi_wvalid => mem_m_axi_WVALID,
    mem_calib => mem_calib,
    mem_clk => mem_clk,
    mem_reset(0) => mem_reset
  );
  
--  //PCIe ref clock
--IBUFDS_GTE4 pcie_ibuf_inst (
--    .O(pcie_ref_clk_gt),         // 1-bit output: Refer to Transceiver User Guide
--    .ODIV2(pcie_ref_clk),            // 1-bit output: Refer to Transceiver User Guide
--    .CEB(1'b0),          // 1-bit input: Refer to Transceiver User Guide
--    .I(pcie_clk_p),        // 1-bit input: Refer to Transceiver User Guide
--    .IB(pcie_clk_n)        // 1-bit input: Refer to Transceiver User Guide
--);
--  ibufds_drp_clk: IBUFDS_GTE4
--    port map(
--      I  => pcie_clk_p,
--      IB => pcie_clk_n,
--      O  => pcie_ref_clk_gt,
--      ODIV2 => pcie_ref_clk,
--      CEB => '0'
--  );
  refclk_buff: IBUFDS_GTE4 port map(
    CEB => '0',
    I => sys_clk_p,
    IB => sys_clk_n,
    O => sys_clk_gt,
    ODIV2 => sys_clk             -- default is no divide
  );
			
  drp_clk_block: block
    signal clk_100_ibuf: std_logic;
  begin
  
    ibufds_drp_clk: IBUFDS
    port map(
      I  => qdr4_clk_p,
      IB => qdr4_clk_n,
      O  => clk_100_ibuf
    );
    
    drp_bufg: BUFG
    port map(
      I => clk_100_ibuf,
      O => drp_clk
    );
  end block;
  
  ddr_clk_block: block
    signal clk_100_ibuf: std_logic;
  begin
  
    ibufds_ddr_clk: IBUFDS
    port map(
      I  => ddr4_clk_p,
      IB => ddr4_clk_n,
      O  => clk_100_ibuf
    );
    
    ddr_bufg: BUFG
    port map(
      I => clk_100_ibuf,
      O => ddr4_clk
    );
  end block;
  
  sys_rst_n <= not sys_rst;
  qsfp4_resetl_ls  <= sys_rst_n;
  qsfp4_modskll_ls <= '1'; -- use i2c
  qsfp4_lpmode_ls  <= '0'; -- no low power
  
  -- 14 physical address pin available. Other pins have different functions.
  ddr4_adr   <= ddr4_adr_1(13 downto 0);
  ddr4_we_b  <= ddr4_adr_1(14);
  ddr4_cas_b <= ddr4_adr_1(15);
  ddr4_ras_b <= ddr4_adr_1(16);
  
    
  qsfp28_ila : ila_0
  PORT MAP (
	clk => clk_200,
	probe0(0) => qsfp4_modprsl_ls,
	probe1(0) => qsfp4_intl_ls
   );
  
end architecture;
