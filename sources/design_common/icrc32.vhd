library ieee;
use ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.crc32.all;
use work.utility.all;

entity icrc32 is
  port ( 
    signal core_clk : in std_logic;
    signal core_rst_n : in std_logic;
    
    -- input streaming signals from IB
    signal crc_in_axis_tdata : in std_logic_vector(511 downto 0);
    signal crc_in_axis_tkeep : in std_logic_vector(63 downto 0);
    signal crc_in_axis_tlast : in std_logic;
    signal crc_in_axis_tuser : in std_logic_vector(0 downto 0);
    signal crc_in_axis_tvalid: in std_logic;
    
    -- Reversed output for debug
    signal rev_out_axis_tdata : out std_logic_vector(511 downto 0);
    signal rev_out_axis_tkeep : out std_logic_vector(63 downto 0);
    signal rev_out_axis_tlast : out std_logic;
    signal rev_out_axis_tuser : out std_logic_vector(0 downto 0);
    signal rev_out_axis_tvalid : out std_logic;
    
    -- Response to CMAC (IB_ConReq, IB_Ready etc.)
    signal crc_out_axis_tdata : out std_logic_vector(511 downto 0);
    signal crc_out_axis_tkeep : out std_logic_vector(63 downto 0);
    signal crc_out_axis_tlast : out std_logic;
    signal crc_out_axis_tuser : out std_logic_vector(0 downto 0);
    signal crc_out_axis_tvalid : out std_logic
  );
end icrc32;

architecture rtl of icrc32 is
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER of crc_in_axis_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
  ATTRIBUTE X_INTERFACE_PARAMETER of rev_out_axis_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
  ATTRIBUTE X_INTERFACE_PARAMETER of crc_out_axis_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
  
  type t_cmac_tx is record
    tdata : std_logic_vector(511 downto 0);
    tkeep : std_logic_vector(63 downto 0);
    tlast : std_logic;
    tuser : std_logic_vector(0 downto 0);
    tvalid: std_logic;
  end record t_cmac_tx;
  
  type cmac_tx_arr is array(1 to 5) of t_cmac_tx;
  signal cmac_tx : cmac_tx_arr;

   type t_crc_pipe is record
    tdata : std_logic_vector(511 downto 0);
    tkeep : std_logic_vector(63 downto 0);
    tlast : std_logic;
    tuser : std_logic_vector(0 downto 0);
    tvalid: std_logic;
  end record t_crc_pipe;
  
  type crc_pipe_arr is array(0 to 5) of t_crc_pipe;
  signal crc_data, crc_array : crc_pipe_arr;
  signal crc_pipe : crc_pipe_arr;
  signal crc_pipe_reset : t_crc_pipe := ( 
    tdata => (others => '0'),
    tkeep => (others => '0'),
    tlast => '0',            
    tuser => (others => '0'),
    tvalid => '0'
  );
  
 COMPONENT fifo_generator_0
  PORT (
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC;
    s_aclk : IN STD_LOGIC;
    s_aresetn : IN STD_LOGIC;
    s_axis_tvalid : IN STD_LOGIC;
    s_axis_tready : OUT STD_LOGIC;
    s_axis_tdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_tkeep : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    s_axis_tlast : IN STD_LOGIC;
    s_axis_tuser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axis_tvalid : OUT STD_LOGIC;
    m_axis_tready : IN STD_LOGIC;
    m_axis_tdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_tkeep : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_tlast : OUT STD_LOGIC;
    m_axis_tuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;
  
  COMPONENT fifo_generator_1
    PORT (
      clk : IN STD_LOGIC;
      srst : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      rd_en : IN STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(575 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC;
      wr_rst_busy : OUT STD_LOGIC;
      rd_rst_busy : OUT STD_LOGIC
    );
  END COMPONENT;

  signal crc_out, crc_value      : std_logic_vector(31 downto 0); 
  signal crc_in_axis_tdata_s     : std_logic_vector(511 downto 0);
  
  signal fifo_inp_data :std_logic_vector(583 downto 0);
  signal fifo_inp_out: std_logic_vector(72 downto 0);
  signal fifo_inp_valid: std_logic;
  signal fifo_inp_wen: std_logic;
  signal fifo_inp_ren: std_logic;
  signal fifo_inp_empty, fifo_inp_full : std_logic;
  signal fifo_rst: std_logic;
  signal crc_input_s : std_logic_vector(31 downto 0);
  signal tkeep_val_s : integer := 0;
  signal start_word, start_word_p1 : std_logic;
  
  signal crc_block_rev : std_logic_vector(31 downto 0); 
  signal crc_block_out: std_logic_vector(31 downto 0); 
  signal crc_out_signal : std_logic_vector(31 downto 0); 
  signal crc_data_check : std_logic_vector(511 downto 0); 
  signal crc_rst, crc_done : std_logic;
  
  signal crc_busy, crc_busy_p1 : std_logic;
  signal icrc_position : integer;
  attribute KEEP : string;
   attribute KEEP of crc_in_axis_tdata_s: signal is "TRUE";
    attribute DONT_TOUCH : string;
    attribute DONT_TOUCH of crc_value: signal is "TRUE";
    attribute DONT_TOUCH of crc_in_axis_tdata_s: signal is "TRUE";
    
begin
  
  ready_data : process(core_clk, core_rst_n)
    variable icrc_in : std_logic_vector(511 downto 0);
    variable tkeep_in : std_logic_vector(63 downto 0);
    variable tkeep_val : integer;
    --variable start_word : std_logic;
  begin
    if rising_edge(core_clk) then
      if core_rst_n = '0' then
        icrc_in := (others => '0');
        tkeep_in := (others => '0');
        start_word <= '1';
        start_word_p1 <= '0';
      else
        start_word_p1 <= '0';
        if crc_in_axis_tvalid = '1' then
          if start_word = '1' then
            icrc_in(63 downto 0)    := (others => '1'); -- Dummy LRH field
            icrc_in(463 downto 64)  := crc_in_axis_tdata(511 downto 112);
            icrc_in(79 downto 72)   := (others => '1'); -- Type of service 1s
            icrc_in(135 downto 128) := (others => '1'); -- TTL 1s
            icrc_in(159 downto 144) := (others => '1'); -- IP Checksum 1s
            icrc_in(287 downto 272) := (others => '1'); -- UDP Checksum ls
            icrc_in(327 downto 320) := (others => '1'); -- Resv8a 1s
            
            for i in 0 to 63 loop
              if crc_in_axis_tkeep(i) = '1' then
                tkeep_val := i;
              end if;
            end loop;
            tkeep_in(63 downto 0) := (others => '0');
            tkeep_in(tkeep_val-6 downto 0) := (others => '1');
            --tkeep_in := std_logic_vector(to_unsigned(tkeep_val - 6, tkeep_in'length));
--            tkeep_in(57 downto 0) := (others => '1');
--            tkeep_in(63 downto 58) := (others => '0');
            start_word <= '0';
            if crc_in_axis_tlast = '1' then
              start_word <= '1';
              start_word_p1 <= '1';
            end if;
            
          elsif crc_in_axis_tlast = '1' then
            icrc_in  := crc_in_axis_tdata;
            tkeep_in := crc_in_axis_tkeep;
            start_word <= '1';
          else
            icrc_in := crc_in_axis_tdata;
            tkeep_in := crc_in_axis_tkeep;
          end if;
        end if;
      end if;
      crc_data(0).tdata <= reverse_vector(icrc_in);
      crc_data(0).tkeep <= tkeep_in;
      crc_data(0).tvalid <= crc_in_axis_tvalid;
      crc_data(0).tlast <= crc_in_axis_tlast;
      crc_data(0).tuser <= crc_in_axis_tuser;
      
      crc_data_check <= reverse_bytes(icrc_in);
      
    end if;
  end process;
  

  --Process data for CRC, result will be available 4 cycles after data entered this entity
  loop_data :process(core_clk)
    variable tkeep_val : integer;
    variable tlast_val : std_logic;
  begin
    if rising_edge(core_clk) then
      if core_rst_n = '0' then 
        tkeep_val := 0;
        tkeep_val_s <= 0;
        crc_array(0) <= crc_pipe_reset;
      else
        tkeep_val := 0;
        if crc_data(0).tvalid = '1' then
          for i in 0 to 63 loop
            if crc_data(0).tkeep(i) = '1' then
              tkeep_val := tkeep_val + 1;
            end if;
          end loop;
          tkeep_val_s <= tkeep_val;
--          tkeep_val := tkeep_val mod 8;
          
        end if;
        
        for i in 0 to 7 loop
         tlast_val := '0';
        -- if i = tkeep_val+1 and crc_in_axis_tlast = '1' then
         if crc_data(0).tlast = '1' then
           tlast_val := '1';
         end if;
         fifo_inp_data(583-(i*73) downto 511-(i*73)) <= tlast_val & crc_data(0).tkeep((i+1)*8-1 downto i*8) & crc_data(0).tdata((i+1)*64-1 downto i*64);
        end loop;
        
        fifo_inp_wen <= crc_in_axis_tvalid;
        fifo_inp_ren <= not fifo_inp_empty;
        
        crc_array(0).tdata <= crc_in_axis_tdata;
        crc_array(0).tkeep <= crc_in_axis_tkeep;
        crc_array(0).tvalid <= crc_in_axis_tvalid;
        crc_array(0).tlast <= crc_in_axis_tlast;
        crc_array(0).tuser <= crc_in_axis_tuser;
        
        crc_array(1) <= crc_array(0);
        crc_array(2) <= crc_array(1);
        crc_array(3) <= crc_array(2);
        crc_array(4) <= crc_array(3);
        crc_array(5) <= crc_array(4);
        
        
        crc_data(1) <= crc_data(0);
        --crc_data(1).tdata <= (reverse_vector(crc_data(0).tdata));
        crc_data(2) <= crc_data(1);
        crc_data(3) <= crc_data(2);
        crc_data(4) <= crc_data(3);
        
        if start_word_p1 = '1' then 
          crc_data(1).tkeep(tkeep_val+5 downto tkeep_val) <= (others => '1');
        end if;
        
        case tkeep_val is
        when 6 => 
          crc_data(1).tdata(47 downto 0) <= crc_data(0).tdata(511 downto 464); 
        when 10 => 
          crc_data(1).tdata(79 downto 0) <= crc_data(0).tdata(511 downto 432); 
        when 52 => 
          crc_data(1).tdata(415 downto 0) <= crc_data(0).tdata(511 downto 96); 
        when 58 => 
          crc_data(1).tdata(463 downto 0) <= crc_data(0).tdata(511 downto 48); 
        when 62 =>
          crc_data(1).tdata(495 downto 0) <= crc_data(0).tdata(511 downto 16); 
        when others =>
          crc_data(1).tdata <= (crc_data(0).tdata);
        end case;
        
        crc_done <= '0';
        if crc_data(3).tlast = '1' then
          crc_done <= '1';
        end if;
        
        crc_block_rev <= reverse_vector(crc_block_out);
        if crc_data(3).tlast = '1' then
          crc_out_signal <= not crc_block_rev;
        end if;
        --crc_out_axis_tdata(31 downto 0) <= not crc_block_rev;
        
        
        
--        crc_out_axis_tkeep <= crc_data(1).tkeep;
--        crc_out_axis_tlast <= crc_data(1).tlast;
--        crc_out_axis_tuser <= crc_data(1).tuser;
--        crc_out_axis_tvalid <= crc_data(1).tvalid;
      end if;
    end if;
  end process;

  pipeline : process (core_clk, core_rst_n)
    variable last_word_valid : integer;
  begin
    if core_rst_n = '0' then
      crc_out_axis_tdata <= (others => '0');
      crc_out_axis_tkeep <= (others => '0');
      crc_out_axis_tlast <= '0';
      crc_out_axis_tuser <= (others => '0');
      crc_out_axis_tvalid <= '0';      
      
      rev_out_axis_tdata <= (others => '0');
      rev_out_axis_tkeep <= (others => '0');
      rev_out_axis_tlast <= '0';
      rev_out_axis_tuser <= (others => '0');
      rev_out_axis_tvalid <= '0';
      
      cmac_tx(2).tdata   <= (others => '0');   
      cmac_tx(2).tkeep   <= (others => '0');   
      cmac_tx(2).tlast   <= '0';               
      cmac_tx(2).tuser   <= (others => '0');   
      cmac_tx(2).tvalid  <= '0';              
                          
      cmac_tx(1).tdata   <= (others => '0');  
      cmac_tx(1).tkeep   <= (others => '0');  
      cmac_tx(1).tlast   <= '0';              
      cmac_tx(1).tuser   <= (others => '0');  
      cmac_tx(1).tvalid  <= '0';             
      
      crc_busy_p1 <= '0';
      crc_busy <= '0';
    elsif rising_edge(core_clk) then
     
      if crc_data(4).tlast = '1' then    --if icrc_ready = '1' then
      
        cmac_tx(2).tkeep <= (others => '0'); 
        cmac_tx(2).tdata <= (others => '0'); 
        cmac_tx(1).tkeep <= (others => '0'); 
        cmac_tx(1).tdata <= (others => '0'); 
        
        last_word_valid := 0;
        for i in 0 to 63 loop
          if crc_data(4).tkeep(i) = '1' then
            last_word_valid := last_word_valid + 1;
          end if;
        end loop;
        
        case last_word_valid is
        when 0 to 60 =>
          cmac_tx(1).tdata(last_word_valid*8+31 downto last_word_valid*8) <= std_logic_vector(crc_out_signal);
          cmac_tx(1).tdata(last_word_valid*8-1 downto 0) <= crc_array(4).tdata(last_word_valid*8-1 downto 0);
          cmac_tx(1).tkeep <= (others => '0');
          cmac_tx(1).tkeep(last_word_valid + 3 downto 0) <= (others => '1'); --(words-1 + 4)
          cmac_tx(1).tlast     <= '1';
          cmac_tx(1).tvalid     <= '1';
          crc_busy_p1 <= '1';
        when 61 =>
          cmac_tx(2).tdata     <= std_logic_vector(crc_out_signal(15 downto 8)) &  std_logic_vector(crc_out_signal(23 downto 16)) & std_logic_vector(crc_out_signal(31 downto 24)) & cmac_tx(3).tdata(487 downto 0);  
          cmac_tx(2).tkeep     <= (others => '1'); 
          cmac_tx(2).tlast     <= '0'; 
          cmac_tx(2).tuser(0)  <= cmac_tx(3).tuser(0);
          cmac_tx(2).tvalid    <= cmac_tx(3).tvalid;
          
          cmac_tx(1).tdata(7 downto 0) <= std_logic_vector(crc_out_signal(7 downto 0));  
          cmac_tx(1).tkeep(0)  <= '1';   
          cmac_tx(1).tlast     <= '1'; 
          cmac_tx(1).tuser(0)  <= '0';
          cmac_tx(1).tvalid    <= '1';
          
          crc_busy <= '1';
        when 62 =>
          --cmac_tx(2).tdata     <= std_logic_vector(crc_out_signal(23 downto 16)) & std_logic_vector(crc_out_signal(31 downto 24)) & crc_array(4).tdata(495 downto 0);  
          cmac_tx(2).tdata     <= std_logic_vector(crc_out_signal(15 downto 8)) & std_logic_vector(crc_out_signal(7 downto 0)) & crc_array(4).tdata(495 downto 0);  
          cmac_tx(2).tkeep     <= (others => '1'); 
          cmac_tx(2).tlast     <= '0'; 
          cmac_tx(2).tuser(0)  <= '0';
          cmac_tx(2).tvalid    <= crc_array(4).tvalid;
          
          --cmac_tx(1).tdata(15 downto 0) <= std_logic_vector(crc_out_signal(7 downto 0)) & std_logic_vector(crc_out_signal(15 downto 8));  
          cmac_tx(1).tdata(15 downto 0) <= std_logic_vector(crc_out_signal(31 downto 24)) & std_logic_vector(crc_out_signal(23 downto 16));  
          cmac_tx(1).tkeep(1 downto 0)  <= "11";   
          cmac_tx(1).tlast     <= '1'; 
          cmac_tx(1).tuser(0)  <= '0';
          cmac_tx(1).tvalid    <= '1';
          
           crc_busy <= '1';
        when 63 =>
          cmac_tx(2).tdata     <= std_logic_vector(crc_out_signal(31 downto 24)) & cmac_tx(3).tdata(503 downto 0);
          cmac_tx(2).tkeep     <= (others => '1'); 
          cmac_tx(2).tlast     <= '0'; 
          cmac_tx(2).tuser(0)  <= cmac_tx(3).tuser(0);
          cmac_tx(2).tvalid    <= cmac_tx(3).tvalid;
          
          cmac_tx(1).tdata(23 downto 0) <= std_logic_vector(crc_out_signal(7 downto 0)) & std_logic_vector(crc_out_signal(15 downto 8)) &  std_logic_vector(crc_out_signal(23 downto 16));
          cmac_tx(1).tkeep(2 downto 0)  <= "111";   
          cmac_tx(1).tlast     <= '1'; 
          cmac_tx(1).tuser(0)  <= '0';
          cmac_tx(1).tvalid    <= '1';
          
           crc_busy <= '1';
        when 64 =>
        --new word
          cmac_tx(2).tdata      <= cmac_tx(3).tdata;  
          cmac_tx(2).tkeep      <= cmac_tx(3).tkeep;   
          cmac_tx(2).tlast      <= '0';
          cmac_tx(2).tuser(0)   <= cmac_tx(3).tuser(0);
          cmac_tx(2).tvalid     <= cmac_tx(3).tvalid;
          
          cmac_tx(1).tdata(31 downto 0) <= std_logic_vector(crc_out(7 downto 0)) & std_logic_vector(crc_out(15 downto 8)) &  std_logic_vector(crc_out(23 downto 16)) &  std_logic_vector(crc_out(31 downto 24));
          cmac_tx(1).tkeep(3 downto 0)  <= "1111";   
          cmac_tx(1).tlast     <= '1'; 
          cmac_tx(1).tuser(0)  <= '0';
          cmac_tx(1).tvalid    <= '1';
          
           crc_busy <= '1';
        when others =>
        end case;  
      end if;
      
      if crc_busy = '1' then
        crc_out_axis_tdata <= cmac_tx(2).tdata;   
        crc_out_axis_tkeep <= cmac_tx(2).tkeep;   
        crc_out_axis_tlast <= cmac_tx(2).tlast;   
        crc_out_axis_tuser <= cmac_tx(2).tuser;
        crc_out_axis_tvalid <=cmac_tx(2).tvalid;  
        crc_busy <= '0';
        crc_busy_p1 <= '1';
        rev_out_axis_tdata  <= reverse_bytes(cmac_tx(2).tdata);     
        rev_out_axis_tkeep  <= cmac_tx(2).tkeep;  
        rev_out_axis_tlast  <= cmac_tx(2).tlast;  
        rev_out_axis_tuser  <= cmac_tx(2).tuser;  
        rev_out_axis_tvalid <= cmac_tx(2).tvalid; 
      elsif crc_busy_p1 = '1' then
        crc_out_axis_tdata <= cmac_tx(1).tdata;   
        crc_out_axis_tkeep <= cmac_tx(1).tkeep;   
        crc_out_axis_tlast <= cmac_tx(1).tlast;   
        crc_out_axis_tuser <= cmac_tx(1).tuser;
        crc_out_axis_tvalid <=cmac_tx(1).tvalid;  
        crc_busy_p1 <= '0';
        rev_out_axis_tdata  <= reverse_bytes(cmac_tx(1).tdata);    
        rev_out_axis_tkeep  <= cmac_tx(1).tkeep;  
        rev_out_axis_tlast  <= cmac_tx(1).tlast;  
        rev_out_axis_tuser  <= cmac_tx(1).tuser;  
        rev_out_axis_tvalid <= cmac_tx(1).tvalid; 
      else
        crc_out_axis_tdata <= crc_array(5).tdata;   
        crc_out_axis_tkeep <= crc_array(5).tkeep;   
        crc_out_axis_tlast <= crc_array(5).tlast;   
        crc_out_axis_tuser <= crc_array(5).tuser;
        crc_out_axis_tvalid <=crc_array(5).tvalid;  
        
        rev_out_axis_tdata  <= reverse_bytes(crc_array(5).tdata);   
        rev_out_axis_tkeep  <= crc_array(5).tkeep;   
        rev_out_axis_tlast  <= crc_array(5).tlast;   
        rev_out_axis_tuser  <= crc_array(5).tuser;   
        rev_out_axis_tvalid <= crc_array(5).tvalid;  
      end if;
      
    end if;
    
  end process;
  fifo_rst <= not core_rst_n;

  
  crc_block: entity work.crc_cal 
  port map (
    clk     => core_clk,
    rst     => crc_rst,
    crc_en  => crc_data(1).tvalid,   
    data_in     => crc_data(1).tdata,
    valid_bytes => tkeep_val_s,
    crc_out     => crc_block_out
  );
  
  crc_rst <= core_rst_n and not (crc_data(1).tvalid);-- or crc_data(2).tvalid);

end rtl;
