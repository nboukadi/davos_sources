
####################################################
#############   GTY pin definitions    #############
####################################################
#QSFP4 connected pins
set_property PACKAGE_PIN AA53 [get_ports gt_rx_0_p]
set_property PACKAGE_PIN AA54 [get_ports gt_rx_0_n]
set_property PACKAGE_PIN AA44 [get_ports gt_tx_0_p]
set_property PACKAGE_PIN AA45 [get_ports gt_tx_0_n]
set_property PACKAGE_PIN Y51 [get_ports gt_rx_1_p]
set_property PACKAGE_PIN Y52 [get_ports gt_rx_1_n]
set_property PACKAGE_PIN Y46 [get_ports gt_tx_1_p]
set_property PACKAGE_PIN Y47 [get_ports gt_tx_1_n]
set_property PACKAGE_PIN W53 [get_ports gt_rx_2_p]
set_property PACKAGE_PIN W54 [get_ports gt_rx_2_n]
set_property PACKAGE_PIN W48 [get_ports gt_tx_2_p]
set_property PACKAGE_PIN W49 [get_ports gt_tx_2_n]
set_property PACKAGE_PIN V51 [get_ports gt_rx_3_p]
set_property PACKAGE_PIN V52 [get_ports gt_rx_3_n]
set_property PACKAGE_PIN W44 [get_ports gt_tx_3_p]
set_property PACKAGE_PIN W45 [get_ports gt_tx_3_n]

#####################################################
#############   Clock and reset pins    #############
#####################################################

# CPU_RESET
set_property PACKAGE_PIN BM29 [get_ports sys_rst]
set_property IOSTANDARD LVCMOS12 [get_ports sys_rst]

# DDR4_RESET
set_property PACKAGE_PIN BH50 [get_ports ddr4_reset_n]
set_property IOSTANDARD LVCMOS12 [get_ports ddr4_reset_n]

set_property PACKAGE_PIN BJ4 [get_ports qdr4_clk_p]
set_property PACKAGE_PIN BK3 [get_ports qdr4_clk_n]
set_property IOSTANDARD DIFF_SSTL12 [get_ports qdr4_clk_p]
set_property IOSTANDARD DIFF_SSTL12 [get_ports qdr4_clk_n]
create_clock -period 10.000 -name qdr_clock [get_ports qdr4_clk_p]

set_property PACKAGE_PIN AB43 [get_ports qsfp4_clock_n]
set_property PACKAGE_PIN AB42 [get_ports qsfp4_clock_p]

set_property PACKAGE_PIN BH51 [get_ports ddr4_clk_p]
set_property PACKAGE_PIN BJ51 [get_ports ddr4_clk_n]
set_property IOSTANDARD DIFF_SSTL12 [get_ports ddr4_clk_p]
set_property IOSTANDARD DIFF_SSTL12 [get_ports ddr4_clk_n]
create_clock -period 10.000 -name sys_clk [get_ports ddr4_clk_p]
#create_clock -period 6.000 -name sys_clk [get_ports qspf4_clock_p]
#set_property IOSTANDARD LVCMOS12 [get_ports sys_reset_n]
#set_property PULLUP true [get_ports sys_reset_n]
#set_false_path -from [get_ports sys_reset_n]


######################################################
############   LED pin definitions (B40)  ############
######################################################
#set_property PACKAGE_PIN BH24 [get_ports {led[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {led[0]}]
#set_property DRIVE 8 [get_ports {led[0]}]
#set_property SLEW SLOW [get_ports {led[0]}]

#set_property PACKAGE_PIN BG24 [get_ports {led[1]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {led[1]}]
#set_property DRIVE 8 [get_ports {led[1]}]
#set_property SLEW SLOW [get_ports {led[1]}]

#set_property PACKAGE_PIN BG25 [get_ports {led[2]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {led[2]}]
#set_property DRIVE 8 [get_ports {led[2]}]
#set_property SLEW SLOW [get_ports {led[2]}]

#set_property PACKAGE_PIN BF25 [get_ports {led[3]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {led[3]}]
#set_property DRIVE 8 [get_ports {led[3]}]
#set_property SLEW SLOW [get_ports {led[3]}]

#############################################################
###############   I2C0 pin definitions    ###################
#############################################################

set_property PACKAGE_PIN BM27 [get_ports i2c0_scl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports i2c0_scl_ls]
set_property PACKAGE_PIN BL28 [get_ports i2c0_sda_ls]
set_property IOSTANDARD LVCMOS18 [get_ports i2c0_sda_ls]

#############################################################
##############   UART0 pin definitions    ###################
#############################################################
set_property PACKAGE_PIN BP26 [get_ports uart_0_rxd]
set_property IOSTANDARD LVCMOS18 [get_ports uart_0_rxd]
set_property PACKAGE_PIN BN26 [get_ports uart_0_txd]
set_property IOSTANDARD LVCMOS18 [get_ports uart_0_txd]

#############################################################
###############   QSFP4 pin definitions    ##################
#############################################################

set_property PACKAGE_PIN BH21 [get_ports qsfp4_intl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_intl_ls]
set_property PACKAGE_PIN BK23 [get_ports qsfp4_modskll_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_modskll_ls]
set_property PACKAGE_PIN BK24 [get_ports qsfp4_resetl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_resetl_ls]
set_property PACKAGE_PIN BL22 [get_ports qsfp4_modprsl_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_modprsl_ls]
set_property PACKAGE_PIN BF23 [get_ports qsfp4_lpmode_ls]
set_property IOSTANDARD LVCMOS18 [get_ports qsfp4_lpmode_ls]

#############################################################
###############   PCIe pin definitions    ###################
#############################################################
# PCIE Active Low Reset
set_property PACKAGE_PIN BF41 [get_ports perst_n]
set_property IOSTANDARD LVCMOS12 [get_ports perst_n]
set_property PULLUP true [get_ports perst_n]
set_false_path -from [get_ports perst_n]

# PCIE Reference Clock 0 MGTREFCLK0N_227
#set_property PACKAGE_PIN AL14 [get_ports {sys_clk_n}]
#set_property PACKAGE_PIN AL15 [get_ports {sys_clk_p}]
set_property PACKAGE_PIN AR14 [get_ports sys_clk_n] # PCIE Reference Clock 1
set_property PACKAGE_PIN AR15 [get_ports sys_clk_p]

#set_property PACKAGE_PIN AR14 [get_ports {sys_clk_n[1]}]
#set_property PACKAGE_PIN AR15 [get_ports {sys_clk_p[1]}]

create_clock -period 10.000 -name sys_clk0 [get_ports {sys_clk_p}]
#create_clock -period 10.000 -name sys_clk1 [get_ports {sys_clk_p[1]}]

#set_property LOC [get_package_pins -of_objects [get_bels [get_sites -filter {NAME =~ *COMMON*} -of_objects [get_iobanks -of_objects [get_sites GTYE4_CHANNEL_X1Y15]]]/REFCLK0P]] [get_ports sys_clk_p]
#set_property LOC [get_package_pins -of_objects [get_bels [get_sites -filter {NAME =~ *COMMON*} -of_objects [get_iobanks -of_objects [get_sites GTYE4_CHANNEL_X1Y15]]]/REFCLK0N]] [get_ports sys_clk_n]

set_clock_groups -name async18 -asynchronous -group [get_clocks {sys_clk*}] -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *gen_channel_container[*].*gen_gtye4_channel_inst[*].GTYE4_CHANNEL_PRIM_INST/TXOUTCLK}]]
set_clock_groups -name async19 -asynchronous -group [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ *gen_channel_container[*].*gen_gtye4_channel_inst[*].GTYE4_CHANNEL_PRIM_INST/TXOUTCLK}]] -group [get_clocks {sys_clk*}]
set_clock_groups -name async5 -asynchronous -group [get_clocks {sys_clk*}] -group [get_clocks -of_objects [get_pins design_1_i/design_1_i/os_wrapper_vhdl_0/U0/os_wrapper_i/dma_driver_inst/dma_inst/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]]
set_clock_groups -name async6 -asynchronous -group [get_clocks -of_objects [get_pins design_1_i/design_1_i/os_wrapper_vhdl_0/U0/os_wrapper_i/dma_driver_inst/dma_inst/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]] -group [get_clocks {sys_clk*}]
set_clock_groups -name async1 -asynchronous -group [get_clocks {sys_clk*}] -group [get_clocks -of_objects [get_pins design_1_i/design_1_i/os_wrapper_vhdl_0/U0/os_wrapper_i/dma_driver_inst/dma_inst/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_pclk/O]]
set_clock_groups -name async2 -asynchronous -group [get_clocks -of_objects [get_pins design_1_i/design_1_i/os_wrapper_vhdl_0/U0/os_wrapper_i/dma_driver_inst/dma_inst/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_pclk/O]] -group [get_clocks {sys_clk*}]
set_clock_groups -name async24 -asynchronous -group [get_clocks -of_objects [get_pins design_1_i/design_1_i/os_wrapper_vhdl_0/U0/os_wrapper_i/dma_driver_inst/dma_inst/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_intclk/O]] -group [get_clocks {sys_clk}]

set_property PACKAGE_PIN AL11 [get_ports {pcie_txp[0]}]
set_property PACKAGE_PIN AL10 [get_ports {pcie_txn[0]}]
set_property PACKAGE_PIN AL2  [get_ports {pcie_rxp[0]}]
set_property PACKAGE_PIN AL1  [get_ports {pcie_rxn[0]}]

set_property PACKAGE_PIN AM9  [get_ports {pcie_txp[1]}]
set_property PACKAGE_PIN AM8  [get_ports {pcie_txn[1]}]
set_property PACKAGE_PIN AM4  [get_ports {pcie_rxp[1]}]
set_property PACKAGE_PIN AM3  [get_ports {pcie_rxn[1]}]

set_property PACKAGE_PIN AN11 [get_ports {pcie_txp[2]}]
set_property PACKAGE_PIN AN10 [get_ports {pcie_txn[2]}]
set_property PACKAGE_PIN AN6  [get_ports {pcie_rxp[2]}]
set_property PACKAGE_PIN AN5  [get_ports {pcie_rxn[2]}]

set_property PACKAGE_PIN AP9  [get_ports {pcie_txp[3]}]
set_property PACKAGE_PIN AP8  [get_ports {pcie_txn[3]}]
set_property PACKAGE_PIN AN2  [get_ports {pcie_rxp[3]}]
set_property PACKAGE_PIN AN1  [get_ports {pcie_rxn[3]}]

set_property PACKAGE_PIN AR11 [get_ports {pcie_txp[4]}]
set_property PACKAGE_PIN AR10 [get_ports {pcie_txn[4]}]
set_property PACKAGE_PIN AP4  [get_ports {pcie_rxp[4]}]
set_property PACKAGE_PIN AP3  [get_ports {pcie_rxn[4]}]

set_property PACKAGE_PIN AR7  [get_ports {pcie_txp[5]}]
set_property PACKAGE_PIN AR6  [get_ports {pcie_txn[5]}]
set_property PACKAGE_PIN AR2  [get_ports {pcie_rxp[5]}]
set_property PACKAGE_PIN AR1  [get_ports {pcie_rxn[5]}]

set_property PACKAGE_PIN AT9  [get_ports {pcie_txp[6]}]
set_property PACKAGE_PIN AT8  [get_ports {pcie_txn[6]}]
set_property PACKAGE_PIN AT4  [get_ports {pcie_rxp[6]}]
set_property PACKAGE_PIN AT3  [get_ports {pcie_rxn[6]}]

set_property PACKAGE_PIN AU11 [get_ports {pcie_txp[7]}]
set_property PACKAGE_PIN AU10 [get_ports {pcie_txn[7]}]
set_property PACKAGE_PIN AU2  [get_ports {pcie_rxp[7]}]
set_property PACKAGE_PIN AU1  [get_ports {pcie_rxn[7]}]

set_property PACKAGE_PIN AU7  [get_ports {pcie_txp[8]}]
set_property PACKAGE_PIN AU6  [get_ports {pcie_txn[8]}]
set_property PACKAGE_PIN AV4  [get_ports {pcie_rxp[8]}]
set_property PACKAGE_PIN AV3  [get_ports {pcie_rxn[8]}]

set_property PACKAGE_PIN AV9  [get_ports {pcie_txp[9]}]
set_property PACKAGE_PIN AV8  [get_ports {pcie_txn[9]}]
set_property PACKAGE_PIN AW6  [get_ports {pcie_rxp[9]}]
set_property PACKAGE_PIN AW5  [get_ports {pcie_rxn[9]}]

set_property PACKAGE_PIN AW11 [get_ports {pcie_txp[10]}]
set_property PACKAGE_PIN AW10 [get_ports {pcie_txn[10]}]
set_property PACKAGE_PIN AW2  [get_ports {pcie_rxp[10]}]
set_property PACKAGE_PIN AW1  [get_ports {pcie_rxn[10]}]

set_property PACKAGE_PIN AY9  [get_ports {pcie_txp[11]}]
set_property PACKAGE_PIN AY8  [get_ports {pcie_txn[11]}]
set_property PACKAGE_PIN AY4  [get_ports {pcie_rxp[11]}]
set_property PACKAGE_PIN AY3  [get_ports {pcie_rxn[11]}]

set_property PACKAGE_PIN BA11 [get_ports {pcie_txp[12]}]
set_property PACKAGE_PIN BA10 [get_ports {pcie_txn[12]}]
set_property PACKAGE_PIN BA6  [get_ports {pcie_rxp[12]}]
set_property PACKAGE_PIN BA5  [get_ports {pcie_rxn[12]}]

set_property PACKAGE_PIN BB9  [get_ports {pcie_txp[13]}]
set_property PACKAGE_PIN BB8  [get_ports {pcie_txn[13]}]
set_property PACKAGE_PIN BA2  [get_ports {pcie_rxp[13]}]
set_property PACKAGE_PIN BA1  [get_ports {pcie_rxn[13]}]

set_property PACKAGE_PIN BC11 [get_ports {pcie_txp[14]}]
set_property PACKAGE_PIN BC10 [get_ports {pcie_txn[14]}]
set_property PACKAGE_PIN BB4  [get_ports {pcie_rxp[14]}]
set_property PACKAGE_PIN BB3  [get_ports {pcie_rxn[14]}]

set_property PACKAGE_PIN BC7  [get_ports {pcie_txp[15]}]
set_property PACKAGE_PIN BC6  [get_ports {pcie_txn[15]}]
set_property PACKAGE_PIN BC2  [get_ports {pcie_rxp[15]}]
set_property PACKAGE_PIN BC1  [get_ports {pcie_rxn[15]}]

#############################################################
#############   DDR4 memory pin definitions    ##############
#############################################################
current_instance design_1_i/design_1_i/ddr4_0/inst
set_property LOC MMCM_X0Y1 [get_cells -hier -filter {NAME =~ */u_ddr4_infrastructure/gen_mmcme*.u_mmcme_adv_inst}]
set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_pins -hier -filter {NAME =~ */u_ddr4_infrastructure/gen_mmcme*.u_mmcme_adv_inst/CLKIN1}]


set_property PACKAGE_PIN BF50 [get_ports {ddr4_adr[0]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[0]}]
set_property PACKAGE_PIN BD51 [get_ports {ddr4_adr[1]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[1]}]
set_property PACKAGE_PIN BG48 [get_ports {ddr4_adr[2]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[2]}]
set_property PACKAGE_PIN BE50 [get_ports {ddr4_adr[3]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[3]}]
set_property PACKAGE_PIN BE49 [get_ports {ddr4_adr[4]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[4]}]
set_property PACKAGE_PIN BE51 [get_ports {ddr4_adr[5]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[5]}]
set_property PACKAGE_PIN BF53 [get_ports {ddr4_adr[6]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[6]}]
set_property PACKAGE_PIN BG50 [get_ports {ddr4_adr[7]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[7]}]
set_property PACKAGE_PIN BF51 [get_ports {ddr4_adr[8]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[8]}]
set_property PACKAGE_PIN BG47 [get_ports {ddr4_adr[9]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[9]}]
set_property PACKAGE_PIN BF47 [get_ports {ddr4_adr[10]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[10]}]
set_property PACKAGE_PIN BG49 [get_ports {ddr4_adr[11]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[11]}]
set_property PACKAGE_PIN BF48 [get_ports {ddr4_adr[12]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[12]}]
set_property PACKAGE_PIN BF52 [get_ports {ddr4_adr[13]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_adr[13]}]

set_property PACKAGE_PIN BE54 [get_ports {ddr4_ba[0]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_ba[0]}]
set_property PACKAGE_PIN BE53 [get_ports {ddr4_ba[1]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_ba[1]}]
set_property PACKAGE_PIN BG54 [get_ports {ddr4_bg[0]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_bg[0]}]
set_property PACKAGE_PIN BG53 [get_ports ddr4_we_b]
set_property IOSTANDARD SSTL12_DCI [get_ports ddr4_we_b]
set_property PACKAGE_PIN BJ54 [get_ports ddr4_ras_b]
set_property IOSTANDARD SSTL12_DCI [get_ports ddr4_ras_b]
set_property PACKAGE_PIN BH54 [get_ports ddr4_cas_b]
set_property IOSTANDARD SSTL12_DCI [get_ports ddr4_cas_b]

set_property PACKAGE_PIN BH52 [get_ports {ddr4_cke[0]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_cke[0]}]
set_property PACKAGE_PIN BG52 [get_ports ddr4_act_n]
set_property IOSTANDARD SSTL12_DCI [get_ports ddr4_act_n]
#set_property PACKAGE_PIN BJ53       [get_ports {ddr4_ten}];
#set_property IOSTANDARD  SSTL12_DCI [get_ports {ddr4_ten}];

set_property PACKAGE_PIN BH49 [get_ports {ddr4_odt[0]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_odt[0]}]
set_property PACKAGE_PIN BP49 [get_ports {ddr4_cs_n[0]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_cs_n[0]}]
# DDR4_BOT_CS_B
set_property PACKAGE_PIN BK48 [get_ports {ddr4_cs_n[1]}]
set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_cs_n[1]}]
#set_property PACKAGE_PIN BJ52       [get_ports {DDR4_ALERT_B}];
#set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_ALERT_B}];
#set_property PACKAGE_PIN BL48       [get_ports {DDR4_PARITY}];
#set_property IOSTANDARD  SSTL12_DCI [get_ports {DDR4_PARITY}];

set_property PACKAGE_PIN BL53 [get_ports {ddr4_dq[69]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[69]}]
set_property PACKAGE_PIN BL52 [get_ports {ddr4_dq[67]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[67]}]
set_property PACKAGE_PIN BM52 [get_ports {ddr4_dq[65]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[65]}]
set_property PACKAGE_PIN BL51 [get_ports {ddr4_dq[71]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[71]}]
set_property PACKAGE_PIN BN49 [get_ports {ddr4_dq[70]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[70]}]
set_property PACKAGE_PIN BM48 [get_ports {ddr4_dq[68]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[68]}]
set_property PACKAGE_PIN BN51 [get_ports {ddr4_dq[64]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[64]}]
set_property PACKAGE_PIN BN50 [get_ports {ddr4_dq[66]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[66]}]
set_property PACKAGE_PIN BE44 [get_ports {ddr4_dq[30]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[30]}]
set_property PACKAGE_PIN BE43 [get_ports {ddr4_dq[24]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[24]}]
set_property PACKAGE_PIN BD42 [get_ports {ddr4_dq[28]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[28]}]
set_property PACKAGE_PIN BC42 [get_ports {ddr4_dq[26]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[26]}]
set_property PACKAGE_PIN BF43 [get_ports {ddr4_dq[27]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[27]}]
set_property PACKAGE_PIN BF42 [get_ports {ddr4_dq[25]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[25]}]
set_property PACKAGE_PIN BF46 [get_ports {ddr4_dq[31]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[31]}]
set_property PACKAGE_PIN BF45 [get_ports {ddr4_dq[29]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[29]}]
set_property PACKAGE_PIN BG45 [get_ports {ddr4_dq[21]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[21]}]
set_property PACKAGE_PIN BG44 [get_ports {ddr4_dq[17]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[17]}]
set_property PACKAGE_PIN BG43 [get_ports {ddr4_dq[22]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[22]}]
set_property PACKAGE_PIN BG42 [get_ports {ddr4_dq[18]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[18]}]
set_property PACKAGE_PIN BK41 [get_ports {ddr4_dq[16]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[16]}]
set_property PACKAGE_PIN BJ41 [get_ports {ddr4_dq[23]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[23]}]
set_property PACKAGE_PIN BH45 [get_ports {ddr4_dq[20]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[20]}]
set_property PACKAGE_PIN BH44 [get_ports {ddr4_dq[19]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[19]}]
set_property PACKAGE_PIN BJ44 [get_ports {ddr4_dq[13]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[13]}]
set_property PACKAGE_PIN BJ43 [get_ports {ddr4_dq[15]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[15]}]
set_property PACKAGE_PIN BK44 [get_ports {ddr4_dq[9]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[9]}]
set_property PACKAGE_PIN BK43 [get_ports {ddr4_dq[11]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[11]}]
set_property PACKAGE_PIN BL43 [get_ports {ddr4_dq[12]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[12]}]
set_property PACKAGE_PIN BL42 [get_ports {ddr4_dq[14]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[14]}]
set_property PACKAGE_PIN BL46 [get_ports {ddr4_dq[10]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[10]}]
set_property PACKAGE_PIN BL45 [get_ports {ddr4_dq[8]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[8]}]
set_property PACKAGE_PIN BN45 [get_ports {ddr4_dq[3]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[3]}]
set_property PACKAGE_PIN BM45 [get_ports {ddr4_dq[0]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[0]}]
set_property PACKAGE_PIN BN44 [get_ports {ddr4_dq[5]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[5]}]
set_property PACKAGE_PIN BM44 [get_ports {ddr4_dq[4]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[4]}]
set_property PACKAGE_PIN BP44 [get_ports {ddr4_dq[1]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[1]}]
set_property PACKAGE_PIN BP43 [get_ports {ddr4_dq[7]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[7]}]
set_property PACKAGE_PIN BP47 [get_ports {ddr4_dq[2]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[2]}]
set_property PACKAGE_PIN BN47 [get_ports {ddr4_dq[6]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[6]}]
set_property PACKAGE_PIN BJ31 [get_ports {ddr4_dq[58]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[58]}]
set_property PACKAGE_PIN BH31 [get_ports {ddr4_dq[60]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[60]}]
set_property PACKAGE_PIN BF33 [get_ports {ddr4_dq[63]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[63]}]
set_property PACKAGE_PIN BF32 [get_ports {ddr4_dq[61]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[61]}]
set_property PACKAGE_PIN BG32 [get_ports {ddr4_dq[59]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[59]}]
set_property PACKAGE_PIN BF31 [get_ports {ddr4_dq[56]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[56]}]
set_property PACKAGE_PIN BH30 [get_ports {ddr4_dq[57]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[57]}]
set_property PACKAGE_PIN BH29 [get_ports {ddr4_dq[62]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[62]}]
set_property PACKAGE_PIN BH35 [get_ports {ddr4_dq[51]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[51]}]
set_property PACKAGE_PIN BH34 [get_ports {ddr4_dq[50]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[50]}]
set_property PACKAGE_PIN BF36 [get_ports {ddr4_dq[55]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[55]}]
set_property PACKAGE_PIN BF35 [get_ports {ddr4_dq[53]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[53]}]
set_property PACKAGE_PIN BG35 [get_ports {ddr4_dq[49]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[49]}]
set_property PACKAGE_PIN BG34 [get_ports {ddr4_dq[54]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[54]}]
set_property PACKAGE_PIN BJ34 [get_ports {ddr4_dq[48]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[48]}]
set_property PACKAGE_PIN BJ33 [get_ports {ddr4_dq[52]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[52]}]
set_property PACKAGE_PIN BL33 [get_ports {ddr4_dq[45]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[45]}]
set_property PACKAGE_PIN BK33 [get_ports {ddr4_dq[43]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[43]}]
set_property PACKAGE_PIN BL31 [get_ports {ddr4_dq[44]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[44]}]
set_property PACKAGE_PIN BK31 [get_ports {ddr4_dq[47]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[47]}]
set_property PACKAGE_PIN BM33 [get_ports {ddr4_dq[46]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[46]}]
set_property PACKAGE_PIN BL32 [get_ports {ddr4_dq[40]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[40]}]
set_property PACKAGE_PIN BP34 [get_ports {ddr4_dq[41]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[41]}]
set_property PACKAGE_PIN BN34 [get_ports {ddr4_dq[42]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[42]}]
set_property PACKAGE_PIN BP32 [get_ports {ddr4_dq[32]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[32]}]
set_property PACKAGE_PIN BN32 [get_ports {ddr4_dq[36]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[36]}]
set_property PACKAGE_PIN BM30 [get_ports {ddr4_dq[37]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[37]}]
set_property PACKAGE_PIN BL30 [get_ports {ddr4_dq[39]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[39]}]
set_property PACKAGE_PIN BP31 [get_ports {ddr4_dq[34]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[34]}]
set_property PACKAGE_PIN BN31 [get_ports {ddr4_dq[38]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[38]}]
set_property PACKAGE_PIN BP29 [get_ports {ddr4_dq[33]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[33]}]
set_property PACKAGE_PIN BP28 [get_ports {ddr4_dq[35]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dq[35]}]

set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[0]}]
set_property PACKAGE_PIN BN46 [get_ports {ddr4_dqs_t[0]}]
set_property PACKAGE_PIN BP46 [get_ports {ddr4_dqs_c[0]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[0]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[1]}]
set_property PACKAGE_PIN BK45 [get_ports {ddr4_dqs_t[1]}]
set_property PACKAGE_PIN BK46 [get_ports {ddr4_dqs_c[1]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[1]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[2]}]
set_property PACKAGE_PIN BH46 [get_ports {ddr4_dqs_t[2]}]
set_property PACKAGE_PIN BJ46 [get_ports {ddr4_dqs_c[2]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[2]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[3]}]
set_property PACKAGE_PIN BE45 [get_ports {ddr4_dqs_t[3]}]
set_property PACKAGE_PIN BE46 [get_ports {ddr4_dqs_c[3]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[3]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[4]}]
set_property PACKAGE_PIN BN29 [get_ports {ddr4_dqs_t[4]}]
set_property PACKAGE_PIN BN30 [get_ports {ddr4_dqs_c[4]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[4]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[5]}]
set_property PACKAGE_PIN BL35 [get_ports {ddr4_dqs_t[5]}]
set_property PACKAGE_PIN BM35 [get_ports {ddr4_dqs_c[5]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[5]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[6]}]
set_property PACKAGE_PIN BK34 [get_ports {ddr4_dqs_t[6]}]
set_property PACKAGE_PIN BK35 [get_ports {ddr4_dqs_c[6]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[6]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[7]}]
set_property PACKAGE_PIN BJ29 [get_ports {ddr4_dqs_t[7]}]
set_property PACKAGE_PIN BK30 [get_ports {ddr4_dqs_c[7]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[7]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_c[8]}]
set_property PACKAGE_PIN BM49 [get_ports {ddr4_dqs_t[8]}]
set_property PACKAGE_PIN BM50 [get_ports {ddr4_dqs_c[8]}]
set_property IOSTANDARD DIFF_POD12_DCI [get_ports {ddr4_dqs_t[8]}]

set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports {ddr4_ck_c[0]}]
set_property PACKAGE_PIN BK53 [get_ports {ddr4_ck_t[0]}]
set_property PACKAGE_PIN BK54 [get_ports {ddr4_ck_c[0]}]
set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports {ddr4_ck_t[0]}]

set_property PACKAGE_PIN BN42 [get_ports {ddr4_dm_n[0]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[0]}]
set_property PACKAGE_PIN BL47 [get_ports {ddr4_dm_n[1]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[1]}]
set_property PACKAGE_PIN BH42 [get_ports {ddr4_dm_n[2]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[2]}]
set_property PACKAGE_PIN BD41 [get_ports {ddr4_dm_n[3]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[3]}]
set_property PACKAGE_PIN BM28 [get_ports {ddr4_dm_n[4]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[4]}]
set_property PACKAGE_PIN BM34 [get_ports {ddr4_dm_n[5]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[5]}]
set_property PACKAGE_PIN BH32 [get_ports {ddr4_dm_n[6]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[6]}]
set_property PACKAGE_PIN BG29 [get_ports {ddr4_dm_n[7]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[7]}]
set_property PACKAGE_PIN BP48 [get_ports {ddr4_dm_n[8]}]
set_property IOSTANDARD POD12_DCI [get_ports {ddr4_dm_n[8]}]

#set_multicycle_path -setup -from [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_in_axis_tdata_s_reg[*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_value_reg[*]/D}] 4
#set_multicycle_path -hold -from [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_value_reg[*]/D}] 3

#set_multicycle_path -setup -from [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/D}] 4

#set_multicycle_path -setup -from [get_pins {design_1_i/design_1_i/IB_handler_0/U0/cmac_tx_reg[2][tdata][*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_value_reg[*]/D}] 4
#set_multicycle_path -setup -from [get_pins {design_1_i/design_1_i/IB_handler_0/U0/cmac_tx_reg[2][tdata][*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/D}] 4
#set_multicycle_path -setup -from [get_pins {design_1_i/design_1_i/IB_handler_0/U0/cmac_tx_reg[2][tkeep][*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/D}] 4
#set_multicycle_path -setup -from [get_pins {design_1_i/design_1_i/IB_handler_0/U0/cmac_tx_reg[2][tkeep][*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_value_reg[*]/D}] 4

#set_multicycle_path -hold -from [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/D}] 3
#set_multicycle_path -hold -from [get_pins {design_1_i/design_1_i/IB_handler_0/U0/cmac_tx_reg[2][tdata][*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/D}] 3
#set_multicycle_path -hold -from [get_pins {design_1_i/design_1_i/IB_handler_0/U0/cmac_tx_reg[2][tdata][*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_value_reg[*]/D}] 3
#set_multicycle_path -hold -from [get_pins {design_1_i/design_1_i/IB_handler_0/U0/cmac_tx_reg[2][tkeep][*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_value_reg[*]/D}] 3
#set_multicycle_path -hold -from [get_pins {design_1_i/design_1_i/IB_handler_0/U0/cmac_tx_reg[2][tkeep][*]/C}] -to [get_pins {design_1_i/design_1_i/icrc32_0/U0/crc_result_reg[*]/D}] 3


set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk_200]
