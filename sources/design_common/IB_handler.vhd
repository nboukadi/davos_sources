------------------------------------------------------------------- 
-- This entity is reponsible for handling:
-- RDMA ConnectRequests
-- RDMA Send messages containing reserved memory info from the server (Vaddr, rkey, DMA len)
-- The HLS core is not (yet) able to process these instructions itself
------------------------------------------------------------------- 

library ieee;
use ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.crc32.all;
use work.utility.all;
use work.rdma_pkg.all;

entity IB_handler is
--  generic(
--    constant qpn : integer
--  );
  port ( 
    core_clk : in std_logic;
    core_rst_n : in std_logic;
   
    con_start : in std_logic;
    
    -- signals for qp connection
    qp_connected : out std_logic;
    rem_psn      : out std_logic_vector(23 downto 0);
    rem_qpn      : out std_logic_vector(23 downto 0);
    rem_ip       :  out std_logic_vector(31 downto 0);
    rem_mac      :  out std_logic_vector(23 downto 0);
    
    s_axis_qp_interface_tvalid    : out std_logic;          
    s_axis_qp_interface_tready    : in std_logic;                              
    s_axis_qp_interface_tdata     : out std_logic_vector(143 downto 0);     
                             
    s_axis_qp_conn_interface_tvalid : out std_logic;                            
    s_axis_qp_conn_interface_tready : in std_logic;                           
    s_axis_qp_conn_interface_tdata  : out std_logic_vector(183 downto 0);     
   
    s_axis_roce_role_tx_meta_tvalid : out std_logic;                         
    s_axis_roce_role_tx_meta_tready : in std_logic;                        
    s_axis_roce_role_tx_meta_tdata  : out std_logic_vector(191 downto 0); 
    
    -- Provide data for in RDMA Write messages
    s_axis_roce_role_tx_data_tvalid : out std_logic;                         
    s_axis_roce_role_tx_data_tready : in std_logic;                        
    s_axis_roce_role_tx_data_tdata  : out std_logic_vector(511 downto 0);    
    s_axis_roce_role_tx_data_tkeep  : out std_logic_vector(63 downto 0);     
    s_axis_roce_role_tx_data_tlast  : out std_logic; 
    
    m_axis_os_data_tvalid : out std_logic;                         
    --m_axis_os_data_tready : in std_logic;                        
    m_axis_os_data_tdata  : out std_logic_vector(511 downto 0);    
    m_axis_os_data_tkeep  : out std_logic_vector(63 downto 0);     
    m_axis_os_data_tlast  : out std_logic; 
    
    --signal mem  : out t_mem;
    
    -- input streaming signals from CMAC
    cmac_rx_axis_tdata : in std_logic_vector(511 downto 0);
    cmac_rx_axis_tkeep : in std_logic_vector(63 downto 0);
    cmac_rx_axis_tlast : in std_logic;
    cmac_rx_axis_tuser : in std_logic_vector(0 downto 0);
    cmac_rx_axis_tvalid: in std_logic;
    --input         s_axis_tready,
    
    -- Response to CMAC (IB_ConReq, IB_Ready etc.)
    cmac_tx_axis_tdata : out std_logic_vector(511 downto 0);
    cmac_tx_axis_tkeep : out std_logic_vector(63 downto 0);
    cmac_tx_axis_tlast : out std_logic;
    cmac_tx_axis_tuser : out std_logic_vector(0 downto 0);
    cmac_tx_axis_tvalid : out std_logic
    
  );
end IB_handler;

architecture filter of IB_handler is
    ------------------------------------------------------------------- 
    -- Defines frequency values used in the Vivado Block Design
    ------------------------------------------------------------------- 
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER of s_axis_qp_interface_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
    ATTRIBUTE X_INTERFACE_PARAMETER of s_axis_qp_conn_interface_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
    ATTRIBUTE X_INTERFACE_PARAMETER of s_axis_roce_role_tx_meta_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
    ATTRIBUTE X_INTERFACE_PARAMETER of s_axis_roce_role_tx_data_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
    ATTRIBUTE X_INTERFACE_PARAMETER of m_axis_os_data_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
    ATTRIBUTE X_INTERFACE_PARAMETER of cmac_rx_axis_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
    ATTRIBUTE X_INTERFACE_PARAMETER of cmac_tx_axis_tdata : SIGNAL is "ASSOCIATED_RESET reset_in, FREQ_HZ 200000232";
          
    type state_type is (IDLE, ConReq, ConRep, RdyUse, RcAck, DisReq, DisRep, RX_PKT, DMA_PKT, ChkSum, MadWord, RecvSend);
    signal current_state : state_type;
    
    type cmac_tx_arr is array(1 to 5) of t_axis_out;
    signal cmac_tx : cmac_tx_arr;
    signal cmac_tx_reset : t_axis_out := ( 
      tdata => (others => '0'),
      tkeep => (others => '0'),
      tlast => '0',            
      tuser => (others => '0'),
      tvalid => '0'
    );
    signal mem  : t_mem;
    
    type t_qp_int is record
      qpState    : std_logic_vector(2 downto 0);
      qp_num     : std_logic_vector(23 downto 0);
      remote_psn : std_logic_vector(23 downto 0);
      local_psn  : std_logic_vector(23 downto 0);
      r_key      : std_logic_vector(15 downto 0);
      vaddr      : std_logic_vector(47 downto 0);
      valid      : std_logic;
    end record t_qp_int;
    
    type t_conn_int is record
      local_qp   : std_logic_vector(15 downto 0);
      remote_qp  : std_logic_vector(23 downto 0);
      remote_ip  : std_logic_vector(127 downto 0);
      remote_ud  : std_logic_vector(15 downto 0);
      valid      : std_logic;
    end record t_conn_int;
    
    type t_qp_temp is record
      local_qp   : std_logic_vector(23 downto 0);
      r_key      : std_logic_vector(31 downto 0);
      v_addr     : std_logic_vector(47 downto 0);
      dma_len    : std_logic_vector(31 downto 0);
      valid      : std_logic;
    end record t_qp_temp;
    
    signal qp   : t_qp_int;
    signal qpt  : t_qp_temp;
    signal conn : t_conn_int;
    
    -- IB frame related signals 
    constant source_ip : std_logic_vector(31 downto 0) := x"20a0a8c0";      --external
    constant source_mac : std_logic_vector(47 downto 0) := x"9a5edc177620"; --external
    
    constant target_mac : std_logic_vector(47 downto 0) :=  x"5a38b8078a24"; --Change to register later  (24:8a:07:b8:38:5a)
    constant target_ip  : std_logic_vector(31 downto 0) :=  x"0aa0a8c0";     --Change to register later

    signal mad_data : std_logic_vector(511 downto 0);
        
    -- Store in QPx record?
    signal LocQPN   : std_logic_vector(23 downto 0) := x"cd0000"; 
    signal RemQPN   : std_logic_vector(23 downto 0);
    signal LocGUID  : std_logic_vector(63 downto 0) := x"5a38b80003078a24";
    signal DesPort  : std_logic_vector(15 downto 0) := x"2f1d";       --external
    signal LocComID : std_logic_vector(31 downto 0) := x"2eb279dc";   --external
    signal RemComID : std_logic_vector(31 downto 0);
    signal AttrID   : std_logic_vector(31 downto 0) := x"00001000"; 
    signal TransID  : std_logic_vector(63 downto 0) := x"dc79d28e02000000";
    signal QueKey   : std_logic_vector(31 downto 0) := x"00000180";
    signal StartPSN : std_logic_vector(23 downto 0) := x"da2519";
    signal RemPSN   : std_logic_vector(23 downto 0);
    signal ConReady, ConEstablished : std_logic;
    signal MsgSeqNum, MsgSeqNum_p : std_logic_vector(23 downto 0) := x"100000";
    signal SeqNum : integer;
    signal IpId : std_logic_vector(15 downto 0);
    
    signal recv_mac : std_logic_vector(47 downto 0);
    signal recv_ip : std_logic_vector(31 downto 0);
    
    signal word_cnt : integer;
    signal cal_checksum : std_logic;
    
    -- QP related signals
    signal qp_disconnected : std_logic;
    
    signal s_axis_qp_conn_interface_tbusy : std_logic;
    signal s_axis_qp_interface_tbusy: std_logic;
    
    signal datagen_enable : std_logic;
    signal tx_active, tx_meta_active : std_logic;
  
begin

    ------------------------------------------------------------------- 
    -- Passthrough so all RDMA data will enter the RoCEv2 core
    ------------------------------------------------------------------- 
    m_axis_os_data_tvalid <= cmac_rx_axis_tvalid;
    m_axis_os_data_tdata  <= cmac_rx_axis_tdata ;
    m_axis_os_data_tkeep  <= cmac_rx_axis_tkeep ;
    m_axis_os_data_tlast  <= cmac_rx_axis_tlast ;
    
    
     -- QP configurations 
    --s_axis_qp_interface_tdata(2 downto 0)   <= qp.qpState;
    --s_axis_qp_interface_tdata(26 downto 3)  <= qp.qp_num;
    --s_axis_qp_interface_tdata(50 downto 27) <= qp.remote_psn;
    --s_axis_qp_interface_tdata(74 downto 51) <= qp.local_psn;
    --s_axis_qp_interface_tdata(90 downto 75) <= qp.r_key;
    --s_axis_qp_interface_tdata(138 downto 91)<= qp.vaddr;
    --s_axis_qp_interface_tdata(143 downto 139) <= (others => '0');
    --s_axis_qp_interface_tvalid <= qp.valid;
    
    --qpState    <= "001";
    --qp_num     <= x"000011";
    --remote_psn <= x"80ce4e";
    --local_psn  <= x"80ce4e";
    --r_key      <= x"0411";
    --vaddr      <= (others => '0');
    qp.local_psn  <=  x"da2519";
    qp.r_key      <=  x"ffff";
    qp.vaddr      <=  x"000000001200";
                
    
    
    --s_axis_qp_conn_interface_tdata(15 downto 0)    <= conn.local_qp;
    --s_axis_qp_conn_interface_tdata(39 downto 16)   <= conn.remote_qp;
    --s_axis_qp_conn_interface_tdata(167 downto 40)  <= conn.remote_ip;
    --s_axis_qp_conn_interface_tdata(183 downto 168) <= conn.remote_ud;
    --s_axis_qp_conn_interface_tvalid <= qp.valid;
    --s_axis_qp_conn_interface_tvalid <= '1' when s_axis_qp_conn_interface_tready = '1' and qp.valid = '1';
    --local_qp <= x"0011"; 
    --remote_qp <= x"000003";
    --remote_ip (95 downto 0) <= (others => '0');
    --remote_ip (127 downto 96) <= x"0aa0a8c0";
    --remote_ud <= x"4853";
    
    ------------------------------------------------------------------- 
    -- Handles the RDMA ConnectRequest data
    ------------------------------------------------------------------- 
    proc_qpconn : process (core_clk)
      variable count : integer := 0;
    begin
      if rising_edge (core_clk) then
        if core_rst_n = '0' then
          s_axis_qp_conn_interface_tvalid <= '0';
          s_axis_qp_conn_interface_tdata  <= (others => '0');
          s_axis_qp_conn_interface_tbusy  <= '0';
        else
          if s_axis_qp_conn_interface_tbusy = '1' then
            if s_axis_qp_conn_interface_tready = '1' then
              s_axis_qp_conn_interface_tvalid <= '0';
              s_axis_qp_conn_interface_tbusy  <= '0';
            end if;
          elsif qp.valid = '1' then
            s_axis_qp_conn_interface_tdata(15 downto 0)    <= conn.local_qp(7 downto 0) & conn.local_qp(15 downto 8);
            s_axis_qp_conn_interface_tdata(39 downto 16)   <= conn.remote_qp(7 downto 0) & conn.remote_qp(15 downto 8) & conn.remote_qp(23 downto 16);
            s_axis_qp_conn_interface_tdata(167 downto 40)  <= conn.remote_ip;
            s_axis_qp_conn_interface_tdata(183 downto 168) <= conn.remote_ud;
            s_axis_qp_conn_interface_tvalid <= '1';
            s_axis_qp_conn_interface_tbusy <= '1';
          end if;      
        end if;
      end if;
    end process;
    conn.remote_ud <= x"f1c6";
    
    proc_qpint : process (core_clk)
      variable count : integer := 0;
    begin
      if rising_edge (core_clk) then
        if core_rst_n = '0' then
          s_axis_qp_interface_tvalid <= '0';
          s_axis_qp_interface_tdata  <= (others => '0');
          s_axis_qp_interface_tbusy  <= '0';
        else
          if s_axis_qp_interface_tbusy = '1' then
            if s_axis_qp_interface_tready = '1' then
              s_axis_qp_interface_tvalid <= '0';
              s_axis_qp_interface_tbusy  <= '0';
            end if;
          elsif qp.valid = '1' then
            s_axis_qp_interface_tdata(2 downto 0)   <= qp.qpState;
            s_axis_qp_interface_tdata(26 downto 3)  <= qp.qp_num;
            s_axis_qp_interface_tdata(50 downto 27) <= qp.remote_psn(7 downto 0) & qp.remote_psn(15 downto 8) & qp.remote_psn(23 downto 16);
            s_axis_qp_interface_tdata(74 downto 51) <= qp.local_psn(7 downto 0) & qp.local_psn(15 downto 8) & qp.local_psn(23 downto 16);
            s_axis_qp_interface_tdata(90 downto 75) <= qp.r_key(7 downto 0) & qp.r_key(15 downto 8);
            s_axis_qp_interface_tdata(138 downto 91)<= qp.vaddr;
            s_axis_qp_interface_tdata(143 downto 139) <= (others => '0');
            s_axis_qp_interface_tvalid <= '1';
            s_axis_qp_interface_tbusy <= '1';
          end if;      
        end if;
      end if;
    end process;
    
    ------------------------------------------------------------------- 
    -- After establishing a connection, data will be generated
    -- If done earlier, the core cannot handle the data
    ------------------------------------------------------------------- 
    enable_datagen : process(core_clk) 
    begin
      if rising_edge (core_clk) then
        if core_rst_n = '0' then
          datagen_enable <= '0';
        else
          if qpt.valid = '1' then
            datagen_enable <= '1';
          end if;
        end if;
      end if;
    end process; 
    
    ------------------------------------------------------------------- 
    -- Generates data to transmit in a RDMA Write message 
    ------------------------------------------------------------------- 
    s_arp4 : process (core_clk)
      variable count : integer := 0;
    begin
      if rising_edge (core_clk) then
        if core_rst_n = '0' then
          s_axis_roce_role_tx_data_TVALID <= '0';
          s_axis_roce_role_tx_data_TDATA  <= (others => '0');
          s_axis_roce_role_tx_data_TKEEP  <=(others => '0') ;
          s_axis_roce_role_tx_data_TLAST  <= '0';
          count := 0;
        else
          s_axis_roce_role_tx_data_TKEEP  <=(others => '1') ;
          s_axis_roce_role_tx_data_TLAST  <= '0';
          
          if qpt.valid = '1' then
            tx_active <= '1';
          end if; 
          
          if tx_active = '1' then
            if count = 0 then
              s_axis_roce_role_tx_data_TVALID <= '1';
              if s_axis_roce_role_tx_data_TREADY = '1' then
                count := 1;
                s_axis_roce_role_tx_data_TLAST  <= '0';
              end if;
              
            elsif count = 20 and s_axis_roce_role_tx_data_TREADY = '1' then
                s_axis_roce_role_tx_data_TLAST  <= '1';
                count := count + 1;
                
            elsif count = 21 and s_axis_roce_role_tx_data_TREADY = '1'then
                s_axis_roce_role_tx_data_TVALID <= '0';
                s_axis_roce_role_tx_data_TLAST  <= '0';
                count := count + 1;
                
            elsif count = 22 then --and s_axis_tx_data_TREADY = '1' then
              count := 0;
              tx_active <= '0';
              
            elsif s_axis_roce_role_tx_data_TREADY = '1' then
              count := count + 1;
            end if;  
          end if;
          
          s_axis_roce_role_tx_data_TDATA  <= (others => '1');
          s_axis_roce_role_tx_data_TDATA(511 downto 504)  <= std_logic_vector(to_unsigned(count,8));    
        end if;
      end if;
    end process;
    
    ------------------------------------------------------------------- 
    -- Generates meta data with additional tx data info
    ------------------------------------------------------------------- 
    s_arp5 : process (core_clk)
      variable count : integer := 0;
    begin
      if rising_edge (core_clk) then
        if core_rst_n = '0' then
          s_axis_roce_role_tx_meta_tvalid <= '0';
          count := 0;
          
        else
          s_axis_roce_role_tx_meta_tvalid <= '0';
          
          if qpt.valid = '1' then
            tx_meta_active <= '1';
          end if; 
          
          if tx_meta_active = '1' then
            if count = 0  then
              s_axis_roce_role_tx_meta_tvalid <= '1';
              if s_axis_roce_role_tx_meta_tready = '1' then
                count := 1;
              end if;
            elsif count = 1 then
              s_axis_roce_role_tx_meta_tvalid <= '0';
            elsif count = 21 then
              count := 0;
              tx_meta_active <= '0';
            else
              count := count + 1;
            end if;  
          end if;
              
        end if;
      end if;
    end process;
    
    --s_arp4 : process (core_clk)
    --  variable count : integer := 0;
    --begin
    --  if rising_edge (core_clk) then
    --    if core_rst_n = '0' then
    --      s_axis_roce_role_tx_meta_tvalid <= '0';
    --      s_axis_roce_role_tx_data_TVALID <= '0';
    --      s_axis_roce_role_tx_data_TDATA  <= (others => '0');
    --      s_axis_roce_role_tx_data_TKEEP  <=(others => '0') ;
    --      s_axis_roce_role_tx_data_TLAST  <= '0';
    --      count := 0;
    --    else
    --      s_axis_roce_role_tx_meta_tvalid <= '0';
    --      s_axis_roce_role_tx_data_tvalid <= '0';
          
    --      s_axis_roce_role_tx_data_TDATA  <= (others => '1');
    --      s_axis_roce_role_tx_data_TKEEP  <=(others => '1') ;
    --      s_axis_roce_role_tx_data_TLAST  <= '0';
    --      if qp.qpState = "001" then
    --        if count = 0 then
    --          s_axis_roce_role_tx_meta_tvalid <= '1';
    --          s_axis_roce_role_tx_data_TVALID <= '1';
    --          s_axis_roce_role_tx_data_TLAST  <= '1';
    --          if s_axis_roce_role_tx_data_TREADY = '1' then
    --            count := 1;
    --            s_axis_roce_role_tx_meta_tvalid <= '0';
    --            s_axis_roce_role_tx_data_TVALID <= '0';
    --            s_axis_roce_role_tx_data_TLAST  <= '0';
    --          end if;
    --          -- TX data config
    ----          s_axis_tx_meta_V_TDATA(2 downto 0) <= "001"; --APP_WRITE
    ----          s_axis_tx_meta_V_TDATA(26 downto 3) <= x"000011";--rem_qpn; --QPN
    ----          s_axis_tx_meta_V_TDATA(74 downto 27) <= x"000000001200";--
    ----          s_axis_tx_meta_V_TDATA(122 downto 75) <= (others => '0');--rem_vaddr;
    ----          s_axis_tx_meta_V_TDATA(154 downto 123) <= x"00000040"; --length;
    ----          s_axis_tx_meta_V_TDATA(159 downto 155) <= (others => '0');
    --        elsif count = 10 then
    --          count := 0;
    --        else
    --          count := count + 1;
    --        end if; 
    --      end if;     
    --    end if;
    --  end if;
    --end process;
    process (core_clk) 
    begin
      if rising_edge(core_clk) then
        if qpt.valid = '1' then
          s_axis_roce_role_tx_meta_tdata(2 downto 0) <= "001"; --APP_WRITE
          s_axis_roce_role_tx_meta_tdata(26 downto 3) <= reverse_bytes(qpt.local_qp);-- x"000003";--rem_qpn; --QPN
          s_axis_roce_role_tx_meta_tdata(74 downto 27) <= (others => '0');--
          s_axis_roce_role_tx_meta_tdata(122 downto 75) <= reverse_bytes(qpt.v_addr);--x"000000001200";--x"000070000000";--(others => '0');--rem_vaddr;
          s_axis_roce_role_tx_meta_tdata(154 downto 123) <= reverse_bytes(qpt.r_key);--x"12ad4521"; --rkey;z
          s_axis_roce_role_tx_meta_tdata(186 downto 155) <= reverse_bytes(qpt.dma_len);--x"00000580"; --length;
          s_axis_roce_role_tx_meta_tdata(191 downto 187) <= (others => '0');
          
        end if;
      end if;
    end process;
    
    --gen_instructions : process (core_clk)
    --  variable count : integer := 0;
    --begin
    --  if rising_edge (core_clk) then
    --    if core_rst_n = '0' then
    --      s_axis_qp_interface_TVALID <= '0';
    --      count := 0;
    --    else
    --      s_axis_qp_interface_TVALID <= '0';
    --      if count = 0 then
    --        s_axis_qp_interface_TVALID <= '1'; 
    --        if s_axis_qp_interface_TREADY = '1' then
    --          count := 1;
    --          s_axis_qp_interface_TVALID <= '0'; 
    --        end if;
    --        -- QP configurations 
    ----        s_axis_qp_interface_V_TDATA(2 downto 0)   <= qpState;
    ----        s_axis_qp_interface_V_TDATA(26 downto 3)  <= qp_num;
    ----        s_axis_qp_interface_V_TDATA(50 downto 27) <= remote_psn;
    ----        s_axis_qp_interface_V_TDATA(74 downto 51) <= local_psn;
    ----        s_axis_qp_interface_V_TDATA(90 downto 75) <= r_key;
    ----        s_axis_qp_interface_V_TDATA(138 downto 91) <= vaddr;
    ----        s_axis_qp_interface_V_TDATA(143 downto 139) <= (others => '0');
    --      elsif count = 10 then
    --        count := 0;
    --      else
    --        count := count + 1;
    --      end if;      
    --    end if;
    --  end if;
    --end process;
    
    ------------------------------------------------------------------ 
    -- Calculate the checksum in the IP header
    -------------------------------------------------------------------
      process(core_clk) is
        variable checksum_inv : std_logic_vector(23 downto 0);
        variable checksum  : std_logic_vector(15 downto 0);
        variable ip_header : std_logic_vector(159 downto 0);
        variable checksum_carry : std_logic_vector(7 downto 0);
      begin
        if rising_edge(core_clk) then
          if(core_rst_n = '0') then
             cmac_tx(2).tdata    <= (others => '0');
             cmac_tx(2).tkeep    <= (others => '0');
             cmac_tx(2).tlast    <= '0';
             cmac_tx(2).tuser(0) <= '0';
             cmac_tx(2).tvalid   <= '0';
          else
            if (cal_checksum = '1') then
              checksum_inv := (others => '0');
              ip_header := cmac_tx(1).tdata(271 downto 112);
              
              for i in 0 to 9 loop
                checksum_inv := checksum_inv + ip_header(16*i+15 downto 16*i);
              end loop; 
              
              if(checksum_inv(23 downto 16) /= x"00") then
                checksum_carry := checksum_inv(23 downto 16);
                checksum_inv(23 downto 16) := x"00";
                checksum_inv(15 downto 0)  := (checksum_inv(15 downto 0) + checksum_carry);
              end if;
              
              if(checksum_inv(23 downto 16) = x"00") then
                cmac_tx(2).tdata    <= cmac_tx(1).tdata;  
                cmac_tx(2).tdata(207 downto 192) <= not checksum_inv(15 downto 0);   
                cmac_tx(2).tkeep    <= cmac_tx(1).tkeep;   
                cmac_tx(2).tlast    <= cmac_tx(1).tlast;
                cmac_tx(2).tuser(0) <= cmac_tx(1).tuser(0);
                cmac_tx(2).tvalid   <= cmac_tx(1).tvalid;
              end if;
            else
              cmac_tx(2) <= cmac_tx(1);
            end if; 
          end if;
        end if;
      end process;
      
      cmac_tx_axis_tdata    <= cmac_tx(2).tdata;
      cmac_tx_axis_tkeep    <= cmac_tx(2).tkeep;
      cmac_tx_axis_tlast    <= cmac_tx(2).tlast;
      cmac_tx_axis_tuser(0) <= cmac_tx(2).tuser(0);
      cmac_tx_axis_tvalid   <= cmac_tx(2).tvalid;
      
      ------------------------------------------------------------------ 
      -- State machine which generates MAD words/replies
      ------------------------------------------------------------------ 
      process(core_clk,  con_start, cmac_rx_axis_tvalid ) is 
        variable rec_psn : std_logic_vector(23 downto 0);
      begin
        if rising_edge(core_clk) then
          if(core_rst_n = '0') then 
            current_state<= IDLE;        
            cmac_tx(1)   <= cmac_tx_reset;
            cal_checksum <= '0';
            MsgSeqNum    <= StartPSN;
            MsgSeqNum_p  <= x"010000";
            SeqNum   <= 0;
            word_cnt <= 0;
            IpId     <= x"1000";
            qp_disconnected <= '0';
            RemPSN <= (others => '0');
            RemQPN <= (others => '0');
            ConEstablished  <= '0';
            
            mem.transid <= (others => '0');
            mem.ip      <= (others => '0');
            mem.mac     <= (others => '0');
            mem.valid   <= '0';
            qp.valid <= '0';
            mem.locqpn  <= (others => '0');
            mem.remqpn  <= (others => '0');
            mem.enable  <= '0';
            
            qpt.v_addr   <= (others => '0');
            qpt.r_key    <= (others => '0');
            qpt.local_qp <= (others => '0');
            qpt.dma_len  <= (others => '0');
            qpt.valid    <= '0';
          else  
            mem.valid  <= '0';
            qp.valid <= '0';
            cmac_tx(1) <= cmac_tx_reset;
            IpId       <= IpId + 1;
            MsgSeqNum  <= reverse_bytes(std_logic_vector(to_unsigned(SeqNum,MsgSeqNum'length)));
            
            case (current_state) is
            when IDLE =>
              word_cnt <= 0;
               -- Has already been checked whether the package is IPv4, UDP and destport(4791)
              if(cmac_rx_axis_tvalid = '1') then
                
                --Receiving MAD packet. Next 512 bit word wil contain which MAD word has been send.
                --Modify with pipeline to see next bits in this cycle?
                if((cmac_rx_axis_tdata(343 downto 336 ) = x"64") and (cmac_rx_axis_tdata(511 downto 504) = x"07" )) then 
                  QueKey        <= cmac_rx_axis_tdata(463 downto 432);
                  current_state <= MadWord;
                  
                  recv_mac <= cmac_rx_axis_tdata(95 downto 48);
                  recv_ip  <= cmac_rx_axis_tdata(239 downto 208);
                  conn.remote_ip (95 downto 0) <= (others => '0');
                  conn.remote_ip(127 downto 96) <= cmac_rx_axis_tdata(239 downto 208);--cmac_rx_axis_tdata(215 downto 208) & cmac_rx_axis_tdata(223 downto 216) & cmac_rx_axis_tdata(231 downto 224) & cmac_rx_axis_tdata(239 downto 232);
                
                -- Reads RDMA Send (Contains Vaddr, Rkey and DMA length)
                elsif((cmac_rx_axis_tdata(343 downto 336 ) = (x"04"))) then -- and mem.enable = '1') then--qp_connected = '1') then
                  qpt.v_addr                 <= cmac_rx_axis_tdata(495 downto 448);--cmac_rx_axis_tdata(495 downto 432);
                  qpt.r_key(15 downto 0)     <= cmac_rx_axis_tdata(511 downto 496);
                  qpt.local_qp(23 downto 0) <= cmac_rx_axis_tdata(399 downto 376);
                  current_state <= RecvSend;
                  
    --              target_ip  <= mem.ip;
    --              target_mac <= mem.mac;
    --              RemPSN     <= mem.rempsn;
                  rec_psn := cmac_rx_axis_tdata(431 downto 408);  
                  
                --elsif((cmac_rx_axis_tdata(343 downto 336 ) = (x"0A")) and ConEstablished = '1') then
                  --ConEstablished <= '0';
                
                -- No MAD Packet but RoCE UDP so must be RDMA Read/Send/Write. Has to edited later!!!!  
                else 
                
                end if;
              
              -- Option to let the FPGA start RDMA connection. Registers to be added!           
              elsif(con_start = '1') then
                mad_data      <= x"07010100000000000180" & MsgSeqNum & x"0001000000ffff4064" & x"00002001b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"34010245" & x"0008" & source_mac & target_mac;
                AttrID        <= x"00001000"; -- Connect request
                word_cnt      <= 0;
                current_state <= ConReq;
                SeqNum <= SeqNum + 1;
                IpId <= IpId + 1;
              end if;
              TransID <= TransID + 1;
              cal_checksum <= '0';
              --StartPSN <= StartPSN + x"FF";
            
            -- Processed receiverd RDMA send message
            when RecvSend => 
              cal_checksum <= '0';
              case word_cnt is
              when 0 =>
                qpt.r_key(31 downto 16) <= cmac_rx_axis_tdata(15 downto 0);
                qpt.dma_len             <= cmac_rx_axis_tdata(47 downto 16);
                qpt.valid               <= '1';
                --qp_send_info <= '1';
                
                mad_data(511 downto 0) <= (others => '0');
                mad_data(463 downto 0) <= x"010000" & x"00" & rec_psn & x"00" & RemQPN & x"00ffff4011" & x"00001c00b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"30000245" & x"0008" & source_mac & target_mac;
                
                MsgSeqNum_p <= MsgSeqNum_p + 1;
                SeqNum <= SeqNum + 1;
                IpId <= IpId + 1;
    
              when 1 =>
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep(63 downto 58) <= (others => '0');
                cmac_tx(1).tkeep(57 downto 0) <= (others => '1');
                cmac_tx(1).tlast    <= '1';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
                cal_checksum <= '1';
                qpt.valid    <= '0';
                --ConEstablished <= '1'; -- Now for single QP. Should be done for every QP and connection and stored in register
                --rev_PSN := reverse_bytes(RemPSN);
                current_state <= idle;
              when others => 
                current_state <= idle;
              end case;
              word_cnt <= word_cnt + 1;
              
            -- Transmit ConnectRequest
            when ConReq =>
              cal_checksum <= '0';
              case word_cnt is
              when 0 =>
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep    <= (others => '1');
                cmac_tx(1).tlast    <= '0';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
                cal_checksum <= '1';
                --cal_icrc <= '1';
                mad_data <= x"00001000000010" & LocQPN & x"0000000000000000" & LocGUID & DesPort & x"060100000000_00000000" & LocComID & x"00000000" & AttrID & TransID & x"000000000302";
                
              when 1 =>
                mad_data(511 downto 0)   <= (others => '0');
                mad_data(431 downto 320) <= x"9800400007000000" & target_ip & x"ffff";
                mad_data(239 downto 192) <= source_ip & x"ffff";
                mad_data(111 downto 0)   <= x"ffffffff" & x"f037ffffb791ba19b000";
                
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep    <= (others => '1');
                cmac_tx(1).tlast    <= '0';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
              when 2 =>
                mad_data(511 downto 0)   <= (others => '0');
                mad_data(431 downto 400) <= source_ip;
                mad_data(303 downto 280) <= x"2fe140";
                
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep    <= (others => '1');
                cmac_tx(1).tlast    <= '0';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
              when 3 => 
                mad_data(511 downto 0) <= (others => '0');
                mad_data(47 downto 16) <= target_ip;
                
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep    <= (others => '1');
                cmac_tx(1).tlast    <= '0';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
              when 4 =>
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep(63 downto 62) <= (others => '0');
                cmac_tx(1).tkeep(61 downto 0)  <= (others => '1');
                cmac_tx(1).tlast    <= '1';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
                
                current_state <= idle;
                word_cnt <= 0;
              when others => 
                current_state <= idle;
              end case;
              word_cnt <= word_cnt + 1;
    
             -- Transmit ReadyToUse         
             when RdyUse =>  
               ConReady <= '0';
               cal_checksum <= '0';
               case word_cnt is
               when 0 =>
                 cmac_tx(1).tdata    <= mad_data;
                 cmac_tx(1).tkeep    <= (others => '1');
                 cmac_tx(1).tlast    <= '0';
                 cmac_tx(1).tuser(0) <= '0';
                 cmac_tx(1).tvalid   <= '1';
                 cal_checksum <= '1';
                 mad_data(511 downto 240) <= (others => '0');
                 mad_data(239 downto 0)   <= RemComID & LocComID & x"00000000" & AttrID & TransID & x"000000000302";
                 
               when 1 to 3 =>
                 mad_data(511 downto 0)   <= (others => '0');
                 cmac_tx(1).tdata    <= mad_data;
                 cmac_tx(1).tkeep    <= (others => '1');
                 cmac_tx(1).tlast    <= '0';
                 cmac_tx(1).tuser(0) <= '0';
                 cmac_tx(1).tvalid   <= '1';
                 
               when 4 =>
                 cmac_tx(1).tdata    <= (others => '0');
                 cmac_tx(1).tkeep(63 downto 62) <= (others => '0');
                 cmac_tx(1).tkeep(61 downto 0)  <= (others => '1');
                 cmac_tx(1).tlast    <= '1';
                 cmac_tx(1).tuser(0) <= '0';
                 cmac_tx(1).tvalid   <= '1';
                 
                 current_state <= idle;
                 word_cnt <= 0;
                 ConReady <= '1';
               when others => 
                 current_state <= idle;
               end case;
               word_cnt <= word_cnt + 1;
            
            -- Decode management diagram type   
            when MadWord =>
              word_cnt <= 0;
              case( cmac_rx_axis_tdata(127 downto 120)) is
              
              -- ConnectReply
              when x"13" => 
                if(cmac_rx_axis_tdata(239 downto 208) = LocComID) then
                  RemComID      <= cmac_rx_axis_tdata(207 downto 176);
                  RemQPN        <= cmac_rx_axis_tdata(295 downto 272);
                  mad_data      <= x"07010100000000000180" & MsgSeqNum & x"0001000000ffff4064" & x"00002001b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"34010245" & x"0008" & source_mac & target_mac;
                  AttrID        <= x"00001400"; --Ready to use
                  current_state <= RdyUse;
                  SeqNum     <= SeqNum + 1;
                  IpId <= IpId + 1;-- IpId(5) xor IpId(12) xor IpId(15);
                else
                  current_state <= idle;
                end if;
                
                -- ReadyToUse
              when x"14" =>  -- Send RC Acknowledge
                ConEstablished <= '1';
                mem.enable  <= '1';
                mem.valid   <= '1';
                mem.locqpn  <= x"030000";
                
                current_state <= idle;
                
              -- DisconnectRequest
              when x"15" => 
                mad_data      <= x"070101000000" & QueKey & MsgSeqNum & x"0001000000ffff4064" & x"00002001b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"34010245" & x"0008" & source_mac & target_mac;
                AttrID        <= x"00001600"; -- DisconnectReply
                TransID       <= cmac_rx_axis_tdata(111 downto 48);
                RemComID      <= cmac_rx_axis_tdata(207 downto 176);
                RemQPN        <= cmac_rx_axis_tdata(455 downto 432);
                SeqNum     <= SeqNum + 1;
                IpId <= IpId + 1;
                current_state <= DisRep;
                ConEstablished <= '0';
                mem.enable  <= '0';
                mem.valid   <= '1';
                mem.locqpn  <= x"030000";
                QP_disconnected <= '1';
                
                qp.qpState    <= "000";
                qp.qp_num     <= x"000003";
                qp.valid      <= '1';
              -- ConnectRequest
              when x"10" => 
                mad_data      <= x"070101000000" & QueKey & MsgSeqNum & x"0001000000ffff4064" & x"00002001b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"34010245" & x"0008" & source_mac & target_mac;
                AttrID        <= x"00001300"; --Ready to use
                LocQPN        <= x"030000"; --QP3 connected to remote QP
                TransID       <= cmac_rx_axis_tdata(111 downto 48);
                RemComID      <= cmac_rx_axis_tdata(207 downto 176);
                RemQPN        <= cmac_rx_axis_tdata(455 downto 432);--cmac_rx_axis_tdata(295 downto 272);
                LocGUID       <= x"9a5edc0003177620";
                
                conn.local_qp  <= x"0300";
                conn.remote_qp <= cmac_rx_axis_tdata(455 downto 432);
                
                qp.qpState    <= "001";
                qp.qp_num     <= x"000003";
                qp.valid <= '0';
                --Store essential QP info
                mem.transid <= cmac_rx_axis_tdata(111 downto 48);
                mem.ip      <= recv_ip;
                mem.mac     <= recv_mac;
                mem.valid   <= '0';
                mem.locqpn  <= x"030000";
                mem.remqpn  <= cmac_rx_axis_tdata(455 downto 432);
                mem.enable  <= '0';
                
                SeqNum     <= SeqNum + 1;
                current_state <= ConRep;
                IpId <= IpId + 1;
              when others =>
                current_state <= idle;
              end case;
              
            -- ReliableConnection Acknowledge  
            when RcAck =>
              cmac_tx(1).tdata    <= mad_data;
              cmac_tx(1).tkeep(63 downto 58) <= (others => '0');
              cmac_tx(1).tkeep(57 downto 0) <= (others => '1');
              cmac_tx(1).tlast    <= '1';
              cmac_tx(1).tuser(0) <= '0';
              cmac_tx(1).tvalid   <= '1';
              cal_checksum <= '1';
              --ConEstablished <= '1'; -- Now for single QP. Should be done for every QP and connection and stored in register
              current_state <= idle;
              
            -- Transmit ConnectReply  
            when ConRep => 
              cal_checksum <= '0';
              case word_cnt is
              when 0 =>
                RemPSN <= cmac_rx_axis_tdata(39 downto 16);
                mem.valid   <= '1';
                mem.rempsn <= cmac_rx_axis_tdata(39 downto 16);
                
                qp.remote_psn <= cmac_rx_axis_tdata(39 downto 16);
                qp.valid <= '1';
                
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep    <= (others => '1');
                cmac_tx(1).tlast    <= '0';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
                cal_checksum <= '1';
                mad_data <= x"000000000000" & LocGUID & x"e080101000" & StartPSN & x"0000000000" & LocQPN & x"00000000" & RemComID & LocComID & x"00000000" & AttrID & TransID & x"000000000302";
                
              when 1 to 3 =>
                mad_data(511 downto 0)   <= (others => '0');
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep    <= (others => '1');
                cmac_tx(1).tlast    <= '0';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
                 
              when 4 =>
                cmac_tx(1).tdata    <= (others => '0');
                cmac_tx(1).tkeep(63 downto 62) <= (others => '0');
                cmac_tx(1).tkeep(61 downto 0)  <= (others => '1');
                cmac_tx(1).tlast    <= '1';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
               
                current_state <= idle;
                word_cnt <= 0;
              when others => 
                current_state <= idle;
              end case;
              word_cnt <= word_cnt + 1;
              
            -- Disconnect Reply
            when DisRep =>
              QP_disconnected <= '0';
              cal_checksum <= '0';
              case word_cnt is
              when 0 =>
                QP_disconnected <= '0';
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep    <= (others => '1');
                cmac_tx(1).tlast    <= '0';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
                cal_checksum <= '1';
                mad_data <= (others => '0');
                mad_data(239 downto 0) <= RemComID & LocComID & x"00000000" & AttrID & TransID & x"000000000302";
                
              when 1 to 3 =>
                mad_data(511 downto 0)   <= (others => '0');
                cmac_tx(1).tdata    <= mad_data;
                cmac_tx(1).tkeep    <= (others => '1');
                cmac_tx(1).tlast    <= '0';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
                 
              when 4 =>
                cmac_tx(1).tdata    <= (others => '0');
                cmac_tx(1).tkeep(63 downto 62) <= (others => '0');
                cmac_tx(1).tkeep(61 downto 0)  <= (others => '1');
                cmac_tx(1).tlast    <= '1';
                cmac_tx(1).tuser(0) <= '0';
                cmac_tx(1).tvalid   <= '1';
               
                current_state <= idle;
                word_cnt <= 0;
              when others => 
                current_state <= idle;
              end case;
              word_cnt <= word_cnt + 1;
            -- Receive non management packets  
    --        when RX_PKT =>
    --          if cmac_rx_axis_tlast = '1' then
    --            current_state <= idle;
    --          else
    --            current_state <= RX_PKT;
    --          end if;
              
    --          qp_axis.tdata    <= cmac_rx_axis_tdata;
    --          qp_axis.tkeep    <= cmac_rx_axis_tkeep;
    --          qp_axis.tlast    <= cmac_rx_axis_tlast;
    --          qp_axis.tuser(0) <= cmac_rx_axis_tlast;
    --          qp_axis.tvalid   <= cmac_rx_axis_tvalid;
            when others =>
              current_state <= IDLE;
              cmac_tx(1)  <= cmac_tx_reset;
            end case;  
            
          end if;
        end if;
      end process; 
      
      qp_connected <= ConEstablished;
      rem_qpn <= RemQPN;
      rem_psn <= RemPSN;
  
end architecture filter;
