
library ieee;
use ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.crc32.all;
use work.utility.all;
use work.rdma_pkg.all;

entity QP_handler is
generic(
--  constant source_ip  : std_logic_vector(31 downto 0) := x"20a0a8c0";
--  constant source_mac : std_logic_vector(47 downto 0) := x"9a5edc177620"; 
  constant qp_num : integer := 0
);
port (
  signal core_clk : in std_logic;
  signal core_rst_n : in std_logic;

  signal qp_connected : in std_logic;
  --signal target_mac : in std_logic_vector(47 downto 0);-- :=  x"5a38b8078a24"; --Change to register later  (24:8a:07:b8:38:5a)
  --signal target_ip  : in std_logic_vector(31 downto 0);-- :=  x"0aa0a8c0"; --Change to register later
  signal target_qpn : in std_logic_vector(23 downto 0);
  signal start_psn  : in  std_logic_vector(23 downto 0);
  signal mem : in t_mem;

  -- Streaming from ERNIC/RDMA logic
  signal rdma_s_axis_tdata : in std_logic_vector(511 downto 0);
  signal rdma_s_axis_tkeep : in std_logic_vector(63 downto 0);
  signal rdma_s_axis_tlast : in std_logic;
  signal rdma_s_axis_tuser : in std_logic_vector(0 downto 0);
  signal rdma_s_axis_tvalid: in std_logic;

  -- Streaming from data logic
  signal data_s_axis_tdata : in std_logic_vector(511 downto 0);
  signal data_s_axis_tkeep : in std_logic_vector(63 downto 0);
  signal data_s_axis_tlast : in std_logic;
  signal data_s_axis_tuser : in std_logic_vector(0 downto 0);
  signal data_s_axis_tvalid: in std_logic;
  signal data_s_axis_tready: out std_logic;

  -- Response to CMAC (IB_ConReq, IB_Ready etc.)
  signal cmac_tx_axis_tdata : out std_logic_vector(511 downto 0);
  signal cmac_tx_axis_tkeep : out std_logic_vector(63 downto 0);
  signal cmac_tx_axis_tlast : out std_logic;
  signal cmac_tx_axis_tuser : out std_logic_vector(0 downto 0);
  signal cmac_tx_axis_tvalid : out std_logic
);
end QP_handler;

architecture Behavioral of QP_handler is

  signal RemQPN : std_logic_vector(23 downto 0); --RemQPN <= target_qpn;
  signal RemPSN : std_logic_vector(23 downto 0);
  
  signal MsgSeqNum, MsgSeqNum_p : std_logic_vector(23 downto 0) := x"100000";
  signal SeqNum : integer;
  signal IpId : std_logic_vector(15 downto 0);
  signal source_ip  : std_logic_vector(31 downto 0) := x"20a0a8c0";
  signal source_mac : std_logic_vector(47 downto 0) := x"9a5edc177620"; 
  signal target_ip : std_logic_vector(31 downto 0); 
  signal target_mac : std_logic_vector(47 downto 0); 
  signal qp_write_data, qp_send_info : std_logic;

  -- Remote and local QP connection information
  type state_type_qp is (IDLE, rdata_1, rdata_2, ldata_1, ldata_2, ldata_3, wdata, send, ack, rcack);
  signal current_state_qp : state_type_qp;

  signal r_qp_mem : t_qp_mem;
  signal l_qp_mem : t_qp_mem := (x"0053ed2300000000", x"ce1d0000", x"04000000");

  type qp_data_arr is array(1 to 5) of t_axis_out;
  signal qp_data : qp_data_arr;
  signal qp_storedata : qp_data_arr;
  signal qp_word : std_logic_vector(511 downto 0);
  signal cal_checksum_qp : std_logic;

begin

  process(core_clk) is
    variable word_count : integer;
    variable data_count : integer;
    variable packet_count : integer;
    variable wait_count : integer;
    variable rev_psn, rem_psn, rec_psn: std_logic_vector(23 downto 0);
  begin
    if rising_edge(core_clk) then
      if(core_rst_n = '0') then
        qp_send_info  <= '0';
        qp_write_data <= '0';
        cal_checksum_qp  <= '0';
        current_state_qp <= idle;
        packet_count := 0;
        
        data_s_axis_tready  <= '0';
        qp_data(1).tdata    <= (others => '0');
        qp_data(1).tkeep    <= (others => '0');
        qp_data(1).tlast    <= '0';
        qp_data(1).tuser(0) <= '0';
        qp_data(1).tvalid   <= '0';
        
        RemQPN   <= (others => '0');
        IpId     <= (others => '0');
        qp_word  <= (others => '0');
        r_qp_mem <= ((others => '0'), (others => '0'), (others => '0'));
      else
        qp_data(1).tdata    <= (others => '0');
        qp_data(1).tkeep    <= (others => '0');
        qp_data(1).tlast    <= '0';
        qp_data(1).tuser(0) <= '0';
        qp_data(1).tvalid   <= '0';

        RemQPN <= target_qpn;
        data_s_axis_tready <= '0';

        -- QP state machine
        case (current_state_qp) is

        --Wait until send data with remote memory information arrives
        when idle =>
          if((rdma_s_axis_tdata(343 downto 336 ) = (x"04")) and mem.enable = '1') then--qp_connected = '1') then
            r_qp_mem.virt_addr               <= rdma_s_axis_tdata(495 downto 448);--rdma_s_axis_tdata(495 downto 432); <-- 64-bit address (now using 48-bit)
            r_qp_mem.remote_key(15 downto 0) <= rdma_s_axis_tdata(511 downto 496);
            --r_qp_mem.dma_length              <= rdma_s_axis_tdata(5 downto 336 );
            current_state_qp <= rdata_1;
            --RemPSN <= start_psn;
            target_ip  <= mem.ip;
            target_mac <= mem.mac;
            RemPSN <= mem.rempsn;
            rec_psn := rdma_s_axis_tdata(431 downto 408);
          end if;

        when rdata_1 =>
          r_qp_mem.remote_key(31 downto 16) <= rdma_s_axis_tdata(15 downto 0);
          r_qp_mem.dma_length               <= rdma_s_axis_tdata(47 downto 16);
          qp_send_info <= '1';
          current_state_qp <= ldata_1;

          
          qp_word(511 downto 0) <= (others => '0');
          --mad_data(463 downto 0) <= MsgSeqNum_p & x"00" & StartPSN+1 & RemQPN & x"00ffff4011" & x"00001c00b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"30000245" & x"0008" & source_mac & target_mac;
          --mad_data(463 downto 0) <= x"020000" & x"00" & StartPSN+1 & x"00" & RemQPN & x"00ffff4011" & x"00001c00b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"30000245" & x"0008" & source_mac & target_mac;
          qp_word(463 downto 0) <= x"010000" & x"00" & rec_psn & x"00" & RemQPN & x"00ffff4011" & x"00001c00b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"30000245" & x"0008" & source_mac & target_mac;
          MsgSeqNum_p <= MsgSeqNum_p + 1;
          SeqNum <= SeqNum + 1;
          IpId <= IpId + 1;
          current_state_qp <= rcack;

        when rcack =>
          qp_data(1).tdata    <= qp_word;
          qp_data(1).tkeep(63 downto 58) <= (others => '0');
          qp_data(1).tkeep(57 downto 0) <= (others => '1');
          qp_data(1).tlast    <= '1';
          qp_data(1).tuser(0) <= '0';
          qp_data(1).tvalid   <= '1';
          cal_checksum_qp <= '1';
          --ConEstablished <= '1'; -- Now for single QP. Should be done for every QP and connection and stored in register
          current_state_qp <= ldata_1;
          rev_PSN := reverse_bytes(RemPSN);
          
        -- RDMA send with local memory information
        when ldata_1 =>
           Rem_PSN := reverse_bytes(rev_PSN + packet_count);
           qp_word <= l_qp_mem.remote_key(15 downto 0) & l_qp_mem.virt_addr & Rem_PSN & x"80" & RemQPN & x"00ffff4004" & x"00002800b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"3c000245" & x"0008" & source_mac & target_mac;
           current_state_qp <= ldata_2;

        when ldata_2 =>
          qp_data(1).tdata    <= qp_word;
          qp_data(1).tkeep    <= (others => '1');
          qp_data(1).tlast    <= '0';
          qp_data(1).tuser(0) <= '0';
          qp_data(1).tvalid   <= '1';
          qp_word(511 downto 48) <= (others => '0');
          qp_word(47 downto 0)   <= l_qp_mem.dma_length & l_qp_mem.remote_key(31 downto 16);
          cal_checksum_qp  <= '1';
          current_state_qp <= ldata_3;


        when ldata_3 =>
          qp_data(1).tdata <= qp_word;
          qp_data(1).tkeep(63 downto 6) <= (others => '0');
          qp_data(1).tkeep(5 downto 0)  <= (others => '1');
          qp_data(1).tlast    <= '1';
          qp_data(1).tuser(0) <= '0';
          qp_data(1).tvalid   <= '1';
          cal_checksum_qp  <= '0';
          current_state_qp <= wdata;
--          packet_count := 0;
          --rev_PSN := reverse_bytes(RemPSN);

        -- RDMA write test data
        -- WQE handler reads data out of DDR and provides that here
        when wdata =>
          cal_checksum_qp <= '0';
          Rem_PSN := reverse_bytes(rev_PSN + packet_count+1);

          --If message ready!

          -- maybe send/read/write signal?
          --64 byte transfer
          --data_s_axis_tready <= '0';
          data_s_axis_tready  <= '1';
          if data_s_axis_tvalid = '1' then
            case word_count is
            when 0 =>
              data_s_axis_tready  <= '1';

              qp_word <= r_qp_mem.remote_key(15 downto 0) & r_qp_mem.virt_addr & Rem_PSN & x"80" & RemQPN & x"00ffff400a" & x"0000a80fb71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"bc0f0245" & x"0008" & source_mac & target_mac;
              word_count := word_count +1;

            when 1 =>
              data_s_axis_tready  <= '1';
              qp_data(1).tdata    <= qp_word;
              qp_data(1).tkeep    <= (others => '1');
              qp_data(1).tlast    <= '0';
              qp_data(1).tuser(0) <= '0';
              qp_data(1).tvalid   <= '1';

              qp_storedata(1).tdata <= data_s_axis_tdata;
              qp_word(511 downto 48) <= data_s_axis_tdata(511 downto 48);

              if data_s_axis_tlast = '1' then
                word_count := 3;
              else
                word_count := 2;
              end if;
              
              qp_word(47 downto 0) <= r_qp_mem.dma_length & r_qp_mem.remote_key(31 downto 16);
              cal_checksum_qp  <= '1';

            when 2 =>
              -- doesn't work yet because the IP and UDP header have fixed length for 64-bytes of data
              qp_storedata(1).tdata <= data_s_axis_tdata;

              qp_word(511 downto 48) <= data_s_axis_tdata(511 downto 48);
              qp_word(47 downto 0) <= qp_storedata(1).tdata(47 downto 0);
              if data_s_axis_tlast = '1' then
                word_count := 3;
                data_s_axis_tready  <= '0';
              end if;
              qp_data(1).tdata  <= qp_word;
              qp_data(1).tkeep    <= (others => '1');
              qp_data(1).tlast    <= '0';
              qp_data(1).tuser(0) <= '0';
              qp_data(1).tvalid   <= '1';
              data_s_axis_tready  <= '1';

            when 3 =>
              qp_storedata(2) <= qp_storedata(1);
              qp_storedata(1).tdata <= data_s_axis_tdata;

              qp_data(1).tdata  <= qp_word;
              qp_data(1).tkeep    <= (others => '1');
              qp_data(1).tlast    <= '0';
              qp_data(1).tuser(0) <= '0';
              qp_data(1).tvalid   <= '1';
              qp_word(511 downto 48) <= (others => '0');
              qp_word(47 downto 0) <= qp_storedata(1).tdata(47 downto 0);
              word_count := word_count +1;
            when 4 =>
              qp_data(1).tdata  <= qp_word;
              qp_data(1).tkeep(63 downto 10) <= (others => '0');
              qp_data(1).tkeep(5 downto 0)   <= (others => '1');
              qp_data(1).tlast    <= '1';
              qp_data(1).tuser(0) <= '0';
              qp_data(1).tvalid   <= '1';
              word_count := 0;

              packet_count := packet_count + 1;
--              if packet_count = 3 then
--                current_state_qp <= idle;
--              end if;

              current_state_qp <= send;
            when others =>
              word_count := 0;
            end case;
          end if;
        when send =>
          cal_checksum_qp <= '0';
          Rem_PSN := reverse_bytes(rev_PSN + packet_count+1);

          case word_count is
          when 0 =>
            --qp_word <= r_qp_mem.remote_key(15 downto 0) & r_qp_mem.virt_addr & (MsgSeqNum_p+1+packet_count) & x"80" & RemQPN & x"00ffff400a" & x"00006800b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"7c000245" & x"0008" & source_mac & target_mac;
            qp_word(511 downto 0) <= (others => '0');
            qp_word(431 downto 0) <= (Rem_PSN) & x"80" & RemQPN & x"00ffff4004" & x"00002800b71200fc" & target_ip & source_ip & x"0000" & x"11400040" & IpId & x"3c000245" & x"0008" & source_mac & target_mac;
            word_count := word_count +1;
          when 1 =>
            qp_data(1).tdata    <= qp_word;
            qp_data(1).tkeep    <= (others => '1');
            qp_data(1).tlast    <= '0';
            qp_data(1).tuser(0) <= '0';
            qp_data(1).tvalid   <= '1';
            qp_word(511 downto 0)<= (others => '0');
            cal_checksum_qp  <= '1';
            word_count := word_count +1;
          when 2 =>
            qp_data(1).tdata <= qp_word;
            qp_data(1).tkeep(63 downto 6) <= (others => '0');
            qp_data(1).tkeep(5 downto 0)  <= (others => '1');
            qp_data(1).tlast    <= '1';
            qp_data(1).tuser(0) <= '0';
            qp_data(1).tvalid   <= '1';
            cal_checksum_qp  <= '0';
            word_count := 0;
            wait_count := 0;
            current_state_qp <= ack;

          --When disconnect go to idle state

          -- Wait for ack from client

          when others =>
            word_count := 0;
          end case;
          
        -- wait for ack
        when ack =>
          wait_count := wait_count + 1;
          if wait_count = 1000 then
            current_state_qp <= send;
          end if;
          
          if ((rdma_s_axis_tdata(431 downto 408 ) = Rem_PSN) and rdma_s_axis_tdata(343 downto 336 ) = (x"11")) then
            packet_count := packet_count + 1;
            current_state_qp <= idle;
          end if;
          
        when others =>
          current_state_qp <= idle;
        end case;

        -- Ensure the QP will go to idle in case the a disconnect is received
        if qp_connected = '0' then
          current_state_qp <= idle;
          packet_count := 0;
        end if;

      end if;
    end if;
  end process;

   -- Calculate the checksum in the IP header
  process(core_clk) is
    variable checksum_inv : std_logic_vector(23 downto 0);
    variable checksum : std_logic_vector(15 downto 0);
    variable ip_header : std_logic_vector(159 downto 0);
    variable checksum_carry : std_logic_vector(7 downto 0);
  begin
    if rising_edge(core_clk) then
      if(core_rst_n = '0') then
         qp_data(2).tdata    <= (others => '0');
         qp_data(2).tkeep    <= (others => '0');
         qp_data(2).tlast    <= '0';
         qp_data(2).tuser(0) <= '0';
         qp_data(2).tvalid   <= '0';
      else
        if (cal_checksum_qp = '1') then
          checksum_inv := (others => '0');
          ip_header := qp_data(1).tdata(271 downto 112);

          for i in 0 to 9 loop
            checksum_inv := checksum_inv + ip_header(16*i+15 downto 16*i);
            if(checksum_inv(23 downto 16) /= x"00") then
              checksum_carry := checksum_inv(23 downto 16);
              checksum_inv(23 downto 16) := x"00";
              checksum_inv(15 downto 0) := (checksum_inv(15 downto 0) + checksum_carry);
            end if;
          end loop;

--          if(checksum_inv(23 downto 16) /= x"00") then
--            checksum_carry := checksum_inv(23 downto 16);
--            checksum_inv(23 downto 16) := x"00";
--            checksum_inv(15 downto 0) := (checksum_inv(15 downto 0) + checksum_carry);
--          end if;

          if(checksum_inv(23 downto 16) = x"00") then
            qp_data(2).tdata    <= qp_data(1).tdata;
            qp_data(2).tdata(207 downto 192) <= not checksum_inv(15 downto 0);
            qp_data(2).tkeep    <= qp_data(1).tkeep;
            qp_data(2).tlast    <= qp_data(1).tlast;
            qp_data(2).tuser(0) <= qp_data(1).tuser(0);
            qp_data(2).tvalid   <=qp_data(1).tvalid;
          end if;
        else
          qp_data(2) <= qp_data(1);
        end if;
      end if;
    end if;
  end process;

  cmac_tx_axis_tdata    <= qp_data(2).tdata;
  cmac_tx_axis_tkeep    <= qp_data(2).tkeep;
  cmac_tx_axis_tlast    <= qp_data(2).tlast;
  cmac_tx_axis_tuser(0) <= qp_data(2).tuser(0);
  cmac_tx_axis_tvalid   <= qp_data(2).tvalid;

end Behavioral;
