--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
--Date        : Wed Mar 24 11:54:48 2021
--Host        : n-desk running 64-bit Ubuntu 20.04.2 LTS
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    clk_100MHz : in STD_LOGIC;
    clk_out_200 : out STD_LOGIC;
    clk_out_200_rst : out STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_rx_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_rx_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_rx_tlast : out STD_LOGIC;
    cmac_rx_tready : in STD_LOGIC;
    cmac_rx_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_rx_tvalid : out STD_LOGIC;
    cmac_tx_0_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_tx_0_tdest : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_0_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_tx_0_tlast : in STD_LOGIC;
    cmac_tx_0_tready : out STD_LOGIC;
    cmac_tx_0_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_0_tvalid : in STD_LOGIC;
    cmac_tx_1_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_tx_1_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_tx_1_tlast : in STD_LOGIC;
    cmac_tx_1_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_1_tvalid : in STD_LOGIC;
    ddr4_out_act_n : out STD_LOGIC;
    ddr4_out_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
    ddr4_out_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_out_bg : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_ck_c : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_ck_t : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_cs_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_out_dm_n : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
    ddr4_out_dqs_c : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_dqs_t : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_reset_n : out STD_LOGIC;
    ddr_clk : in STD_LOGIC;
    gt_rx_0_gt_port_0_n : in STD_LOGIC;
    gt_rx_0_gt_port_0_p : in STD_LOGIC;
    gt_rx_0_gt_port_1_n : in STD_LOGIC;
    gt_rx_0_gt_port_1_p : in STD_LOGIC;
    gt_rx_0_gt_port_2_n : in STD_LOGIC;
    gt_rx_0_gt_port_2_p : in STD_LOGIC;
    gt_rx_0_gt_port_3_n : in STD_LOGIC;
    gt_rx_0_gt_port_3_p : in STD_LOGIC;
    gt_tx_0_gt_port_0_n : out STD_LOGIC;
    gt_tx_0_gt_port_0_p : out STD_LOGIC;
    gt_tx_0_gt_port_1_n : out STD_LOGIC;
    gt_tx_0_gt_port_1_p : out STD_LOGIC;
    gt_tx_0_gt_port_2_n : out STD_LOGIC;
    gt_tx_0_gt_port_2_p : out STD_LOGIC;
    gt_tx_0_gt_port_3_n : out STD_LOGIC;
    gt_tx_0_gt_port_3_p : out STD_LOGIC;
    mem_calib : out STD_LOGIC;
    mem_clk : out STD_LOGIC;
    mem_m_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_m_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_arid : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    mem_m_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_arready : out STD_LOGIC;
    mem_m_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_arvalid : in STD_LOGIC;
    mem_m_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_m_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_awid : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    mem_m_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_awready : out STD_LOGIC;
    mem_m_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_awvalid : in STD_LOGIC;
    mem_m_axi_bid : out STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_bready : in STD_LOGIC;
    mem_m_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_bvalid : out STD_LOGIC;
    mem_m_axi_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    mem_m_axi_rid : out STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_rlast : out STD_LOGIC;
    mem_m_axi_rready : in STD_LOGIC;
    mem_m_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_rvalid : out STD_LOGIC;
    mem_m_axi_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    mem_m_axi_wlast : in STD_LOGIC;
    mem_m_axi_wready : out STD_LOGIC;
    mem_m_axi_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
    mem_m_axi_wvalid : in STD_LOGIC;
    mem_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    qsfp4_156mhz_clk_n : in STD_LOGIC;
    qsfp4_156mhz_clk_p : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    clk_100MHz : in STD_LOGIC;
    reset : in STD_LOGIC;
    clk_out_200 : out STD_LOGIC;
    ddr_clk : in STD_LOGIC;
    mem_clk : out STD_LOGIC;
    mem_calib : out STD_LOGIC;
    mem_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_out_200_rst : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_act_n : out STD_LOGIC;
    ddr4_out_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
    ddr4_out_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_out_bg : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_ck_c : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_ck_t : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_cs_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ddr4_out_dm_n : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
    ddr4_out_dqs_c : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_dqs_t : inout STD_LOGIC_VECTOR ( 8 downto 0 );
    ddr4_out_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
    ddr4_out_reset_n : out STD_LOGIC;
    gt_rx_0_gt_port_0_n : in STD_LOGIC;
    gt_rx_0_gt_port_0_p : in STD_LOGIC;
    gt_rx_0_gt_port_1_n : in STD_LOGIC;
    gt_rx_0_gt_port_1_p : in STD_LOGIC;
    gt_rx_0_gt_port_2_n : in STD_LOGIC;
    gt_rx_0_gt_port_2_p : in STD_LOGIC;
    gt_rx_0_gt_port_3_n : in STD_LOGIC;
    gt_rx_0_gt_port_3_p : in STD_LOGIC;
    gt_tx_0_gt_port_0_n : out STD_LOGIC;
    gt_tx_0_gt_port_0_p : out STD_LOGIC;
    gt_tx_0_gt_port_1_n : out STD_LOGIC;
    gt_tx_0_gt_port_1_p : out STD_LOGIC;
    gt_tx_0_gt_port_2_n : out STD_LOGIC;
    gt_tx_0_gt_port_2_p : out STD_LOGIC;
    gt_tx_0_gt_port_3_n : out STD_LOGIC;
    gt_tx_0_gt_port_3_p : out STD_LOGIC;
    qsfp4_156mhz_clk_n : in STD_LOGIC;
    qsfp4_156mhz_clk_p : in STD_LOGIC;
    cmac_tx_0_tvalid : in STD_LOGIC;
    cmac_tx_0_tready : out STD_LOGIC;
    cmac_tx_0_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_tx_0_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_tx_0_tlast : in STD_LOGIC;
    cmac_tx_0_tdest : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_0_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_1_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_tx_1_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_tx_1_tlast : in STD_LOGIC;
    cmac_tx_1_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_tx_1_tvalid : in STD_LOGIC;
    cmac_rx_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    cmac_rx_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
    cmac_rx_tlast : out STD_LOGIC;
    cmac_rx_tready : in STD_LOGIC;
    cmac_rx_tuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    cmac_rx_tvalid : out STD_LOGIC;
    mem_m_axi_awid : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_m_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    mem_m_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_awvalid : in STD_LOGIC;
    mem_m_axi_awready : out STD_LOGIC;
    mem_m_axi_wdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    mem_m_axi_wstrb : in STD_LOGIC_VECTOR ( 63 downto 0 );
    mem_m_axi_wlast : in STD_LOGIC;
    mem_m_axi_wvalid : in STD_LOGIC;
    mem_m_axi_wready : out STD_LOGIC;
    mem_m_axi_bid : out STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_bvalid : out STD_LOGIC;
    mem_m_axi_bready : in STD_LOGIC;
    mem_m_axi_arid : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_m_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    mem_m_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mem_m_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_m_axi_arvalid : in STD_LOGIC;
    mem_m_axi_arready : out STD_LOGIC;
    mem_m_axi_rid : out STD_LOGIC_VECTOR ( 0 to 0 );
    mem_m_axi_rdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    mem_m_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    mem_m_axi_rlast : out STD_LOGIC;
    mem_m_axi_rvalid : out STD_LOGIC;
    mem_m_axi_rready : in STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      clk_100MHz => clk_100MHz,
      clk_out_200 => clk_out_200,
      clk_out_200_rst(0) => clk_out_200_rst(0),
      cmac_rx_tdata(511 downto 0) => cmac_rx_tdata(511 downto 0),
      cmac_rx_tkeep(63 downto 0) => cmac_rx_tkeep(63 downto 0),
      cmac_rx_tlast => cmac_rx_tlast,
      cmac_rx_tready => cmac_rx_tready,
      cmac_rx_tuser(0) => cmac_rx_tuser(0),
      cmac_rx_tvalid => cmac_rx_tvalid,
      cmac_tx_0_tdata(511 downto 0) => cmac_tx_0_tdata(511 downto 0),
      cmac_tx_0_tdest(0) => cmac_tx_0_tdest(0),
      cmac_tx_0_tkeep(63 downto 0) => cmac_tx_0_tkeep(63 downto 0),
      cmac_tx_0_tlast => cmac_tx_0_tlast,
      cmac_tx_0_tready => cmac_tx_0_tready,
      cmac_tx_0_tuser(0) => cmac_tx_0_tuser(0),
      cmac_tx_0_tvalid => cmac_tx_0_tvalid,
      cmac_tx_1_tdata(511 downto 0) => cmac_tx_1_tdata(511 downto 0),
      cmac_tx_1_tkeep(63 downto 0) => cmac_tx_1_tkeep(63 downto 0),
      cmac_tx_1_tlast => cmac_tx_1_tlast,
      cmac_tx_1_tuser(0) => cmac_tx_1_tuser(0),
      cmac_tx_1_tvalid => cmac_tx_1_tvalid,
      ddr4_out_act_n => ddr4_out_act_n,
      ddr4_out_adr(16 downto 0) => ddr4_out_adr(16 downto 0),
      ddr4_out_ba(1 downto 0) => ddr4_out_ba(1 downto 0),
      ddr4_out_bg(0) => ddr4_out_bg(0),
      ddr4_out_ck_c(0) => ddr4_out_ck_c(0),
      ddr4_out_ck_t(0) => ddr4_out_ck_t(0),
      ddr4_out_cke(0) => ddr4_out_cke(0),
      ddr4_out_cs_n(1 downto 0) => ddr4_out_cs_n(1 downto 0),
      ddr4_out_dm_n(8 downto 0) => ddr4_out_dm_n(8 downto 0),
      ddr4_out_dq(71 downto 0) => ddr4_out_dq(71 downto 0),
      ddr4_out_dqs_c(8 downto 0) => ddr4_out_dqs_c(8 downto 0),
      ddr4_out_dqs_t(8 downto 0) => ddr4_out_dqs_t(8 downto 0),
      ddr4_out_odt(0) => ddr4_out_odt(0),
      ddr4_out_reset_n => ddr4_out_reset_n,
      ddr_clk => ddr_clk,
      gt_rx_0_gt_port_0_n => gt_rx_0_gt_port_0_n,
      gt_rx_0_gt_port_0_p => gt_rx_0_gt_port_0_p,
      gt_rx_0_gt_port_1_n => gt_rx_0_gt_port_1_n,
      gt_rx_0_gt_port_1_p => gt_rx_0_gt_port_1_p,
      gt_rx_0_gt_port_2_n => gt_rx_0_gt_port_2_n,
      gt_rx_0_gt_port_2_p => gt_rx_0_gt_port_2_p,
      gt_rx_0_gt_port_3_n => gt_rx_0_gt_port_3_n,
      gt_rx_0_gt_port_3_p => gt_rx_0_gt_port_3_p,
      gt_tx_0_gt_port_0_n => gt_tx_0_gt_port_0_n,
      gt_tx_0_gt_port_0_p => gt_tx_0_gt_port_0_p,
      gt_tx_0_gt_port_1_n => gt_tx_0_gt_port_1_n,
      gt_tx_0_gt_port_1_p => gt_tx_0_gt_port_1_p,
      gt_tx_0_gt_port_2_n => gt_tx_0_gt_port_2_n,
      gt_tx_0_gt_port_2_p => gt_tx_0_gt_port_2_p,
      gt_tx_0_gt_port_3_n => gt_tx_0_gt_port_3_n,
      gt_tx_0_gt_port_3_p => gt_tx_0_gt_port_3_p,
      mem_calib => mem_calib,
      mem_clk => mem_clk,
      mem_m_axi_araddr(31 downto 0) => mem_m_axi_araddr(31 downto 0),
      mem_m_axi_arburst(1 downto 0) => mem_m_axi_arburst(1 downto 0),
      mem_m_axi_arcache(3 downto 0) => mem_m_axi_arcache(3 downto 0),
      mem_m_axi_arid(0) => mem_m_axi_arid(0),
      mem_m_axi_arlen(7 downto 0) => mem_m_axi_arlen(7 downto 0),
      mem_m_axi_arlock(0) => mem_m_axi_arlock(0),
      mem_m_axi_arprot(2 downto 0) => mem_m_axi_arprot(2 downto 0),
      mem_m_axi_arqos(3 downto 0) => mem_m_axi_arqos(3 downto 0),
      mem_m_axi_arready => mem_m_axi_arready,
      mem_m_axi_arregion(3 downto 0) => mem_m_axi_arregion(3 downto 0),
      mem_m_axi_arsize(2 downto 0) => mem_m_axi_arsize(2 downto 0),
      mem_m_axi_arvalid => mem_m_axi_arvalid,
      mem_m_axi_awaddr(31 downto 0) => mem_m_axi_awaddr(31 downto 0),
      mem_m_axi_awburst(1 downto 0) => mem_m_axi_awburst(1 downto 0),
      mem_m_axi_awcache(3 downto 0) => mem_m_axi_awcache(3 downto 0),
      mem_m_axi_awid(0) => mem_m_axi_awid(0),
      mem_m_axi_awlen(7 downto 0) => mem_m_axi_awlen(7 downto 0),
      mem_m_axi_awlock(0) => mem_m_axi_awlock(0),
      mem_m_axi_awprot(2 downto 0) => mem_m_axi_awprot(2 downto 0),
      mem_m_axi_awqos(3 downto 0) => mem_m_axi_awqos(3 downto 0),
      mem_m_axi_awready => mem_m_axi_awready,
      mem_m_axi_awregion(3 downto 0) => mem_m_axi_awregion(3 downto 0),
      mem_m_axi_awsize(2 downto 0) => mem_m_axi_awsize(2 downto 0),
      mem_m_axi_awvalid => mem_m_axi_awvalid,
      mem_m_axi_bid(0) => mem_m_axi_bid(0),
      mem_m_axi_bready => mem_m_axi_bready,
      mem_m_axi_bresp(1 downto 0) => mem_m_axi_bresp(1 downto 0),
      mem_m_axi_bvalid => mem_m_axi_bvalid,
      mem_m_axi_rdata(511 downto 0) => mem_m_axi_rdata(511 downto 0),
      mem_m_axi_rid(0) => mem_m_axi_rid(0),
      mem_m_axi_rlast => mem_m_axi_rlast,
      mem_m_axi_rready => mem_m_axi_rready,
      mem_m_axi_rresp(1 downto 0) => mem_m_axi_rresp(1 downto 0),
      mem_m_axi_rvalid => mem_m_axi_rvalid,
      mem_m_axi_wdata(511 downto 0) => mem_m_axi_wdata(511 downto 0),
      mem_m_axi_wlast => mem_m_axi_wlast,
      mem_m_axi_wready => mem_m_axi_wready,
      mem_m_axi_wstrb(63 downto 0) => mem_m_axi_wstrb(63 downto 0),
      mem_m_axi_wvalid => mem_m_axi_wvalid,
      mem_reset(0) => mem_reset(0),
      qsfp4_156mhz_clk_n => qsfp4_156mhz_clk_n,
      qsfp4_156mhz_clk_p => qsfp4_156mhz_clk_p,
      reset => reset
    );
end STRUCTURE;
