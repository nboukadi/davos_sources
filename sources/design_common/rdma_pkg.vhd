----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/09/2020 04:16:56 PM
-- Design Name: 
-- Module Name: rdma_pkg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


package rdma_pkg is
 
  type t_qp_mem is record
    virt_addr  : std_logic_vector(63 downto 0);
    remote_key : std_logic_vector(31 downto 0);
    dma_length : std_logic_vector(31 downto 0);
  end record t_qp_mem;
  
  type t_axis_out is record
    tdata : std_logic_vector(511 downto 0);
    tkeep : std_logic_vector(63 downto 0);
    tlast : std_logic;
    tuser : std_logic_vector(0 downto 0);
    tvalid: std_logic;
  end record t_axis_out;
  
  type t_axis_in is record
    tready: std_logic;
  end record t_axis_in;
  
  type t_mem is record
    transid : std_logic_vector(63 downto 0);
    mac : std_logic_vector(47 downto 0);
    ip  : std_logic_vector(31 downto 0);
    valid  : std_logic; 
    locqpn : std_logic_vector(23 downto 0);
    remqpn : std_logic_vector(23 downto 0);
    enable : std_logic;
    rempsn : std_logic_vector(23 downto 0);
  end record t_mem;
  
  type t_wqe is record
    instr : std_logic_vector(15 downto 0);   
    id    : std_logic_vector(15 downto 0);   
    length: std_logic_vector(15 downto 0);   
    address : std_logic_vector(63 downto 0);
  end record t_wqe;

end package rdma_pkg;
 
-- Package Body Section
package body rdma_pkg is
 
  
 
end package body rdma_pkg;

