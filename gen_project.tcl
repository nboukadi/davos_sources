# TCL script to generate a project
set scriptdir [pwd]
set firmware_dir $scriptdir/

set BD_FILES ""
set XDC_FILES_VCU128 ""
set XCI_FILES ""
set VHDL_FILES ""
set VERILOG_FILES ""

set PROJECT_NAME VCU128_netstack_test_noES
set BOARD_TYPE 128
set TOPLEVEL rdma_top
set PART xcvu37p-fsvh2892-2-e

set project_dir $firmware_dir/../Projects/$PROJECT_NAME
set davos_sources $scriptdir/sources/

set BD_FILES [concat $BD_FILES\
  design_common/bd/design_1.bd]

set VHDL_FILES [concat $VHDL_FILES \
  design_common/rdma_top.vhd \
  design_common/crc.vhd \
  design_common/crc32.vhd \
  design_common/crc32_512.vhd \
  design_common/icrc32.vhd \
  design_common/utility.vhd \
  design_common/rdma_pkg.vhd \
  design_common/IB_handler.vhd \
  design_common/tb_netstack.vhd \
  roce_files/os_wrapper.vhd ]
  
set VERILOG_FILES [concat $VERILOG_FILES \
  roce_files/os_wrapper_sv.sv \
  roce_files/udp_stack.sv \
  roce_files/tcp_stack.sv \
  roce_files/role_wrapper.sv \
  roce_files/roce_stack.sv \
  roce_files/register_slice_wrapper.sv \
  roce_files/network_stack.sv \
  roce_files/network_controller.sv \
  roce_files/mem_single_inf.sv \
  roce_files/dma_inf.sv \
  roce_files/dma_driver.sv \
  roce_files/dma_controller.sv \
  roce_files/ddr_controller.sv \
  roce_files/benchmark_role.sv \
  roce_files/benchmark_controller.sv \
  roce_files/axil_interconnect_done_right.sv \
  roce_files/davos_types.svh \
  roce_files/davos_config.svh \
  roce_files/os.sv]

set XDC_FILES_VCU128 [concat $XDC_FILES_VCU128 \
  design_common/vcu128_rdma.xdc]

set XCI_FILES [concat $XCI_FILES \
  design_common/ila_0.xci]

#set WCFG_FILES $WCFG_FILES

close_project -quiet
create_project -force -part $PART $PROJECT_NAME $project_dir
set_property target_language VHDL [current_project]
set_property default_lib work [current_project]

set_property IP_REPO_PATHS [list \
    [file normalize [file join $firmware_dir/iprepo]] \
] [current_project]
update_ip_catalog

foreach BD_FILE $BD_FILES {
	add_files ${davos_sources}/${BD_FILE}
}
export_ip_user_files -of_objects  [get_files  ${davos_sources}/design_common/bd/design_1.bd]

foreach VHDL_FILE $VHDL_FILES {
	read_vhdl -library work ${davos_sources}/${VHDL_FILE}
}

foreach VERILOG_FILE $VERILOG_FILES {
	read_verilog -library work ${davos_sources}/${VERILOG_FILE}
}

foreach XCI_FILE $XCI_FILES {
	import_ip ${davos_sources}/${XCI_FILE}
}

foreach XDC_FILE $XDC_FILES_VCU128 {
	read_xdc -verbose ${davos_sources}/${XDC_FILE}
}

# Generating IP cores used in simulation
set device_ip_dir ${davos_sources}/../ip
create_ip -name ethernet_frame_padding_512 -vendor ethz.systems.fpga -library hls -version 0.1 -module_name ethernet_frame_padding_512_0 -dir $device_ip_dir
generate_target {instantiation_template} [get_files $device_ip_dir/ethernet_frame_padding_512_0/ethernet_frame_padding_512_0.xci]

create_ip -name arp_server_subnet -vendor ethz.systems.fpga -library hls -version 1.1 -module_name arp_server_subnet_0 -dir $device_ip_dir
generate_target {instantiation_template} [get_files $device_ip_dir/arp_server_subnet_0/arp_server_subnet_0.xci]

create_ip -name mac_ip_encode -vendor ethz.systems.fpga -library hls -version 2.0 -module_name mac_ip_encode_0 -dir $device_ip_dir
generate_target {instantiation_template} [get_files $device_ip_dir/mac_ip_encode_0/mac_ip_encode_0.xci]

create_ip -name axi_datamover -vendor xilinx.com -library ip -version 5.1 -module_name axi_datamover_0 -dir $device_ip_dir
set_property -dict [list CONFIG.Component_Name {axi_datamover_0} CONFIG.c_m_axi_mm2s_data_width {512} CONFIG.c_m_axis_mm2s_tdata_width {512} CONFIG.c_mm2s_burst_size {2} CONFIG.c_include_s2mm {Omit} CONFIG.c_include_s2mm_stsfifo {false} CONFIG.c_s2mm_addr_pipe_depth {3} CONFIG.c_s2mm_include_sf {false} CONFIG.c_m_axi_s2mm_awid {1} CONFIG.c_enable_s2mm {0}] [get_ips axi_datamover_0]
generate_target {instantiation_template} [get_files $device_ip_dir/axi_datamover_0/axi_datamover_0.xci]

create_ip -name axis_interconnect -vendor xilinx.com -library ip -version 1.1 -module_name axis_interconnect_0 -dir $device_ip_dir
#set_property -dict [list CONFIG.HAS_TLAST {true} CONFIG.SWITCH_PACKET_MODE {false} CONFIG.C_SWITCH_MAX_XFERS_PER_ARB {1}] [get_ips axis_interconnect_0]
set_property -dict [list CONFIG.C_NUM_SI_SLOTS {2} CONFIG.HAS_TSTRB {false} CONFIG.HAS_TKEEP {true} CONFIG.HAS_TLAST {true} CONFIG.HAS_TID {false} CONFIG.HAS_TDEST {false} CONFIG.C_M00_AXIS_REG_CONFIG {1} CONFIG.M00_AXIS_TDATA_NUM_BYTES {64} CONFIG.C_S00_AXIS_REG_CONFIG {1} CONFIG.S00_AXIS_TDATA_NUM_BYTES {64} CONFIG.C_S01_AXIS_REG_CONFIG {1} CONFIG.S01_AXIS_TDATA_NUM_BYTES {64} CONFIG.SWITCH_PACKET_MODE {false} CONFIG.C_SWITCH_MAX_XFERS_PER_ARB {1} CONFIG.C_SWITCH_NUM_CYCLES_TIMEOUT {0} CONFIG.M00_S01_CONNECTIVITY {true}] [get_ips axis_interconnect_0]
generate_target {instantiation_template} [get_files $device_ip_dir/axis_interconnect_0/axis_interconnect_0.xci]

create_ip -name ip_handler -vendor ethz.systems.fpga -library hls -version 2.0 -module_name ip_handler_0 -dir $device_ip_dir
generate_target {instantiation_template} [get_files $device_ip_dir/ip_handler_0/ip_handler_0.xci]

create_ip -name rocev2 -vendor ethz.systems.fpga -library hls -version 0.82 -module_name rocev2_0 -dir $device_ip_dir
generate_target {instantiation_template} [get_files $device_ip_dir/rocev2_0/rocev2_0.xci]
update_compile_order -fileset sources_1

source ./scripts/network_ultraplus.tcl
source ./scripts/benchmark_role.tcl
source ./scripts/axi_infrastructure.tcl
source ./scripts/network_stack.tcl
source ./scripts/dram_ultraplus.tcl
source ./scripts/dma_ultraplus.tcl

add_files ${davos_sources}/design_common/tb_rdma_core_behav.wcfg
set_property top tb_rdma_core [get_filesets sim_1]
set_property top rdma_top [current_fileset]

update_compile_order -fileset sources_1
upgrade_ip [get_ips *]
make_wrapper -files [get_files $davos_sources/design_common/bd/design_1.bd] -top
add_files -norecurse $davos_sources/design_common/bd/hdl/design_1_wrapper.vhd
generate_target all [get_files  /home/nayib/Documents/vivado_clean/rdma_project/sources/design_common/bd/design_1.bd]

set_property target_constrs_file $davos_sources/design_common/vcu128_rdma.xdc [current_fileset -constrset]
